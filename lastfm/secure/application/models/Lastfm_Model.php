<?php

/**
 * Last FM model Class
 *
 * Last fm Model, get data from rest api and store in local
 *
 * @category    	Model
 * @author        	Rajapandian a
 * @license         NA
 * @link            NA
 */
class Lastfm_model extends CI_Model {

    //api key
    const LASTFM_APIKEY = 'bbf3d84f5c8eb16b51140643b71fab8d';
    //cache prefix
    const LASTFM_CPRFIX = 'ARTIST2';

    //cache key
    private $cache_key = '';

    /**
     * initialize cache
     *
     * @function __construct
     * @return NA
     */
    public function __construct() {
        $this->load->driver('cache', array('adapter' => 'file', 'key_prefix' => 'BC_') );
        // Call the CI_Model constructor
        parent::__construct();
    }

    /**
     * Country List
     *
     * @function country_list
     * @return array country list
     */
    public function country_list() {
        return array(
            "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia",
            "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
            "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bonaire, Sint Eustatius and Saba", "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil",
            "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cabo Verde", "Cambodia", "Cameroon", "Canada", "Cayman Islands",
            "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo",
            "Cook Islands", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea (Democratic People's Republic of)", "Korea (Republic of)", "Kuwait", "Kyrgyzstan", "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia (Federated States of)", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine, State of", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Romania", "Russian Federation", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard and Jan Mayen", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Viet Nam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna", "Western Sahara", "Yemen", "Zambia"
            , "Zimbabwe", "Åland Islands", "Curaçao", "Reunion", "Saint Barthélemy", "Cote d'Ivoire");
    }

    /**
     * remove special char from string
     * using in country list
     *
     * @function remove_splchar
     * @param string
     * @return string
     */
    public function remove_splchar($string) {
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    /**
     * set cache key locally to use it for other functions
     * 
     * @function set_cache_key
     * @param string 
     * @return string 
     */
    public function set_cache_key($key) {
        return $this->cache_key = $key;
    }

    /**
     * get current cache key
     * 
     * @function get_cache_key
     * @return string 
     */
    public function get_cache_key() {
        if (empty($this->cache_key)) {
            throw new Exception('Get Cache Key called before set');
        }
        return $this->cache_key;
    }

    /**
     * get artist list from cache
     * 
     * @function get_artist_cache
     * @return array data
     */
    public function get_artist_cache() {
        $cache_key = $this->get_cache_key();
        return $this->cache->get($cache_key);
    }

    /**
     * setting artist list cache
     * 
     * @function set_artist_cache
     * @param array $content
     * @return NA
     */
    public function set_artist_cache($content) {
        $cache_key = $this->get_cache_key();
        //store in cache for 24 hrs...
        $this->cache->save($cache_key, $content, 1 * 24 * 60 * 60);
    }

    /**
     * limit 5 per page
     * check memecache if data exist return it
     * or get it from api
     * 
     * @function get_artist_list
     * @param string  $country
     * @param number $page
     * @return array array data for artist list or error message
     */
    public function get_artist_list($country, $page = 1) {
        $limit = 5;
        $this->set_cache_key(self::LASTFM_CPRFIX . ":" . $this->remove_splchar($country) . ':' . $page . ':' . $limit);
        $cache = $this->get_artist_cache();
        if (!empty($cache)) {
            return $cache;
        } else {
            return $this->get_artist($country, $page, $limit);
        }
    }

    /**
     * get artist from curl
     * build url and cache it, return it
     * 
     * @function get_artists
     * @param string $country
     * @param number $page
     * @param number $limit
     * @return array error code and message 
     */
    public function get_artist($country, $page = 1, $limit = 5) {

        //Basic config for curl Library
        $base_url = 'http://ws.audioscrobbler.com';
        $api_key = self::LASTFM_APIKEY;

        // Refer :: http://www.last.fm/api/show/geo.getTopArtists
        $final_url = $base_url . '/2.0/?'
                . 'method=geo.gettopartists'
                . '&country=' . urlencode($country)
                . '&api_key=' . $api_key
                . '&format=json'
                . '&page=' . $page
                . '&limit=' . $limit;
        //get json data using CURL
        $data = $this->get_url_content($final_url);
        if (!empty($data)) {
            //decode as an array
            $result = json_decode($data, true);
            if (!empty($result['error'])) {
                return array('success' => 0, 'message' => $this->__error_code($result['error']));
            } else {
                $result = array('success' => 1, 'data' => $result);
                $this->set_artist_cache($result);
                return $result;
            }
        }
        return array('success' => 0, 'message'=>'Something went wrong');
    }

    /**
     * set cache key locally to use it for other functions
     * 
     * @function __error_code
     * @param string $code
     * @return string  error code
     */
    private function __error_code($code) {

        $error_codes = array(2 => "Invalid service - This service does not exist",
            3 => "Invalid Method - No method with that name in this package",
            4 => "Authentication Failed - You do not have permissions to access the service",
            5 => "Invalid format - This service doesn't exist in that format",
            6 => "Invalid parameters - Your request is missing a required parameter",
            7 => "Invalid resource specified",
            8 => "Operation failed - Something else went wrong",
            9 => "Invalid session key - Please re-authenticate",
            10 => "Invalid API key - You must be granted a valid key by last.fm",
            11 => "Service Offline - This service is temporarily offline. Try again later.",
            13 => "Invalid method signature supplied",
            16 => "There was a temporary error processing your request. Please try again",
            26 => "Suspended API key - Access for your account has been suspended, please contact Last.fm",
            29 => "Rate limit exceeded - Your IP has made too many requests in a short period");
        //if error code exists then send error or send detault error message
        //we can save error count in db or memcach and based on the cout we can alert developers
        if (isset($error_codes[$code])) {
            return $error_codes[$code];
        } else {
            return "Something Went Wrong";
        }
    }

    /**
     * curl function - rest api call
     * 
     * @function get_url_content
     * @param string full url
     * @return string or bool
     */
    public function get_url_content($url) {

        $this->load->library('curl');
        $this->curl->create($url);
        $this->curl->http_method('get');
        $this->curl->option('connecttimeout', 600);
        $data = $this->curl->execute();
        $info = $this->curl->info;
        if ($info['http_code'] == 200) {
            return $data;
        }
        return false;
    }

}
