<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

    /**
     * Test controller class file
     */
    function __construct() {
        
        parent::__construct();
        if (ENVIRONMENT != 'development') {
            die('Test file');
        }
        $this->load->model('lastfm_model');
        $this->load->library('unit_test');
    }

    public function index() {
        //$this->unit->run('Foo', 'Foo');
        
        //Maintain order.. 
        $this->cache_key_exception();
        $this->check_cache_key();
        $this->check_artist_cache();
        $this->check_artist_list();
        $this->empty_cache_check();
        $this->wrong_country_check();
        
        
        //echo $this->cache_test();
        //$this->verify_counry_list();
        //$this->get_country_list();
        
    }
    
    private function wrong_country_check() {
        $data = $this->lastfm_model->get_artist('nocountry',1,5);
        $test_name = 'Success False - Wrong Country - Error Code is working';
        echo $this->unit->run($data['success'], 0, $test_name);
    }
    
    
    private function empty_cache_check() {
        
        $this->lastfm_model->set_artist_cache('');
        $test_name = 'Remove Artist Cache - check again';
        echo $this->unit->run('', $this->lastfm_model->get_artist_cache(), $test_name);
        
        $this->check_artist_list();
    }
    private function check_artist_list() {
        
        $aus_data = $this->lastfm_model->get_artist_list('australia',1);
        $test_name = 'API Data check (cache)';
        echo $this->unit->run($aus_data, $this->lastfm_model->get_artist_cache(), $test_name);
    }
    
    private function check_artist_cache() {
        $cache_data = array('test');
        $this->lastfm_model->set_artist_cache(array('test'));
        $test_name = 'Artist set and get cache';
        echo $this->unit->run($cache_data, $this->lastfm_model->get_artist_cache(), $test_name);
    }
    
    private function cache_key_exception() {
        $try = false;
        try {
            $this->lastfm_model->get_cache_key();
            $try = true;
        } catch (Exception $e) {
            
        }
        $test_name = 'call get_cache_key before set_cache_key';
        echo $this->unit->run($try, false, $test_name);
    }
    
    private function check_cache_key() {
        $key = 'cache_key1';
        $this->lastfm_model->set_cache_key($key);
        $test_name = 'Set/Get Cache Key';
        echo $this->unit->run($key, $this->lastfm_model->get_cache_key(), $test_name);
    }
    

    private function cache_test() {

        $cache = $this->load->driver('cache', array('adapter' => 'file', 'key_prefix' => 'BC_')
        );
        $store = 'raja';
        $this->cache->save('temp_store', $store, 300);
        $data = $this->cache->get('temp_store');

        if ($data == $store) {
            return "Mem cache working";
        } else {
            return "Mem cache not working";
        }
    }

    //removed few countries and couldn't get correct country name.. 
    private function verify_counry_list() {
        $country_list = $this->lastfm_model->country_list();
        foreach ($country_list as $index => $country) {

            $url = 'http://ws.audioscrobbler.com';
            $api_key = 'bbf3d84f5c8eb16b51140643b71fab8d';
            //$country = 'spain';
            /*
             *  country (Required) : A country name, as defined by the ISO 3166-1 country names standard
              limit (Optional) : The number of results to fetch per page. Defaults to 50.
              page (Optional) : The page number to fetch. Defaults to first page.
              api_key (Required) : A Last.fm API key.
             */
            $final_url = $url . '/2.0/?method=geo.gettopartists&country=' . $country . '&api_key=' . $api_key . '&format=json&limit=1';

            $data = $this->lastfm_model->get_url_content($final_url);

            if (!empty($data)) {

                $result = json_decode($data, true);
                if (empty($result['topartists'])) {

                    echo $country . " ================= Not Valid ==== <br>";
                    sleep(1);
                } else {

                    echo $country . " ==== Valid ==== <br>";
                    sleep(1);
                }
            } else {

                echo $country . " ==== Empty Response ==== <br>";
                sleep(1);
            }
        }
    }

    //country list extracted from wiki page.. 
    private function get_country_list() {
        $url = 'https://en.wikipedia.org/wiki/ISO_3166-1';
        $content = $this->lastfm_model->get_url_content($url);


        $url = 'https://en.wikipedia.org/wiki/ISO_3166-1';
        $content = $this->get_url_content($url);

        preg_match('|sortable(.*)table|msU', $content, $data);
        $country_array = array();
        if (!empty($data[0])) {
            preg_match_all('|<tr>(.*)</a>|msU', $data[0], $ss);
            print_r($ss);
            exit;
            if (!empty($ss[0])) {
                foreach ($ss[0] as $value) {
                    preg_match('|<td><a(.*)>(.*)</a>|msU', $value, $matc);
                    if (!empty($matc) && !empty($matc[2])) {
                        $country_array[] = $matc[2];
                    }
                }
            }
        }
        print_r($country_array);
        exit;
    }

}
