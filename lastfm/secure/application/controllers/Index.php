<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {


    /**
     * get artist list from api
     *
     * This function will get artist list from third party api
     * we will be caching it in cache and return to api
     * If cache exist then will send cached data
     * 
     * @function artists
     * @return json data
     * @throws Exception if any error
     */
    public function artists() {
        try {
            //get params
            $country    = $this->input->get('country');
            $page       = $this->input->get('page');
            $page       = !empty($page) ? $page : 1;

            $this->load->model('lastfm_model');
            //get country list to validate.. 
            $country_list   = $this->lastfm_model->country_list();
            $country        = !empty($country) ? urldecode($country) : '';
            if (in_array($country, $country_list)) {
                
                $data = $this->lastfm_model->get_artist_list($country, $page);
            } else {
                
                $data = array('success' => 0, 'message' => 'Please select country');
            }
        } catch (Exception $e) {
            
            $data = array('success' => 0, 'message' => 'Something went wrong');
        }
        echo json_encode($data);
        exit;
    }

    /**
     * country list api
     *
     * This function will get country list from Model 
     * and will return it to api in json format
     * 
     * @function country_list
     * @return json data
     */
    public function country_list() {
        $this->load->model('lastfm_model');
        /* Create a table if it doesn't exist already */
        echo json_encode($this->lastfm_model->country_list());
        exit;
    }

}
