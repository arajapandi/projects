
App = Ember.Application.create();



Handlebars.registerHelper('if_eq', function(lvalue, rvalue, options) {
    
    if( lvalue!=rvalue ) {
        return options.inverse(this);
    } else {
        return options.fn(this);
    }
});

//Route Mapping, we are not using multiple pages so calling index
App.Router.map(function () {
     // No need of any route since we are using only one 
});

//Host Name or URL name to call methods. 
var HOST = 'secure/index.php/index';

//get Json content from server
var getjson = function (url) {
    var mydata = null;
    $.ajax({
        url: HOST + '/' + url,
        async: false,
        dataType: 'json',
        success: function (json) {
            mydata = json;
        }
    });
    return mydata;
};
/* We are not using Model, store..
App.Store = DS.Store.extend({
    revision: 4,
    adapter: DS.RESTAdapter.create({'url': HOST, namespace: ''}),
    reload: function (object, controller) {
        object.store.unloadAll(controller);
        object.set('model', object.store.find(controller));
    },
    getJSON: getjson
});*/

//load country list and assign to dict, we can use this on top.. 
App.Dictionary = {
    init: function () {
        this.set('countryDict', getjson('country_list'));
    },
    countryDict: null
};

//Index Route
App.IndexRoute = Ember.Route.extend({
  model: function(params){
    
    var country = ('country' in params && params.country) ? params.country : "";
    var page    = ('page' in params && params.page) ? params.page : 1;
    
    if(country && page) {
        this.controllerFor('index').loadArtist(country,page);
    }
    return false;
  }
});

App.IndexviewRoute = Ember.Route.extend({});

App.IndexController = Ember.Controller.extend({
    queryParams : ['country','page'],
    page:null,
    country:null,
    //empty container to cache data
    cacheData: {},
    
    /**
     * empty Artist List and hide pagination.
     * 
     * @function emptyList
     * 
     */
    emptyList: function () {
        this.set('artist_list', '');
        $('#pagination').hide();
    },
    /**
     * get cache data.. if exist the no need to call backend
     * 
     * @function getCache
     * @param {string} key
     */
    getCache: function (key) {
        if (key in this.cacheData) {
            return this.cacheData[key];
        }
        return false;
    },
    
    /**
     * set cache data and cache minimum entries
     * this funciton is local cache for entries you can change local cache numbers below
     * 
     * @function setCache
     * @param {string} key 
     * @param {object} cache_data 
     */
    setCache: function (key, cache_data) {
        var obj_length = Object.keys(this.cacheData).length;
        if (obj_length > 10) {
            console.log('Deleting...' + Object.keys(this.cacheData)[0]);
            delete this.cacheData[Object.keys(this.cacheData)[0]];
        }
        this.cacheData[key] = cache_data;
        return true;
    },
    
    /**
     * pagination part - based on attr data from api..we will be formatting pagination code
     * 
     * @function pagination
     * @param {object} object this controller object
     * @param {string} country 
     * @param {object} data artist data  
     */
    pagination: function (object, country, data) {
        return function() {
        //get pagination attr values and store in local variable...
        var attr = data['@attr'];
        var total = parseInt(attr['total'], 10);
        var page = parseInt(attr['page'], 10);
        var total_page = parseInt(attr['totalPages'], 10);
        
        //we are not getting data after 2000 tracks..so limit to 2000
        total_page = Math.min(total_page, 2000);

        // pagination disabled state removal and unbind existing click event.. 
        var css_list = ['last_page', 'next_page', 'first_page', 'prev_page'];
        $.each(css_list, function (index, value) {
            $('#' + value).parent('li').attr('class', '');
            $('#' + value).unbind('click');
        });

        //add linkes default and then unbind necessary href.. 
        $('#first_page').click(function () {
            object.send('selectCountry', country, 1)
        });
        $('#prev_page').click(function () {
            object.send('selectCountry', country, page - 1)
        });
        $('#last_page').click(function () {
            object.send('selectCountry', country, total_page)
        });
        $('#next_page').click(function () {
            object.send('selectCountry', country, page + 1);
        });

        //if its last page, or first page then disable prev/next button respectively.. 
        
        if (page == total_page) {
            $('#last_page').unbind('click').parent().attr('class', 'disabled');
            $('#next_page').unbind('click').parent().attr('class', 'disabled');
        } else if (page == 1) {
            $('#first_page').unbind('click').parent().attr('class', 'disabled');
            $('#prev_page').unbind('click').parent().attr('class', 'disabled');
        }

        //after pushing all values to pagination container.. enable it.. 
        $('#pagination').show();
        $('#info').text('Showing ' + page + ' of ' + total_page + ' pages');
        
        
        
        //this.transitionToRoute('index',{"queryParams":{"country":country,"page":page}});
        //this.transitionTo('index',{"country":country,"page":page});
        
    }


    },
    /**
    * Set artist data to ember view, call pagination function
    * Manupulate data for image, css..etc
    * 
    * @function setArtistData
    * @param {artist_list} Object Artist list data
    * @param {country} String  Selected Country
    * @param {cache} bool data is from cache or not
    */
    setArtistData: function (artist_list, country, cache) {
        //artist image and css for offset
        cache = cache == undefined ? false : true;
        if (cache == false) {
            $.each(artist_list.artist, function (index, value) {
                artist_list.artist[index].css = (index == 0) ? "col-md-offset-1" : "";
                artist_list.artist[index].short_name =  jQuery.trim(artist_list.artist[index].name).substring(0, 15)
                          .trim(this) + "...";;
               
                artist_list.artist[index].smimage = (value.image[2]['#text']) ? value.image[2]['#text'] : 'http://placehold.it/174x174';
                artist_list.artist[index].lgimage = (value.image[3]['#text']) ? value.image[3]['#text'] : 'http://placehold.it/300x300';
            });
        }

        //set artist to artist_list and ember will take care of loading..
        this.set('artist_list', artist_list.artist);
        Ember.run.schedule("afterRender",this,this.pagination(this, country, artist_list));
    },
    
    /**
    * This is action which is triggered from dropdown or country and with page number
    * We are getting country from dropdown
    * 
    * @function loadArtist
    * @param {country} String  Selected Country
    * @param {page} Number page number 
    */
    loadArtist: function (country, page) {
    
        //Hide main container and show loading div
        $('#artist_container').hide();
        $('#loading').show();

        var cache_key = country + ':' + page.toString();

        var artist_data = this.getCache(cache_key);
        if (artist_data != false && typeof artist_data == 'object') {
            this.setArtistData(artist_data, country, true);
        } else {

            var artists = getjson('artists?country=' + country + '&page=' + page);
            if ('success' in artists && artists['success'] == 0) {
                //empty artist list if any eror shown.. 
                this.emptyList();

            } else {
                if ('topartists' in artists.data 
                        && 'artist' in artists.data.topartists 
                        && artists.data.topartists.artist.length > 0) {
                    this.setCache(cache_key, artists.data.topartists);
                    this.setArtistData(artists.data.topartists, country);
                } else {
                    //empty artist list if artist is empty.. 
                    this.emptyList();
                }
            }
        }

        //hide loadig div and show artist.. 
        $('#artist_container').show();
        $('#loading').hide();
    }
    ,
    actions: {
        /**
        * reset modal data when user close modal
        * 
        * @function resetModal
        */
        resetModal:function() {
            this.set('sartist',{});
        },
        /**
        * view artist and open modal
        * 
        * @function viewArtist
        * @param artist object artist data
        */
        viewArtist: function(artist) {
          console.log(artist);
          this.set('sartist',artist);
          $('#artModal').modal();
        },
        /**
        * This is action which is triggered from dropdown or country and with page number
        * We are getting country from dropdown
        * 
        * @function selectCountry
        * @param {country} String  Selected Country
        * @param {page} Number page number 
        */
        selectCountry: function (country, page) {
            this.set('country', country);
            page = page == undefined ? 1 : page;
            country = encodeURI(country);
            this.transitionToRoute({"queryParams":{"country":country,"page":page}});
            //this.loadArtist(country,page);
        }
    }
}, App.Dictionary);
