<?php

/**
 * Profile model Class
 *
 * @category        Profiles Model
 * @author          Rajapandian a
 * @license         NA
 * @link            NA
 */
class Profile_model extends CI_Model {



      function saveFeedback($fvalue) {

          $fvalue['updated_at'] = date('Y-m-d H:i:s');
          return $this->db->insert('profile_feedback', $fvalue);
      }


    /**
     * @function getAccounts
     * @return object
     */
    function getAccounts($offset=0, $per_page=10, $status=1, $gender = 0, $lang=1)
    {


        $this->db->select('p.profile_id, p.name, p.education, um.image, p.father_name, um.district, um.dob, p.sevvai, um.totalpro, um.created_at, um.updated_at')
                     ->from('profiles as um')
                     ->join('profile_details as p', 'um.id = p.profile_id and p.lang_id='. $lang);


        $this->db->where('gender =', $gender);

        if(!empty($status)) {
            $this->db->where('status =', $status);
        }

        $this->db->order_by('p.id', 'desc');
        $this->db->limit($per_page, $offset);

        return $this->db->get()->result_array();
    }


    function getTotalProfiles($status = 1, $gender = 0, $lang=1) {
         $this->db->select(' count(1) as cnt')
                     ->from('profiles as um')
                     ->join('profile_details as p', 'um.id = p.profile_id and p.lang_id='. $lang);


        $this->db->where('um.gender =', $gender);

        if(!empty($status)) {
            $this->db->where('um.status =', $status);
        }
        $row = $this->db->get()->row_array();
        if(!empty($row)) {
            return $row['cnt'];
        }
        return 0;
    }


    function getLang($profile_id, $lang=1) {

        $this->db->select(' * ')
                     ->from('profile_details as um');

        $this->db->where(array('profile_id'=>$profile_id,'lang_id'=>$lang));
        return $this->db->get()->row_array();

    }

        function getProfile($profile_id) {

            $this->db->select(' * ')
                         ->from('profiles as um');

            $this->db->where('id =', $profile_id);

            return $this->db->get()->row_array();
        }

}
