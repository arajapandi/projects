<?php

/**
 * Client model Class
 *
 * @category    	Client Model
 * @author        	Rajapandian a
 * @license         NA
 * @link            NA
 */
class Client_Model extends CI_Model {


    public function getMember($data, $status=1, $field='email') {
        $this->db->where($field, $data);
        $this->db->where('status', $status);
        $query = $this->db->get('profiles');
        if($query->num_rows() >= 1) {
            return $query->first_row();
        }
        return false;
    }
    public function login($login, $password) {
        if(!empty($login) && !empty($password)) {
            $row = $this->getMember($login,1,'phone');

            if (!empty($row)) {
                $hasedPassword = md5($password.$row->salt);

                if ($hasedPassword == $row->passwd) {
                    $this->session->set_userdata('email', $row->email);
                    $this->session->set_userdata('phone', $row->phone);
                    $this->session->set_userdata('profile_id', $row->id);
                    return true;
                }
            }
        }
        return false;
    }


    function otp_check($otp) {
        if($this->session->userdata('profile_id') &&
           $this->session->userdata('email')) {
            $data = $this->getMember($this->session->userdata('profile_id'));
            if(!empty($data) && !empty($data->extra)) {
                $extra = json_decode($data->extra, true);
                if(!empty($extra['otp']) && $extra['otp'] == $otp) {
                    $this->session->set_userdata('otp_verified', true);
                    return true;
                }
            }
        }
        return false;
    }

    function get_id() {
        return $this->session->userdata('profile_id');
    }

    function is_logged() {
        if ($this->session->userdata('email')
                && $this->session->userdata('profile_id')) {
            return TRUE;
        }
        return FALSE;
    }

    function send_pass_mail($profile_id) {
        $this->db->where("id", $profile_id);
        $query = $this->db->get('profiles');
        $row = $query->first_row();

        $fvalue['password_salt'] = $this->_get_random_string();
        $password = $this->_get_random_string(6);
        $fvalue['password_hash'] = md5($password . $fvalue['password_salt']);
        $this->db->where('id', $profile_id);
        $this->db->update('profiles', $fvalue);



        $content = 'Hello {name}, <br><br>

                        Please check your Login credential for gavaramahajanamatrimony.com.<br><br>

                        Username : {email}<br>
                        Password : {password}<br>

                        Please contact Gavara Mahajana Matrimony support to help you.<br><br>

                        Thank You,<br>
                        IT Team';



        $message = str_replace('{name}', $row->name, $content);
        $message = str_replace('{password}', $password, $message);

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.sempiyan.com',
            'smtp_port' => 25,
            'smtp_user' => 'test@sempiyan.com',
            'smtp_pass' => 'test@123',
            'mailtype'  => 'plain',
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->from('info@gavaramahajanamatrimony.com', 'SafePlan');
        $this->email->to($row->email);
        $this->email->subject('Gavara Mahajana Matrimony : Login Credential');
        $this->email->message($message);
        $this->email->set_alt_message($message);
        $this->email->send();
        return true;
    }

    function _get_random_string($length=12) {
        $length = $length;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $string = '';
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters))];
        }
        return $string;
    }

    function logout() {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('profile_id');
    }

    function validate_reset_password($key) {
        /* check the player id is exist or not */

        $this->db->where("reset_hash", $key);
        $query = $this->db->get('profiles');


        if (count($query->result())) {
            $row = $query->first_row();
            if (!empty($row->reset_hash_date) && strtotime('now') < strtotime('+1 day', strtotime($row->reset_hash_date))) {
                return $row->id;
            }
            return false;
        }
        return false;
    }

    /**
     * remove the reset password hash from database.
     * @function removeResetHash
     * @parem admin_id - primary id of admin
     * @return boolean
     */
    function remove_reset_hash($email) {

        $data = array(
            'reset_hash' => null,
            'reset_hash_date' => null
        );
        $this->db->where('email', $email);
        return $this->db->update('profiles', $data);
    }

    /**
     * change the password form give admin_id
     * @function changePassword
     * @parem password - password to change
     * @parem admin_id - primary id of admin
     * @return boolean
     */
    function change_password($password, $email) {

        $this->db->where('email', $email);
        $query = $this->db->get('profiles');
        $row = $query->first_row();

        if(!empty($row)) {
          $data = array(
              'passwd' => md5($password . $row->salt ),
              'updated_at' => 'now()'
          );
          $this->db->where('email', $email);
          return $this->db->update('profiles', $data);
        }
        return false;
    }

    /**
     * This method perform Reset Password functionality. Password reset link will be sent to user's email ID.
     * @function resetPassword
     * @parem string email - string
     * @return boolean
     */
    function reset_password($email) {

        $this->db->where("email", $email);
        $query = $this->db->get('profiles');

        if (count($query->result())) {
            $row = $query->first_row();
            $email = $row->email;
            $profile_id = $row->id;

            $name = $row->name;
            /* if the email is not empty, then send mail to that email */
            if (!empty($email)) {
                /*$content = 'Hello {name},

                    A request was just made to reset your account password.

                    Please visit <a href="{link}">{link}</a> in order to reset your password, and we will email you again with your current username and new password.

                    Thank You,
                    SafePlan';
                 * */
                $content = 'Hello {name},<br><br>

                    A request was just made to reset your account password.<br><br>

                    Please visit <a href="{link}" target="_blank">Link</a> in order to reset your Password, and we will email you again with your current E-Mail and new Password.<br><br>

                    Thank You<br>,
                    IT Team';

                $encKey = md5($email . $profile_id);
                $data = array(
                    'reset_hash' => $encKey,
                    'reset_hash_date' => date('Y-m-d H:i:s')
                );

                $this->db->where('email', $email);
                $this->db->update('profiles', $data);

                $message = str_replace('{name}', $name, $content);
                $message = str_replace('{link}', site_url('login/change/?key=' . urlencode($encKey)), $message);
                $message = str_replace('{link}', site_url('login/change/?key=' . urlencode($encKey)), $message);

                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'mail.sempiyan.com',
                    'smtp_port' => 25,
                    'smtp_user' => 'test@sempiyan.com',
                    'smtp_pass' => 'test@123',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email', $config);
                $this->email->from('info@gavaramahajanamatrimony.com', 'Gavara Mahajana Matrimony');
                $this->email->to($email);
                $this->email->subject('Gavara Mahajana Matrimony : Password Reset Request');
                $this->email->message($message);
                $this->email->set_alt_message($message);
                $this->email->send();
                return true;
            }
        }
        return false;
    }

    /* validate the login-function */

    public function isLogged() {
        if ($this->session->userdata('profile_id')) {
            return true;
        }
        return false;
    }

}
