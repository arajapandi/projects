<?php

/**
 * Profile model Class
 *
 * @category        Profiles Model
 * @author          Rajapandian a
 * @license         NA
 * @link            NA
 */
class Profile_model extends CI_Model {

    /**
     * @function getAccounts
     * @return object
     */
    function getAccounts($offset=0, $per_page=10, $status=1)
    {


        $this->db->select('id, name, status, image, created_at, updated_at')
                     ->from('profiles as um');

        if(!empty($status)) {
            $this->db->where('status =', $status);
        }

        $this->db->order_by('id', 'desc');
        $this->db->limit($per_page, $offset);

        return $this->db->get()->result_array();
    }

    function check_unique_user_phone($profile_id = '', $phone) {
        $this->db->where('phone', $phone);
        if(!empty($profile_id)) {
            $this->db->where_not_in('id', $profile_id);
        }
        return $this->db->get('profiles')->num_rows();
    }
    function check_unique_user_email($profile_id = '', $email) {
        $this->db->where('email', $email);
        if(!empty($profile_id)) {
            $this->db->where_not_in('id', $profile_id);
        }
        return $this->db->get('profiles')->num_rows();
    }

    function getTotalProfiles($status = 1) {
         $this->db->select(' count(1) as cnt')
                     ->from('profiles as um');

        if(!empty($status)) {
            $this->db->where('status =', $status);
        }
        $row = $this->db->get()->row_array();
        if(!empty($row)) {
            return $row['cnt'];
        }
        return 0;
    }
    function getActiveProfiles() {
        return $this->getTotalProfiles(1);
    }

    function getProfile($profile_id) {

        $this->db->select(' * ')
                     ->from('profiles as um');

        $this->db->where('id =', $profile_id);

        return $this->db->get()->row_array();
    }

    function getLang($profile_id, $lang=1) {

        $this->db->select(' * ')
                     ->from('profile_details as um');

        $this->db->where(array('profile_id'=>$profile_id,'lang_id'=>$lang));
        return $this->db->get()->row_array();

    }

    function updateLangDetails($fvalue, $lang_id, $profile_id) {

        $accounts_db_field = array('profile_id','lang_id','name','sevvai','ragukethu','education', 'job','annual_income' ,'father_name','mother_name','res_city' ,'height' ,'color' ,'kulam' ,'kulam_temple','tamil_birth','tamil_year' ,'tamil_month' ,'tamil_date', 'tamil_nazhigai','tamil_time' ,'created_at' ,'updated_at');
        $profile_val = $this->process_db_fields($accounts_db_field, $fvalue);

        $profile_val['lang_id'] = $lang_id;
        $profile_val['profile_id'] = $profile_id;
        $profile_val['updated_at'] = date('Y-m-d H:i:s');

        if(isset($profile_id) && !empty($lang_id)) {

            $old_data = $this->getLang($profile_id, $lang_id);

            if(empty($old_data)) {

                $this->db->set($profile_val);
                $this->db->insert('profile_details');
                $profile_id = $this->db->insert_id();

            } else {

                $this->db->where('id', $old_data['id']);
                $this->db->update('profile_details', $profile_val);
            }

        }
    }

    function save($fvalue) {


        $accounts_db_field = array( 'name',  'email', 'gender', 'status', 'updated_at'
            ,'nakshatram','laknam', 'rasi','jathagam', 'thisai','balance','byear','month','information'
            ,'expectation','district','bromarried','brounmarried','sismarried','dob','reg_date','phone',
            'sisunmarried','house','land','vehicle','provalue1','provalue2','provalu3','totalpro','jatsquare','image');
        $profile_val = $this->process_db_fields($accounts_db_field, $fvalue);
        $profile_val['updated_at'] = date('Y-m-d H:i:s');

        if(!empty($profile_val['dob'])) {
            $profile_val['dob'] = explode('/',$profile_val['dob']);
            if(count($profile_val['dob']) == 3) {
                $profile_val['dob'][4] = $profile_val['dob'][1];
                $profile_val['dob'][1] = $profile_val['dob'][0];
                $profile_val['dob'][0] = $profile_val['dob'][4];
                unset($profile_val['dob'][4]);
                $profile_val['dob'] = date('Y-m-d H:i:s',strtotime(implode('/', $profile_val['dob'])));
            } else {
                $profile_val['dob'] = null;
            }
        }
        if(!empty($profile_val['reg_date'])) {
            $profile_val['reg_date'] = explode('/',$profile_val['reg_date']);
            if(count($profile_val['reg_date']) == 3) {
                $profile_val['reg_date'][4] = $profile_val['reg_date'][1];
                $profile_val['reg_date'][1] = $profile_val['reg_date'][0];
                $profile_val['reg_date'][0] = $profile_val['reg_date'][4];
                unset($profile_val['reg_date'][4]);
                $profile_val['reg_date'] = date('Y-m-d H:i:s',strtotime(implode('/', $profile_val['reg_date'])));
            } else {
                $profile_val['reg_date'] = null;
            }
        }

        if(!empty($fvalue['jatsquare']['rasi'])) {
          $profile_val['jatsquare']['rasi'] = $fvalue['jatsquare']['rasi'];
        } else if(!empty($fvalue['rasi'])) {
            $profile_val['jatsquare']['rasi'] = $fvalue['rasi'];
        }
        if(!empty($fvalue['jatsquare']['amsam'])) {
          $profile_val['jatsquare']['amsam'] = $fvalue['jatsquare']['amsam'];
        } else if(!empty($fvalue['amsam'])) {
            $profile_val['jatsquare']['amsam'] = $fvalue['rasi'];
        }
        if(!empty($profile_val['jatsquare'])) {
            $profile_val['jatsquare'] = json_encode($profile_val['jatsquare']);
        }

        if(isset($fvalue['profile_id']) && !empty($fvalue['profile_id'])) {

            $profile_id = $fvalue['profile_id'];
            $old_data = $this->getProfile($profile_id);
            if(empty($old_data)) {
                return false;
            }

            $this->db->where('id', $profile_id);
            $this->db->update('profiles', $profile_val);

        } else {

            $this->db->set($profile_val);
            $this->db->insert('profiles');
            $profile_id = $this->db->insert_id();
        }



        return $profile_id;
    }

    function process_db_fields($fields, $data) {
        $common_array = array();
        foreach($fields as $val) {
            if(isset($data[$val])) {
                $common_array[$val] = $data[$val];
            }
        }
        return $common_array;
    }

    function deactivateAccount($id, $status=0) {
        $data = array(
               'status' => $status,
            );
        $this->db->where('id', $id);
        $this->db->update('profiles', $data);
        return true;
    }
    function deleteMessages($id) {

        $this->db->where('id', $id);
        $this->db->delete('profiles');

        $this->db->where('profile_id', $id);
        $this->db->delete('profile_details');
        return true;
    }

}
