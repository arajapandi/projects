<?php

/**
 * Client model Class
 *
 * @category    	Client Model
 * @author        	Rajapandian a
 * @license         NA
 * @link            NA
 */
class Client_Model extends CI_Model {

    var $permission = '';
    function __construct() {
        parent::__construct();
        $this->access = 2;
        if($this->is_logged()) {
            $this->get_permission();
        }    //member_id
    }

    public function getMember($data, $status=1, $field='username') {
        $this->db->where($field, $data);
        $this->db->where('status', $status);
        $query = $this->db->get('accounts');
        if($query->num_rows() >= 1) {
            return $query->first_row();
        }
        return false;
    }
    public function login($login, $password) {
        if(!empty($login) && !empty($password)) {
            $row = $this->getMember($login,1,'username');
            if (!empty($row)) {
                $hasedPassword = md5($password.$row->salt);

                if ($hasedPassword == $row->passwd) {
                    $this->session->set_userdata('username', $row->username);
                    $this->session->set_userdata('user_id', $row->id);
                    $this->session->set_userdata('access', 1);
                    return true;
                }
            }
        }
        return false;
    }


    function otp_check($otp) {
        if($this->session->userdata('member_id') &&
           $this->session->userdata('email')) {
            $data = $this->getMember($this->session->userdata('member_id'));
            if(!empty($data) && !empty($data->extra)) {
                $extra = json_decode($data->extra, true);
                if(!empty($extra['otp']) && $extra['otp'] == $otp) {
                    $this->session->set_userdata('otp_verified', true);
                    return true;
                }
            }
        }
        return false;
    }

    function get_permission() {
     return true;
    }
    function get_id() {
        return $this->session->userdata('user_id');
    }

    function is_logged() {
        if ($this->session->userdata('email')
                && $this->session->userdata('member_id')
                && $this->session->userdata('otp_verified')) {
            return TRUE;
        }
        return FALSE;
    }

    function is_super_admin() {
        if ($this->session->userdata('access') == 1) {
            return TRUE;
        }
        return FALSE;
    }

    function is_admin() {

        if ($this->session->userdata('access') == 2) {
            return TRUE;
        }
        return FALSE;
    }

    function get_total_admins() {
        $this->db->select('count(member_id) AS member_count')->from('member')->where('access', $this->access);
        $query = $this->db->get();
        $row = $query->first_row();
        return $row->member_count;
    }

    function get_admins($offset, $per_page, $order_by='') {
        if (is_array($order_by)) {
            $this->db->order_by($order_by['order'], $order_by['by']);
        }
        $this->db->from('member')->where('access', $this->access)->limit($per_page, $offset);
        $query = $this->db->get();
        return $query;
    }

    function get_admin($member_id) {
        $this->db->where('member_id', $member_id);
        $this->db->from('member');
        $query = $this->db->get();
        return $query->row_array();
    }

    function save_admin($fvalue, $member_id='') {

        if ($member_id) {
            $this->db->where('member_id', $member_id);
            return $this->db->update('member', $fvalue);
        } else {
            $this->db->insert('member', $fvalue);
            $member_id = $this->db->insert_id();
            return $member_id;
        }
    }

    function send_pass_mail($profile_id) {
        $this->db->where("id", $profile_id);
        $query = $this->db->get('profiles');
        $row = $query->first_row();

        $fvalue['salt'] = $this->_get_random_string();
        $password = $this->_get_random_string(6);
        $fvalue['passwd'] = md5($password. $fvalue['salt']);
        $this->db->where('id', $profile_id);
        $this->db->update('profiles', $fvalue);



        $content = 'Hello {name},<br><br>

                        Please check your Login credential for Gavara Mahajana Sangam Website.<br><br>

                        Username : {phone}<br>
                        Password : {password}<br>
                        Status : {status}<br>
                        Please contact Gavara Mahajana Sangam support to help you.<br><br>

                        Thank You,<br>
                        Gavara Mahajana Sangam';



        $message = str_replace('{name}', $row->name, $content);
        $message = str_replace('{password}', $password, $message);
        $message = str_replace('{email}', $row->email, $message);
        $message = str_replace('{phone}', $row->phone, $message);
        //$status = $row->status == 1 ? $this->lang->line('active') : $this->lang->line('in_active');
        $status = $row->status == 1 ? 'Active' : 'In_active';
        $message = str_replace('{status}', $status, $message);

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.sempiyan.com',
            'smtp_port' => 25,
            'smtp_user' => 'test@sempiyan.com',
            'smtp_pass' => 'test@123',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->from('info@gavaramahajanamatrimony.com', 'Gavara Mahajana Sangam');
        $this->email->to($row->email);
        $this->email->subject('Gavara Mahajana Sangam : Login Credential');
        $this->email->message($message);
        $this->email->set_alt_message($message);
        $this->email->send();
        return true;
    }

    function _get_random_string($length=12) {
        $length = $length;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $string = '';
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, (strlen($characters)-1))];
        }
        return $string;
    }

    function logout() {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('access');
    }

    function validate_reset_password($key) {
        /* check the player id is exist or not */

        $this->db->where("reset_hash", $key);
        $query = $this->db->get('member');


        if (count($query->result())) {
            $row = $query->first_row();
            if (strtotime('now') < strtotime('+1 day', strtotime($row->reset_hash_date))) {
                return $row->member_id;
            }
            return false;
        }
        return false;
    }

    function delete_admin($selected) {
        $to_delete = '';
        if (is_array($selected) && count($selected)) {
            $to_delete = implode(',', $selected);
        } else if ((int) $selected) {
            $to_delete = (int) $selected;
        }
        if ($to_delete) {
            $this->db->where_in('member_id', $to_delete);
            $this->db->delete('member');
            return true;
        }
        return false;
    }

    /**
     * remove the reset password hash from database.
     * @function removeResetHash
     * @parem admin_id - primary id of admin
     * @return boolean
     */
    function remove_reset_hash($email) {

        $data = array(
            'reset_hash' => null,
            'reset_hash_date' => null
        );
        $this->db->where('email', $email);
        return $this->db->update('member', $data);
    }

    /**
     * change the password form give admin_id
     * @function changePassword
     * @parem password - password to change
     * @parem admin_id - primary id of admin
     * @return boolean
     */
    function change_password($password, $email) {

        $this->db->where('email', $email);
        $query = $this->db->get('member');
        $row = $query->first_row();

        $data = array(
            'password_hash' => md5($row->password_salt . $password),
            'last_modified_date' => 'now()'
        );
        $this->db->where('email', $email);
        return $this->db->update('member', $data);
    }

    /**
     * This method perform Reset Password functionality. Password reset link will be sent to user's email ID.
     * @function resetPassword
     * @parem string email - string
     * @return boolean
     */
    function reset_password($email) {

        $this->db->where("email", $email);
        $query = $this->db->get('member');



        if (count($query->result())) {
            $row = $query->first_row();
            $email = $row->email;
            $member_id = $row->member_id;

            $name = $row->first_name . ' ' . $row->last_name;
            $id = $user_query->row['id'];
            /* if the email is not empty, then send mail to that email */
            if (!empty($email)) {
                /*$content = 'Hello {name},

                    A request was just made to reset your account password.

                    Please visit <a href="{link}">{link}</a> in order to reset your password, and we will email you again with your current username and new password.

                    Thank You,
                    SafePlan';
                 * */
                $content = 'Hello {name},<br><br>

                    A request was just made to reset your account password.<br><br>

                    Please visit {link} in order to reset your password, and we will email you again with your current username and new password.<br><br>

                    Thank You,<br>
                    SafePlan<br>';



                $encKey = md5($email . $member_id);
                $data = array(
                    'reset_hash' => $encKey
                );

                $this->db->where('email', $email);
                $this->db->update('member', $data);

                $message = str_replace('{name}', $name, $content);
                $message = str_replace('{link}', site_url('login/change/?key=' . urlencode($encKey)), $message);

                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'mail.sempiyan.com',
                    'smtp_port' => 25,
                    'smtp_user' => 'test@sempiyan.com',
                    'smtp_pass' => 'test@123',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email', $config);
                $this->email->from('donotreply@safeplan.com', 'SafePlan');
                $this->email->to($email);
                $this->email->subject('SafePlan : Password Reset Request');
                $this->email->message($message);
                $this->email->set_alt_message($message);
                $this->email->send();
                return true;
            }
        }
        return false;
    }

    /* validate the login-function */

    public function isLogged() {
        if ($this->session->userdata('user_id')) {
            return true;
        }
        return false;
    }

}
