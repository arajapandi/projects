<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
                die('foo');
		$this->load->helper('url');
		$userdata = $this->session->userdata('userData');

		if(!empty($userdata) && !empty($userdata['userId'])) {

			redirect(base_url().'messages', 'refresh');
		}

		$this->load->view('login');
	}

	public function loginnow()
	{
		$this->load->model('client_model');
		$username        	=   $this->input->post('username');
		$passwd        		=   $this->input->post('passwd');

		if(!empty($username) && !empty($passwd)) {
			$result = $this->client_model->authUser($username, $passwd);
	        if(!empty($result) && $result['success'] == true) {
	            $this->session->set_userdata('userData', $result['auth']);
	        }
			echo json_encode($result);
		}

	}

	public function logout()
	{
		$this->load->helper('url');
		$this->load->model('client_model');
		$userdata = $this->session->userdata('userData');
		if(!empty($userdata)) {
			$this->session->unset_userdata('userData');
		}
		redirect(base_url().'login', 'refresh');
	}
}
