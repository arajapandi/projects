<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messages extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('client_model');
		$valid = $this->client_model->isLogged();
		if($valid == false) {
			redirect(base_url().'logme/', 'refresh');
		}
	}

	public function index() {

		$this->load->helper('url');
		$sessionData = $this->client_model->get_id();
		$this->load->view('messages', $sessionData);
	}
}
