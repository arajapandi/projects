<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profiles extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('client_model');
        $this->load->model('profile_model');
        $this->load->library('form_validation');
        $valid = $this->client_model->isLogged();
        if ($valid == false) {
            redirect(base_url() . 'logme/', 'refresh');
        }
    }

    public function index() {

        $this->load->helper('url');
        $sessionData = $this->client_model->get_id();

        $data = array();



        $page = intval(trim($this->input->get('page'), '/'));
        $per_page = 5;
        $this->load->library('pagination');
        $config['total_rows'] = $this->profile_model->getTotalProfiles('');
        $config['per_page'] = $per_page;
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['cur_page'] = $page;
        $config['base_url'] = base_url() . 'profiles?page=';

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();


        $data['url'] = base_url();
        $data['site_url'] = str_replace('apanel.php', '',base_url());
        $data['profiles'] = $this->profile_model->getAccounts($page, $per_page, '');
        $data['message'] = $this->session->flashdata('message');
        $data['body_content'] = $this->load->view('profiles', $data, true);

        $data['base_url'] = str_replace('apanel.php', '', base_url()) . 'application/apanel/views/';
        $data['user_id'] = $this->client_model->get_id();

        $this->load->view('template', $data);
    }

    public function sendpass() {
        $id     = (int)$this->input->get('id',true);
        if(!empty($id)) {
          $this->client_model->send_pass_mail($id);
          $this->session->set_flashdata('message', "Password Email has been sent to the user!");
        }
        redirect(base_url() . 'profiles', 'refresh');
    }


    public function pdf() {

        $id     = (int)$this->input->get('id',true);
        if(empty($id)) {
          die('foo - No id specified');
        }
        $fvalue = $this->profile_model->getProfile($id);
        $ftamil = $this->profile_model->getLang($id, 2);

        $Lagnam = array(1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
        $thisaiiruppu = array(1=>"Guru thisai","Sani thisai","kethu thisai","Sukura thisai","Suriya thisai","Chandra thisai","Sewwai thisai","Ragu thisai","Budhan thisai");
        $rasi = array(1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
        $nakshatram = array(1=>"Aswini","Bharani","Karthikai",
"Rohini","Mirugasiridam","Thiruvathirai","Punarpusam","Pusam","Ayilyam","Maham","Puram","Uthiram","Atham",
"Chithirai","Swathiv","Visakam","Anusam","Ketai","Mulam", "Puradam","Uthiradam","Thiruvonam","Avitam",
"Sathayam","Puratathi","Uthiratathi","Revathi");

        $Lagnam = array(1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $thisaiiruppu = array(1=>"குருதிசை","சனிதிசை","கேதுதிசை","சுக்ரதிசை","சூரியதிசை","சந்திரதிசை","செவ்வாய்திசை","ராகுதிசை","புதன்திசை");
        $rasi = array(1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $nakshatram = array(1=>"அசுவினி","பரணி","கிருத்திகை",
        "ரோஹிணி","மிருகசீரிஷம்","திருவாதிரை","புனர்பூசம்","பூசம்","ஆயில்யம்","மகம்","பூரம்","உத்திரம்","ஹஸ்தம்",
        "சித்திரை","ஸ்வாதி","விசாகம்","அனுசம்","கேட்டை","மூலம்", "புராடம்","உத்ராடம்","திருவோணம்","அவிட்டம்",
        "சதயம்","புரட்டாதி","உத்திரட்டாதி","ரேவதி");

        $aninco = array(1=>"ஒரு லட்சத்திற்கு கீழ் ","1 லட்சம் முதல் 2 லட்சம் வரை","2 லட்சம் முதல் 4 லட்சம் வரை","4 லட்சம் முதல் 6 லட்சம் வரை","6 லட்சம் முதல் 10 லட்சம் வரை","10 லட்சம் முதல் 20 லட்சம் வரை","20 லட்சத்திற்கு மேல் ","மற்றவை");

        $jatsquare = array('rasi'=>array(),'amsam'=>array());
        if(!empty($fvalue['jatsquare'])) {
            $jatsquare = json_decode($fvalue['jatsquare'],true);
        }

        $Lagnam = array(1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $thisaiiruppu = array(1=>"குருதிசை","சனிதிசை","கேதுதிசை","சுக்ரதிசை","சூரியதிசை","சந்திரதிசை","செவ்வாய்திசை","ராகுதிசை","புதன்திசை");
        $rasi = array(1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $nakshatram = array(1=>"அசுவினி","பரணி","கிருத்திகை",
        "ரோஹிணி","மிருகசீரிஷம்","திருவாதிரை","புனர்பூசம்","பூசம்","ஆயில்யம்","மகம்","பூரம்","உத்திரம்","ஹஸ்தம்",
        "சித்திரை","ஸ்வாதி","விசாகம்","அனுசம்","கேட்டை","மூலம்", "புராடம்","உத்ராடம்","திருவோணம்","அவிட்டம்",
        "சதயம்","புரட்டாதி","உத்திரட்டாதி","ரேவதி");

        $rasidata = array('லக்னம்','சன்','புதன்','சுக்கிரன்','செவ்வாய்','ராகு','கேது','குரு','சந்திரன்','சனி','மந்தி');
        $laknamdata   = array('லக்னம்','சன்','புதன்','சுக்கிரன்','செவ்வாய்','ராகு','கேது','குரு','சந்திரன்','சனி','மந்தி');

        $rasi_details = array();
        $amsam_details = array();
        if(!empty($jatsquare['rasi'])) {
          foreach((array)$jatsquare['rasi'] as $val=>$key) {
             $rasi_details[$key] = isset($rasi_details[$key]) ? $rasi_details[$key].','.$rasidata[$val] : $rasidata[$val];
          }
        }
        if(!empty($jatsquare['amsam'])) {
          foreach((array)$jatsquare['amsam'] as $val=>$key) {
             $amsam_details[$key] = isset($amsam_details[$key]) ? $amsam_details[$key].' <br/>  '.$laknamdata[$val] : $laknamdata[$val];
          }
        }
        $url = '';
        if(!empty($fvalue['image'])) {
          $url = str_replace('apanel.php', '',base_url()).'uploads/'.$fvalue['image'];
        };
        $jathagam = array(1=>'சுத்தமான ஜாதகம்','செவ்வாய்', 'ராகு கேது செவ்வாய்','ரகு கேது');

        $stylesheet = ' table {font-family:"arialuni" ;FONT-SIZE:9px}
        .lfh {font-family:arialuni;FONT-SIZE:16px}
        .lf {font-family:arialuni;FONT-SIZE:9px}
        .latha{font-family:"latha";FONT-SIZE:16px}';

        $html = '
        <table border="0" cellspacing="2" cellpadding="2" width="100%">
           <tr>
              <td align="left" class="lf"> தொலைபேசி  : 0422 2471928 - அலைபேசி : 94425 71928 </td>
              <td align="center" width="35%" class="lf"> ஸ்ரீ  ராமஜெயம் </td>
              <td align="right" width="30%" class="lf"> வேலை நேரம்  : <span class="lf"> 10 AM to 6 PM </span> </td>
           </tr>

                      <tr>
                         <td colspan="4" align="center" class="lf"> <hr>   </td>
                      </tr>
           <tr>
              <td colspan="3" align="center" class="latha"> கவர மகாஜனசங்கம் திருமணத் தகவல் மையம்  </td>
           </tr>
           <tr>
              <td align="left" class="lf"> பிரதி செவ்வாய் விடுமுறை </td>
              <td align="center" class="lf"> &nbsp; </td>
              <td align="right" class="lf"> நாள்  : '.date('d/m/Y').' </td>
           </tr>
           <tr>
              <td colspan="3" align="center" class="lf"> 170 சுப்ரமணியம் ரோடு, <span class="lf">R.S</span> புரம், கோயமுத்துர் - 641 002   </td>
           </tr>

           <tr>
              <td colspan="4" align="center" class="lf"> <hr>   </td>
           </tr>
           <tr>
              <td align="left" class="lf"> பதிவு தேதி  : '.date('d/m/Y',strtotime($fvalue['reg_date'])).' </td>
              <td align="center" class="lf"> ஜனன ஜாதகம் </td>
              <td align="right" class="lf"> பதிவுஎண் : '.$fvalue['id'].' </td>
           </tr>
           <tr>
              <td colspan="4"><hr></td>
           </tr>
           <tr>
              <td colspan="3" align="center">
                 <table border="0" width="100%">
                    <tr>
                       <td width="50%" valign="top">
                          <table border="0" width="100%" cellspacing="2" cellpadding="5">
                             <!--<tr>
                                <td align="left" class="lf">கணினி  எண்</td>
                                <td colspan="3" align="left">: '.$fvalue['id'].' </td>
                             </tr>-->
                             <tr>
                                <td align="left" class="lf">பெயர்</td>
                                <td colspan="3" align="left" class="lf">: '.$ftamil['name'].' </td>
                             </tr>
                             <tr>
                                <td align="left" class="lf">பிறந்த தேதி </td>
                                <td align="left">: '.date('d/m/Y',strtotime($fvalue['dob'])).' </td>
                                <td class="lf" align="right"> தொலைபேசி </td>
                                <td  align="left">:  '.$fvalue['phone'].' </td>

                             </tr>
                             <tr>
                                <td class="lf" align="left">வசிக்கும் நகரம்</td>
                                <td colspan="3" align="left" class="lf">: '.$ftamil['res_city'].' </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">உயரம் </td>
                                <td align="left">: '.$ftamil['height'].' </td>
                                <td class="lf" align="right"> நிறம் </td>
                                <td  align="left">:  '.$ftamil['color'].' </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left"> குலம்   </td>
                                <td  align="left" class="lf" colspan="3">: '.$ftamil['kulam'].'</td>

                             </tr>
                             <tr>
                             <td class="lf" align="left"> குலதெய்வம்கோயில்  </td>
                             <td align="left" class="lf" colspan="3">: '.$ftamil['kulam_temple'].' </td>
                             </tr>

                             <tr>
                                <td class="lf" align="left">செவ்வாய் </td>
                                <td align="left" class="lf">: '.$ftamil['sevvai'].'  </td>
                                <td class="lf" align="right" > ராகு கேது  </td>
                                <td  align="left" class="lf">:  '.$ftamil['ragukethu'].'  </td>
                             </tr>


                             <tr>
                                <td class="lf" align="left">ஜென்ம நட்சத்திரம் </td>
                                <td align="left" class="lf">: '.$nakshatram[$fvalue['nakshatram']].'  </td>
                                <td class="lf" align="right" > ராசி </td>
                                <td  align="left" class="lf">:  '.$rasi[$fvalue['rasi']].'  </td>
                                <!--<td class="lf" align="right"> பாதம் </td>
                                <td align="left">  2 </td>-->
                             </tr>
                             <tr>
                                <td class="lf" align="left">லக்னம்</td>
                                <td align="left" class="lf">: '.$Lagnam[$fvalue['laknam']].' </td>
                                <td class="lf" align="right">திசையிருப்பு </td>
                                <td align="left" class="lf">: '.$thisaiiruppu[$fvalue['thisai']].' </td>

                             </tr>
                             <tr>
                                <td class="lf" align="left">பிறப்பு விபரம்</td>
                                <td colspan="3" align="left" class="lf">:  தமிழ் '.$ftamil['tamil_birth'].'
                                ஆண்டு '.$ftamil['tamil_year'].'
                                மாதம் '.$ftamil['tamil_month'].'
                                தேதி '.$ftamil['tamil_month'].'
                                நாழிைகக்கு   '.$ftamil['tamil_nazhigai'].'
                                நேரம் '.$ftamil['tamil_time'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">கல்வி</td>
                                <td colspan="3" align="left">: '.$ftamil['education'].' </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">உத்யோகம்</td>
                                <td colspan="3" align="left">: '.$ftamil['job'].' </td>
                             </tr>

                             <tr>
                                <td class="lf" align="left"> வருமானம்</td>
                                <td colspan="3" align="left" class="lf">: '.$aninco[$fvalue['annual_income']].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">தகப்பனார் பெயர் </td>
                                <td colspan="3" align="left" class="lf">:  '.$ftamil['father_name'].'  </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">தயார் பெயர் </td>
                                <td colspan="3" align="left" class="lf">: '.$ftamil['mother_name'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left"> சகோதரர்கள் </td>
                                <td  class="lf" colspan="3" align="left">திருமணமானவர்கள் : '.$fvalue['bromarried'].'
                                திருமணமாகாதவர்கள் : '.$fvalue['brounmarried'].' </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">சகோதரிகள் </td>
                                <td class="lf" colspan="3" align="left">திருமணமானவர்கள் : '.$fvalue['sismarried'].' திருமணமாகாதவர்கள் : '.$fvalue['sisunmarried'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">தகவல்கள் </td>
                                <td colspan="3" align="left">: '.$fvalue['information'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">எதிர்பார்ப்பு </td>
                                <td colspan="3" align="left">: '.$fvalue['expectation'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">மாவட்டம் </td>
                                <td colspan="3" align="left">: '.$fvalue['district'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">விடு (அடுக்கு / தனி )</td>
                                <td align="left">: '.$fvalue['house'].'</td>
                                <td class="lf" align="left">நிலம்</td>
                                <td align="left">: '.$fvalue['land'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">வாகனம்</td>
                                <td align="left">: '.$fvalue['vehicle'].'</td>
                                <td class="lf" align="left">சொத்து மதிப்பு 1</td>
                                <td align="left">: '.$fvalue['provalue1'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">சொத்து மதிப்பு 2</td>
                                <td align="left">: '.$fvalue['provalue2'].'</td>
                                <td class="lf" align="left">சொத்து மதிப்பு 3</td>
                                <td align="left">: '.$fvalue['provalue3'].'</td>
                             </tr>

                             <tr>
                                <td class="lf" align="left">மொத்த சொத்து விபரம் </td>
                                <td colspan="3" align="left">: '.$fvalue['totalpro'].'</td>
                             </tr>



                          </table>
                       </td>
                       <td rowspan="10" colspan="2" valign="top">
                          <img src="'.$url.'" width="350px">
                       </td>
                    </tr>
                 </table>
              </td>
           </tr>
           <tr>
              <td colspan="3"  >
                 <table width="100%">
                    <tr>
                       <td>
                          <table border="1" cellspacing="0" cellpadding="2" width="360px">
                             <tr>
                                <td height="60px" width="25%"> '.(isset($rasi_details[1]) ? $rasi_details[1] : '&nbsp;' ).'  </td>
                                <td width="25%"> '.(isset($rasi_details[2]) ? $rasi_details[2] : '&nbsp;' ).' </td>
                                <td width="25%"> '.(isset($rasi_details[2]) ? $rasi_details[2] : '&nbsp;' ).' </td>
                                <td width="25%"> '.(isset($rasi_details[2]) ? $rasi_details[2] : '&nbsp;' ).' </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($rasi_details[12]) ? $rasi_details[12] : '&nbsp;' ).' </td>
                                <td rowspan="2" colspan="2" align="center" height="120px" width="50%">
                                   <div style="vertica-align: middle;padding-top: 10px;">
                                      ராசி
                                   </div>
                                </td>
                                <td width="25%"> '.(isset($rasi_details[5]) ? $rasi_details[5] : '&nbsp;' ).' </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($rasi_details[11]) ? $rasi_details[11] : '&nbsp;' ).' </td>
                                <td width="25%"> '.(isset($rasi_details[6]) ? $rasi_details[6] : '&nbsp;' ).' </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($rasi_details[10]) ? $rasi_details[10] : '&nbsp;' ).' </td>
                                <td width="25%"> '.(isset($rasi_details[9]) ? $rasi_details[9] : '&nbsp;' ).' </td>
                                <td width="25%">'.(isset($rasi_details[8]) ? $rasi_details[8] : '&nbsp;' ).' </td>
                                <td width="25%"> '.(isset($rasi_details[7]) ? $rasi_details[7] : '&nbsp;' ).' </td>
                             </tr>
                          </table>
                       </td>
                       <td>
                          <table border="1" cellspacing="0" cellpadding="2"  width="360px">
                             <tr>
                                <td height="60px" width="25%"> '.(isset($amsam_details[1]) ? $amsam_details[1] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[2]) ? $amsam_details[2] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[3]) ? $amsam_details[3] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[4]) ? $amsam_details[4] : '' ).' &nbsp; </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($amsam_details[12]) ? $amsam_details[12] : '' ).' &nbsp; </td>
                                <td rowspan="2" colspan="2" align="center" height="120px" width="50%">
                                   <div style="vertica-align: middle;padding-top: 10px;">
                                      அம்சம்
                                   </div>
                                </td>
                                <td width="25%">    '.(isset($amsam_details[5]) ? $amsam_details[5] : '' ).' &nbsp; </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($amsam_details[11]) ? $amsam_details[11] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[6]) ? $amsam_details[6] : '' ).' &nbsp; </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($amsam_details[10]) ? $amsam_details[10] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[9]) ? $amsam_details[9] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[8]) ? $amsam_details[8] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[7]) ? $amsam_details[7] : '' ).' &nbsp; </td>
                             </tr>
                          </table>
                       </td>
                       <td width="10%" class="lf">
                          '.$jathagam[$fvalue['jathagam']].'
                          '.(!empty($fvalue['balance']) ? '<br> - இருப்பு : '.$fvalue['balance'] : '').'
                          '.(!empty($fvalue['byear']) ? '<br> - ஆண்டு : '.$fvalue['byear'] : '').'
                          '.(!empty($fvalue['month']) ? '<br> - மாதம் : '.$fvalue['month'] : '').'
                       </td>
                    </tr>
                 </table>
              </td>
           </tr>
           <!--<tr>
              <td colspan ="3" align="center" class="lf">செவ்வாய் தோசம் : no
                 ராகு கேது தோசம் : no
                 குடும்ப செல்வநிலை : naduthara kudumbam
              </td>
           </tr>-->
        </table>';


        $this->load->library("Mpdf");
        $mpdf = new Mpdf('ta_IN','A4',9,'lf',2,2,2,0,0,0);
        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html);
        ob_clean();
        $mpdf->Output();
        exit;
    }

    public function edit() {

        $id = $this->input->get('id');
        if (!empty($id)) {
            $data['fvalue'] = $this->profile_model->getProfile($id);

            $data['fenglish'] = $this->profile_model->getLang($id, 1);
            $data['ftamil'] = $this->profile_model->getLang($id, 2);
            $data['url'] = base_url();
        }

        $data['message'] = $this->session->flashdata('message');

        $data['site_url'] = str_replace('apanel.php', '',base_url());
        $data['base_url'] = str_replace('apanel.php', '', base_url()) . 'application/apanel/views/';
        $data['url'] = base_url();

        $data['body_content'] = $this->load->view('profile_edit', $data, true);
        $data['user_id'] = $this->client_model->get_id();
        $this->load->view('template', $data);
    }

    private function imgupload($profile_id) {
        if (!empty($_FILES["image"]["name"])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['remove_spaces'] = TRUE;
            $config['file_name'] = $profile_id;

            $this->load->library('upload', $config);

            if (!empty($_FILES["image"]["name"])) {

                if (!$this->upload->do_upload('image')) {
                    $error = array('error' => $this->upload->display_errors());
                    //echo '<script>parent.checkalert('.$this->upload->display_errors().');</script>';
                    //$this->session->set_flashdata('message',$this->upload->display_errors());
                    return '';
                } else {
                    $upload_data = $this->upload->data();

                    //$fvalue['base_name'] = $profile_id.''.$upload_data['file_ext'];
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $upload_data['full_path'];
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 75;
                    $config['height'] = 50;
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    return $upload_data['file_name'];
                }
            }
        }
    }

    public function save() {

        $fvalue = $this->input->post('fvalue');
        $ftamil = $this->input->post('ftamil');
        $fenglish = $this->input->post('fenglish');

        if (!empty($fenglish) && !empty($fenglish['name'])) {
            $fvalue['name'] = $fenglish['name'];
        }
        if (!empty($ftamil) && !empty($ftamil['name'])) {
            $fvalue['name'] = empty($fvalue['name']) ? $ftamil['name'] : $fvalue['name'] . ' / ' . $ftamil['name'];
        }

        $this->form_validation->set_rules('fvalue[email]', 'Email', 'trim|required|valid_email|callback_check_user_email');

        try {
            $profile_id = !empty($fvalue['profile_id']) ? $fvalue['profile_id'] : 0;
            if($this->form_validation->run() == TRUE) {
                $profile_id = $this->profile_model->save($fvalue);
                if (!empty($profile_id)) {
                    $message = '';
                    $image_name = '';
                    if (!empty($_FILES["image"]["name"]) && !$image_name = $this->imgupload($profile_id)) {
                        $message = '<span class="label label-danger">Image Upload Failed. </span> &nbsp; ';
                    }
                    if (!empty($image_name)) {
                        $this->profile_model->save(array('image' => $image_name, 'profile_id' => $profile_id));
                    }

                    $this->profile_model->updateLangDetails($fenglish, 1, $profile_id);
                    $this->profile_model->updateLangDetails($ftamil, 2, $profile_id);

                    $this->session->set_flashdata('message', $message . '<span class="label label-success"> Profile Updated Successfully! </span>');
                }
            } else {
                $this->session->set_flashdata('message', '<span class="label label-danger"> Validation Failed! </span>');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('message', '<span class="label label-error"> '.$e->getMessage().' </span>');
        }
        redirect(base_url() . 'profiles/edit?id=' . $profile_id);
    }

    public function check_user_email($email) {
        $fvalue = $this->input->post('fvalue');
        $profile_id = !empty($fvalue['profile_id']) ? $fvalue['profile_id'] : 0;
        $result = $this->profile_model->check_unique_user_email($profile_id, $fvalue['email']);
        if($result == 0)
            $response = true;
        else {
            $this->form_validation->set_message('check_user_email', 'Email must be unique');
            $response = false;
        }
        return $response;
    }

    public function delete() {
        $id = $this->input->get('id');
        if (!empty($id)) {
            $this->profile_model->deleteMessages($id);
            $this->session->set_flashdata('message', 'Account Deleted Successfully!');
        }
        redirect(base_url() . 'profiles', 'refresh');
    }

    public function activate() {

        $id = $this->input->get('id');
        if (!empty($id)) {
            $this->profile_model->deactivateAccount($id, $status = 1); //enable it
            $this->session->set_flashdata('message', 'Profile Enabled Successfully!');
        }
        redirect(base_url() . 'profiles', 'refresh');
    }

    public function deactivate() {

        $id = $this->input->get('id');
        if (!empty($id)) {
            $this->profile_model->deactivateAccount($id);
            $this->session->set_flashdata('message', 'Account Disabled Successfully!');
        }
        redirect(base_url() . 'profiles', 'refresh');
    }

}
