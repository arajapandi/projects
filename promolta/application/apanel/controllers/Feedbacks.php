<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedbacks extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('client_model');
		$this->load->model('profile_model');
                $this->load->model('feedback_model');

		$valid = $this->client_model->isLogged();
		if($valid == false) {
			redirect(base_url().'logme/', 'refresh');
		}
	}

	public function index() {

		$this->load->helper('url');
		$sessionData = $this->client_model->get_id();

		$data = array();



		$page = intval(trim($this->input->get('page'),'/'));
		$per_page = 5;
		$this->load->library('pagination');
                $config['total_rows'] = $this->feedback_model->getTotalFeedbacks('');
                $config['per_page'] = $per_page;
                $config['next_link'] = 'Next';
                $config['prev_link'] = 'Prev';
                $config['cur_page'] = $page;
                $config['base_url'] = base_url().'packages?page=';

                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['prev_tag_open'] = '<li class="prev">';
                $config['prev_tag_close'] = '</li>';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="active"><a href="#">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();


                $data['url'] = base_url();
		$data['feedbacks'] = $this->feedback_model->getFeedbacks($page, $per_page,'');
		$data['message'] = $this->session->flashdata('message');
		$data['body_content'] = $this->load->view('feedbacks', $data, true);

		$data['base_url'] = str_replace('apanel.php','',base_url()).'application/apanel/views/';

                $data['url'] = base_url();
                $data['user_id'] = $this->client_model->get_id();
		$this->load->view('template', $data);
	}

	public function edit() {

                $this->load->helper('url');
		$id      = $this->input->get('id');

		if(!empty($id)) {
                    $data['fvalue'] = $this->feedback_model->getFeedback($id);
                }

                $data['message'] = $this->session->flashdata('message');
		$data['body_content'] = $this->load->view('feedback_edit', $data, true);
                $data['url'] = base_url();
		$data['base_url'] = str_replace('apanel.php','',base_url()).'application/apanel/views/';
                $data['user_id'] = $this->client_model->get_id();
		$this->load->view('template', $data);



	}

	public function save() {

		$fvalue = $this->input->post('fvalue');

	 	$profile_id = $this->feedback_model->save($fvalue);
                if(empty($profile_id)) {
                    $this->session->set_flashdata('message', 'Update Failed!');
                    redirect(base_url().'feedbacks');
                } else {
                    $this->session->set_flashdata('message', ' Saved Successfully!');
                }

		redirect(base_url().'feedbacks/edit?id='.$profile_id);

	}

	public function delete() {
		$id      = $this->input->get('id');
		if(!empty($id)) {
			$this->feedback_model->deleteFeedback($id);
			$this->session->set_flashdata('message', 'Feedback Deleted Successfully!');
                }
                redirect(base_url().'feedbacks', 'refresh');
	}
	public function activate() {

		$id      = $this->input->get('id');
		if(!empty($id)) {
	        $this->feedback_model->deactivateFeedback($id, $status = 1);
	        $this->session->set_flashdata('message', 'Feedback Enabled Successfully!');
    	}
    	redirect(base_url().'feedbacks', 'refresh');
	}
        public function deactivate() {

		$id      = $this->input->get('id');
		if(!empty($id)) {
	        $this->feedback_model->deactivateFeedback($id);
	        $this->session->set_flashdata('message', 'Feedback Disabled Successfully!');
    	}
    	redirect(base_url().'feedbacks', 'refresh');
	}
}
