<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logme extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('client_model');
        $this->data = array();
    }

    function index()
    {

        /*if($this->member_model->is_logged() == TRUE
            && $this->member_model->is_super_admin()) {
            redirect('member');
        } else if($this->member_model->is_logged() == TRUE
            && $this->member_model->is_admin()) {
            redirect('member');
        } else if($this->member_model->is_logged()) {
            redirect('member');
        } */
        $user_id = $this->session->userdata('user_id');
        if(!empty($user_id)) {

            redirect(base_url().'dashboard', 'refresh');
        }

        $data['message'] = $message = $this->session->flashdata('message');;

        $data['url'] = base_url();
        $data['body_content'] = $this->load->view('logme', $data, true);
        $data['base_url'] = str_replace('apanel.php','',base_url()).'application/apanel/views/';
        //$data['user_id'] = $this->client_model->get_id();

        $this->load->view('template', $data);
    }
    function change()
    {
        $this->data['session_for_safe'] = $this->_set_session();;

        /* while post method  */
        $key_post = @$this->input->post('key');

        if($key_post) {
            if($this->member_model->validate_reset_password($key_post)){
                   if($this->input->post('password') && strlen(trim($this->input->post('password')))>5 && $this->input->post('password') == $this->input->post('re_password')) {
                    if($this->member_model->change_password($this->input->post('password'),$this->session->userdata('email'))) {
                        /* remove reset hash from the databse */
                        $this->member_model->remove_reset_hash($this->session->userdata('email'));
                        //$this->redirect($this->url->http('login&from=reset'));
                        $this->session->set_flashdata('from','password_changed');
                        sleep(1);
                        redirect('logme');
                    }
                } else {
                    //$this->session->set_flashdata('message',$this->lang->line('invalid_input_change'));
                    $this->data['message'] = $this->lang->line('invalid_input_change');
                }

            }
        } else {

            /* before post */
            $key = ($key_post) ?$key_post:$this->input->get('key');
            $this->data['key'] = $key;
            if($key) {
                $this->member_model->validate_reset_password($key);
                if($this->member_model->validate_reset_password($key)) {
                    $this->data['message'] = $this->lang->line('change_password_using_below');
                } else {
                    redirect('logme');
                }
            } else {
                redirect('logme');
            }
        }
        $this->load->view('logme/change',$this->data);
    }

    function forgot()
    {

        $email = $this->input->post('username');
        if($email) {
            if($this->member_model->reset_password($email)) {
                //$this->session->set_flashdata('message',$this->lang->line('check_for_ver_mail'));
                $this->data['message'] = $this->lang->line('check_for_ver_mail');
            } else {
                //$this->session->set_flashdata('message',$this->lang->line('unable_to_recover'));
                $this->data['message'] = $this->lang->line('unable_to_recover');
            }
        }
        $this->data['session_for_safe'] = $this->_set_session();
        $this->load->view('logme/forgot',$this->data);
    }
    function logout()
    {
        $this->client_model->logout();
        redirect(base_url().'logme');
    }

    function otp()
    {
        //if(!$this->_check_session($this->input->post('session_for_safe'))) {
          //  die($this->lang->line('error_user_pass'));
        //}
        if($this->input->post('otp') && $this->member_model->otp_check($this->input->post('otp'))) {
            echo 1;
            exit;
        }
        die($this->lang->line('error_user_pass'));
    }

    function check()
    {

        if($this->client_model->login($this->input->post('username'), $this->input->post('password'))) {
            redirect(base_url().'dashboard', 'refresh');
        } else {
            $this->session->set_flashdata('message', 'Login Failed!');
            redirect(base_url().'logme');
        }

        die($this->lang->line('error_user_pass'));
    }
}
