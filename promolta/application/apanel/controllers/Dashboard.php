<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
$this->load->model('client_model');
		$valid = $this->client_model->isLogged();
		if($valid == false) {

			redirect(base_url().'logme/', 'refresh');
		}
	}

	public function index() {


		$this->load->model('profile_model');
		$this->load->model('package_model');
		$this->load->model('feedback_model');

		$data = array();
		$data['total_feedback'] 	= $this->feedback_model->getTotalFeedbacks('');;
		$data['total_profiles'] 	= $this->profile_model->getTotalProfiles($acnull = 2);
		$data['total_acprofiles'] = $this->profile_model->getActiveProfiles();
    $data['total_packages'] 	= $this->package_model->getTotalPackages('');

		$data['total_men'] 	= $this->profile_model->getTotalProfiles($acnull = 2, array('gender'=>0));
		$data['total_women'] 	= $this->profile_model->getTotalProfiles($acnull = 2, array('gender'=>1));

		$data['base_url'] = str_replace('apanel.php','',base_url()).'application/apanel/views/';
		$data['url'] = base_url();
    $data['user_id'] = $this->client_model->get_id();
		$data['body_content'] = $this->load->view('dashboard', $data, true);

		$this->load->view('template', $data);
	}




    public function test() {

/*
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_DEFAULT_PAPER_SIZE", "a4");
define("DOMPDF_UNICODE_ENABLED", true);
$this->load->library("Dompdf");
$dompdf = new Dompdf\Dompdf();
 $dompdf->loadHtml($html);
$dompdf->setPaper('A4', 'landscape');
$dompdf->render();
$dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
exit;
*/

	$stylesheet = ' table {font-family:"ta" ;FONT-SIZE:8px} .lfh {font-family:latha;FONT-SIZE:16px}.lf {font-family:latha;FONT-SIZE:8px}.ta{font-family:"ta"}';
    $html = '

<table border="1" cellspacing="0" cellpadding="0" width="100%">
   <tr>
      <td align="left" class="lf"> அலைபேசி  : 899798 </td>
      <td align="center" width="35%" class="lf"> ஸ்ரீ  ராமஜெயம் </td>
      <td align="right" width="30%" class="lf"> வேலை நேரம்  : <span class="ta"> 10am to 12 pm </span> </td>
   </tr>
   <tr>
      <td colspan="3" align="center" class="lfh"> கவர நாயுடு மஹாஜனசங்கம் திருமணத் தகவல் மையம்  </td>
   </tr>
   <tr>
      <td align="left" class="lf"> பிரதி செவ்வாய் விடுமுறை </td>
      <td align="center" class="lf"> (நாயடுயினத்தாருக்கு மட்டும்) </td>
      <td align="left" class="lf"> நாள்  : 12:23:232 </td>
   </tr>
   <tr>
      <td colspan="3" align="center" class="lf"> 170 சுப்ரமணியம் ரோடு, <span class="ta">R.S</span> புரம், கோயமுத்துர் - 641 002   </td>
   </tr>
   <tr>
      <td align="left" class="lf"> பதிவு தேதி  : 12:23:232 </td>
      <td align="center" class="lf"> ஜனன ஜாதகம் </td>
      <td align="left" class="lf"> மறு  பதிவு : 09/12/2045 </td>
   </tr>
   <tr>
      <td colspan="3" align="center">
         <table border=1 width="100%">
            <tr>
               <td width="50%">
                  <table border=1 width="100%">
                     <tr>
                        <td align="left" class="lf">கணினி  எண்</td>
                        <td colspan="3" align="left"> adsf </td>
                     </tr>
                     <tr>
                        <td align="left" class="lf">பெயர்</td>
                        <td colspan="3" align="left"> adsf </td>
                     </tr>
                     <tr>
                        <td align="left" class="lf">பிறந்த தேதி </td>
                        <td align="left"> adsf </td>
                        <td align="right" class="lf">பிறந்த நேரம்</td>
                        <td align="left" class="lf">  adsf velli கிழமை</td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">பிறந்த ஊர்</td>
                        <td colspan="3" align="left"> thirucchur </td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">உயரம் </td>
                        <td align="left">6</td>
                        <td class="lf" align="right"> நிறம் </td>
                        <td  align="left">  sivappu </td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">இனம்  </td>
                        <td  align="left">kavara</td>
                        <td class="lf" align="right"> கோத்திரம் </td>
                        <td align="left">  bala </td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">ஜென்ம நட்சத்திரம் </td>
                        <td align="left">pooram</td>
                        <td class="lf" align="right"> பாதம் </td>
                        <td align="left">  2 </td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">லக்னம்</td>
                        <td align="left">kumbamm</td>
                        <td class="lf" align="right"> ராசி </td>
                        <td  align="left" >  சிம்மம் </td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">படிப்பு</td>
                        <td colspan="3" align="left">kumbamm</td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">உத்யோகம்</td>
                        <td colspan="3" align="left">veilai</td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">விவரம்</td>
                        <td colspan="3" align="left"> otehr dtails</td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">மத வருமானம்</td>
                        <td colspan="3" align="left"> otehr dtails</td>
                     </tr>
                     <tr>
                        <td align="left"><span  class="lf">வருடம்</span> : sad </td>
                        <td colspan="2" align="center"> <span>மதம்  </span>: karthigai</td>
                        <td align="left"> நாள் : 13 </td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">சொத்து விபரம் </td>
                        <td colspan="3" align="left">matham : karthigai</td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">எதிர்பார்ப்பு </td>
                        <td colspan="3" align="left">matham : karthigai</td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">தகப்பனார் பெயர் </td>
                        <td colspan="3" align="left">matham : karthigai</td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">தொழில் </td>
                        <td colspan="3" align="left">matham : karthigai</td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">தயார் பெயர் </td>
                        <td colspan="3" align="left">matham : karthigai</td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">தொழில் </td>
                        <td colspan="3" align="left">matham : karthigai</td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">உடன் பிரதோர் </td>
                        <td  class="lf">ஆண் : 1</td>
                        <td ></td>
                        <td  class="lf">பெண் : 2 </td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">திருமணமானமானவர் </td>
                        <td class="lf">ஆண் : 1</td>
                        <td ></td>
                        <td class="lf">பெண் : 2 </td>
                     </tr>
                     <tr>
                        <td class="lf" align="left">சூரிய உதயாகி நாழிகை </td>
                        <td >An : 1</td>
                        <td class="lf" align="left">விழிநாழிகை</td>
                        <td >pand : 2 </td>
                     </tr>
                  </table>
               </td>
               <td rowspan="10" colspan="2" valign="top">
                  <img src="https://www.w3schools.com/css/paris.jpg" width="350px">
               </td>
            </tr>
         </table>
      </td>
   </tr>
   <tr>
      <td colspan="3"  >
         <table width="100%">
            <tr>
               <td>
                  <table border="1" width="100%">
                     <tr>
                        <td height="60px">1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                     </tr>
                     <tr>
                        <td height="60px">5</td>
                        <td rowspan="2" colspan="2" align="center" height="120px">
                           <div style="vertica-align: middle;padding-top: 10px;">
                              Rasi
                           </div>
                        </td>
                        <td>6</td>
                     </tr>
                     <tr>
                        <td height="60px">7</td>
                        <td>8</td>
                     </tr>
                     <tr>
                        <td height="60px">1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                     </tr>
                  </table>
               </td>
               <td>
                  <table border="1" width="100%">
                     <tr>
                        <td height="60px">1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                     </tr>
                     <tr>
                        <td height="60px">5</td>
                        <td rowspan="2" colspan="2" align="center" height="120px">
                           <div style="vertica-align: middle;padding-top: 10px;">
                              Rasi
                           </div>
                        </td>
                        <td>6</td>
                     </tr>
                     <tr>
                        <td height="60px">7</td>
                        <td>8</td>
                     </tr>
                     <tr>
                        <td height="60px">1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                     </tr>
                  </table>
               </td>
               <td width="10%">
                  asdfa
                  asdf
                  asdfasd
               </td>
            </tr>
         </table>
      </td>
   </tr>
   <tr>
      <td colspan ="3" align="center" class="lf">செவ்வாய் தோசம் : no
         ராகு கேது தோசம் : no
         குடும்ப செல்வநிலை : naduthara kudumbam
      </td>
   </tr>
</table>';


$this->load->library("Mpdf");
$mpdf = new Mpdf('','A4',9,'latha',2,2,2,0,0,0);
$mpdf->WriteHTML($stylesheet,1);
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;


//ind_ta_1_001
$this->load->library("Pdf");
//ob_start();
$pdf = new Pdf();
/*$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$fontname = TCPDF_FONTS::addTTFfont('OT-Fonts/latha.ttf', 'TrueTypeUnicode', '', 32);
var_dump(file_exists('/Mydrive/htdocs/promolta/OT-Fonts/latha.ttf'));
//TMOTKDN_Ship.TTF, TMOTCHN_Ship, TMOTCHI_Ship,tmotchi_ship
var_dump($fontname);
exit;*/

$pdf->AddPage();
//$pdf->SetFont('tmotchi_ship','B',12);
//$pdf->SetFont('tmotkdn_ship','B',10);
//$pdf->SetFont('tmotchn_ship','B',10);
$pdf->SetFont('latha','B',10);
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('example_001.pdf', 'I');
ob_end_flush();
exit;


        //$this->load->helper('pdf_helper');

        $this->load->library("Pdf");
        //ob_start();
        $pdf = new Pdf();

/*$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$fontname = TCPDF_FONTS::addTTFfont('OT-Fonts/latha.ttf', 'TrueTypeUnicode', '', 32);
var_dump(file_exists('/Mydrive/htdocs/promolta/OT-Fonts/latha.ttf'));
//TMOTKDN_Ship.TTF, TMOTCHN_Ship, TMOTCHI_Ship,tmotchi_ship
var_dump($fontname);
exit;*/

    $pdf->AddPage();
    //$pdf->SetFont('tmotchi_ship','B',12);
    //$pdf->SetFont('tmotkdn_ship','B',10);
		//$pdf->SetFont('tmotchn_ship','B',10);
		$pdf->SetFont('latha','',10);
		$pdf->writeHTML($html, true, false, true, false, '');

		    $pdf->Output('example_001.pdf', 'I');
		             ob_end_flush();


    /*<h2>HTML TABLE:</h2>
    <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <td align="left"> phone : 899798 </td>
     <td align="center" width="35%"> rama jeyam </td>
     <td align="right" width="30%"> velai neram : 10am to 12 pm </td>
    </tr>

    <tr>
     <td colspan="4" align="center"> கவர மகாஜன சங்க திமண தகவல் ைமயம் </td>
    </tr>
    <tr>
     <td align="left"> sevvai holiday </td>
     <td align="center"> (oly for naidu) </td>
     <td align="left"> day  : 12:23:232 </td>
    </tr>

    <tr>
     <td colspan="4" align="center"> selva vinayagar coil stree, address goes here..  </td>
    </tr>

    <tr>
     <td align="left"> pathivu day  : 12:23:232 </td>
     <td align="center"> janana jagatham </td>
     <td align="left"> maru pathivu : 09/12/2045 </td>
    </tr>

    <tr>
     <td colspan="4" align="center">
                <table border=1 width="100%">
             <tr>
             <td width="50%">
                <table border=1 width="100%">
                 <tr>
                 <td>computer no </td>
                 <td colspan="3"> adsf </td>
                 </tr>
                 <tr>
                 <td>birth date </td>
                 <td> adsf </td>
                   <td> birth time </td>
                   <td> adsf velli kilamai</td>
                 </tr>

                 <tr>
                 <td>birth oor </td>
                 <td colspan="3"> thirucchur </td>
                 </tr>

                                  <tr>
                 <td>height </td>
                 <td>6</td>
                   <td> color </td>
                   <td>  sivappu </td>
                 </tr>

                  <tr>
                 <td>inam </td>
                 <td>kavara</td>
                   <td> kothram </td>
                   <td>  bala </td>
                 </tr>

                                   <tr>
                 <td>jenma natchatram </td>
                 <td>pooram</td>
                   <td> patham </td>
                   <td>  2 </td>
                 </tr>

                          <tr>
                 <td>laknam</td>
                 <td>kumbamm</td>
                   <td> rasi </td>
                   <td>  simman </td>
                 </tr>


                          <tr>
                 <td>padippu</td>
                 <td colspan="3">kumbamm</td>
                 </tr>

                          <tr>
                 <td>uthyogam</td>
                 <td colspan="3">veilai</td>
                 </tr>

                          <tr>
                 <td>details</td>
                 <td colspan="3"> otehr dtails</td>
                 </tr>


                          <tr>
                 <td>matha varumananm</td>
                 <td colspan="3"> otehr dtails</td>
                 </tr>



                          <tr>
                 <td>varudam : sad </td>
                 <td colspan="2">matham : karthigai</td>
                   <td> naal : 13 </td>
                 </tr>



                          <tr>
                 <td>sothu patriya vivaram </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>


                          <tr>
                 <td>Ethirparpu </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>


                          <tr>
                 <td>dad name </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>



                          <tr>
                 <td>dad work </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>


                          <tr>
                 <td>mom name </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>


                          <tr>
                 <td>mom work </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>

                   <tr>
                 <td>udan piranthor</td>
                 <td >An : 1</td>
              <td ></td>
              <td >pand : 2 </td>
                 </tr>

                                    <tr>
                 <td>married </td>
                 <td >An : 1</td>
              <td ></td>
              <td >pand : 2 </td>
                 </tr>

                        <tr>
                 <td>suriyan nzhigai </td>
                 <td >An : 1</td>
              <td >vinazhigai</td>
              <td >pand : 2 </td>
                 </tr>


                </table>
            </td>
            <td rowspan="10" colspan="2" valign="top">
            <img src="https://www.w3schools.com/css/paris.jpg"></td>
            </tr>
            </table>
     </td>
    </tr>
    <tr>
    <td colspan="3"  width="100%">
         <table width="100%"><tr>
         <td><table border="1" width="100%">
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
            <tr>
                <td height="60px">5</td>
                <td rowspan="2" colspan="2" align="center" height="120px">
                <div style="vertica-align: middle;padding-top: 10px;">
                    Rasi
                </div>
                </td>
                <td>6</td>
            </tr>
            <tr>
                <td height="60px">7</td>
                <td>8</td>
            </tr>
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
        </table></td>
        <td>
        <table border="1" width="100%">
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
            <tr>
                <td height="60px">5</td>
                <td rowspan="2" colspan="2" align="center" height="120px">
                <div style="vertica-align: middle;padding-top: 10px;">
                    Rasi
                </div>
                </td>
                <td>6</td>
            </tr>
            <tr>
                <td height="60px">7</td>
                <td>8</td>
            </tr>
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
        </table></td>
        <td width="10%">
        asdfa
        asdf
        asdfasd
        </td>
         <tr></table>
    </td>
    </tr>
    <tr>
    <td colspan ="3">sevvai thosam : no
 ragu kethu : no
    kudumba sulnila : naduthara kudumbam</td>
    </tr>



    <!--

    <tr>
     <td colspan="4" align="center"> 170 , ப்ரமணியம் ேரா, R.S ரம் , ேகாயத்ர் - 641 00 </td>
    </tr>
    <tr>
     <td colspan="4" align="center"> க்ம் பிறப்பியம் விபரங்கள் </td>
    </tr>
    <tr>
     <td align="center"> பதி எண் : 123் </td>
     <td align="center"> ேததி் : 21/08/2017 </td>
     <td align="center"> Sevvai / ெசவ்வாய்் : இல்ைல </td>
     <td align="center"> Raagu / ேக :் : இல்ைல </td>
    </tr>

   <tr>
     <td align="center"> ெபயர் </td>
     <td align="center"> :E.மேனாஜ் அரவிந்த் </td>
     <td align="center"> கல்வி ததி </td>
     <td align="center"> B.Tech MBA </td>
    </tr>

<tr>
     <td align="center"> ெபயர் </td>
     <td align="center"> :E.மேனாஜ் அரவிந்த் </td>
     <td align="center"> கல்வி ததி </td>
     <td align="center"> B.Tech MBA </td>
    </tr>

<tr>
     <td align="center"> ேவைல </td>
     <td align="center"> :Philiphs Marketing Rs.40,000/- </td>
     <td align="center">  </td>
     <td align="center"> </td>
    </tr>

   <tr>
     <td align="center"> பதி எண் : 123் </td>
     <td align="center"> ேததி் : 21/08/2017 </td>
     <td align="center"> Sevvai / ெசவ்வாய்் : இல்ைல </td>
     <td align="center"> Raagu / ேக :் : இல்ைல </td>
    </tr>
    <tr>
     <td colspan="4" align="center" height="50px"> ் </td></tr>
    <tr>
     <td colspan="4" align="center"> <span  style="font-weight: normal;">
தமிழ்  பிரேமாத ஆண்  1990 மாதம்  .............................. ேததி  ........................ தினம்
Udhayathi நாைழிகக்   ................................................ ேநரம்  12.14 am Suba Jananam
பிறந்த நட்சத்திரம்  Maham ராசி  Simmam Lagnam  Kadagam
மகா Thisai இப்  kethu thisai ஆண்  4 மாதம்  11-27  நாட்களில் </span></td></tr>
<tr>
 <td colspan="2">
        <table border="1">
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
            <tr>
                <td height="60px">5</td>
                <td rowspan="2" colspan="2" align="center" height="120px">
                <div style="vertica-align: middle;padding-top: 10px;">
                    Rasi
                </div>
                </td>
                <td>6</td>
            </tr>
            <tr>
                <td height="60px">7</td>
                <td>8</td>
            </tr>
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
        </table>
        </td>
 <td colspan="2">

<table border="1">
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
            <tr>
                <td height="60px">5</td>
                <td rowspan="2" colspan="2">Rasi</td>
                <td>6</td>
            </tr>
            <tr>
                <td height="60px">7</td>
                <td>8</td>
            </tr>
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
        </table>


 </td>
</tr>-->



நாயுடு மஹாஜனசங்கம் திருமணத் தகவல் மையம்
ஸ்ரீ  ராமஜெயம்
வேலை நேரம்
அலைபேசி
பிரதி செவ்வாய் விடுமுறை
நாய்டுயினத்தாருக்கு மட்டும்
நாள்
கோவை
பதிவு தேதி
ஜனன ஜாதகம் , மறு  பதிவு , கணினி  எண் , பெயர் , பிறந்த தேதி , பிறந்த நேரம் , கிழமை , பிறந்த ஊர் , உயரம் , நிறம் , இனம் கோத்திரம் , ஜென்ம நட்சத்திரம் , பாதம் , லக்னம் , ராசி , சிம்மம் , கும்பம் , படிப்பு , உத்யோகம் , விவரம் , மத வருமானம் , வருடம் , மதம் , நாள், சொத்து விபரம் , எதிர்பார்ப்பு , தகப்பனார் பெயர் , தொழில் , தயார் பெயர் , தொழில் , உடன் பிரதோர் , ஆண் பெண் , திருமணமாகாதவர் , திருமணமானமானவர் , சூரிய  உதயாகி நாழிகை , விழிநாழிகை ,
செவ்வாய் தோசம் , கிடையாது , ராகு கேது தோசம் , குடும்ப செல்வநிலை  நடுத்தர குடும்பம்


</table>*/

    $html = '

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Untitled Document</title>

</head>
<body>
<table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <td align="left"> phone : 899798 </td>
     <td align="center" width="35%"> rama jeyam </td>
     <td align="right" width="30%"> velai neram : 10am to 12 pm </td>
    </tr>

    <tr>
     <td colspan="3" align="center"> கவர நாயுடு மஹாஜனசங்கம் திருமணத் தகவல் மையம்  </td>
    </tr>
    <tr>
     <td align="left"> sevvai holiday </td>
     <td align="center"> (oly for naidu) </td>
     <td align="left"> day  : 12:23:232 </td>
    </tr>

    <tr>
     <td colspan="3" align="center"> selva vinayagar coil stree, address goes here..  </td>
    </tr>

    <tr>
     <td align="left"> pathivu day  : 12:23:232 </td>
     <td align="center"> janana jagatham </td>
     <td align="left"> maru pathivu : 09/12/2045 </td>
    </tr>

    <tr>
     <td colspan="3" align="center">
                <table border=1 width="100%">
             <tr>
             <td width="50%">
                <table border=1 width="100%">
                 <tr>
                    <td align="left">computer no </td>
                    <td colspan="3" align="left"> adsf </td>
                 </tr>
                 <tr>
                    <td align="left">birth date </td>
                    <td align="left"> adsf </td>
                   <td align="right"> birth time </td>
                   <td align="left">  adsf velli kilamai</td>
                 </tr>

                 <tr>
                 <td>birth oor </td>
                 <td colspan="3"> thirucchur </td>
                 </tr>

                                  <tr>
                 <td>height </td>
                 <td>6</td>
                   <td> color </td>
                   <td>  sivappu </td>
                 </tr>

                  <tr>
                 <td>inam </td>
                 <td>kavara</td>
                   <td> kothram </td>
                   <td>  bala </td>
                 </tr>

                                   <tr>
                 <td>jenma natchatram </td>
                 <td>pooram</td>
                   <td> patham </td>
                   <td>  2 </td>
                 </tr>

                          <tr>
                 <td>laknam</td>
                 <td>kumbamm</td>
                   <td> rasi </td>
                   <td>  simman </td>
                 </tr>


                          <tr>
                 <td>padippu</td>
                 <td colspan="3">kumbamm</td>
                 </tr>

                          <tr>
                 <td>uthyogam</td>
                 <td colspan="3">veilai</td>
                 </tr>

                          <tr>
                 <td>details</td>
                 <td colspan="3"> otehr dtails</td>
                 </tr>


                          <tr>
                 <td>matha varumananm</td>
                 <td colspan="3"> otehr dtails</td>
                 </tr>



                          <tr>
                 <td>varudam : sad </td>
                 <td colspan="2">matham : karthigai</td>
                   <td> naal : 13 </td>
                 </tr>



                          <tr>
                 <td>sothu patriya vivaram </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>


                          <tr>
                 <td>Ethirparpu </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>


                          <tr>
                 <td>dad name </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>



                          <tr>
                 <td>dad work </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>


                          <tr>
                 <td>mom name </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>


                          <tr>
                 <td>mom work </td>
                 <td colspan="3">matham : karthigai</td>
                 </tr>

                   <tr>
                 <td>udan piranthor</td>
                 <td >An : 1</td>
              <td ></td>
              <td >pand : 2 </td>
                 </tr>

                                    <tr>
                 <td>married </td>
                 <td >An : 1</td>
              <td ></td>
              <td >pand : 2 </td>
                 </tr>

                        <tr>
                 <td>suriyan nzhigai </td>
                 <td >An : 1</td>
              <td >vinazhigai</td>
              <td >pand : 2 </td>
                 </tr>


                </table>
            </td>
            <td rowspan="10" colspan="2" valign="top">
            <img src="https://www.w3schools.com/css/paris.jpg"></td>
            </tr>
            </table>
     </td>
    </tr>
    <tr>
    <td colspan="3"  width="100%">
         <table width="100%"><tr>
         <td><table border="1" width="100%">
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
            <tr>
                <td height="60px">5</td>
                <td rowspan="2" colspan="2" align="center" height="120px">
                <div style="vertica-align: middle;padding-top: 10px;">
                    Rasi
                </div>
                </td>
                <td>6</td>
            </tr>
            <tr>
                <td height="60px">7</td>
                <td>8</td>
            </tr>
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
        </table></td>
        <td>
        <table border="1" width="100%">
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
            <tr>
                <td height="60px">5</td>
                <td rowspan="2" colspan="2" align="center" height="120px">
                <div style="vertica-align: middle;padding-top: 10px;">
                    Rasi
                </div>
                </td>
                <td>6</td>
            </tr>
            <tr>
                <td height="60px">7</td>
                <td>8</td>
            </tr>
            <tr>
                <td height="60px">1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
        </table></td>
        <td width="10%">
        asdfa
        asdf
        asdfasd
        </td>
         </tr></table>
    </td>
    </tr>
    <tr>
    <td colspan ="3">sevvai thosam : no
 ragu kethu : no
    kudumba sulnila : naduthara kudumbam</td>
    </tr></table>
		</body></html>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

    $pdf->Output('example_001.pdf', 'I');
             ob_end_flush();


exit;


        $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "PDF Report";
$obj_pdf->SetTitle($title);
$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
    // we can have any view part here like HTML, PHP etc
    $content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('output.pdf', 'I');
    exit;

        $this->load->library('pdf');
  	$this->pdf->load_view('mypdf');
  	$this->pdf->render();
  	$this->pdf->stream("welcome.pdf");
    }
}
