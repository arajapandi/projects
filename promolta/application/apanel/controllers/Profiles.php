<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profiles extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('client_model');
        $this->load->model('profile_model');
        $this->load->library('form_validation');
        $valid = $this->client_model->isLogged();
        if ($valid == false) {
            redirect(base_url() . 'logme/', 'refresh');
        }
    }

    public function index() {

        $this->load->helper('url');
        $sessionData = $this->client_model->get_id();

        $data = array();

        $page = intval(trim($this->input->get('page'), '/'));

        $search_array = array();
        $search     = $this->input->get('search',true);
        if(!empty($search)) {
          //$this->form_validation->set_rules('name', 'Name', 'required|trim|xss_clean');
          $search_array['search'] = $search;
        }
        $age_from   = intval($this->input->get('age_from'));
        if(!empty($age_from)) {
          $search_array['age_from'] = $age_from;
        }
        $age_to     = intval($this->input->get('age_to'));
        if(!empty($age_to)) {
          $search_array['age_to'] = $age_to;
        }
        $gender     = $this->input->get('gender');
        if(isset($gender) && $gender !='' && $gender != null) {
          $search_array['gender'] = $gender;
        }
        $active     = $this->input->get('active');
        $search_array['active'] = 2;
        if(isset($active) && $active !='' && $active != null) {
          $search_array['active'] = $active;
        }

        $profile_id   = intval($this->input->get('profile_id'));
        if(!empty($profile_id)) {
          $search_array['profile_id'] = $profile_id;
        }

        $per_page = 15;
        $this->load->library('pagination');
        $config['total_rows'] = $this->profile_model->getTotalProfiles($search_array['active'], $search_array);
        $config['per_page'] = $per_page;
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['cur_page'] = $page;
        $config['base_url'] = base_url() . 'profiles?page=';

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        unset($_GET['page']);
         $config['suffix'] = '&'.http_build_query($_GET,"","&");
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();


        $data['url'] = base_url();
        $data['site_url'] = str_replace('apanel.php', '',base_url());
        $data['profiles'] = $this->profile_model->getAccounts($page, $per_page, $search_array['active'], $search_array);
        $data['search_array'] = $search_array;
        $data['message'] = $this->session->flashdata('message');
        $data['body_content'] = $this->load->view('profiles', $data, true);

        $data['base_url'] = str_replace('apanel.php', '', base_url()) . 'application/apanel/views/';
        $data['user_id'] = $this->client_model->get_id();

        $this->load->view('template', $data);
    }

    public function sendpass() {
        $id     = (int)$this->input->get('id',true);
        if(!empty($id)) {
          $this->client_model->send_pass_mail($id);
          $this->session->set_flashdata('message', "Password Email has been sent to the user!");
        }
        redirect(base_url() . 'profiles', 'refresh');
    }


   public function pdf() {


      	$lang   = $this->input->get('lang',true);

	      $this->lang->load('common', empty($lang) ? 'english' : $lang, $return = FALSE, $add_suffix = TRUE, $alt_path = __DIR__.'/../../');
        $id     = (int)$this->input->get('id',true);
        if(empty($id)) {
          die('foo - No id specified');
        }
        $fvalue = $this->profile_model->getProfile($id);
      	$ftamil = $this->profile_model->getLang($id, $lang == 'english' ? 1 : 2);
        $fvalue['laknam'] = empty($fvalue['laknam']) ? 0 : $fvalue['laknam'];
        $fvalue['thisai'] = empty($fvalue['thisai']) ? 0 : $fvalue['thisai'];
        $fvalue['annual_income'] = empty($fvalue['annual_income']) ? 0 : $fvalue['annual_income'];
        $fvalue['nakshatram'] = empty($fvalue['nakshatram']) ? 0 : $fvalue['nakshatram'];
        $fvalue['rasi'] = empty($fvalue['rasi']) ? 0 : $fvalue['rasi'];
        $fvalue['jathagam'] = empty($fvalue['jathagam']) ? 0 : $fvalue['jathagam'];


      if($lang == 'english') {

        $Lagnam = array(0=>"",1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
        $thisaiiruppu = array(0=>"",1=>"Guru thisai","Sani thisai","kethu thisai","Sukura thisai","Suriya thisai","Chandra thisai","Sewwai thisai","Ragu thisai","Budhan thisai");
        $rasi = array(0=>"",1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
        $nakshatram = array(0=>"",1=>"Aswini","Bharani","Karthikai",
  "Rohini","Mirugasiridam","Thiruvathirai","Punarpusam","Pusam","Ayilyam","Maham","Puram","Uthiram","Atham",
  "Chithirai","Swathiv","Visakam","Anusam","Ketai","Mulam", "Puradam","Uthiradam","Thiruvonam","Avitam",
  "Sathayam","Puratathi","Uthiratathi","Revathi");
      } else {

        $Lagnam = array(0=>"",1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $thisaiiruppu = array(0=>"",1=>"குருதிசை","சனிதிசை","கேதுதிசை","சுக்ரதிசை","சூரியதிசை","சந்திரதிசை","செவ்வாய்திசை","ராகுதிசை","புதன்திசை");
        $rasi = array(0=>"",1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $nakshatram = array(0=>"",1=>"அசுவினி","பரணி","கிருத்திகை",
        "ரோஹிணி","மிருகசீரிஷம்","திருவாதிரை","புனர்பூசம்","பூசம்","ஆயில்யம்","மகம்","பூரம்","உத்திரம்","ஹஸ்தம்",
        "சித்திரை","ஸ்வாதி","விசாகம்","அனுசம்","கேட்டை","மூலம்", "புராடம்","உத்ராடம்","திருவோணம்","அவிட்டம்",
        "சதயம்","புரட்டாதி","உத்திரட்டாதி","ரேவதி");
      }

      $aninco = $this->lang->line('anincoar');
      $rasidata = $this->lang->line('rasidataar');
      $laknamdata = $this->lang->line('laknamdataar');
      $jathagam = $this->lang->line('jathagamar');

        $jatsquare = array('rasi'=>array(),'amsam'=>array());
        if(!empty($fvalue['jatsquare'])) {
            $jatsquare = json_decode($fvalue['jatsquare'],true);
        }
/*
        $Lagnam = array(1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $thisaiiruppu = array(1=>"குருதிசை","சனிதிசை","கேதுதிசை","சுக்ரதிசை","சூரியதிசை","சந்திரதிசை","செவ்வாய்திசை","ராகுதிசை","புதன்திசை");
        $rasi = array(1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $nakshatram = array(1=>"அசுவினி","பரணி","கிருத்திகை",
        "ரோஹிணி","மிருகசீரிஷம்","திருவாதிரை","புனர்பூசம்","பூசம்","ஆயில்யம்","மகம்","பூரம்","உத்திரம்","ஹஸ்தம்",
        "சித்திரை","ஸ்வாதி","விசாகம்","அனுசம்","கேட்டை","மூலம்", "புராடம்","உத்ராடம்","திருவோணம்","அவிட்டம்",
        "சதயம்","புரட்டாதி","உத்திரட்டாதி","ரேவதி");*/

        $rasi_details = array();
        $amsam_details = array();
        if(!empty($jatsquare['rasi'])) {
          foreach((array)$jatsquare['rasi'] as $val=>$key) {
             $rasi_details[$key] = isset($rasi_details[$key]) ? $rasi_details[$key].' <br/> '.$rasidata[$val] : $rasidata[$val];
          }
        }

        if(!empty($jatsquare['amsam'])) {
          foreach((array)$jatsquare['amsam'] as $val=>$key) {
             $amsam_details[$key] = isset($amsam_details[$key]) ? $amsam_details[$key].' <br/>  '.$laknamdata[$val] : $laknamdata[$val];
          }
        }

        $url = '';
        if(!empty($fvalue['image'])) {
          $url = str_replace('apanel.php', '',base_url()).'uploads/'.$fvalue['image'];
        };

        $stylesheet = ' table {font-family:"arialuni" ;FONT-SIZE:11px}
        .lfh {font-family:arialuni;FONT-SIZE:17px}
        .lf {font-family:arialuni;FONT-SIZE:12px}
        .lfs {font-family:arialuni;FONT-SIZE:11px}
        .latha{font-family:"arialuni";FONT-SIZE:17px}';

        $html = '
        <table border="0" cellspacing="2" cellpadding="2" width="100%">
        <tr>
           <td align="right" class="lf" nowrap> '.$this->lang->line('telephoneno').': 0422 2471928 - '.$this->lang->line('phonenew').': 94425 71928 </td>
           <td align="center" width="25%" class="lf"> '.$this->lang->line('ramajeyam').' </td>
           <td align="right" width="35%" class="lf"> '.$this->lang->line('workinghrs').'  : <span class="lf"> 10.00 AM to 5.00 PM </span> </td>
        </tr>

                   <tr>
                      <td colspan="4" align="center" class="lf"> <hr>   </td>
                   </tr>
        <tr>
           <td colspan="3" align="center" class="latha"> '.$this->lang->line('pdftitle').' </td>
        </tr>
        <tr>

           <td colspan="3">
           <table width="100%"><tr>
           <td align="left" class="lf" width="25%"> '.$this->lang->line('sevvaiholiday').' </td>
           <td align="center" class="lf"> '.$this->lang->line('addressfull').'  </td>
           <td align="right" class="lf" width="25%"> '.$this->lang->line('date').'  : '.date('d/m/Y').' </td>
           </tr></table>
           </td>

        </tr>


        <tr>
           <td colspan="4" align="center" class="lf"> <hr>   </td>
        </tr>
        <tr>
           <td align="right" class="lf"> '.$this->lang->line('regno').' : '.$fvalue['id'].' </td>
           <td align="center" class="lf"> '.$this->lang->line('jananjathagam').' </td>
           <td align="left" class="lf"> '.$this->lang->line('regnodate').'  : '.date('d/m/Y',strtotime($fvalue['reg_date'])).' </td>
        </tr>
        <tr>
           <td colspan="4"><hr></td>
        </tr>
           <tr>
              <td colspan="3" align="center">
                 <table border="0" width="100%">
                    <tr>
                       <td width="80%" valign="top">
                          <table border="0" width="100%" cellspacing="2" cellpadding="5">
                             <tr>
                                <td align="left" class="lf">'.$this->lang->line('name').'</td>
                                <td colspan="3" align="left" class="lf">: '.$ftamil['name'].' </td>
                             </tr>
                             <tr>
                                <td align="left" width="25%" class="lf">'.$this->lang->line('birthdate').' </td>
                                <td align="left" width="25%" >: '.date('d/m/Y',strtotime($fvalue['dob'])).' </td>
                                <td class="lf" align="right" width="25%" >'.$this->lang->line('birthtime').' </td>
                                <td  align="left" width="25%" >:  '.$fvalue['dobtime'].' </td>

                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('telephoneno').'</td>
                                <td colspan="3" align="left" class="lf">:   '.$fvalue['phone'].'  </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('livingcity').'</td>
                                <td colspan="3" align="left" class="lf">: '.$ftamil['res_city'].' </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('Height').' </td>
                                <td align="left">: '.$ftamil['height'].' </td>
                                <td class="lf" align="right">'.$this->lang->line('Color').'  </td>
                                <td  align="left">:  '.$ftamil['color'].' </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('kulam').'    </td>
                                <td  align="left" class="lf" colspan="3">: '.$ftamil['kulam'].'</td>

                             </tr>
                             <tr>
                             <td class="lf" align="left">'.$this->lang->line('kulathievam').'   </td>
                             <td align="left" class="lf" colspan="3">: '.$ftamil['kulam_temple'].' </td>
                             </tr>

                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('sevvai').' </td>
                                <td align="left" class="lf">: '.$ftamil['sevvai'].'  </td>
                                <td class="lf" align="right" >'.$this->lang->line('ragukethu').'    </td>
                                <td  align="left" class="lf">:  '.$ftamil['ragukethu'].'  </td>
                             </tr>


                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('jenmanakshatra').' </td>
                                <td align="left" class="lf">: '.$nakshatram[$fvalue['nakshatram']].'  </td>
                                <td class="lf" align="right" >'.$this->lang->line('rasi').'  </td>
                                <td  align="left" class="lf">:  '.$rasi[$fvalue['rasi']].'  </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('laknam').'</td>
                                <td align="left" class="lf">: '.$Lagnam[$fvalue['laknam']].' </td>
                                <td class="lf" align="right">'.$this->lang->line('thisaiiruppu').' </td>
                                <td align="left" class="lf">: '.$thisaiiruppu[$fvalue['thisai']].' </td>

                             </tr>

                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('Education').'</td>
                                <td colspan="3" align="left">: '.$ftamil['education'].' </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('work').'</td>
                                <td colspan="3" align="left">: '.$ftamil['job'].' </td>
                             </tr>

                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('incometotal').'</td>
                                <td colspan="3" align="left" class="lf">: '.$ftamil['annual_income'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('Fathersn').' </td>
                                <td colspan="3" align="left" class="lf">:  '.$ftamil['father_name'].'  </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('Mothers').' </td>
                                <td colspan="3" align="left" class="lf">: '.$ftamil['mother_name'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('bromarried').' </td>
                                <td  class="lf" colspan="3" align="left">'.$this->lang->line('marriedalt').' : '.$fvalue['bromarried'].'
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; '.$this->lang->line('unmarriedalt').' : '.$fvalue['brounmarried'].' </td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('sismarried').' </td>
                                <td class="lf" colspan="3" align="left">'.$this->lang->line('marriedalt').' : '.$fvalue['sismarried'].' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  '.$this->lang->line('unmarriedalt').' : '.$fvalue['sisunmarried'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('birthinfo').'</td>
                                <td colspan="3" align="left" class="lf">:  '.$this->lang->line('tamil').' '.$ftamil['tamil_birth'].'
                                '.$this->lang->line('year').' '.$ftamil['tamil_year'].'
                                '.$this->lang->line('month').' '.$ftamil['tamil_month'].'
                                '.$this->lang->line('date').' '.$ftamil['tamil_date'].'
                                '.$this->lang->line('seconds').'   '.$ftamil['tamil_nazhigai'].'
                                '.$this->lang->line('time').' '.$ftamil['tamil_time'].'</td>
                             </tr>
                             <!--<tr>
                                <td class="lf" align="left">'.$this->lang->line('informations').' </td>
                                <td colspan="3" align="left">: '.$fvalue['information'].'</td>
                             </tr>-->
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('expectation').' </td>
                                <td colspan="3" align="left">: '.$fvalue['expectation'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('district').' </td>
                                <td colspan="3" align="left">: '.$fvalue['district'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('houseall').'</td>
                                <td align="left">: '.$fvalue['house'].'</td>
                                <td class="lf" align="right">'.$this->lang->line('land').'</td>
                                <td align="left">: '.$fvalue['land'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('vehicle').' </td>
                                <td align="left">: '.$fvalue['vehicle'].'</td>
                                <td class="lf" align="right">'.$this->lang->line('totalassetinfo').'  1</td>
                                <td align="left">: '.$fvalue['provalue1'].'</td>
                             </tr>
                             <tr>
                                <td class="lf" align="left">'.$this->lang->line('totalassetinfo').' 2</td>
                                <td align="left">: '.$fvalue['provalue2'].'</td>
                                <td class="lf" align="right">'.$this->lang->line('totalassetinfo').' 3</td>
                                <td align="left">: '.$fvalue['provalue3'].'</td>
                             </tr>

                             <tr>
                                <td class="lfs" align="left">'.$this->lang->line('totalasset').' </td>
                                <td colspan="3" align="left">: '.$fvalue['totalpro'].'</td>
                             </tr>
                          </table>
                       </td>
                       <td rowspan="10" colspan="2" valign="top">
                          <img src="'.$url.'" width="200px">
                       </td>
                    </tr>
                 </table>
              </td>
           </tr>
           <tr>
              <td colspan="3"  >
                 <table width="100%">
                    <tr>
                       <td>
                          <table border="1" cellspacing="0" cellpadding="2" width="360px">
                             <tr>
                                <td height="60px" width="25%"> '.(isset($rasi_details[12]) ? $rasi_details[12] : '&nbsp;' ).'  </td>
                                <td width="25%"> '.(isset($rasi_details[1]) ? $rasi_details[1] : '&nbsp;' ).' </td>
                                <td width="25%"> '.(isset($rasi_details[2]) ? $rasi_details[2] : '&nbsp;' ).' </td>
                                <td width="25%"> '.(isset($rasi_details[3]) ? $rasi_details[3] : '&nbsp;' ).' </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($rasi_details[11]) ? $rasi_details[11] : '&nbsp;' ).' </td>
                                <td rowspan="2" colspan="2" align="center" height="120px" width="50%">
                                   <div style="vertica-align: middle;padding-top: 10px;font-size:30px;font-color:#cdcccc;text-shadow: 2px 2px #cdcdcd;">
                                      '.$this->lang->line('rasi').'
                                   </div>
                                </td>
                                <td width="25%"> '.(isset($rasi_details[4]) ? $rasi_details[4] : '&nbsp;' ).' </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($rasi_details[10]) ? $rasi_details[10] : '&nbsp;' ).' </td>
                                <td width="25%"> '.(isset($rasi_details[5]) ? $rasi_details[5] : '&nbsp;' ).' </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($rasi_details[9]) ? $rasi_details[9] : '&nbsp;' ).' </td>
                                <td width="25%"> '.(isset($rasi_details[8]) ? $rasi_details[8] : '&nbsp;' ).' </td>
                                <td width="25%">'.(isset($rasi_details[7]) ? $rasi_details[7] : '&nbsp;' ).' </td>
                                <td width="25%"> '.(isset($rasi_details[6]) ? $rasi_details[6] : '&nbsp;' ).' </td>
                             </tr>
                          </table>
                       </td>

                       <td width="10%" class="lf">

                       </td>
                       <td>
                          <table border="1" cellspacing="0" cellpadding="2"  width="360px">
                             <tr>
                                <td height="60px" width="25%"> '.(isset($amsam_details[12]) ? $amsam_details[12] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[1]) ? $amsam_details[1] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[2]) ? $amsam_details[2] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[3]) ? $amsam_details[3] : '' ).' &nbsp; </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($amsam_details[11]) ? $amsam_details[11] : '' ).' &nbsp; </td>
                                <td rowspan="2" colspan="2" align="center" height="120px" width="50%">
                                   <div style="vertica-align: middle;padding-top: 10px;font-size:30px;font-color:#cdcccc;text-shadow: 2px 2px #cdcdcd;">
                                      '.$this->lang->line('amsam').'
                                   </div>
                                </td>
                                <td width="25%">    '.(isset($amsam_details[4]) ? $amsam_details[4] : '' ).' &nbsp; </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($amsam_details[10]) ? $amsam_details[10] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[5]) ? $amsam_details[5] : '' ).' &nbsp; </td>
                             </tr>
                             <tr>
                                <td height="60px" width="25%"> '.(isset($amsam_details[9]) ? $amsam_details[9] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[8]) ? $amsam_details[8] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[7]) ? $amsam_details[7] : '' ).' &nbsp; </td>
                                <td width="25%"> '.(isset($amsam_details[6]) ? $amsam_details[6] : '' ).' &nbsp; </td>
                             </tr>
                          </table>
                       </td>
                    </tr>

                    <tr>
                    <td colspan="5" align="center">

                    <!-- '.$jathagam[$fvalue['jathagam']].' -->
                    '.(!empty($fvalue['balance']) ? ' - '.$this->lang->line('iruppu').' : '.$fvalue['balance'] : '').'
                    '.(!empty($fvalue['byear']) ? ' - '.$this->lang->line('year').' : '.$fvalue['byear'] : '').'
                    '.(!empty($fvalue['month']) ? ' - '.$this->lang->line('month').' : '.$fvalue['month'] : '').'
                    </td>
                    </tr>
                 </table>
              </td>
           </tr>

        </table>';


        $this->load->library("Mpdf");
        $mpdf = new Mpdf('ta_IN','A4',9,'lf',2,2,2,0,0,0);
        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html);
        ob_clean();
        $mpdf->Output();
        exit;
    }

    public function edit() {

        $id = $this->input->get('id');
        if (!empty($id)) {
            $data['fvalue'] = $this->profile_model->getProfile($id);

            $data['fenglish'] = $this->profile_model->getLang($id, 1);
            $data['ftamil'] = $this->profile_model->getLang($id, 2);
            $data['url'] = base_url();
        }

        $data['message'] = $this->session->flashdata('message');

        $data['site_url'] = str_replace('apanel.php', '',base_url());
        $data['base_url'] = str_replace('apanel.php', '', base_url()) . 'application/apanel/views/';
        $data['url'] = base_url();

        $data['body_content'] = $this->load->view('profile_edit', $data, true);
        $data['user_id'] = $this->client_model->get_id();
        $this->load->view('template', $data);
    }

    private function imgupload($profile_id) {
        if (!empty($_FILES["image"]["name"])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['remove_spaces'] = TRUE;
            $config['file_name'] = $profile_id;

            $this->load->library('upload', $config);

            if (!empty($_FILES["image"]["name"])) {

                if (!$this->upload->do_upload('image')) {
                    $error = array('error' => $this->upload->display_errors());
                    //echo '<script>parent.checkalert('.$this->upload->display_errors().');</script>';
                    //$this->session->set_flashdata('message',$this->upload->display_errors());
                    return '';
                } else {
                    $upload_data = $this->upload->data();

                    //$fvalue['base_name'] = $profile_id.''.$upload_data['file_ext'];
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $upload_data['full_path'];
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 75;
                    $config['height'] = 50;
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    return $upload_data['file_name'];
                }
            }
        }
    }

    public function save() {

        $fvalue = $this->input->post('fvalue');
        $ftamil = $this->input->post('ftamil');
        $fenglish = $this->input->post('fenglish');

        if (!empty($fenglish) && !empty($fenglish['name'])) {
            $fvalue['name'] = $fenglish['name'];
        }
        if (!empty($ftamil) && !empty($ftamil['name'])) {
            $fvalue['name'] = empty($fvalue['name']) ? $ftamil['name'] : $fvalue['name'] . ' / ' . $ftamil['name'];
        }

        $this->form_validation->set_rules('fvalue[email]', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('fvalue[phone]', 'Phone', 'trim|required|regex_match[/^[0-9]{10}$/]|callback_check_user_phone');
$this->form_validation->set_error_delimiters('', '');
        try {
            $profile_id = !empty($fvalue['profile_id']) ? $fvalue['profile_id'] : 0;
            if($this->form_validation->run() == TRUE) {
                $profile_id = $this->profile_model->save($fvalue);
                if (!empty($profile_id)) {
                    $message = '';
                    $image_name = '';
                    if (!empty($_FILES["image"]["name"]) && !$image_name = $this->imgupload($profile_id)) {
                        $message = '<span class="label label-danger">Image Upload Failed. </span> &nbsp; ';
                    }
                    if (!empty($image_name)) {
                        $this->profile_model->save(array('image' => $image_name, 'profile_id' => $profile_id));
                    }

                    $this->profile_model->updateLangDetails($fenglish, 1, $profile_id);
                    $this->profile_model->updateLangDetails($ftamil, 2, $profile_id);

                    $this->session->set_flashdata('message', $message . '<span class="label label-success"> Profile Updated Successfully! </span>');
                }
            } else {
                $this->session->set_flashdata('message', '<p class="label label-danger"> '.validation_errors().' </p>');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('message', '<span class="label label-error"> '.$e->getMessage().' </span>');
        }
        redirect(base_url() . 'profiles/edit?id=' . $profile_id);
    }

    public function check_user_phone($phone) {
        $fvalue = $this->input->post('fvalue');
        $profile_id = !empty($fvalue['profile_id']) ? $fvalue['profile_id'] : 0;
        $result = $this->profile_model->check_unique_user_phone($profile_id, $fvalue['phone']);
        if($result == 0)
            $response = true;
        else {
            $this->form_validation->set_message('check_user_phone', 'Phone must be unique');
            $response = false;
        }
        return $response;
    }

    public function check_user_email($email) {
        $fvalue = $this->input->post('fvalue');
        $profile_id = !empty($fvalue['profile_id']) ? $fvalue['profile_id'] : 0;
        $result = $this->profile_model->check_unique_user_email($profile_id, $fvalue['email']);
        if($result == 0)
            $response = true;
        else {
            $this->form_validation->set_message('check_user_email', 'Email must be unique');
            $response = false;
        }
        return $response;
    }

    public function delete() {
        $id = $this->input->get('id');
        if (!empty($id)) {
            $this->profile_model->deleteMessages($id);
            $this->session->set_flashdata('message', 'Account Deleted Successfully!');
        }
        redirect(base_url() . 'profiles', 'refresh');
    }

    public function activate() {

        $id = $this->input->get('id');
        if (!empty($id)) {
            $this->profile_model->deactivateAccount($id, $status = 1); //enable it
            $this->session->set_flashdata('message', 'Profile Enabled Successfully!');
        }
        redirect(base_url() . 'profiles', 'refresh');
    }

    public function deactivate() {

        $id = $this->input->get('id');
        if (!empty($id)) {
            $this->profile_model->deactivateAccount($id);
            $this->session->set_flashdata('message', 'Account Disabled Successfully!');
        }
        redirect(base_url() . 'profiles', 'refresh');
    }

}
