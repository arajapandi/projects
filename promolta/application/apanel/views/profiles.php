            <div id="page-wrapper" style="min-height: 631px;">
                <div class="row">

                    <div class="col-lg-10">
                        <h1 class="page-header"> Profiles </h1>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group"> &nbsp; </div>
                        <input type="button" onclick="location.href='<?php echo $url; ?>profiles/edit'" value="Add New Profile" name="Submit" class="btn btn-large">
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                <?php if(!empty($message)): ?>
                  <div class="alert alert-info">
                    <?php echo $message; ?>
                  </div>
                <?php endif; ?>



                <a href="javascript:;"  data-toggle="collapse" data-target="#filters" class="pull-right">Filters</a>

                <br/>
                <div id="filters" class="collapse in">
                  <div class="well">
                    <form class="form-inline" action="">
                      <div class="form-group input-group-sm">
                      <label for="profile_id">Profile Id:</label>
                      <input type="input" class="form-control" value="<?php echo isset($search_array['profile_id']) ? $search_array['profile_id'] : ''; ?>" id="profile_id" placeholder="Profile Id" name="profile_id">
                      </div>

                      <div class="form-group input-group-sm">
                      <label for="age_from">Age:</label>
                      <input type="input" class="form-control" value="<?php echo isset($search_array['age_from']) ? $search_array['age_from'] : ''; ?>" id="age_from" placeholder="From" name="age_from" style="width:60px;">
                      <input type="input" class="form-control" value="<?php echo isset($search_array['age_to']) ? $search_array['age_to'] : ''; ?>" id="age_to" placeholder="To" name="age_to" style="width:60px;">
                      </div>

                      <div class="form-group input-group-sm">
                      <label for="profile_id">Gender:</label>
                      <select name="gender" class="form-control">
                        <option value="">Select..</option>
                        <option value="0" <?php echo (isset($search_array['gender']) && $search_array['gender'] == 0) ? 'selected' : ''; ?>>Male</option>
                        <option value="1" <?php echo (isset($search_array['gender']) && $search_array['gender'] == 1) ? 'selected' : ''; ?>>FeMale</option>
                      </select>
                      </div>

                      <div class="form-group input-group-sm">
                      <label for="active">Active:</label>
                      <select name="active" class="form-control">
                        <option value="">..</option>
                        <option value="1" <?php echo (isset($search_array['active']) && $search_array['active'] == 1) ? 'selected' : ''; ?>>Yes</option>
                        <option value="0" <?php echo (isset($search_array['active']) && $search_array['active'] == 0) ? 'selected' : ''; ?>>No</option>
                      </select>
                      </div>

                      <div class="form-group input-group-sm">
                      <label for="search">Search:</label>
                      <input type="input" class="form-control" value="<?php echo isset($search_array['search']) ? $search_array['search'] : ''; ?>" id="search" placeholder="Search" name="search">
                      </div>

                      <div class="form-group  input-group-sm">
                      <input type="submit" class="form-control" id="searchit" value="Search" placeholder="" name="sub">
                      </div>

                    </form>
                  </div>
                </div>



                <div class="tab-content col-md-12">

                <div id="sent" class="tab-pane fade active in">

                   <table class="table table-stripped" id="sentTbl">
                      <thead>
                        <tr>
                          <th width="10%">Reg No</th>
                          <th width="10%">Ref No</th>
                          <th width="45%">Name</th>
                          <th width="15%">Photo</th>
                          <th width="20%">Action</th>
                        </tr>
                      </thead>
                      <tbody>

                      <?php if(!empty($profiles)):
                        foreach($profiles as $data): ?>
                        <tr class="<?php echo $data['status'] == 0 ? "active":''; ?>">
                        <td scope="row"><?php echo $data['id']; ?></td>
                          <td><?php $extra = json_decode($data['extra'],true);
                                    if(!empty($extra['regno']))  { echo $extra['regno'];}?></td>

                          <td><?php echo $data['name']; ?></td>
                          <td>
                          <?php if(!empty($data['image'])) { ?>
                           <a href="<?php echo $site_url.'uploads/'.$data['image']; ?>" target="_blank">
                               <img src="<?php echo $site_url.'uploads/'.$data['image']; ?>" border="0" width="40" height="40"/> </a>
                           <?php };?>
                          </td>
                          <td>
                            <a href="<?php echo $url; ?>profiles/edit?id=<?php echo $data['id']; ?>" class="fwdMessage" data-index="0" data-toggle="tooltip" title="Edit"><span class="glyphicon glyphicon-edit"></span></a> &nbsp;
                            <a href="<?php echo $url; ?>profiles/<?php echo $data['status'] == 0 ? "activate":'deactivate'; ?>?id=<?php echo $data['id']; ?>" class="fwdMessage" data-index="0" data-toggle="tooltip" title="<?php echo $data['status'] == 0 ? "Activate":'De-Activate'; ?>"><span class="glyphicon glyphicon-off"></span></a> &nbsp;
                            <a href="<?php echo $url; ?>profiles/pdf?id=<?php echo $data['id']; ?>&lang=tamil" target="_blank" class="fwdMessage" data-index="0" data-toggle="tooltip" title="View Tamil"><span class="glyphicon glyphicon-save"></span></a> &nbsp;
                            <a href="<?php echo $url; ?>profiles/pdf?id=<?php echo $data['id']; ?>&lang=english" target="_blank" class="fwdMessage1" data-index="0" data-toggle="tooltip" title="View English"><span class="glyphicon glyphicon-save"></span></a> &nbsp;
                            <a href="<?php echo $url; ?>profiles/sendpass?id=<?php echo $data['id']; ?>" class="fwdMessage" data-index="0" data-toggle="tooltip" title="Send Password"><span class="glyphicon glyphicon-send"></span></a> &nbsp;
                            <a href="javascript:;" class="linkpackage" data-profile_id="<?php echo $data['id']; ?>" title="assign membership"><span class="glyphicon glyphicon-expand"></span></a> &nbsp;
                            <a href="<?php echo $url; ?>profiles/delete?id=<?php echo $data['id']; ?>" class="fwdMessage" data-index="0" data-toggle="tooltip" title="Delete"><span class="glyphicon glyphicon-remove"></span></a> &nbsp;

                          </td>
                        </tr>
                      <?php endforeach;
                    else: ?>
                    <tr><td colspan="6" align="center"><h3>No Profiles Found!</h3></td</tr>
                  <?php endif; ?>
                    </table>
                    <?php if(!empty($pagination)): ?>
                    <div class="pull-right">
                        <?php echo $pagination; ?>
                    </div>
                    <?php endif; ?>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">

                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Assign Package</h4>
                            </div>
                            <div class="modal-body">



                              <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#package">Package</a></li>
                                <li><a data-toggle="tab" href="#memberlist">Member List</a></li>
                              </ul>

                              <div class="tab-content">
                                <div id="package" class="tab-pane fade in active">
                                    <br>
                                    <form action="save" id="modelform" method="post" enctype="multipart/form-data">
                                    <div class="message alert alert-info"></div>
                                    <div class="row">
                                        <div class="col-lg-12">

                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label"> Profile Id</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="profile_id" name="fvalue[profile_id]" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label"> Package id </label>
                                                <div class="col-sm-8">
                                                <select class="form-control" id="package_id" name="fvalue[package_id]" >

                                                </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label"> End Date  </label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="endtime" name="fvalue[endtime]" >

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label"> Profile Views</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="profile_view" name="fvalue[profile_view]" >

                                                </div>
                                            </div>
                                            <div class="form-group row" align="center">
                                              <input type="button" value="Update" name="Submit" id="submit" class="btn btn-small">
                                            </div>
                                        </div>
                                    </div>
                                    </form>

                                </div>
                                <div id="memberlist" class="tab-pane fade">
                                    <table class="table table-stripped" id="profileAssigned">
                                       <thead>
                                         <tr>
                                           <th width="10%">Id</th>
                                           <th width="50%">Name</th>
                                           <th width="35%">Assigned Date</th>
                                           <th width="10%">#</th>
                                         </tr>
                                       </thead>
                                       <tbody>
                                     </table>
                                     <div>
                                       <div class="form-group row">
                                           <label class="col-sm-3 col-form-label"> Profile Number </label>
                                           <div class="col-sm-3">
                                             <input class="form-control" id="profile_assignid" name="fvalue[profile_assignid]" >
                                           </div>
                                           <div class="col-sm-3">
                                             <input type="button" value="Assign" name="Submit" id="profile_assign" class="btn btn-small">
                                           </div>
                                       </div>

                                     </div>
                                </div>
                              </div>

                                <!-- /.row (nested) -->
                            </div>
                            <!--<div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>-->
                          </div>

                        </div>
                    </div>
                    <script>
                        $(function() {


                            $('#profile_assign').on('click',function() {
                                var profile_assignid = $('#profile_assignid').val();
                                var profile_id = $('#profile_id').val();
                                $.ajax({
                                       type: "POST",
                                       url: "<?php echo $url; ?>packages/assignprofile",
                                       data: {'profile_assignid' : profile_assignid, 'profile_id' : profile_id}, // serializes the form's elements.
                                       success: function(data)
                                       {
                                         if(data == '') {
                                           alert('Assign Failed!');
                                         } else {
                                           alert(data);
                                         }
                                         $('.modal').modal('toggle');
                                       }
                                     });
                                e.preventDefault(); // a
                            });
                            $('#submit').on('click',function() {

                                $.ajax({
                                       type: "POST",
                                       url: "<?php echo $url; ?>packages/profilepackage",
                                       data: $("#modelform").serialize(), // serializes the form's elements.
                                       success: function(data)
                                       {
                                           if(data == 1) {
                                             $('.message').show().html('Updated Successfully!');
                                           } else {
                                             $('.message').show().html('Something went wrong!');
                                           }
                                       }
                                     });

                                e.preventDefault(); // a
                            });


                            $('#package_id').change(function() {

                                var selected = $(this).find('option:selected');
                                var days_profileview = selected.data('jsosn');

                                if(days_profileview) {
                                    var res = days_profileview.split("#");
                                    $('#endtime').val(res[0]);
                                    $('#profile_view').val(res[1]);
                                } else {
                                    $('#endtime').val('');
                                    $('#profile_view').val('');
                                }
                            });

                            $(".modal").on("hidden.bs.modal", function(){
                                $(".modal-body1").html("");
                            });

                            function callBackAssign() {
                              var profile_assignid = $(this).data('profile_assignid');
                              var profile_id = $('#profile_id').val();
                              $.ajax({
                                type: "POST",
                                  url: "<?php echo $url; ?>packages/deleteAssigneProfile",
                                  data: { "profile_id": profile_id, 'profile_assignid':profile_assignid},
                                }).done(function(data) {
                                  if(data == '') {
                                    alert('Assign Failed!');
                                  } else {
                                    alert(data);
                                  }
                                  $('.modal').modal('toggle');
                                });
                            }

                            $('.linkpackage').on('click',function(){

                                $('#modelform').trigger('reset');
                                $('.message').hide().html('');


                                var profile_id = $(this).data('profile_id');
                                $('#modelform #profile_id').val(profile_id);
                                $.ajax({
                                    url: "<?php echo $url; ?>packages/getprofilepackage",
                                    data: { "profile_id": profile_id},
                                  }).done(function(data) {
                                        if(data != null) {
                                          data = $.parseJSON(data);
                                        }
                                        var form_data = {};
                                        if(data != null && data.package) {
                                          form_data = data.package;
                                        }
                                        if(data != null && data.profiles) {
                                          $('#profileAssigned tr').not(function(){ return !!$(this).has('th').length; }).remove();
                                          $.each( data.profiles, function( key, value ) {
                                            $('#profileAssigned').append('<tr id="addr'+key+'"></tr>');
                                            $('#addr' + key).html("<td>"+value.assignd_id+"</td><td>"
                                                                  +value.name+"</td><td>"
                                                                  +value.created_at+"</td><td>"+
                                                                  '<a href="javascript:;" class="removeAssnClose" data-profile_assignid="'+value.assignd_id+'" data-toggle="tooltip" title="Remove"><span class="glyphicon glyphicon-remove"></span></a></td>');


                                          });
                                          $('.removeAssnClose').on('click',callBackAssign);
                                        }

                                        $.ajax({
                                            url: "<?php echo $url; ?>packages/getpackages",
                                          }).done(function(data) {
                                            if(data.length) {
                                                var json_data = $.parseJSON(data);
                                                if(json_data.length) {
                                                    $('#package_id').empty();

                                                    $('#package_id').append($("<option data-jsosn='' />")
                                                              .val('')
                                                              .text('Select'));
                                                    $.each(json_data,function(index, value){
                                                      $('#package_id').append($("<option data-jsosn='"+value.enddate+"#"+value.profile_view+"' />")
                                                              .val(value.id)
                                                              .text(value.name));
                                                    });

                                                    $('#profile_id').val(profile_id);
                                                    if(form_data !== null && form_data !== {}) {
                                                        $('#package_id option[value=' + form_data.package_id + ']')
                                                        .attr('selected',true);

                                                        $('#endtime').val(form_data.endtime);
                                                        $('#profile_view').val(form_data.profile_view);
                                                    }

                                                    $('#myModal').modal();
                                                } else {
                                                    alert('Package List error!');
                                                }
                                            } else {
                                                alert('Package List error!');
                                            }
                                          });
                                          return false;
                                  });

                            });
                        })
                    </script>
                </div>


              </div>
              </div>
