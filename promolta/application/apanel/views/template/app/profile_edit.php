<?php
$_lang = array(
        'english'=>array(   
                            'regno'=>' Registration Number ',
                            'name'=>'Name',
                            'mars'=>'Mars',
                            'ragukethu'=>'Raagu/Kethu',
                            'education'=>'Education',
                            'aninco'=>'Annual Income',
                            'job'=>'Job',
                            'fname'=>' Father\'s Name',
                            'mname'=>'Mother\'s Name',
                            'rescity'=>' Residing City',
                            'height'=>'Height',
                            'color'=>'Color',
                            'kulam'=>'Kulam',
                            'kulatemple'=>' Kula Theivam Temple',
                            'tamil'=>'Tamil',
                            'year'=>'Year',
                            'month'=>'Month',
                            'date'=>'Date',
                            'nazhigai'=>'Nazhigai',
                            'time'=>'Time',
                            'nakshatram'=>'Nakshatram',
                            'rasi'=>'Rasi',
                            'lagnam'=>'Lagnam',
                            'thisaieruppu'=>'Thisai Eruppu',
                            'laknam'=>'Laknam',
                            'suriyan'=>'Suriyan',
                            'budhan'=>'Budhan',
                            'sukuran'=>'Sukuran',
                            'sevvai'=>'Sevvai',
                            'ragu'=>'Ragu',
                            'kethu'=>'Kethu',
                            'guru'=>'Guru',
                            'chandran'=>'chandran',
                            'sani'=>'Sani',
                            'manthi'=>'Manthi',
                            'rasi'=>'Rasi',
                            'amsam'=>'Amsam',
                            'cleanhoro'=>'Clean Horoscope',
                            'rakese'=>'Ragu Kethu Sevvai',
                            'ragukethu'=>'Ragu Kethu',
                            'balance'=>'Balance',
                            'year'=>'Year',
                            'month'=>'Month',
                            'info'=>'Informations',
                            'expect'=>'Expectations',
                            'dist'=>'District',
                            'dob'=>'Date of Birth',
                            'gender'=>'Gender',
                            'male'=>'Male',
                            'female'=>'Female',
                            'familpar'=>'Family Particulars',
                            'brother'=>'Brothers',
                            'sister'=>'Sisters',
                            'married'=>'Married',
                            'unmarried'=>'Unmarried',
                            'familyproperty'=>'Family Property Particulars',
                            'house'=>'House(Concrete)/(Tile)',
                            'land'=>'Land',
                            'vehicle'=>'Vehicle',
                            'value'=>'Value',
                            'value1'=>'Value1',
                            'value2'=>'Value2',
                            'value3'=>'Value3',
                            'tav'=>'Total asset value',
                            'uploadimg'=>'Upload Photo ' ),
        'tamil'=>array(   
                            'regno'=>' Registration Number ',
                            'name'=>'Name',
                            'mars'=>'Mars',
                            'ragukethu'=>'Raagu/Kethu',
                            'education'=>'Education',
                            'aninco'=>'Annual Income',
                            'job'=>'Job',
                            'fname'=>' Father\'s Name',
                            'mname'=>'Mother\'s Name',
                            'rescity'=>' Residing City',
                            'height'=>'Height',
                            'color'=>'Color',
                            'kulam'=>'Kulam',
                            'kulatemple'=>' Kula Theivam Temple',
                            'tamil'=>'Tamil',
                            'year'=>'Year',
                            'month'=>'Month',
                            'date'=>'Date',
                            'nazhigai'=>'Nazhigai',
                            'time'=>'Time',
                            'nakshatram'=>'Nakshatram',
                            'rasi'=>'Rasi',
                            'lagnam'=>'Lagnam',
                            'thisaieruppu'=>'Thisai Eruppu',
                            'laknam'=>'Laknam',
                            'suriyan'=>'Suriyan',
                            'budhan'=>'Budhan',
                            'sukuran'=>'Sukuran',
                            'sevvai'=>'Sevvai',
                            'ragu'=>'Ragu',
                            'kethu'=>'Kethu',
                            'guru'=>'Guru',
                            'chandran'=>'chandran',
                            'sani'=>'Sani',
                            'manthi'=>'Manthi',
                            'rasi'=>'Rasi',
                            'amsam'=>'Amsam',
                            'cleanhoro'=>'Clean Horoscope',
                            'rakese'=>'Ragu Kethu Sevvai',
                            'ragukethu'=>'Ragu Kethu',
                            'balance'=>'Balance',
                            'year'=>'Year',
                            'month'=>'Month',
                            'info'=>'Informations',
                            'expect'=>'Expectations',
                            'dist'=>'District',
                            'dob'=>'Date of Birth',
                            'gender'=>'Gender',
                            'male'=>'Male',
                            'female'=>'Female',
                            'familpar'=>'Family Particulars',
                            'brother'=>'Brothers',
                            'sister'=>'Sisters',
                            'married'=>'Married',
                            'unmarried'=>'Unmarried',
                            'familyproperty'=>'Family Property Particulars',
                            'house'=>'House(Concrete)/(Tile)',
                            'land'=>'Land',
                            'vehicle'=>'Vehicle',
                            'value'=>'Value',
                            'value1'=>'Value1',
                            'value2'=>'Value2',
                            'value3'=>'Value3',
                            'tav'=>'Total asset value',
                            'uploadimg'=>'Upload Photo ' )

    );
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Admin Panel</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">



    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    
      <script src="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
 <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>



        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">Startmin</a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><a href="#"><i class="fa fa-home fa-fw"></i> Website</a></li>
                </ul>

                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown navbar-inverse">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell fa-fw"></i> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Comment
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="pull-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> Message Sent
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-tasks fa-fw"></i> New Task
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> Admin <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            
                            <li>
                                <a href="dashboard.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="flot.html">Flot Charts</a>
                                    </li>
                                    <li>
                                        <a href="morris.html">Morris.js Charts</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                           
                            <li class="active">
                                <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a class="active" href="blank.html">Blank Page</a>
                                    </li>
                                    <li>
                                        <a href="login.html">Login Page</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Blank</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->


                     <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Basic Form Elements

                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <form role="form">    
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['regno']; ?> </label>
                                                <input class="form-control">
                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-lg-6">
                                        <form role="form">
                                        <h1>Form Validation States</h1>
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['name']; ?></label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['education']; ?></label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['job']; ?></label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['fname']; ?></label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['mname']; ?></label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['rescity']; ?></label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['height']; ?></label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['color']; ?></label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['kulam']; ?></label>
                                                <input class="form-control">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['kulatemple']; ?></label>
                                                <input class="form-control">
                                            </div>

                                          <h1>Birth Details</h1>  
                                          <div class="form-group">
                                                <label><?php echo $_lang['english']['tamil']; ?> </label>
                                                <input class="form-control">
                                            </div>
                                             <div class="form-group">
                                                <label><?php echo $_lang['english']['year']; ?> </label>
                                                <input class="form-control">
                                            </div>
                                             <div class="form-group">
                                                <label><?php echo $_lang['english']['month']; ?> </label>
                                                <input class="form-control">
                                            </div>
                                             <div class="form-group">
                                                <label><?php echo $_lang['english']['date']; ?> </label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['nazhigai']; ?> </label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['time']; ?> </label>
                                                <input class="form-control">
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                    <div class="col-lg-6">
                                    <h1>  - </h1>
                                        <form role="form">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>kalvi</label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>velai</label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>father</label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>ma</label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>city</label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>color</label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>kulam</label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>kulam temple</label>
                                                <input class="form-control">
                                            </div>
                                            
                                            <h1>Form Validation States</h1>  
                                          <div class="form-group">
                                                <label>kulam temple</label>
                                                <input class="form-control">
                                            </div>
                                             <div class="form-group">
                                                <label>kulam temple</label>
                                                <input class="form-control">
                                            </div>
                                             <div class="form-group">
                                                <label>kulam temple</label>
                                                <input class="form-control">
                                            </div>
                                             <div class="form-group">
                                                <label>kulam temple</label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>kulam temple</label>
                                                <input class="form-control">
                                            </div>
                                        </form>
                                        
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                    <div class="col-lg-7">
                                        <form role="form">    

                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['aninco']; ?></label>
                                                <select class="form-control">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['sevvai']; ?>  </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox"> Yes / 
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox"> No /
                                                </label>
                                            </div>

                                             <div class="form-group">
                                                <label> <?php echo $_lang['english']['ragukethu']; ?>  </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox"> Yes / 
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox"> No /
                                                </label>
                                            </div>

                                             

                                            

                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['nakshatram']; ?></label>
                                                <select class="form-control">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['rasi']; ?></label>
                                                <select class="form-control">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['lagnam']; ?></label>
                                                <select class="form-control">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label><?php echo $_lang['english']['thisaieruppu']; ?></label>
                                                <select class="form-control">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>






                                           


                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['balance']; ?>  </label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['year']; ?>  </label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['month']; ?>  </label>
                                                <input class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['info']; ?>  </label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['expect']; ?>  </label>
                                                <input class="form-control">
                                            </div>


                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['dist']; ?>  </label>
                                                <input class="form-control">
                                            </div>
                                            <div class="input-group date" data-provide="datepicker">
                                                <label><?php echo $_lang['english']['dob']; ?></label>
                                                <input type="text" class="form-control">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['gender']; ?>  </label>
                                                 <label class="checkbox-inline">
                                                    <input type="checkbox"> <?php echo $_lang['english']['male']; ?> / 
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox"> <?php echo $_lang['english']['female']; ?> /
                                                </label>
                                            </div>
<h1><?php echo $_lang['english']['familpar']; ?></h1>  
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['brother']; ?>  </label>
                                                <?php echo $_lang['english']['married']; ?> : <input class="form-control"> 
                                                <?php echo $_lang['english']['unmarried']; ?> : <input class="form-control"> 
                                            </div>
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['sister']; ?>  </label>
                                                <?php echo $_lang['english']['married']; ?> : <input class="form-control"> 
                                                <?php echo $_lang['english']['unmarried']; ?> : <input class="form-control"> 
                                            </div>
<h1><?php echo $_lang['english']['familyproperty']; ?></h1>  
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['house']; ?>  </label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['land']; ?>  </label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['vehicle']; ?>  </label>
                                                <input class="form-control">
                                            </div>
<h1><?php echo $_lang['english']['value']; ?></h1>  
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['value1']; ?>  </label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['value2']; ?>  </label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['value3']; ?>  </label>
                                                <input class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label> <?php echo $_lang['english']['tav']; ?>  </label>
                                                <textarea class="form-control" rows="3"></textarea>
                                            </div>




                                            <script type="text/javascript">
                                                $(function(){
                                                    $('.datepicker').datepicker();
                                                });
                                            </script>


                                        </form>
                                    </div>


                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>

    </body>
</html>
