<div class="col-md-4 col-md-offset-4">
    
    <div class="login-panel panel panel-default">
                
        <div class="panel-heading">
            <h3 class="panel-title"> Sign In</h3>
        </div>

        <div class="panel-body">
    
            <?php if(!empty($message)): ?>
            <div class="alert alert-info">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
            
            <form role="form" action="<?php echo $url; ?>/logme/check" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="form-group">
                        <input class="form-control" placeholder="Username" name="username" type="username" autofocus>
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="Password" name="password" type="password" value="">
                    </div>
                    <button class="btn btn-lg btn-success btn-block"> Login </button>
                </fieldset>
            </form>
        </div>
    </div>
</div>