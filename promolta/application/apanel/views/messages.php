<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Snippet - Bootsnipp.com</title>
        <link href='//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css' rel='stylesheet'/>
        <link href='<?php echo str_replace('apanel.php','',base_url()); ?>application/apanel/views/css/bootstrap-tagsinput.css' rel='stylesheet'/>
        <script type='text/javascript' src='//code.jquery.com/jquery-1.10.2.min.js'></script>
        <script type='text/javascript' src='//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js'></script>
        <script type='text/javascript'>
            var baseURL = "<?php echo base_url(); ?>";
            var userId = "<?php echo $userId; ?>";
            var authData = "<?php echo $auth; ?>";
            var userDataArray = ["user1","user2","user3","user4","user5","user6","user7","user8"];


        </script>

        <script type='text/javascript' src='<?php echo str_replace('apanel.php','',base_url()); ?>application/apanel/views/js/typeahead.bundle.js'></script>
        <script type='text/javascript' src='<?php echo str_replace('apanel.php','',base_url()); ?>application/apanel/views/js/bootstrap-tagsinput.js'></script>
        <script type='text/javascript' src='<?php echo str_replace('apanel.php','',base_url()); ?>application/apanel/views/js/app.js'></script>
        <script type='text/javascript'>
            jQuery(document).ready(function() {
                App.Controller.messageInit();

            });
        </script>
    </head>
    <body>

        <div class="container">

          <div class="well h3">
            Messaging System
          </div>
              <ul class="nav nav-pills nav-stacked col-md-2 menustack" id="">
                <li class="active"><a data-toggle="tab" data-attr="inbox" href="#inbox">Inbox</a></li>
                <li><a data-toggle="tab" data-attr="sent" href="#sent">Sent</a></li>
                <li><a data-toggle="tab" data-attr="draft" href="#draft">Draft</a></li>
                <li><a data-toggle="tab" data-attr="trash" href="#trash">Trash</a></li>
              </ul>

              <div class="tab-content col-md-10">
                <div id="inbox" class="tab-pane fade in active">

                  <table class="table table-bordered" id="inboxTbl">
                    <thead>
                      <tr>
                        <th width="20%">From</th>
                        <th width="50%">Subject</th>
                        <th width="20%">Date</th>
                        <th width="10%">##</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <div id="sent" class="tab-pane fade">

                   <table class="table table-bordered" id="sentTbl">
                      <thead>
                        <tr>
                          <th width="20%">To</th>
                          <th width="50%">Subject</th>
                          <th width="20%">Date</th>
                          <th width="10%">##</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>

                </div>

                <div id="draft" class="tab-pane fade">

                  <table class="table table-bordered" id="draftTbl">
                    <thead>
                      <tr>
                        <th width="20%">To</th>
                        <th width="50%">Subject</th>
                        <th width="20%">Date</th>
                        <th width="10%">##</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <div id="trash" class="tab-pane fade">


                  <table class="table table-bordered" id="trashTbl">
                    <thead>
                      <tr>
                        <th width="20%">From</th>
                        <th width="50%">Subject</th>
                        <th width="20%">Date</th>
                        <th width="10%">##</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>

                </div>
              </div>



            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
                <form id="messageForm">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Compose..</h4>
                  </div>
                  <div class="modal-body">

                      <div class="form-group">
                        <label for="formGroupExampleInput">To</label>
                        <input type="text" class="form-control"  id="tomessage" name='fvalue[to]' placeholder="To ..."/>
                      </div>
                      <div class="form-group">
                        <label for="formGroupExampleInput">Subject</label>
                        <input type="text" class="form-control" id="formGroupExampleInput" name='fvalue[subject]' placeholder="Subject..">
                      </div>

                      <div class="form-group">
                        <label for="exampleTextarea">Message</label>
                        <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                      </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default submitData" id="submitData">Submit</button>
                  </div>
                </div>
                <!-- content -->
              </form>

              </div>
            </div>
            <!--end Modal-->

        </div>
    </body>
</html>
