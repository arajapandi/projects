<?php
$_lang = array(
        'english'=>array(
                            'language'=>' English ',
                            'birthdet'=>' Birth Details',
                            'regno'=>' Registration Number ',
                            'name'=>'Name',
                            'mars'=>'Mars',
                            'ragukethu'=>'Raagu/Kethu',
                            'education'=>'Education',
                            'aninco'=>'Annual Income',
                            'job'=>'Job',
                            'fname'=>' Father\'s Name',
                            'mname'=>'Mother\'s Name',
                            'rescity'=>' Residing City',
                            'height'=>'Height',
                            'color'=>'Color',
                            'kulam'=>'Kulam',
                            'kulatemple'=>' Kula Theivam',
                            'tamil'=>'Tamil',
                            'year'=>'Year',
                            'month'=>'Month',
                            'date'=>'Date',
                            'nazhigai'=>'Nazhigai',
                            'time'=>'Time',
                            'nakshatram'=>'Nakshatram',
                            'rasi'=>'Rasi',
                            'lagnam'=>'Lagnam',
                            'thisaieruppu'=>'Thisai Eruppu',
                            'laknam'=>'Laknam',
                            'suriyan'=>'Suriyan',
                            'budhan'=>'Budhan',
                            'sukuran'=>'Sukuran',
                            'sevvai'=>'Sevvai',
                            'ragu'=>'Ragu',
                            'kethu'=>'Kethu',
                            'guru'=>'Guru',
                            'chandran'=>'chandran',
                            'sani'=>'Sani',
                            'manthi'=>'Manthi',
                            'rasi'=>'Rasi',
                            'amsam'=>'Amsam',
                            'cleanhoro'=>'Clean Horoscope',
                            'rakese'=>'Ragu Kethu Sevvai',
                            'ragukethu'=>'Ragu Kethu',
                            'balance'=>'Balance',
                            'year1'=>'Year',
                            'month1'=>'Month',
                            'info'=>'Informations',
                            'expect'=>'Expectations',
                            'dist'=>'District',
                            'dob'=>'Date of Birth',
                            'gender'=>'Gender',
                            'male'=>'Male',
                            'female'=>'Female',
                            'familpar'=>'Family Particulars',
                            'brother'=>'Brothers(சகோதரர்கள்)',
                            'sister'=>'Sisters(சகோதரிகள்)',
                            'married'=>'Married(திருமணம்)',
                            'unmarried'=>'Unmarried(திருமணமாகாதவர்கள்)',
                            'familyproperty'=>'Family Property Particulars',
                            'house'=>'House(Concrete)/(Tile)',
                            'land'=>'Land',
                            'vehicle'=>'Vehicle',
                            'value'=>'Value',
                            'value1'=>'Value1',
                            'value2'=>'Value2',
                            'value3'=>'Value3',
                            'tav'=>'Total asset value',
                            'jathagam'=>'Jathagam',
                            'uploadimg'=>'Upload Photo ' ),
        'tamil'=>array(
            'language'=>' தமிழ் ',
                            'birthdet'=>' Birth Details',
                            'regno'=>' Registration Number ',
                            'name'=>'பெயர்',
                            'mars'=>'Mars',
                            'ragukethu'=>'Raagu/Kethu',
                            'education'=>'கல்வி',
                            'aninco'=>'Annual Income',
                            'job'=>'வேலை',
                            'fname'=>'தந்தையின் பெயர்',
                            'mname'=>'அம்மாவின் பெயர்',
                            'rescity'=>' வசிக்கும் நகரம்',
                            'height'=>'உயரம்',
                            'color'=>'நிறம்',
                            'kulam'=>'குலம்',
                            'kulatemple'=>' குல தெய்வம்',
                            'tamil'=>'தமிழ்',
                            'year'=>'ஆண்டு',
                            'month'=>'மாதம்',
                            'date'=>'தேதி',
                            'nazhigai'=>'நாழிைகக்கு',
                            'time'=>'நேரம்',
                            'nakshatram'=>'Nakshatram',
                            'rasi'=>'Rasi',
                            'lagnam'=>'Lagnam',
                            'thisaieruppu'=>'Thisai Eruppu',
                            'laknam'=>'Laknam',
                            'suriyan'=>'Suriyan',
                            'budhan'=>'Budhan',
                            'sukuran'=>'Sukuran',
                            'sevvai'=>'Sevvai',
                            'ragu'=>'Ragu',
                            'kethu'=>'Kethu',
                            'guru'=>'Guru',
                            'chandran'=>'chandran',
                            'sani'=>'Sani',
                            'manthi'=>'Manthi',
                            'rasi'=>'Rasi',
                            'amsam'=>'Amsam',
                            'cleanhoro'=>'Clean Horoscope',
                            'rakese'=>'Ragu Kethu Sevvai',
                            'ragukethu'=>'Ragu Kethu',
                            'balance'=>'Balance',
                            'year1'=>'Year',
                            'month1'=>'Month',
                            'info'=>'Informations',
                            'expect'=>'Expectations',
                            'dist'=>'District',
                            'dob'=>'Date of Birth',
                            'gender'=>'Gender',
                            'male'=>'ஆண்',
                            'female'=>'பெண்',
                            'familpar'=>'Family Particulars',
                            'brother'=>'Brothers',
                            'sister'=>'Sisters',
                            'married'=>'Married',
                            'unmarried'=>'Unmarried',
                            'familyproperty'=>'Family Property Particulars',
                            'house'=>'House(Concrete)/(Tile)',
                            'land'=>'Land',
                            'vehicle'=>'Vehicle',
                            'value'=>'Value',
                            'value1'=>'Value1',
                            'value2'=>'Value2',
                            'value3'=>'Value3',
                            'tav'=>'Total asset value',
                            'uploadimg'=>'Upload Photo ' )

    );
?>




            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?php echo isset($fvalue['id']) ? 'Update':'New'; ?> Profile </h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->

                <?php if(!empty($message)): ?>
                  <div class="alert alert-info">
                    <?php echo $message; ?>
                  </div>
                <?php endif; ?>                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel-default">

                            <div class="panel-body">
                            <form action="save" method="post" id="profileForm" role="form" data-toggle="validator" enctype="multipart/form-data">
                                <div class="row">
                                    <?php if(!empty($fvalue['id'] )): ?>
                                    <div class="col-lg-7">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label"><?php echo $_lang['english']['regno']; ?> </label>
                                                <div class="col-sm-8">
                                                <input class="form-control input-sm" name="fvalue[profile_id]" id="profile_id" value="<?php echo isset($fvalue['id']) ? $fvalue['id'] : '';?>" readonly="readonly">
                                                </div>
                                            </div>
                                    </div>
                                    <?php endif; ?>
                                     <div class="col-lg-7">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Email</label>
                                                <div class="col-sm-8">
                                                <input class="form-control input-sm" name="fvalue[email]" id="email" value="<?php echo isset($fvalue['email']) ? $fvalue['email'] : '';?>">
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-lg-7">
                                           <div class="form-group row">
                                               <label class="col-sm-4 col-form-label">Phone</label>
                                               <div class="col-sm-8">
                                               <input class="form-control input-sm" name="fvalue[phone]" id="phone" value="<?php echo isset($fvalue['phone']) ? $fvalue['phone'] : '';?>">
                                               </div>
                                           </div>
                                   </div>


                                   <div class="col-lg-7">
                                          <div class="form-group row">
                                              <label class="col-sm-4 col-form-label">Reg Date / பதிவு தேதி </label>
                                              <div class="col-sm-8">
                                              <div class="input-group date " data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                  <input class="form-control input-sm datepicker" name="fvalue[reg_date]" id="reg_date" value="<?php echo isset($fvalue['reg_date']) ? date('d/m/Y',strtotime($fvalue['reg_date'])) : '';?>">
                                                  <div class="input-group-addon">
                                                      <span class="glyphicon glyphicon-th"></span>
                                                  </div>
                                              </div>
                                            </div>
                                          </div>
                                  </div>





                                    <div class="col-lg-7">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label"><?php echo $_lang['english']['gender']; ?></label>
                                                <div class="col-sm-8">
                                                    <label  class="col-sm-6">
                                                 <input type="radio" name="fvalue[gender]" value="0" <?php echo isset($fvalue['gender']) && $fvalue['gender'] == 0 ? 'checked':'';?>> <?php echo $_lang['english']['male']; ?> / <?php echo $_lang['tamil']['male']; ?>
                                                    </label>
                                                    <label  class="col-sm-6">
                                                     <input type="radio" name="fvalue[gender]" value="1" <?php echo isset($fvalue['gender']) && $fvalue['gender'] == 1 ? 'checked':'';?>> <?php echo $_lang['english']['female']; ?> / <?php echo $_lang['tamil']['female']; ?>
                                                     </label>
                                                </div>
                                            </div>
                                    </div>

                                    <div class="col-lg-6 ">
                                        <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><?php echo $_lang['english']['language']; ?></h3>
                                            <span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
                                        </div>
                                        <div class="panel-body">

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label control-label" for="tamilName"><?php echo $_lang['english']['name']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[name]" id="tamilName" value="<?php echo isset($fenglish['name']) ? $fenglish['name'] : '';?>" required>
                                                </div>
                                            </div>



                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['sevvai']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[sevvai]" id="eedsevvai" value="<?php echo isset($fenglish['sevvai']) ? $fenglish['sevvai'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['ragukethu']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[ragukethu]" id="eedragukethu" value="<?php echo isset($fenglish['ragukethu']) ? $fenglish['ragukethu'] : '';?>">
                                                </div>
                                            </div>



                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label control-label" for="eeducationtamil"><?php echo $_lang['english']['education']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[education]" id="eeducationtamil" value="<?php echo isset($fenglish['education']) ? $fenglish['education'] : '';?>" required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['job']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[job]" id="ejob" value="<?php echo isset($fenglish['job']) ? $fenglish['job'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['fname']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[father_name]" id="efather_name" value="<?php echo isset($fenglish['father_name']) ? $fenglish['father_name'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['mname']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[mother_name]" id="emother_name" value="<?php echo isset($fenglish['mother_name']) ? $fenglish['mother_name'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['rescity']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[res_city]" id="eres_city" value="<?php echo isset($fenglish['res_city']) ? $fenglish['res_city'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['height']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[height]" id="eheight" value="<?php echo isset($fenglish['height']) ? $fenglish['height'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['color']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[color]" id="ecolor" value="<?php echo isset($fenglish['color']) ? $fenglish['color'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['kulam']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[kulam]" id="ekulam" value="<?php echo isset($fenglish['kulam']) ? $fenglish['kulam'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['kulatemple']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[kulatemple]" id="ekulatemple" value="<?php echo isset($fenglish['kulatemple']) ? $fenglish['kulatemple'] : '';?>">
                                                </div>
                                            </div>

                                          <h5><?php echo $_lang['english']['birthdet']; ?></h5>
                                          <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['tamil']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[tamil_birth]" id="etamil_birth" value="<?php echo isset($fenglish['tamil_birth']) ? $fenglish['tamil_birth'] : '';?>">
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['year']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[tamil_year]" id="ejob" value="<?php echo isset($fenglish['tamil_year']) ? $fenglish['tamil_year'] : '';?>">
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['month']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[tamil_month]" id="tamil_month" value="<?php echo isset($fenglish['tamil_month']) ? $fenglish['tamil_month'] : '';?>">
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['date']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[tamil_date]" id="etamil_date" value="<?php echo isset($fenglish['tamil_date']) ? $fenglish['tamil_date'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['nazhigai']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[tamil_nazhigai]" id="etamil_nazhigai" value="<?php echo isset($fenglish['tamil_nazhigai']) ? $fenglish['tamil_nazhigai'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['time']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fenglish[tamil_time]" id="etamil_time" value="<?php echo isset($fenglish['tamil_time']) ? $fenglish['tamil_time'] : '';?>">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                    <div class="col-lg-6">

                                        <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"> <?php echo $_lang['tamil']['language']; ?></h3>
                                            <span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
                                        </div>
                                        <div class="panel-body">

                                             <div class="form-group row">
                                                <label class="col-sm-3 col-form-label control-label" for="idnameTamil"><?php echo $_lang['tamil']['name']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[name]" id="idnameTamil" value="<?php echo isset($ftamil['name']) ? $ftamil['name'] : '';?>" required>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['sevvai']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[sevvai]" id="eedsevvai" value="<?php echo isset($ftamil['sevvai']) ? $ftamil['sevvai'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['ragukethu']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[ragukethu]" id="eedragukethu" value="<?php echo isset($ftamil['ragukethu']) ? $ftamil['ragukethu'] : '';?>">
                                                </div>
                                            </div>



                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label control-label" for="eeducationenglish"><?php echo $_lang['tamil']['education']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[education]" id="eeducationenglish" value="<?php echo isset($ftamil['education']) ? $ftamil['education'] : '';?>" required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['job']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[job]" id="ejob" value="<?php echo isset($ftamil['job']) ? $ftamil['job'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['fname']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[father_name]" id="efather_name" value="<?php echo isset($ftamil['father_name']) ? $ftamil['father_name'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['mname']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[mother_name]" id="emother_name" value="<?php echo isset($ftamil['mother_name']) ? $ftamil['mother_name'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['rescity']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[res_city]" id="eres_city" value="<?php echo isset($ftamil['res_city']) ? $ftamil['res_city'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"> <?php echo $_lang['tamil']['height']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[height]" id="eheight" value="<?php echo isset($ftamil['height']) ? $ftamil['height'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['color']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[color]" id="ecolor" value="<?php echo isset($ftamil['color']) ? $ftamil['color'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['kulam']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[kulam]" id="ekulam" value="<?php echo isset($ftamil['kulam']) ? $ftamil['kulam'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['kulatemple']; ?></label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[kulatemple]" id="ekulatemple" value="<?php echo isset($ftamil['kulatemple']) ? $ftamil['kulatemple'] : '';?>">
                                                </div>
                                            </div>

                                          <h5><?php echo $_lang['tamil']['birthdet']; ?></h5>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['tamil']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[tamil_birth]" id="etamil_birth" value="<?php echo isset($ftamil['tamil_birth']) ? $ftamil['tamil_birth'] : '';?>">
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['year']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[tamil_year]" id="ejob" value="<?php echo isset($ftamil['tamil_year']) ? $ftamil['tamil_year'] : '';?>">
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['month']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[tamil_month]" id="tamil_month" value="<?php echo isset($ftamil['tamil_month']) ? $ftamil['tamil_month'] : '';?>">
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['date']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[tamil_date]" id="etamil_date" value="<?php echo isset($ftamil['tamil_date']) ? $ftamil['tamil_date'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['nazhigai']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[tamil_nazhigai]" id="etamil_nazhigai" value="<?php echo isset($ftamil['tamil_nazhigai']) ? $ftamil['tamil_nazhigai'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['time']; ?> </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="ftamil[tamil_time]" id="etamil_time" value="<?php echo isset($ftamil['tamil_time']) ? $ftamil['tamil_time'] : '';?>">
                                                </div>
                                            </div>
                                        </div>
                                        </div>

                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                    <div class="col-sm-12">

                                        <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"> Other Details</h3>
                                            <span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
                                        </div>
                                        <div class="panel-body">



                                            <div class="row">



                                            <div class="form-group form-horizontal row col-sm-6">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['nakshatram']; ?></label>
                                                <?php $nakshatram = array(1=>"Aswini","Bharani","Karthikai",
"Rohini","Mirugasiridam","Thiruvathirai","Punarpusam","Pusam","Ayilyam","Maham","Puram","Uthiram","Atham",
"Chithirai","Swathiv","Visakam","Anusam","Ketai","Mulam", "Puradam","Uthiradam","Thiruvonam","Avitam",
"Sathayam","Puratathi","Uthiratathi","Revathi");
                                                                            ?>
                                                <div class="col-sm-9">
                                                <select class="form-control" name="fvalue[nakshatram]">
                                                    <option value="">Select</option>
                                                    <?php foreach($nakshatram as $key=>$val): ?>
                                                        <option value="<?php echo $key; ?>" <?php echo isset($fvalue['nakshatram']) && $fvalue['nakshatram'] == $key ? 'selected':'';?>><?php echo $val; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                </div>
                                            </div>
                                            <div class="form-group form-horizontal row col-sm-6">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['rasi']; ?></label>
                                                <?php $rasi = array(1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam"); ?>
                                                <div class="col-sm-9">
                                                <select class="form-control" name="fvalue[rasi]">
                                                    <option value="">Select</option>
                                                    <?php foreach($rasi as $key=>$val): ?>
                                                        <option value="<?php echo $key; ?>" <?php echo isset($fvalue['rasi']) && $fvalue['rasi'] == $key ? 'selected':'';?>><?php echo $val; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                </div>
                                            </div>

                                            </div>
                                            <div class="row">
                                            <div class="form-group form-horizontal row col-sm-6">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['lagnam']; ?></label>
                                                <?php  $Lagnam = array(1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
                                                                            ?>
                                                                            <div class="col-sm-9">
                                                <select class="form-control" name="fvalue[laknam]">
                                                    <option value="">Select</option>
                                                    <?php foreach($Lagnam as $key=>$val): ?>
                                                        <option value="<?php echo $key; ?>" <?php echo isset($fvalue['laknam']) && $fvalue['laknam'] == $key ? 'selected':'';?>><?php echo $val; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                </div>
                                            </div>
                                            <div class="form-group form-horizontal row col-sm-6">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['thisaieruppu']; ?></label>
                                                <?php $thisaiiruppu = array(1=>"Guru thisai","Sani thisai","kethu thisai","Sukura thisai","Suriya thisai","Chandra thisai","Sewwai thisai","Ragu thisai","Budhan thisai");

                                                                            ?>
                                                <div class="col-sm-9">
                                                <select class="form-control" name="fvalue[thisai]">
                                                    <option value="">Select</option>
                                                    <?php foreach($thisaiiruppu as $key=>$val): ?>
                                                        <option value="<?php echo $key; ?>" <?php echo isset($fvalue['thisai']) && $fvalue['thisai'] == $key ? 'selected':'';?>><?php echo $val; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                </div>
                                            </div>
                                         </div>
                                        </div>
                                    </div>



                                    <?php
                                    $jatsquare = array('rasi'=>array(),'amsam'=>array());
                                    if(!empty($fvalue['jatsquare'])) {
                                        $jatsquare = json_decode($fvalue['jatsquare'],true);
                                    } ?>
                                    <div class="row">

                                    <div class="form-horizontal  col-sm-6">
                                    <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"> Rasi </h3>
                                        <span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
                                    </div>
                                    <div class="panel-body">

                                                <div class="col-sm-12 ">
                                                <?php
                                                $data = array('Laknam','Sun','Budhan','Sukuran','Sevvai','Ragu','Kethu','Guru','Sandran','Sani','Manthi');
                                                $range = range(0,11);


                                                foreach($data as $key=>$value) { ?>

                                                <div class="col-xs-3" style="padding-left:0px !important" >
                                                    <label class="control-label col-xs-7"> <?php echo $value; ?></label>
                                                    <div class="col-xs-5">
                                                        <select class="selectbox" name="fvalue[jatsquare][rasi][]"  data-name="<?php echo $value; ?>">
                                                        <option value="0"></option>
                                                        <?php foreach ($range as $v) { ?>
                                                            <option value="<?php echo $v+1; ?>" <?php echo isset($jatsquare['rasi'][$key]) && $jatsquare['rasi'][$key] == $v+1 ? 'selected' :''; ?> data-value="<?php echo $value; ?>"><?php echo $v+1; ?></option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                </div>
                                                <div>&nbsp;</div>


                                                <div class="col-sm-12">
                                                <div class="row">
                                                    <div class='col-sm-3 hclass' id="box12">  </div>
                                                    <div class='col-sm-3 hclass right-nob left-nob'  id="box1">   </div>
                                                    <div class='col-sm-3 hclass right-nob'  id="box2">  </div>
                                                    <div class='col-sm-3 hclass'  id="box3"> </div>
                                                </div>
                                                <div class="row rasi">

                                                    <div class="col-sm-3" style="height:180px;padding-left:0px;padding-right:0px">
                                                         <div class='col-sm-12 hclass top-nob bottom-nob' id="box11">  </div>
                                                         <div class='col-sm-12 hclass bottom-nob' id="box10">  </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div align="center" style="height:180px;">  </div>
                                                    </div>
                                                    <div class="col-sm-3" style="height:180px;padding-left:0px;padding-right:0px">
                                                         <div class='col-sm-12 hclass top-nob bottom-nob' id="box4">  </div>
                                                         <div class='col-sm-12 hclass bottom-nob' id="box5">  </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class='col-sm-3 hclass' id="box9">  </div>
                                                    <div class='col-sm-3 hclass left-nob' id="box8">   </div>
                                                    <div class='col-sm-3 hclass left-nob right-nob' id="box7">  </div>
                                                    <div class='col-sm-3 hclass' id="box6"> </div>
                                                </div>
                                                </div>
                                    </div>
                                    </div>
                                        </div>



                                    <div class="form-horizontal col-sm-6 ">
                                        <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"> Amsam </h3>
                                        <span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-sm-12">

                                                <?php
                                                $data = array('Laknam','Sun','Budhan','Sukuran','Sevvai','Ragu','Kethu','Guru','Sandran','Sani','Manthi');
                                                $range = range(0,11);
                                                foreach($data as $key=>$value) { ?>

                                                <div class="col-xs-3" style="padding-left:0px !important" >
                                                    <label class="control-label col-xs-7"> <?php echo $value; ?></label>
                                                    <div class="col-xs-5">
                                                        <select class="selectboxamsam" name="fvalue[jatsquare][amsam][]" data-name="<?php echo $value; ?>">
                                                        <option value="0"></option>
                                                        <?php foreach ($range as $k=>$v) { ?>
                                                            <option value="<?php echo $v+1; ?>" <?php echo isset($jatsquare['amsam'][$key]) && $jatsquare['amsam'][$key] == $v+1 ? 'selected' :''; ?> data-value="<?php echo $value; ?>"><?php echo $v+1; ?></option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                        </div>
                                        <div>&nbsp;</div>

                                                    <div class="col-sm-12">
                                                <div class="row">
                                                    <div class='col-sm-3 hclass' id="boxamsam12">   </div>
                                                    <div class='col-sm-3 hclass right-nob left-nob'  id="boxamsam1">   </div>
                                                    <div class='col-sm-3 hclass right-nob'  id="boxamsam2">  </div>
                                                    <div class='col-sm-3 hclass'  id="boxamsam3"> </div>
                                                </div>
                                                <div class="row amsam">

                                                    <div class="col-sm-3" style="height:180px;padding-left:0px;padding-right:0px">
                                                         <div class='col-sm-12 hclass top-nob bottom-nob' id="boxamsam11">  </div>
                                                         <div class='col-sm-12 hclass bottom-nob' id="boxamsam10">  </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div align="center" style="height:180px;">   </div>
                                                    </div>
                                                    <div class="col-sm-3" style="height:180px;padding-left:0px;padding-right:0px">
                                                         <div class='col-sm-12 hclass top-nob bottom-nob' id="boxamsam4">  </div>
                                                         <div class='col-sm-12 hclass bottom-nob' id="boxamsam5">  </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class='col-sm-3 hclass' id="boxamsam9">  </div>
                                                    <div class='col-sm-3 hclass left-nob' id="boxamsam8">   </div>
                                                    <div class='col-sm-3 hclass left-nob right-nob' id="boxamsam7">  </div>
                                                    <div class='col-sm-3 hclass' id="boxamsam6"> </div>
                                                </div>
                                            </div>
                                    </div>

                                        </div>

                                    </div>
                                    </div>
                                    <div class="col-sm-10">
                                    <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"> Other Details 2 </h3>
                                        <span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
                                    </div>
                                    <div class="panel-body">




                                         <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['tamil']['aninco']; ?></label>
                                                <?php $aninco = array(1=>"Below 1lakh","1Lakh - 2lakhs","2lakhs - 4lakhs","4lakhs - 6lakhs","6lakhs - 10lakhs","10lakhs - 20lakhs","Above 20lakhs","Others"); ?>
                                                <div class="col-sm-9">
                                                <select class="form-control" name="fvalue[annual_income]">
                                                    <option value="">Select</option>
                                                    <?php foreach($aninco as $key=>$val): ?>
                                                        <option value="<?php echo $key; ?>" <?php echo isset($fvalue['annual_income']) && $fvalue['annual_income'] == $key ? 'selected':'';?> ><?php echo $val; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['jathagam']; ?></label>
                                                <?php $rasis = array(1=>"Clean Horoscope","Sewwai","Ragu Kethusewwai","Ragu Kethu"); ?>
                                                <div class="col-sm-9">
                                                <select class="form-control" name="fvalue[jathagam]">
                                                    <option value="">Select</option>
                                                    <?php foreach($rasis as $key=>$val): ?>
                                                        <option value="<?php echo $key; ?>" <?php echo isset($fvalue['jathagam']) && $fvalue['jathagam'] == $key ? 'selected':'';?>><?php echo $val; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['balance']; ?> / இருப்பு  </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fvalue[balance]" id="balance" value="<?php echo isset($fvalue['balance']) ? $fvalue['balance'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['year1']; ?>  </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fvalue[byear]" id="byear" value="<?php echo isset($fvalue['byear']) ? $fvalue['byear'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['month1']; ?>  </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fvalue[month]" id="month" value="<?php echo isset($fvalue['month']) ? $fvalue['month'] : '';?>">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['info']; ?>  </label>
                                                <div class="col-sm-9">
                                                <textarea class="form-control" rows="3" name="fvalue[information]"><?php echo isset($fvalue['information']) ? $fvalue['information'] : '' ; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['expect']; ?>  </label>
                                                <div class="col-sm-9">
                                                <textarea class="form-control" rows="3" name="fvalue[expectation]"><?php echo isset($fvalue['expectation']) ? $fvalue['expectation'] : '' ; ?></textarea>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['dist']; ?>  </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm" name="fvalue[district]" id="district" value="<?php echo isset($fvalue['district']) ? $fvalue['district'] : '';?>">
                                                </div>
                                            </div>



                                            <div class="form-group row">

                                            <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['dob']; ?></label>
                                            <div class="form-group input-group date col-sm-9" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                <input class="form-control input-sm datepicker" name="fvalue[dob]" id="dob" value="<?php echo isset($fvalue['dob']) ? date('d/m/Y',strtotime($fvalue['dob'])) : '';?>">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>

                                            </div>
                                            </div>
                                        </div>
                                    </div>



<div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"> <?php echo $_lang['english']['familpar']; ?> </h3>
                                        <span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
                                    </div>
                                    <div class="panel-body">


                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">
                                                    <?php echo $_lang['english']['brother']; ?>  </label>
                                                <label class="col-sm-4 col-form-label text-right">
                                                <?php echo $_lang['english']['married']; ?> :
                                                </label>
                                                <div class="col-sm-1" align="left">

                                                <input class="form-control input-sm" name="fvalue[bromarried]" id="bromarried" value="<?php echo isset($fvalue['bromarried']) ? $fvalue['bromarried'] : '';?>">
                                                </div>
                                                <label class="col-sm-4 col-form-label text-right">
                                                <?php echo $_lang['english']['unmarried']; ?> : </label>
                                                <div class="col-sm-1">

                                                <input class="form-control input-sm" name="fvalue[brounmarried]" id="brounmarried" value="<?php echo isset($fvalue['brounmarried']) ? $fvalue['brounmarried'] : '';?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label"><?php echo $_lang['english']['sister']; ?>  </label>

                                                <label class="col-sm-4 col-form-label  text-right">
                                                <?php echo $_lang['english']['married']; ?> :
                                                </label>
                                                <div class="col-sm-1">
                                                <input class="form-control input-sm" name="fvalue[sismarried]" id="sismarried" value="<?php echo isset($fvalue['sismarried']) ? $fvalue['sismarried'] : '';?>">
                                                </div>
                                                <label class="col-sm-4 col-form-label  text-right">
                                                <?php echo $_lang['english']['unmarried']; ?> :
                                                </label>
                                                <div class="col-sm-1">
                                                <input class="form-control input-sm" name="fvalue[sisunmarried]" id="sisunmarried" value="<?php echo isset($fvalue['sisunmarried']) ? $fvalue['sisunmarried'] : '';?>">
                                                </div>
                                            </div>
                                    </div>
</div>

<div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"> <?php echo $_lang['english']['familyproperty']; ?> </h3>
                                        <span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
                                    </div>
                                    <div class="panel-body">

                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['house']; ?>  </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm"  name="fvalue[house]" value="<?php echo isset($fvalue['house']) ? $fvalue['house'] : '' ; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['land']; ?>  </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm"  name="fvalue[land]" value="<?php echo isset($fvalue['land']) ? $fvalue['land'] : '' ; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['vehicle']; ?>  </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm"  name="fvalue[vehicle]" value="<?php echo isset($fvalue['vehicle']) ? $fvalue['vehicle'] : '' ; ?>">
                                                </div>
                                            </div>
                                            <h4><?php echo $_lang['english']['value']; ?></h4>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['value1']; ?>  </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm"  name="fvalue[provalue1]" value="<?php echo isset($fvalue['provalue1']) ? $fvalue['provalue1'] : '' ; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['value2']; ?>  </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm"  name="fvalue[provalue2]" value="<?php echo isset($fvalue['provalue2']) ? $fvalue['provalue2'] : '' ; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['value3']; ?>  </label>
                                                <div class="col-sm-9">
                                                <input class="form-control input-sm"  name="fvalue[provalue3]" value="<?php echo isset($fvalue['provalue3']) ? $fvalue['provalue3'] : '' ; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label"><?php echo $_lang['english']['tav']; ?>  </label>
                                                <div class="col-sm-9">
                                                <textarea class="form-control" rows="3" name="fvalue[totalpro]"><?php echo isset($fvalue['totalpro']) ? $fvalue['totalpro'] : '' ; ?></textarea>
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                             <label class="col-sm-3 col-form-label">Select File</label>
                                             <div class="col-sm-9">
                                                <input id="input-4" name="image" type="file" multiple class="file-loading">
                                                <?php if(!empty($fvalue['image'])) { ?>
                                                <a href="<?php echo $site_url.'uploads/'.$fvalue['image']; ?>" target="_blank"> <?php echo $fvalue['image']; ?> </a>
                                                <?php };?>
                                             </div>
                                             </div>
                                    </div>
</div>
                                            <div class="form-group" align="right">

                                                <input type="submit" value="Save" name="Submit" class="btn btn-small">
                                            </div>









                                    </div>


                                </div>
                                <!-- /.row (nested) -->
                                </form>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
<style>

                                .hclass {
                                  padding-right: 0px !important;
                                  padding-left:0px !important;
                                  float:left !important;
                                  border:1px solid #cdcdcd;
                                  height:90px;
                                }
                                .classddd div, .classddd label  {
                                    margin-bottom: 1px !important;
                                }
                                .right-nob {
                                    border-right:0px;
                                }
                                .left-nob {
                                    border-left:0px;
                                }
                                .top-nob {
                                    border-top:0px;
                                }
                                .bottom-nob {
                                    border-bottom:0px;
                                }
                                .selectbox,.selectboxamsam {
                                    margin-top:7px;
                                }
                                label {
                                    font-weight: normal;
                                }
                                #box1,#boxamsam1 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='40' y='55' fill='lightgrey' font-size='50'>1</text></svg>");
                                }
                                #box2,#boxamsam2 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='40' y='55' fill='lightgrey' font-size='50'>2</text></svg>");
                                }
                                #box3,#boxamsam3 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='40' y='55' fill='lightgrey' font-size='50'>3</text></svg>");
                                }
                                #box4,#boxamsam4 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='40' y='55' fill='lightgrey' font-size='50'>4</text></svg>");
                                }
                                #box5,#boxamsam5 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='40' y='55' fill='lightgrey' font-size='50'>5</text></svg>");
                                }
                                #box6,#boxamsam6 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='40' y='55' fill='lightgrey' font-size='50'>6</text></svg>");
                                }
                                #box7,#boxamsam7 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='40' y='55' fill='lightgrey' font-size='50'>7</text></svg>");
                                }
                                #box8,#boxamsam8 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='40' y='55' fill='lightgrey' font-size='50'>8</text></svg>");
                                }
                                #box9,#boxamsam9 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='40' y='55' fill='lightgrey' font-size='50'>9</text></svg>");
                                }
                                #box10,#boxamsam10 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='30' y='55' fill='lightgrey' font-size='50'>10</text></svg>");
                                }
                                #box11,#boxamsam11 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='30' y='55' fill='lightgrey' font-size='50'>11</text></svg>");
                                }
                                #box12,#boxamsam12 {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='30' y='55' fill='lightgrey' font-size='50'>12</text></svg>");
                                }


                                .rasi {
                                    background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='150' y='50' fill='lightgrey' font-size='50'>Rasi</text></svg>");background-repeat: no-repeat;
                                }
                                .amsam {
                                 background-image:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='80px'><text x='130' y='50' fill='lightgrey' font-size='50'>Amsam</text></svg>");background-repeat: no-repeat;
                                }

.clickable
{
    cursor: pointer;
}

.clickable .glyphicon
{
    background: rgba(0, 0, 0, 0.15);
    display: inline-block;
    padding: 6px 12px;
    border-radius: 4px
}

.panel-heading span
{
    margin-top: -23px;
    font-size: 15px;
    margin-right: -9px;
}
a.clickable { color: inherit; }
a.clickable:hover { text-decoration:none; }

                                </style><script type="text/javascript">

    $(document).on('click', '.panel-heading span.clickable', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).on('click', '.panel div.clickable', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});



    function fillBoxes(flag) {

        $("div[id^='minbox"+flag+"']").remove();
        $.each($('.selectbox' + flag), function( index, value ) {
            //$(this).val();$(this).data('name')
            if($(this).val() != 0) {
                $('#box' + flag + $(this).val()).append('<div id="minbox' + flag + $(this).data('name')+'">'+$(this).data('name')+'</div>');
            }
        });
    }


$(function(){
    //alert('te');
    fillBoxes('');
    fillBoxes('amsam');


    //$('.panel-heading span.clickable').click();
    //$('.panel div.clickable').click();

    //$('.datepicker').datepicker({ format: 'dd/mm/yyyy', startDate: '-3d' });




});


    $(".selectbox").on('change',function() {

        fillBoxes('');
    });
    $(".selectboxamsam").on('change',function() {

        fillBoxes('amsam');

        /*var name = $(this).data('name');
        var box = $(this).val();

        if(prev_value1 != '' && $('#minboxamsam'+prev_value1)) {
            $('#minboxamsam'+prev_value1).remove();
        }

        $('#boxamsam' + box).append('<div id="minboxamsam'+name+'">'+name+'</div>');

        prev_value1 = '';*/
    });
</script>
