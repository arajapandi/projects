            <div id="page-wrapper" style="min-height: 631px;">
                <div class="row">
                    <div class="col-lg-10">
                        <h1 class="page-header"> Packages </h1>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group"> &nbsp; </div>
                        <input type="button" onclick="location.href='<?php echo $url; ?>packages/edit'" value="Add New Pack" name="Submit" class="btn btn-large">
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                <?php if(!empty($message)): ?>
                  <div class="alert alert-info">
                    <?php echo $message; ?>
                  </div>
                <?php endif; ?>
                <div class="tab-content col-md-12">
                
                <div id="sent" class="tab-pane fade active in">

                   <table class="table table-stripped" id="sentTbl">
                      <thead>
                        <tr>
                          <th width="20%">Uniq No</th>
                          <th width="40%">Name</th>
                          <th width="10%">Days</th>
                          <th width="10%">Views</th>
                          <th width="10%">Cost</th>
                          <th width="10%">##</th>
                        </tr>
                      </thead>
                      <tbody>
                      
                      <?php if(!empty($profiles)): 
                        foreach($profiles as $data): ?>
                        <tr class="<?php echo $data['status'] == 0 ? "active":''; ?>">
                            <td scope="row"><?php echo $data['id']; ?></td>
                          <td><?php echo $data['name']; ?></td>
                          <td><?php echo $data['days']; ?></td>
                          <td><?php echo $data['profile_view']; ?></td>
                          <td><?php echo $data['cost']; ?></td>
                          <td>
                            <a href="packages/edit?id=<?php echo $data['id']; ?>" class="fwdMessage" data-index="0" data-toggle="tooltip" title="Edit"><span class="glyphicon glyphicon-edit"></span></a> &nbsp;
                            <a href="packages/<?php echo $data['status'] == 0 ? "activate":'deactivate'; ?>?id=<?php echo $data['id']; ?>" class="fwdMessage" data-index="0" data-toggle="tooltip" title="Disable"><span class="glyphicon glyphicon-off"></span></a> &nbsp;
                            <a href="packages/delete?id=<?php echo $data['id']; ?>" class="fwdMessage" data-index="0" data-toggle="tooltip" title="Delete"><span class="glyphicon glyphicon-remove"></span></a> &nbsp;

                          </td>
                        </tr>
                      <?php endforeach;
                        endif; ?>
                        <?php if(!empty($pagination)): ?>
                        
                        <?php endif; ?>
                    </table>
                    <div class="pull-right">
                    <?php echo $pagination; ?>
                    </div>
                         
                </div>


              </div>
              </div>