<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Snippet - Bootsnipp.com</title>
        <link href='//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css' rel='stylesheet'>
        <style></style>
        <script type='text/javascript' src='//code.jquery.com/jquery-1.10.2.min.js'></script>
        <script type='text/javascript' src='//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js'></script>
        <script type='text/javascript'>
            var baseURL = "<?php echo base_url(); ?>";
        </script>
        <script type='text/javascript' src='<?php echo str_replace('apanel.php','',base_url()); ?>application/apanel/views/js/app.js'></script>
        <script type='text/javascript'>
            jQuery(document).ready(function() {
                App.Controller.loginInit();
            });
        </script>
    </head>
    <body>

        <div class="container">
            <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Sign In</div>

                    </div>

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                        <form id="loginform" class="form-horizontal" role="form">

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="inp-user" type="text" class="form-control" name="username" placeholder="Username">
                            </div>

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input id="inp-passwd" type="password" class="form-control" name="passwd" placeholder="Password">
                            </div>




                            <div style="margin-top:10px" class="form-group">

                                <div class="col-sm-12 controls">
                                    <a id="btn-login" href="javascript:;" class="btn btn-success">Login  </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
