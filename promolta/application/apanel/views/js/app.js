var App = {};
var cl = function(text, data) {
    console.info(text, data);
}

App.Controller = {
        
        loginInit: function () {
            $( "#btn-login" ).click(this.__validateLogin());
        },
        //private function -- not to be here we have to move it to function.. 
        __validateLogin:function() {
            $( "#btn-login" ).click(function() {

                var username = $('#inp-user').val();
                var passwd = $('#inp-passwd').val();
                if(username && passwd) {

                    App.Model.loginCall(username, passwd);
                }
            });
        },
        messageInit:function() {

            //App.Model.getAccounts();
            alert('test');
            
            $(document).on('click', '.fwdMessage', App.Controller.__forwardOrTrash('forward'));
            $(document).on('click', '.mvMessage', App.Controller.__forwardOrTrash('trash'));
            $(document).on('click', '.mrkMessage', App.Controller.__forwardOrTrash('mark'));
            $(document).on('click', '.submitData', App.Controller.__submitMessage);
            



                var substringMatcher = function(strs) {
                  return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function(i, str) {
                      if (substrRegex.test(str)) {
                        matches.push(str);
                      }
                    });

                    cb(matches);
                  };
                };



                $('#tomessage').tagsinput(
                {
                    hint: true,
                    freeInput:false,
                    highlight: true,
                    minLength: 1,
                    typeaheadjs:{  
                        name: 'states',
                        source: substringMatcher(userDataArray)}
                  
                    }
            );
            

            $(document).on('click', '.menustack li a', function(){
                var attr = $(this).data('attr');
                App.Model[attr+'Call']();
            });

            //if(userId && authData) {
            App.Model.inboxCall();
            //}
        },
        __forwardOrTrash:function(flag) {

            return function(){


                var index = $(this).data('index');
                var message = App.Model.getMessageByIndex(index);
                
                if(flag == 'forward' && message) {
                    App.Model.openFwdWindow(message);
                } else if(flag=='trash' && message) {
                    message.deleteMessage = $(this).data('delete');
                    App.Controller._moveToTrash(message);
                } else if(flag=='mark' && message) {
                    var mark = $(this).data('mark');
                    if(mark == 1) {
                        $(this).data("mark", 0);
                        $(this).closest("tr").removeClass('active');
                    } else {
                        $(this).data("mark", 1);
                        $(this).closest("tr").addClass('active');
                    }
                    App.Controller._markMessage(message);
                }
            }
        },
        _moveToTrash:function(message) {
           App.Model.moveToTrash(message.messageId, message.deleteMessage);
        },
        _markMessage:function(message) {
          App.Model.markMessage(message.messageId);  
        },
        __submitMessage:function() {

            alert($('#messageForm').serialize());
            
        }

        
}

 App.Model = {
        localStore:{},
        markAsRead:function(messageId) {

        },
        getAccounts:function() {
             var callback = function(data) {
                if(data.success == true && data.data) {
                    $.each( data.data, function( key, value ) {
                        //cl('account', value.value);
                        userDataArray.push[value.value];
                    });
                }
            };

            App.Model.callServer('message', {'call':'getAccounts'}, callback);
        },
        openFwdWindow:function(message) {
            message
            $('#myModal').modal()
        },
        markMessage:function(messageId) {
            var callback = function(data) {
                if(data.success == true ) {
                    //do nothing... 
                } else {
                    App.View.errorMessage('Error - Something went wrong...');
                }

            };

            App.Model.callServer('message', {'call':'markMessage','messageId':messageId}, callback);
        },
        moveToTrash:function(messageId, deleteMessage) {
            var callback = function(data) {

                if(data.success == true) {
                    if(deleteMessage == "yes") {
                        App.Model.trashCall();
                    } else {
                        App.Model.inboxCall();
                    }
                } else {
                    App.View.errorMessage('Error - Something went wrong...');
                }
            };

            App.Model.callServer('message', {'messageId':messageId,'delete':deleteMessage,'call':'trashMessage'}, callback);
        },
        getMessageByIndex: function(index) {
            var messages = App.Model.localStore['messages'];
            cl('message', messages);
            if(messages && messages[index]) {
                cl('getMessageByIndex',messages[index]);
                return messages[index];
            } else {
                App.View.errorMessage('Error reading mail... ');
            }
            return false;
        },
        loginCall:function(username, passwd) {
            $.ajax({
                method: "POST",
                url: baseURL+"login/loginnow",
                data: {username: username, passwd: passwd},
            }).done(function(data) {

                data = jQuery.parseJSON( data );
                if(data.success == false) {

                    App.View.errorMessage('Error - Please try again.. ');
                } else {
                    location.href=baseURL+'messages'
                }
            });
        },
        draftCall:function(messageId) {
            var callback = function(data) {

                if(data.success == true && data.data) {
                    //App.Model.localStore['draft'] = data.data;
                    App.Model.localStore['messages'] = data.data;
                    App.View.assignDraft(data.data);
                } else {
                    App.View.errorDraft();
                }
            };

            App.Model.callServer('message', {'call':'getDraft'}, callback);
        },
        sentCall:function(messageId) {
            var callback = function(data) {

                if(data.success == true && data.data) {
                    //App.Model.localStore['sent'] = data.data;
                    App.Model.localStore['messages'] = data.data;
                    App.View.assignSent(data.data);
                } else {
                    App.View.errorSent();
                }
            };

            App.Model.callServer('message', {'call':'getSent'}, callback);
        },
        inboxCall:function() {

            var callback = function(data) {

                if(data.success == true && data.data) {
                    App.Model.localStore['messages'] = data.data;
                    App.View.assignInbox(data.data);
                } else {
                    App.View.errorInbox();
                }
            };

            App.Model.callServer('message', {'call':'getInbox'}, callback);
        
        },
        trashCall:function() {

             var callback = function(data) {

                if(data.success == true && data.data) {
                    //App.Model.localStore['trash'] = data.data;
                    console.log(data.data);
                    App.Model.localStore['messages'] = data.data;
                    App.View.assignTrash(data.data);
                    
                } else {
                    App.View.errorTrash();
                }
            };

            App.Model.callServer('message', {'call':'getTrash'}, callback);
        
        },
        callServer:function(url, params, callback) {
            if(userId && authData) {
                params.user = userId;
                params.auth = authData;
                
                $.ajax({
                    method: "POST",
                    url: "/promolta/server.php/" + url,
                    data: params
                }).done(callback);
            }
        }


 }
 App.View = {
    errorMessage:function(message) {

            alert(message);
    },
    assignInbox:function(data) {
        $('#inboxTbl tbody').html('');
        $.each( data, function( key, subject ) {

            var html = '<tr '+(subject.mark == 1 ? 'class="active"':'')+'><th scope="row">'+
                        subject.to_accounts+'</th><td>'+
                        subject.subject+'</td> <td>'+
                        subject.created_at+'</td> <td>'+
                        '<a href="javascript:;" class="mrkMessage" data-index="'+key+'" data-mark="'+subject.mark+'" data-toggle="tooltip" title="Mark as Read"><span class="glyphicon glyphicon-ok"></span></a> &nbsp;'+
                        '<a href="javascript:;" class="fwdMessage" data-index="'+key+'" data-toggle="tooltip" title="Forward"><span class="glyphicon glyphicon-forward"></span></a> &nbsp;'+
                        '<a href="javascript:;" class="mvMessage" data-index="'+key+'" data-toggle="tooltip" title="Move To Trash"><span class="glyphicon glyphicon-trash"></span></a></td></tr>';
            $('#inboxTbl tbody').append(html);
        });
    },
    errorInbox:function() {
        $('#inboxTbl tbody').html('');
        var html = '<tr><td colspan="4">Error - No message found</td></tr>';
        $('#inboxTbl tbody').append(html);
    },
    assignSent:function(data) {
        $('#sentTbl tbody').html('');
        $.each( data, function( key, subject ) {

            var html = '<tr class="active"><th scope="row">'+
                        subject.from+'</th><td>'+
                        subject.subject+'</td> <td>'+
                        subject.created_at+'</td> <td>'+
                        '<a href="javascript:;" class="fwdMessage" data-index="'+key+'" data-toggle="tooltip" title="Forward"><span class="glyphicon glyphicon-forward"></span></a> &nbsp;</td></tr>';
                        //'<a href="javascript:;" class="mvMessage" data-index="'+key+'" data-toggle="tooltip" title="Move To Trash"><span class="glyphicon glyphicon-trash"></span></a>';
            $('#sentTbl tbody').append(html)
        });
    },
    errorSent:function() {
        $('#sentTbl tbody').html('');
        var html = '<tr><td colspan="4">Error - No message found</td></tr>';
        $('#sentTbl tbody').append(html);
    },
    assignDraft:function(data) {
        $('#draftTbl tbody').html('');
        $.each( data, function( key, subject ) {

            var html = '<tr class="active"><th scope="row">'+
                        subject.to_accounts+'</th><td>'+
                        ((!subject.subject || subject.subject == '') ? 'no subject..' :  subject.subject)+'</td> <td>'+
                        subject.created_at+'</td> <td>'+
                        '<a href="javascript:;" class="fwdMessage" data-index="'+key+'" data-toggle="tooltip" title="Open.."><span class="glyphicon glyphicon-forward"></span></a> &nbsp;'+
                        '<a href="javascript:;" class="mvMessage" data-index="'+key+'" data-toggle="tooltip" title="Move To Trash"><span class="glyphicon glyphicon-trash"></span></a></td></tr>';
            $('#draftTbl tbody').append(html)
        });
    },
    errorDraft:function() {
        $('#draftTbl tbody').html('');
        var html = '<tr><td colspan="4">Error - No message found</td></tr>';
        $('#draftTbl tbody').append(html);
    },
    assignTrash:function(data) {
        $('#trashTbl tbody').html('');

        $.each( data, function( key, subject ) {
                    
            var html = '<tr><th scope="row">'+
                    subject.from+'</th><td>'+
                    subject.subject+'</td> <td>'+
                    subject.created_at+'</td> <td>'+
                    '<a href="javascript:;" class="fwdMessage" data-index="'+key+'"><span class="glyphicon glyphicon-forward"></span></a> &nbsp;'+
                    '<a href="javascript:;" class="mvMessage" data-index="'+key+'" data-delete="yes" data-toggle="tooltip" title="Delete forever"><span class="glyphicon glyphicon-trash"></span></a></td></tr>';
            $('#trashTbl tbody').append(html);
        });
    },
    errorTrash:function() {
        $('#trashTbl tbody').html('');
        var html = '<tr><td colspan="4">Error - No message found</td></tr>';
        $('#trashTbl tbody').append(html);
    }
 }

