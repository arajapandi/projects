<?php
$name='ind_ta_1_001';
$type='TTF';
$desc=array (
  'CapHeight' => 729.0,
  'XHeight' => 547.0,
  'FontBBox' => '[-377 -370 2422 879]',
  'Flags' => 4,
  'Ascent' => 879.0,
  'Descent' => -236.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 87.0,
  'MissingWidth' => 500.0,
);
$unitsPerEm=2048;
$up=-188;
$ut=59;
$strp=259;
$strs=50;
$ttffile='/Mydrive/htdocs/promolta/application/client/libraries/mpdf/ttfonts/ind_ta_1_001.ttf';
$TTCfontID='0';
$originalsize=85328;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='ind_ta_1_001';
$panose=' 0 0 2 b 6 6 3 8 4 2 2 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 760, -240, 200
// usWinAscent/usWinDescent = 928, -236
// hhea Ascent/Descent/LineGap = 879, -488, 0
$useOTL=0;
$rtlPUAstr='';
?>