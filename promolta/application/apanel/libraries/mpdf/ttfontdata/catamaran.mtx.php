<?php
$name='Catamaran-Light';
$type='TTF';
$desc=array (
  'CapHeight' => 680.0,
  'XHeight' => 485.0,
  'FontBBox' => '[-535 -377 2503 1320]',
  'Flags' => 4,
  'Ascent' => 1100.0,
  'Descent' => -377.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 71.0,
  'MissingWidth' => 645.0,
);
$unitsPerEm=1000;
$up=-100;
$ut=50;
$strp=291;
$strs=50;
$ttffile='/Mydrive/htdocs/promolta/application/client/libraries/mpdf/ttfonts/Catamaran-Light.ttf';
$TTCfontID='0';
$originalsize=70832;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='catamaran';
$panose=' 0 0 0 0 4 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=true;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 770, -230, 0
// usWinAscent/usWinDescent = 1100, -540
// hhea Ascent/Descent/LineGap = 1100, -540, 0
$useOTL=15;
$rtlPUAstr='';
$GSUBScriptLang=array (
  'taml' => 'DFLT ',
  'tml2' => 'DFLT ',
);
$GSUBFeatures=array (
  'taml' => 
  array (
    'DFLT' => 
    array (
      'psts' => 
      array (
        0 => 0,
        1 => 2,
        2 => 3,
        3 => 4,
        4 => 5,
        5 => 6,
        6 => 7,
        7 => 8,
        8 => 9,
      ),
      'abvs' => 
      array (
        0 => 1,
      ),
      'ss01' => 
      array (
        0 => 10,
      ),
      'ss02' => 
      array (
        0 => 11,
      ),
    ),
  ),
  'tml2' => 
  array (
    'DFLT' => 
    array (
      'psts' => 
      array (
        0 => 0,
        1 => 2,
        2 => 3,
        3 => 4,
        4 => 5,
        5 => 6,
        6 => 7,
        7 => 8,
        8 => 9,
      ),
      'abvs' => 
      array (
        0 => 1,
      ),
      'ss01' => 
      array (
        0 => 10,
      ),
      'ss02' => 
      array (
        0 => 11,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 1758,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 1810,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 1836,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2010,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 4,
    'Subtables' => 
    array (
      0 => 2148,
      1 => 2166,
      2 => 2184,
      3 => 2202,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2220,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2238,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2256,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2274,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 3,
    'Subtables' => 
    array (
      0 => 2292,
      1 => 2310,
      2 => 2328,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2346,
    ),
    'MarkFilteringSet' => '',
  ),
  11 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2364,
    ),
    'MarkFilteringSet' => '',
  ),
  12 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2382,
    ),
    'MarkFilteringSet' => '',
  ),
  13 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2404,
    ),
    'MarkFilteringSet' => '',
  ),
  14 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2410,
    ),
    'MarkFilteringSet' => '',
  ),
  15 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2420,
    ),
    'MarkFilteringSet' => '',
  ),
  16 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2426,
    ),
    'MarkFilteringSet' => '',
  ),
  17 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2432,
    ),
    'MarkFilteringSet' => '',
  ),
  18 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2442,
    ),
    'MarkFilteringSet' => '',
  ),
  19 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 2448,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'DFLT' => 'DFLT ',
  'taml' => 'DFLT ',
  'tml2' => 'DFLT ',
);
$GPOSFeatures=array (
  'DFLT' => 
  array (
    'DFLT' => 
    array (
      'abvm' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'kern' => 
      array (
        0 => 2,
      ),
    ),
  ),
  'taml' => 
  array (
    'DFLT' => 
    array (
      'abvm' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'kern' => 
      array (
        0 => 2,
      ),
    ),
  ),
  'tml2' => 
  array (
    'DFLT' => 
    array (
      'abvm' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'kern' => 
      array (
        0 => 2,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 564,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 782,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 992,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>