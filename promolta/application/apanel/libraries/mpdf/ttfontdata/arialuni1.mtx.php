<?php
$name='ArialUnicodeMS';
$type='TTF';
$desc=array (
  'CapHeight' => 716.0,
  'XHeight' => 518.0,
  'FontBBox' => '[-1011 -330 2260 1078]',
  'Flags' => 4,
  'Ascent' => 1069.0,
  'Descent' => -271.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 87.0,
  'MissingWidth' => 1000.0,
);
$unitsPerEm=2048;
$up=-100;
$ut=50;
$strp=309;
$strs=50;
$ttffile='/Mydrive/htdocs/promolta/application/client/libraries/mpdf/ttfonts/ARIALUNI.ttf';
$TTCfontID='0';
$originalsize=23275812;
$sip=true;
$smp=false;
$BMPselected=false;
$fontkey='arialuni1';
$panose=' 8 5 2 b 6 4 2 2 2 2 2 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 728, -210, 132
// usWinAscent/usWinDescent = 1069, -271
// hhea Ascent/Descent/LineGap = 1069, -271, 0
$useOTL=255;
$rtlPUAstr='\x{0E5CD}-\x{0E5D2}\x{0E818}\x{0E864}\x{0E867}\x{0E890}\x{0E8A5}-\x{0E8A7}';
$GSUBScriptLang=array (
  'arab' => 'DFLT FAR  URD  ',
  'deva' => 'DFLT ',
  'gujr' => 'DFLT ',
  'guru' => 'DFLT ',
  'hani' => 'DFLT JAN  KOR  ZHS  ZHT  ',
  'kana' => 'DFLT JAN  ',
  'knda' => 'DFLT ',
  'taml' => 'DFLT ',
);
$GSUBFeatures=array (
  'arab' => 
  array (
    'DFLT' => 
    array (
      'isol' => 
      array (
        0 => 0,
      ),
      'init' => 
      array (
        0 => 1,
      ),
      'medi' => 
      array (
        0 => 2,
      ),
      'fina' => 
      array (
        0 => 3,
      ),
      'liga' => 
      array (
        0 => 4,
        1 => 5,
        2 => 6,
      ),
    ),
    'FAR ' => 
    array (
      'isol' => 
      array (
        0 => 7,
      ),
      'init' => 
      array (
        0 => 1,
      ),
      'medi' => 
      array (
        0 => 2,
      ),
      'fina' => 
      array (
        0 => 8,
      ),
      'liga' => 
      array (
        0 => 4,
        1 => 5,
        2 => 6,
      ),
      'locl' => 
      array (
        0 => 9,
      ),
    ),
    'URD ' => 
    array (
      'isol' => 
      array (
        0 => 10,
      ),
      'init' => 
      array (
        0 => 11,
      ),
      'medi' => 
      array (
        0 => 12,
      ),
      'fina' => 
      array (
        0 => 13,
      ),
      'liga' => 
      array (
        0 => 4,
        1 => 5,
        2 => 6,
      ),
      'locl' => 
      array (
        0 => 14,
      ),
    ),
  ),
  'deva' => 
  array (
    'DFLT' => 
    array (
      'nukt' => 
      array (
        0 => 15,
      ),
      'akhn' => 
      array (
        0 => 16,
      ),
      'rphf' => 
      array (
        0 => 17,
      ),
      'blwf' => 
      array (
        0 => 18,
      ),
      'half' => 
      array (
        0 => 19,
      ),
      'vatu' => 
      array (
        0 => 20,
        1 => 21,
      ),
      'pres' => 
      array (
        0 => 22,
        1 => 23,
        2 => 24,
        3 => 26,
      ),
      'abvs' => 
      array (
        0 => 29,
        1 => 30,
        2 => 31,
        3 => 32,
      ),
      'blws' => 
      array (
        0 => 39,
        1 => 40,
        2 => 42,
      ),
      'psts' => 
      array (
        0 => 44,
      ),
      'haln' => 
      array (
        0 => 46,
      ),
    ),
  ),
  'gujr' => 
  array (
    'DFLT' => 
    array (
      'nukt' => 
      array (
        0 => 59,
      ),
      'akhn' => 
      array (
        0 => 60,
      ),
      'rphf' => 
      array (
        0 => 61,
      ),
      'blwf' => 
      array (
        0 => 62,
      ),
      'half' => 
      array (
        0 => 63,
      ),
      'vatu' => 
      array (
        0 => 64,
        1 => 65,
      ),
      'pres' => 
      array (
        0 => 66,
        1 => 67,
        2 => 68,
        3 => 70,
        4 => 72,
      ),
      'abvs' => 
      array (
        0 => 74,
        1 => 75,
        2 => 76,
        3 => 81,
      ),
      'blws' => 
      array (
        0 => 84,
      ),
      'psts' => 
      array (
        0 => 85,
      ),
      'haln' => 
      array (
        0 => 86,
      ),
    ),
  ),
  'guru' => 
  array (
    'DFLT' => 
    array (
      'nukt' => 
      array (
        0 => 47,
      ),
      'blwf' => 
      array (
        0 => 48,
      ),
      'half' => 
      array (
        0 => 49,
      ),
      'pstf' => 
      array (
        0 => 50,
      ),
      'blws' => 
      array (
        0 => 51,
        1 => 55,
      ),
      'abvs' => 
      array (
        0 => 56,
        1 => 57,
      ),
    ),
  ),
  'hani' => 
  array (
    'DFLT' => 
    array (
      'salt' => 
      array (
        0 => 108,
      ),
      'trad' => 
      array (
        0 => 109,
      ),
      'smpl' => 
      array (
        0 => 110,
      ),
      'vert' => 
      array (
        0 => 111,
      ),
    ),
    'JAN ' => 
    array (
      'vert' => 
      array (
        0 => 111,
      ),
    ),
    'KOR ' => 
    array (
      'locl' => 
      array (
        0 => 108,
      ),
      'vert' => 
      array (
        0 => 111,
      ),
    ),
    'ZHS ' => 
    array (
      'locl' => 
      array (
        0 => 110,
      ),
      'vert' => 
      array (
        0 => 111,
      ),
    ),
    'ZHT ' => 
    array (
      'locl' => 
      array (
        0 => 109,
      ),
      'vert' => 
      array (
        0 => 111,
      ),
    ),
  ),
  'kana' => 
  array (
    'DFLT' => 
    array (
      'vert' => 
      array (
        0 => 111,
      ),
    ),
    'JAN ' => 
    array (
      'vert' => 
      array (
        0 => 111,
      ),
    ),
  ),
  'knda' => 
  array (
    'DFLT' => 
    array (
      'akhn' => 
      array (
        0 => 94,
      ),
      'rphf' => 
      array (
        0 => 95,
      ),
      'blwf' => 
      array (
        0 => 96,
      ),
      'haln' => 
      array (
        0 => 97,
      ),
      'blws' => 
      array (
        0 => 98,
      ),
      'abvs' => 
      array (
        0 => 99,
      ),
      'psts' => 
      array (
        0 => 102,
        1 => 104,
        2 => 105,
        3 => 106,
      ),
    ),
  ),
  'taml' => 
  array (
    'DFLT' => 
    array (
      'akhn' => 
      array (
        0 => 87,
      ),
      'haln' => 
      array (
        0 => 88,
      ),
      'abvs' => 
      array (
        0 => 89,
        1 => 90,
      ),
      'psts' => 
      array (
        0 => 91,
        1 => 92,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23203802,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23203832,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23203998,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23204220,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 4,
    'Flag' => 9,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23204470,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 4,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23204564,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 4,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23204864,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205038,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205058,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205078,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205102,
    ),
    'MarkFilteringSet' => '',
  ),
  11 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205122,
    ),
    'MarkFilteringSet' => '',
  ),
  12 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205142,
    ),
    'MarkFilteringSet' => '',
  ),
  13 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205162,
    ),
    'MarkFilteringSet' => '',
  ),
  14 => 
  array (
    'Type' => 1,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205196,
    ),
    'MarkFilteringSet' => '',
  ),
  15 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205238,
    ),
    'MarkFilteringSet' => '',
  ),
  16 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205862,
    ),
    'MarkFilteringSet' => '',
  ),
  17 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205912,
    ),
    'MarkFilteringSet' => '',
  ),
  18 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205944,
    ),
    'MarkFilteringSet' => '',
  ),
  19 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23205976,
    ),
    'MarkFilteringSet' => '',
  ),
  20 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23206852,
    ),
    'MarkFilteringSet' => '',
  ),
  21 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23207728,
    ),
    'MarkFilteringSet' => '',
  ),
  22 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23208592,
    ),
    'MarkFilteringSet' => '',
  ),
  23 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23209288,
    ),
    'MarkFilteringSet' => '',
  ),
  24 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23209478,
    ),
    'MarkFilteringSet' => '',
  ),
  25 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23209690,
    ),
    'MarkFilteringSet' => '',
  ),
  26 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23210074,
    ),
    'MarkFilteringSet' => '',
  ),
  27 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23210302,
    ),
    'MarkFilteringSet' => '',
  ),
  28 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23210322,
    ),
    'MarkFilteringSet' => '',
  ),
  29 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23210342,
    ),
    'MarkFilteringSet' => '',
  ),
  30 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23210374,
    ),
    'MarkFilteringSet' => '',
  ),
  31 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23210884,
    ),
    'MarkFilteringSet' => '',
  ),
  32 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23211162,
    ),
    'MarkFilteringSet' => '',
  ),
  33 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23211230,
    ),
    'MarkFilteringSet' => '',
  ),
  34 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23211260,
    ),
    'MarkFilteringSet' => '',
  ),
  35 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23211290,
    ),
    'MarkFilteringSet' => '',
  ),
  36 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23211320,
    ),
    'MarkFilteringSet' => '',
  ),
  37 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23211350,
    ),
    'MarkFilteringSet' => '',
  ),
  38 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23211374,
    ),
    'MarkFilteringSet' => '',
  ),
  39 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23211416,
    ),
    'MarkFilteringSet' => '',
  ),
  40 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23212064,
    ),
    'MarkFilteringSet' => '',
  ),
  41 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23212208,
    ),
    'MarkFilteringSet' => '',
  ),
  42 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23212228,
    ),
    'MarkFilteringSet' => '',
  ),
  43 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23212300,
    ),
    'MarkFilteringSet' => '',
  ),
  44 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23212326,
    ),
    'MarkFilteringSet' => '',
  ),
  45 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23212454,
    ),
    'MarkFilteringSet' => '',
  ),
  46 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23212488,
    ),
    'MarkFilteringSet' => '',
  ),
  47 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23212730,
    ),
    'MarkFilteringSet' => '',
  ),
  48 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23213258,
    ),
    'MarkFilteringSet' => '',
  ),
  49 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23213318,
    ),
    'MarkFilteringSet' => '',
  ),
  50 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23214098,
    ),
    'MarkFilteringSet' => '',
  ),
  51 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23214130,
    ),
    'MarkFilteringSet' => '',
  ),
  52 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23214278,
    ),
    'MarkFilteringSet' => '',
  ),
  53 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23214446,
    ),
    'MarkFilteringSet' => '',
  ),
  54 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23214476,
    ),
    'MarkFilteringSet' => '',
  ),
  55 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23214502,
    ),
    'MarkFilteringSet' => '',
  ),
  56 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23214534,
    ),
    'MarkFilteringSet' => '',
  ),
  57 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23214836,
    ),
    'MarkFilteringSet' => '',
  ),
  58 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23214972,
    ),
    'MarkFilteringSet' => '',
  ),
  59 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23214998,
    ),
    'MarkFilteringSet' => '',
  ),
  60 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23215580,
    ),
    'MarkFilteringSet' => '',
  ),
  61 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23215630,
    ),
    'MarkFilteringSet' => '',
  ),
  62 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23215662,
    ),
    'MarkFilteringSet' => '',
  ),
  63 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23215694,
    ),
    'MarkFilteringSet' => '',
  ),
  64 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23216564,
    ),
    'MarkFilteringSet' => '',
  ),
  65 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23217422,
    ),
    'MarkFilteringSet' => '',
  ),
  66 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23218274,
    ),
    'MarkFilteringSet' => '',
  ),
  67 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23218396,
    ),
    'MarkFilteringSet' => '',
  ),
  68 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23218428,
    ),
    'MarkFilteringSet' => '',
  ),
  69 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23218508,
    ),
    'MarkFilteringSet' => '',
  ),
  70 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23218556,
    ),
    'MarkFilteringSet' => '',
  ),
  71 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23218664,
    ),
    'MarkFilteringSet' => '',
  ),
  72 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23218684,
    ),
    'MarkFilteringSet' => '',
  ),
  73 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23218874,
    ),
    'MarkFilteringSet' => '',
  ),
  74 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23218894,
    ),
    'MarkFilteringSet' => '',
  ),
  75 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23218926,
    ),
    'MarkFilteringSet' => '',
  ),
  76 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23219410,
    ),
    'MarkFilteringSet' => '',
  ),
  77 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23219728,
    ),
    'MarkFilteringSet' => '',
  ),
  78 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23219754,
    ),
    'MarkFilteringSet' => '',
  ),
  79 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23219780,
    ),
    'MarkFilteringSet' => '',
  ),
  80 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23219806,
    ),
    'MarkFilteringSet' => '',
  ),
  81 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23219832,
    ),
    'MarkFilteringSet' => '',
  ),
  82 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23219940,
    ),
    'MarkFilteringSet' => '',
  ),
  83 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23219966,
    ),
    'MarkFilteringSet' => '',
  ),
  84 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23220008,
    ),
    'MarkFilteringSet' => '',
  ),
  85 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23220912,
    ),
    'MarkFilteringSet' => '',
  ),
  86 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23220976,
    ),
    'MarkFilteringSet' => '',
  ),
  87 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23221246,
    ),
    'MarkFilteringSet' => '',
  ),
  88 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23221280,
    ),
    'MarkFilteringSet' => '',
  ),
  89 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23221586,
    ),
    'MarkFilteringSet' => '',
  ),
  90 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23221704,
    ),
    'MarkFilteringSet' => '',
  ),
  91 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23221762,
    ),
    'MarkFilteringSet' => '',
  ),
  92 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23222194,
    ),
    'MarkFilteringSet' => '',
  ),
  93 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23222252,
    ),
    'MarkFilteringSet' => '',
  ),
  94 => 
  array (
    'Type' => 4,
    'Flag' => 8,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23222272,
    ),
    'MarkFilteringSet' => '',
  ),
  95 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23222322,
    ),
    'MarkFilteringSet' => '',
  ),
  96 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23222354,
    ),
    'MarkFilteringSet' => '',
  ),
  97 => 
  array (
    'Type' => 4,
    'Flag' => 4,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23222816,
    ),
    'MarkFilteringSet' => '',
  ),
  98 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23223314,
    ),
    'MarkFilteringSet' => '',
  ),
  99 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23223402,
    ),
    'MarkFilteringSet' => '',
  ),
  100 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23223556,
    ),
    'MarkFilteringSet' => '',
  ),
  101 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23223678,
    ),
    'MarkFilteringSet' => '',
  ),
  102 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23224412,
    ),
    'MarkFilteringSet' => '',
  ),
  103 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23224500,
    ),
    'MarkFilteringSet' => '',
  ),
  104 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23224522,
    ),
    'MarkFilteringSet' => '',
  ),
  105 => 
  array (
    'Type' => 6,
    'Flag' => 8,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23224568,
    ),
    'MarkFilteringSet' => '',
  ),
  106 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23224684,
    ),
    'MarkFilteringSet' => '',
  ),
  107 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23224774,
    ),
    'MarkFilteringSet' => '',
  ),
  108 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23224808,
    ),
    'MarkFilteringSet' => '',
  ),
  109 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23228370,
    ),
    'MarkFilteringSet' => '',
  ),
  110 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23242144,
    ),
    'MarkFilteringSet' => '',
  ),
  111 => 
  array (
    'Type' => 1,
    'Flag' => 12,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23268926,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'arab' => 'DFLT ',
  'deva' => 'DFLT ',
  'gujr' => 'DFLT ',
  'guru' => 'DFLT ',
  'knda' => 'DFLT ',
  'taml' => 'DFLT ',
);
$GPOSFeatures=array (
  'arab' => 
  array (
    'DFLT' => 
    array (
      'mark' => 
      array (
        0 => 21,
        1 => 22,
        2 => 23,
        3 => 24,
      ),
    ),
  ),
  'deva' => 
  array (
    'DFLT' => 
    array (
      'abvm' => 
      array (
        0 => 0,
      ),
      'blwm' => 
      array (
        0 => 1,
      ),
      'dist' => 
      array (
        0 => 2,
      ),
    ),
  ),
  'gujr' => 
  array (
    'DFLT' => 
    array (
      'abvm' => 
      array (
        0 => 8,
      ),
      'blwm' => 
      array (
        0 => 9,
      ),
      'dist' => 
      array (
        0 => 10,
      ),
    ),
  ),
  'guru' => 
  array (
    'DFLT' => 
    array (
      'abvm' => 
      array (
        0 => 6,
      ),
      'blwm' => 
      array (
        0 => 7,
      ),
    ),
  ),
  'knda' => 
  array (
    'DFLT' => 
    array (
      'requ' => 
      array (
        0 => 14,
      ),
      'dist' => 
      array (
        0 => 15,
        1 => 17,
        2 => 19,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23194728,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23197440,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 8,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23198580,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23198902,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23198924,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23198946,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23198968,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23199288,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23199716,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23200406,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 8,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23200652,
    ),
    'MarkFilteringSet' => '',
  ),
  11 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23200800,
    ),
    'MarkFilteringSet' => '',
  ),
  12 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23200822,
    ),
    'MarkFilteringSet' => '',
  ),
  13 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23200844,
    ),
    'MarkFilteringSet' => '',
  ),
  14 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23200866,
    ),
    'MarkFilteringSet' => '',
  ),
  15 => 
  array (
    'Type' => 8,
    'Flag' => 8,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23200898,
    ),
    'MarkFilteringSet' => '',
  ),
  16 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23201012,
    ),
    'MarkFilteringSet' => '',
  ),
  17 => 
  array (
    'Type' => 7,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23201050,
    ),
    'MarkFilteringSet' => '',
  ),
  18 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23201202,
    ),
    'MarkFilteringSet' => '',
  ),
  19 => 
  array (
    'Type' => 7,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23201264,
    ),
    'MarkFilteringSet' => '',
  ),
  20 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23201344,
    ),
    'MarkFilteringSet' => '',
  ),
  21 => 
  array (
    'Type' => 5,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23201376,
    ),
    'MarkFilteringSet' => '',
  ),
  22 => 
  array (
    'Type' => 5,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23201816,
    ),
    'MarkFilteringSet' => '',
  ),
  23 => 
  array (
    'Type' => 4,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23202074,
    ),
    'MarkFilteringSet' => '',
  ),
  24 => 
  array (
    'Type' => 4,
    'Flag' => 1,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 23202340,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>