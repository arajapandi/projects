<?php
$name='Catamaran-Light';
$type='TTF';
$desc=array (
  'CapHeight' => 680.0,
  'XHeight' => 485.0,
  'FontBBox' => '[-535 -377 2503 1320]',
  'Flags' => 4,
  'Ascent' => 1100.0,
  'Descent' => -377.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 71.0,
  'MissingWidth' => 645.0,
);
$unitsPerEm=1000;
$up=-100;
$ut=50;
$strp=291;
$strs=50;
$ttffile='/Mydrive/htdocs/promolta/application/client/libraries/mpdf/ttfonts/Catamaran-Light.ttf';
$TTCfontID='0';
$originalsize=70832;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='ca';
$panose=' 0 0 0 0 4 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 770, -230, 0
// usWinAscent/usWinDescent = 1100, -540
// hhea Ascent/Descent/LineGap = 1100, -540, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>