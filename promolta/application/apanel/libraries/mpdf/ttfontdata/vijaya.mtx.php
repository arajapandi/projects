<?php
$name='Vijaya';
$type='TTF';
$desc=array (
  'CapHeight' => 533.0,
  'XHeight' => 356.0,
  'FontBBox' => '[-464 -288 1535 1059]',
  'Flags' => 68,
  'Ascent' => 714.0,
  'Descent' => -288.0,
  'Leading' => 0.0,
  'ItalicAngle' => -11.0,
  'StemV' => 87.0,
  'MissingWidth' => 781.0,
);
$unitsPerEm=2048;
$up=-107;
$ut=50;
$strp=153;
$strs=50;
$ttffile='/Mydrive/htdocs/promolta/application/client/libraries/mpdf/ttfonts/Vijaya.ttf';
$TTCfontID='0';
$originalsize=171192;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='vijaya';
$panose=' 0 0 2 b 6 4 2 2 2 2 2 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 559, -174, 337
// usWinAscent/usWinDescent = 714, -293
// hhea Ascent/Descent/LineGap = 714, -293, 0
$useOTL=8;
$rtlPUAstr='';
$GSUBScriptLang=array (
  'taml' => 'DFLT ',
  'tml2' => 'DFLT ',
);
$GSUBFeatures=array (
  'taml' => 
  array (
    'DFLT' => 
    array (
      'akhn' => 
      array (
        0 => 0,
      ),
      'half' => 
      array (
        0 => 1,
      ),
      'haln' => 
      array (
        0 => 2,
      ),
      'abvs' => 
      array (
        0 => 3,
        1 => 4,
      ),
      'psts' => 
      array (
        0 => 6,
        1 => 7,
        2 => 8,
      ),
    ),
  ),
  'tml2' => 
  array (
    'DFLT' => 
    array (
      'akhn' => 
      array (
        0 => 0,
      ),
      'haln' => 
      array (
        0 => 2,
      ),
      'abvs' => 
      array (
        0 => 3,
        1 => 4,
      ),
      'psts' => 
      array (
        0 => 6,
        1 => 7,
        2 => 8,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 162200,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 162226,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 162542,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 162858,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 162980,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 163272,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 163336,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 163574,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 163890,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'taml' => 'DFLT ',
  'tml2' => 'DFLT ',
);
$GPOSFeatures=array (
  'taml' => 
  array (
    'DFLT' => 
    array (
      'abvm' => 
      array (
        0 => 0,
      ),
      'dist' => 
      array (
        0 => 1,
      ),
    ),
  ),
  'tml2' => 
  array (
    'DFLT' => 
    array (
      'abvm' => 
      array (
        0 => 0,
      ),
      'dist' => 
      array (
        0 => 1,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 161856,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 161908,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>