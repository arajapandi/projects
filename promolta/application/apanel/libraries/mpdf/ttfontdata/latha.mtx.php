<?php
$name='LathaRegular';
$type='TTF';
$desc=array (
  'CapHeight' => 1000.0,
  'XHeight' => 0.0,
  'FontBBox' => '[-539 -428 2123 1084]',
  'Flags' => 4,
  'Ascent' => 1000.0,
  'Descent' => -372.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 87.0,
  'MissingWidth' => 500.0,
);
$unitsPerEm=2048;
$up=-156;
$ut=59;
$strp=259;
$strs=50;
$ttffile='/Mydrive/htdocs/promolta/application/client/libraries/mpdf/ttfonts/latha.ttf';
$TTCfontID='0';
$originalsize=61556;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='latha';
$panose=' 0 0 2 0 4 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 1000, -660, 0
// usWinAscent/usWinDescent = 1000, -372
// hhea Ascent/Descent/LineGap = 1000, -660, 0
$useOTL=255;
$rtlPUAstr='';
$GSUBScriptLang=array (
  'taml' => 'DFLT ',
);
$GSUBFeatures=array (
  'taml' => 
  array (
    'DFLT' => 
    array (
      'akhn' => 
      array (
        0 => 0,
      ),
      'half' => 
      array (
        0 => 1,
      ),
      'haln' => 
      array (
        0 => 2,
      ),
      'abvs' => 
      array (
        0 => 3,
        1 => 4,
      ),
      'psts' => 
      array (
        0 => 8,
        1 => 12,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 4,
    'Flag' => 8,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 59504,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 59530,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 59828,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 60126,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 60180,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 60314,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 60326,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 60338,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 5,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 60350,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 60520,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 60532,
    ),
    'MarkFilteringSet' => '',
  ),
  11 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 60544,
    ),
    'MarkFilteringSet' => '',
  ),
  12 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 60556,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'taml' => 'DFLT ',
);
$GPOSFeatures=array (
  'taml' => 
  array (
    'DFLT' => 
    array (
      'abvm' => 
      array (
        0 => 0,
        1 => 1,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 61024,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 61504,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>