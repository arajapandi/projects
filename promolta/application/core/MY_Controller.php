<?php
/**
 *  File: /application/core/MY_Controller.php
 */
 class MY_Controller extends CI_Controller {

     public function __construct() {

        parent::__construct();

        $this->load->helper('cookie');
        $lang =  get_cookie('lang');
        $this->lang->load('common', empty($lang) ? 'english' : $lang);

        $this->data = array();
        $this->data['lang'] = $lang;
        $profile_id = $this->session->userdata('profile_id');
        $this->data['profile_id'] = $profile_id;
        $this->data['page_name'] = $this->router->fetch_class();
     }

 }
