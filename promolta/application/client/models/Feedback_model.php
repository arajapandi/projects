<?php

/**
 * Feedback model Class
 *
 * @category        Feedback Model
 * @author          Rajapandian a
 * @license         NA
 * @link            NA
 */
class Feedback_model extends CI_Model {

    /**
     * @function getAccounts
     * @return object
     */
    function getFeedbacks($offset=0, $per_page=10, $status=1)
    {


        $this->db->select(' um.*,p.name ')
                     ->from('profile_feedback as um')
                     ->join('profiles as p', 'um.profile_id = p.id');;

        if(!empty($status)) {
            $this->db->where('um.status =', $status);
        }

        $this->db->order_by('um.id', 'desc');
        $this->db->limit($per_page, $offset);

        return $this->db->get()->result_array();
    }


    function getFeedbackById($feedback_id) {
        $this->db->select(' pp.*,p.name ')
                     ->from('profile_feedback as pp')
                     ->join('profiles as p', 'pp.profile_id = p.id');

        $this->db->where('pp.id =', $feedback_id);

        return $this->db->get()->row_array();
    }

    function getTotalFeedbacks($status = 1) {
         $this->db->select(' count(1) as cnt')
                     ->from('profile_feedback as um');
        if(!empty($status)) {
            $this->db->where('status =', $status);
        }
        $row = $this->db->get()->row_array();
        if(!empty($row)) {
            return $row['cnt'];
        }
        return 0;
    }
    function getActiveProfiles() {
        return $this->getTotalPackages();
    }

    function getFeedback($feedback_id) {

        $this->db->select(' um.*,p.name ')
                     ->from('profile_feedback as um')
                     ->join('profiles as p', 'um.profile_id = p.id');

        $this->db->where('um.id =', $feedback_id);

        return $this->db->get()->row_array();
    }

    function save($fvalue) {


        $accounts_db_field = array( 'subject', 'feedback', 'status');
        $profile_val = $this->process_db_fields($accounts_db_field, $fvalue);
        $profile_val['updated_at'] = date('Y-m-d H:i:s');
        $feedback_id = 0;
        if(isset($fvalue['feedback_id']) && !empty($fvalue['feedback_id'])) {

            $feedback_id = $fvalue['feedback_id'];
            $old_data = $this->getFeedback($feedback_id);
            if(empty($old_data)) {
                return false;
            }

            $this->db->where('id', $feedback_id);
            $this->db->update('profile_feedback', $profile_val);

        } else {
            return 0;
        }



        return $feedback_id;
    }

    function process_db_fields($fields, $data) {
        $common_array = array();
        foreach($fields as $val) {
            if(isset($data[$val])) {
                $common_array[$val] = $data[$val];
            }
        }
        return $common_array;
    }

    function deactivateFeedback($id, $status=0) {
        $data = array(
               'status' => $status,
            );
        $this->db->where('id', $id);
        $this->db->update('profile_feedback', $data);
        return true;
    }
    function deleteFeedback($id) {

        $this->db->where('id', $id);
        $this->db->delete('profile_feedback');

        return true;
    }
}
