<?php

/**
 * package model Class
 *
 * @category        package Model
 * @author          Rajapandian a
 * @license         NA
 * @link            NA
 */
class Package_model extends CI_Model {

    /**
     * @function getAccounts
     * @return object
     */
    function getPackages($offset=0, $per_page=10, $status=1)
    {
        $this->db->select(' * ')
                     ->from('packages as um');

        if(!empty($status)) {
            $this->db->where('status =', $status);
        }

        $this->db->order_by('id', 'desc');
        $this->db->limit($per_page, $offset);

        return $this->db->get()->result_array();
    }



    public function getProfilePackageCount($profile_id) {
        $this->db->select(' * ')
                     ->from('profile_package as pp');
        $this->db->where('pp.profile_id =', $profile_id);
        return $this->db->get()->row_array();
    }

    public function checkAlreadyAssigned($profile_id, $profile_assignid) {
      $this->db->select(' 1 ')->from('profile_assigned');
      $this->db->where(array('profile_id ='=>$profile_id, 'assignd_id'=>$profile_assignid));
      return $this->db->get()->num_rows();
    }

    public function removedAssnProfile($profile_id, $profile_assigned) {

      $this->db->where(array('profile_id'=>$profile_id,'assignd_id'=>$profile_assigned));
      $this->db->delete('profile_assigned');

      $profile_data = $this->getProfilePackageCount($profile_id);
      if(!empty($profile_data)) {
        $this->db->set(array('rem_count'=>$profile_data['rem_count'] + 1));
        $this->db->where('id =',$profile_data['id']);
        $this->db->update('profile_package');
      }
      return true;
    }
    public function assignProfile($profile_id, $profile_assignid) {

        if(!$this->checkAlreadyAssigned($profile_id, $profile_assignid)) {
          $profile_data = $this->getProfilePackageCount($profile_id);
          if(!empty($profile_data) && $profile_data['rem_count'] > 0) {
            $this->db->set(array('profile_id'=>$profile_id, 'assignd_id'=>$profile_assignid, 'created_at'=>date('Y-m-d H:i:s')));
            $this->db->insert('profile_assigned');

            $this->db->set(array('rem_count'=>$profile_data['rem_count'] - 1));
            $this->db->where('id =',$profile_data['id']);
            $this->db->update('profile_package');
            return 1;
          } else {
            return 3;
          }

        } else {
          return 2;
        }
        return 0;
    }
    public function getProfileAssign($profile_id) {
      $this->db->select(' pp.profile_id,pp.assignd_id,pp.created_at,p.name ')
                   ->from('profile_assigned as pp')
                   ->join('profiles as p', 'pp.assignd_id = p.id');

      $this->db->where('pp.profile_id =', $profile_id);

      return $this->db->get()->result_array();
    }

    public function profilePackage($fvalue) {
        $profile_id = $fvalue['profile_id'];
        $old_data = $this->getProfilePackage($profile_id);

        $accounts_db_field = array( 'profile_id',  'package_id', 'view_count', 'rem_count', 'end_time');
        $profile_val = $this->process_db_fields($accounts_db_field, $fvalue);
        $profile_val['updated_at'] = date('Y-m-d H:i:s');
        $profile_val['end_time'] = date('Y-m-d H:i:s',time() + $fvalue['days'] * (24* 3600));
        $profile_val['rem_count'] = $profile_val['view_count'] = $fvalue['profile_view'];

        if(!empty($old_data)) {
            $package_id = $old_data['id'];
            $this->db->where('id', $old_data['id']);
            $this->db->update('profile_package', $profile_val);

        } else {
            $this->db->set($profile_val);
            $this->db->insert('profile_package');
            $package_id = $this->db->insert_id();
        }
        return $profile_id;
    }

    function getProfilePackage($profile_id) {
        $this->db->select(' * ')
                     ->from('profile_package as pp')
                     ->join('packages as p', 'pp.package_id = p.id');

        $this->db->where('pp.profile_id =', $profile_id);

        return $this->db->get()->row_array();
    }

    function getTotalPackages($status = 1) {
         $this->db->select(' count(1) as cnt')
                     ->from('packages as um');
        if(!empty($status)) {
            $this->db->where('status =', $status);
        }
        $row = $this->db->get()->row_array();
        if(!empty($row)) {
            return $row['cnt'];
        }
        return 0;
    }
    function getActiveProfiles() {
        return $this->getTotalPackages();
    }

    function getPackage($profile_id) {

        $this->db->select(' * ')
                     ->from('packages as um');

        $this->db->where('id =', $profile_id);

        return $this->db->get()->row_array();
    }

    function save($fvalue) {


        $accounts_db_field = array( 'name',  'days', 'profile_view', 'cost', 'status');
        $profile_val = $this->process_db_fields($accounts_db_field, $fvalue);
        $profile_val['updated_at'] = date('Y-m-d H:i:s');

        if(isset($fvalue['package_id']) && !empty($fvalue['package_id'])) {

            $profile_id = $fvalue['package_id'];
            $old_data = $this->getPackage($profile_id);
            if(empty($old_data)) {
                return false;
            }

            $this->db->where('id', $profile_id);
            $this->db->update('packages', $profile_val);

        } else {

            $this->db->set($profile_val);
            $this->db->insert('packages');
            $profile_id = $this->db->insert_id();
        }



        return $profile_id;
    }

    function process_db_fields($fields, $data) {
        $common_array = array();
        foreach($fields as $val) {
            if(isset($data[$val])) {
                $common_array[$val] = $data[$val];
            }
        }
        return $common_array;
    }

    function deactivatePackage($id, $status=0) {
        $data = array(
               'status' => $status,
            );
        $this->db->where('id', $id);
        $this->db->update('packages', $data);
        return true;
    }
    function deletePackages($id) {

        $this->db->where('id', $id);
        $this->db->delete('packages');

        return true;
    }

}
