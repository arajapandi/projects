<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('client_model');
		$this->load->model('profile_model');
                $this->load->model('package_model');

		$valid = $this->client_model->isLogged();
		if($valid == false) {
			redirect(base_url().'logme/', 'refresh');
		}
	}

	public function index() {

		$this->load->helper('url');
		$sessionData = $this->client_model->get_id();

		$data = array();



		$page = intval(trim($this->input->get('page'),'/'));
		$per_page = 5;
		$this->load->library('pagination');
                $config['total_rows'] = $this->package_model->getTotalPackages('');
                $config['per_page'] = $per_page;
                $config['next_link'] = 'Next';
                $config['prev_link'] = 'Prev';
                $config['cur_page'] = $page;
                $config['base_url'] = base_url().'packages?page=';

                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['prev_tag_open'] = '<li class="prev">';
                $config['prev_tag_close'] = '</li>';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="active"><a href="#">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();


                $data['url'] = base_url();
		$data['profiles'] = $this->package_model->getPackages($page, $per_page,'');
		$data['message'] = $this->session->flashdata('message');
		$data['body_content'] = $this->load->view('packages', $data, true);

		$data['base_url'] = str_replace('apanel.php','',base_url()).'application/client/views/';

                $data['url'] = base_url();
                $data['user_id'] = $this->client_model->get_id();
		$this->load->view('template', $data);
	}
        //ajax call
        public function getpackages() {
            $packages = $this->package_model->getPackages();
            echo json_encode($packages);
            exit;
        }
        public function profilepackage() {
            $fvalue      = $this->input->post('fvalue');
            if(!empty($fvalue)) {
                $this->package_model->profilePackage($fvalue);
                echo 1;
            } else {
                echo 0;
            }


        }
				public function deleteAssigneProfile() {
					$profile_assignid      	= $this->input->post('profile_assignid');
					$profile_id      				= $this->input->post('profile_id');
					$result = $this->package_model->removedAssnProfile($profile_id, $profile_assignid);
					if($result == 1) {
						echo 'Removed Successfully!';
					}
					echo '';
					exit;
				}

				//ajax call
				public function assignprofile() {
					$profile_assignid      	= $this->input->post('profile_assignid');
					$profile_id      				= $this->input->post('profile_id');
					if(!empty($profile_assignid) && $profile_id != $profile_assignid) {

							$result = $this->package_model->assignProfile($profile_id, $profile_assignid);
							if($result == 1) {
								echo "Profile Assigned Successfully!";
							} else if($result == 2) {
								echo "Profile Already Assigned!";
							} else if ($result == 3) {
								echo "Please update Package Count!";
							}
					}
					echo '';
					exit;
				}
        //ajax call
        public function getprofilepackage() {
            $profile_id      = $this->input->get('profile_id');
            if(!empty($profile_id)) {
								$data = array();
                $data['package'] 	= $this->package_model->getProfilePackage($profile_id);
								$data['profiles'] = $this->package_model->getProfileAssign($profile_id);

                echo json_encode($data);
            }
            echo '';
            exit;
        }

	public function edit() {

                $this->load->helper('url');
		$id      = $this->input->get('id');

		if(!empty($id)) {
                    $data['fvalue'] = $this->package_model->getPackage($id);
                }

                $data['message'] = $this->session->flashdata('message');
		$data['body_content'] = $this->load->view('package_edit', $data, true);
                $data['url'] = base_url();
		$data['base_url'] = str_replace('client.php','',base_url()).'application/client/views/';
                $data['user_id'] = $this->client_model->get_id();
		$this->load->view('template', $data);



	}

	public function save() {

		$fvalue = $this->input->post('fvalue');

	 	$profile_id = $this->package_model->save($fvalue);
                if(empty($profile_id)) {
                    $this->session->set_flashdata('message', 'Update Failed!');
                    redirect(base_url().'packages');
                } else {
                    $this->session->set_flashdata('message', ' Saved Successfully!');
                }

		redirect(base_url().'packages/edit?id='.$profile_id);

	}

	public function delete() {
		$id      = $this->input->get('id');
		if(!empty($id)) {
			$this->package_model->deletePackages($id);
			$this->session->set_flashdata('message', 'Package Deleted Successfully!');
                }
                redirect(base_url().'packages', 'refresh');
	}
	public function activate() {

		$id      = $this->input->get('id');
		if(!empty($id)) {
	        $this->package_model->deactivatePackage($id, 1);
	        $this->session->set_flashdata('message', 'Package Enabled Successfully!');
                }
                redirect(base_url().'packages', 'refresh');
	}
        public function deactivate() {

		$id      = $this->input->get('id');
		if(!empty($id)) {
	        $this->package_model->deactivatePackage($id);
	        $this->session->set_flashdata('message', 'Package Disabled Successfully!');
                }
                redirect(base_url().'packages', 'refresh');
	}
}
