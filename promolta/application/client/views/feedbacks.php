            <div id="page-wrapper" style="min-height: 631px;">
                <div class="row">
                    <div class="col-lg-10">
                        <h1 class="page-header">Profiles</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                <?php if(!empty($message)): ?>
                  <div class="alert alert-info">
                    <?php echo $message; ?>
                  </div>
                <?php endif; ?>
                <div class="tab-content col-md-12">

                <div id="sent" class="tab-pane fade active in">

                   <table class="table table-stripped" id="sentTbl">
                      <thead>
                        <tr>
                          <th width="10%">Uniq No</th>
                          <th width="20%">Name</th>
                          <th width="45%">Subject</th>
                          <th width="15%">Updated</th>
                          <th width="10%">##</th>
                        </tr>
                      </thead>
                      <tbody>

                      <?php if(!empty($feedbacks)):
                        foreach($feedbacks as $data): ?>
                        <tr class="<?php echo $data['status'] == 0 ? "active":''; ?>">
                        <th scope="row"><?php echo $data['id']; ?></th>
                          <td><?php echo $data['name']; ?></td>
                          <td><?php echo $data['subject']; ?></td>
                          <td><?php echo $data['updated_at']; ?></td>
                          <td>
                            <a href="feedbacks/edit?id=<?php echo $data['id']; ?>" class="fwdMessage" data-index="0" data-toggle="tooltip" title="Edit"><span class="glyphicon glyphicon-edit"></span></a> &nbsp;
                            <a href="feedbacks/<?php echo $data['status'] == 0 ? "activate":'deactivate'; ?>?id=<?php echo $data['id']; ?>" class="fwdMessage" data-index="0" data-toggle="tooltip" title="Disable"><span class="glyphicon glyphicon-off"></span></a> &nbsp;
                            <a href="feedbacks/delete?id=<?php echo $data['id']; ?>" class="fwdMessage" data-index="0" data-toggle="tooltip" title="Delete"><span class="glyphicon glyphicon-remove"></span></a> &nbsp;
                          </td>
                        </tr>
                      <?php endforeach;
                        endif; ?>
                        <?php if(!empty($pagination)): ?>
                        <tr class="<?php echo $data['status'] == 0 ? "active":''; ?>">
                        <td colspan="6" align="right">
                            <?php echo $pagination; ?>
                        </td>
                        </tr>
                        <?php endif; ?>


                    </table>

                </div>


              </div>
              </div>
