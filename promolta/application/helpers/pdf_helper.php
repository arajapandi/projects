<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('pdf_view_new')) {

      function pdf_view_new() {


          $CI = get_instance();

          $CI->load->model('profile_model');
          $CI->load->helper('url');
          $CI->load->helper('cookie');
          $lang =  get_cookie('lang');

  	      //$CI->lang->load('common', empty($lang) ? 'english' : $lang, $return = FALSE, $add_suffix = TRUE, $alt_path = __DIR__.'/../../');
          $CI->lang->load('common', empty($lang) ? 'english' : $lang);
          $id     = (int)$CI->input->get('id',true);
          if(empty($id)) {
            die('foo - No id specified');
          }
          $fvalue = $CI->profile_model->getProfile($id);
        	$ftamil = $CI->profile_model->getLang($id, $lang == 'english' ? 1 : 2);
          $fvalue['laknam'] = empty($fvalue['laknam']) ? 0 : $fvalue['laknam'];
          $fvalue['thisai'] = empty($fvalue['thisai']) ? 0 : $fvalue['thisai'];
          $fvalue['annual_income'] = empty($fvalue['annual_income']) ? 0 : $fvalue['annual_income'];
          $fvalue['nakshatram'] = empty($fvalue['nakshatram']) ? 0 : $fvalue['nakshatram'];
          $fvalue['rasi'] = empty($fvalue['rasi']) ? 0 : $fvalue['rasi'];
          $fvalue['jathagam'] = empty($fvalue['jathagam']) ? 0 : $fvalue['jathagam'];


        if($lang == 'english') {

          $Lagnam = array(0=>"",1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
          $thisaiiruppu = array(0=>"",1=>"Guru thisai","Sani thisai","kethu thisai","Sukura thisai","Suriya thisai","Chandra thisai","Sewwai thisai","Ragu thisai","Budhan thisai");
          $rasi = array(0=>"",1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
          $nakshatram = array(0=>"",1=>"Aswini","Bharani","Karthikai",
    "Rohini","Mirugasiridam","Thiruvathirai","Punarpusam","Pusam","Ayilyam","Maham","Puram","Uthiram","Atham",
    "Chithirai","Swathiv","Visakam","Anusam","Ketai","Mulam", "Puradam","Uthiradam","Thiruvonam","Avitam",
    "Sathayam","Puratathi","Uthiratathi","Revathi");
        } else {

          $Lagnam = array(0=>"",1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
          $thisaiiruppu = array(0=>"",1=>"குருதிசை","சனிதிசை","கேதுதிசை","சுக்ரதிசை","சூரியதிசை","சந்திரதிசை","செவ்வாய்திசை","ராகுதிசை","புதன்திசை");
          $rasi = array(0=>"",1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
          $nakshatram = array(0=>"",1=>"அசுவினி","பரணி","கிருத்திகை",
          "ரோஹிணி","மிருகசீரிஷம்","திருவாதிரை","புனர்பூசம்","பூசம்","ஆயில்யம்","மகம்","பூரம்","உத்திரம்","ஹஸ்தம்",
          "சித்திரை","ஸ்வாதி","விசாகம்","அனுசம்","கேட்டை","மூலம்", "புராடம்","உத்ராடம்","திருவோணம்","அவிட்டம்",
          "சதயம்","புரட்டாதி","உத்திரட்டாதி","ரேவதி");
        }

        $aninco = $CI->lang->line('anincoar');
        $rasidata = $CI->lang->line('rasidataar');
        $laknamdata = $CI->lang->line('laknamdataar');
        $jathagam = $CI->lang->line('jathagamar');

          $jatsquare = array('rasi'=>array(),'amsam'=>array());
          if(!empty($fvalue['jatsquare'])) {
              $jatsquare = json_decode($fvalue['jatsquare'],true);
          }
  /*
          $Lagnam = array(1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
          $thisaiiruppu = array(1=>"குருதிசை","சனிதிசை","கேதுதிசை","சுக்ரதிசை","சூரியதிசை","சந்திரதிசை","செவ்வாய்திசை","ராகுதிசை","புதன்திசை");
          $rasi = array(1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
          $nakshatram = array(1=>"அசுவினி","பரணி","கிருத்திகை",
          "ரோஹிணி","மிருகசீரிஷம்","திருவாதிரை","புனர்பூசம்","பூசம்","ஆயில்யம்","மகம்","பூரம்","உத்திரம்","ஹஸ்தம்",
          "சித்திரை","ஸ்வாதி","விசாகம்","அனுசம்","கேட்டை","மூலம்", "புராடம்","உத்ராடம்","திருவோணம்","அவிட்டம்",
          "சதயம்","புரட்டாதி","உத்திரட்டாதி","ரேவதி");*/

          $rasi_details = array();
          $amsam_details = array();
          if(!empty($jatsquare['rasi'])) {
            foreach((array)$jatsquare['rasi'] as $val=>$key) {
               $rasi_details[$key] = isset($rasi_details[$key]) ? $rasi_details[$key].' <br/> '.$rasidata[$val] : $rasidata[$val];
            }
          }

          if(!empty($jatsquare['amsam'])) {
            foreach((array)$jatsquare['amsam'] as $val=>$key) {
               $amsam_details[$key] = isset($amsam_details[$key]) ? $amsam_details[$key].' <br/>  '.$laknamdata[$val] : $laknamdata[$val];
            }
          }

          $url = '';
          if(!empty($fvalue['image'])) {
            $url = str_replace('apanel.php', '',base_url()).'uploads/'.$fvalue['image'];
          };

          $stylesheet = ' table {font-family:"arialuni" ;FONT-SIZE:11px}
          .lfh {font-family:arialuni;FONT-SIZE:17px}
          .lf {font-family:arialuni;FONT-SIZE:12px}
          .latha{font-family:"arialuni";FONT-SIZE:17px}';

          $html = '
          <table border="0" cellspacing="2" cellpadding="2" width="100%">
             <tr>
                <td align="right" class="lf" nowrap> '.$CI->lang->line('telephoneno').': 0422-2471928 - '.$CI->lang->line('phonenew').': 94425 71928 </td>
                <td align="center" width="25%" class="lf"> '.$CI->lang->line('ramajeyam').' </td>
                <td align="right" width="35%" class="lf"> '.$CI->lang->line('workinghrs').'  : <span class="lf"> 10.00 AM to 5.00 PM </span> </td>
             </tr>

                        <tr>
                           <td colspan="4" align="center" class="lf"> <hr>   </td>
                        </tr>
             <tr>
                <td colspan="3" align="center" class="latha"> '.$CI->lang->line('pdftitle').' </td>
             </tr>
             <tr>

                <td colspan="3">
                <table width="100%"><tr>
                <td align="left" class="lf" width="25%"> '.$CI->lang->line('sevvaiholiday').' </td>
                <td align="center" class="lf"> '.$CI->lang->line('addressfull').'  </td>
                <td align="right" class="lf" width="25%"> '.$CI->lang->line('date').'  : '.date('d/m/Y').' </td>
                </tr></table>
                </td>

             </tr>


             <tr>
                <td colspan="4" align="center" class="lf"> <hr>   </td>
             </tr>
             <tr>
                <td align="right" class="lf"> '.$CI->lang->line('regno').' : '.$fvalue['id'].' </td>
                <td align="center" class="lf"> '.$CI->lang->line('jananjathagam').' </td>
                <td align="left" class="lf"> '.$CI->lang->line('regnodate').'  : '.date('d/m/Y',strtotime($fvalue['reg_date'])).' </td>
             </tr>
             <tr>
                <td colspan="4"><hr></td>
             </tr>
             <tr>
                <td colspan="3" align="center">
                   <table border="0" width="100%">
                      <tr>
                         <td width="80%" valign="top">
                            <table border="0" width="100%" cellspacing="2" cellpadding="5">
                               <tr>
                                  <td align="left" class="lf">'.$CI->lang->line('name').'</td>
                                  <td colspan="3" align="left" class="lf">: '.$ftamil['name'].' </td>
                               </tr>
                               <tr>
                                  <td align="left" width="25%" class="lf">'.$CI->lang->line('birthdate').' </td>
                                  <td align="left" width="25%" >: '.date('d/m/Y',strtotime($fvalue['dob'])).' </td>
                                  <td class="lf" align="right" width="25%" > </td>
                                  <td  align="left" width="25%" > </td>

                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('livingcity').'</td>
                                  <td colspan="3" align="left" class="lf">: '.$ftamil['res_city'].' </td>
                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('Height').' </td>
                                  <td align="left">: '.$ftamil['height'].' </td>
                                  <td class="lf" align="right">'.$CI->lang->line('Color').'  </td>
                                  <td  align="left">:  '.$ftamil['color'].' </td>
                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('kulam').'    </td>
                                  <td  align="left" class="lf" colspan="3">: '.$ftamil['kulam'].'</td>

                               </tr>
                               <tr>
                               <td class="lf" align="left">'.$CI->lang->line('kulathievam').'   </td>
                               <td align="left" class="lf" colspan="3">: '.$ftamil['kulam_temple'].' </td>
                               </tr>

                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('sevvai').' </td>
                                  <td align="left" class="lf">: '.$ftamil['sevvai'].'  </td>
                                  <td class="lf" align="right" >'.$CI->lang->line('ragukethu').'    </td>
                                  <td  align="left" class="lf">:  '.$ftamil['ragukethu'].'  </td>
                               </tr>


                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('jenmanakshatra').' </td>
                                  <td align="left" class="lf">: '.$nakshatram[$fvalue['nakshatram']].'  </td>
                                  <td class="lf" align="right" >'.$CI->lang->line('rasi').'  </td>
                                  <td  align="left" class="lf">:  '.$rasi[$fvalue['rasi']].'  </td>
                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('laknam').'</td>
                                  <td align="left" class="lf">: '.$Lagnam[$fvalue['laknam']].' </td>
                                  <td class="lf" align="right">'.$CI->lang->line('thisaiiruppu').' </td>
                                  <td align="left" class="lf">: '.$thisaiiruppu[$fvalue['thisai']].' </td>

                               </tr>

                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('Education').'</td>
                                  <td colspan="3" align="left">: '.$ftamil['education'].' </td>
                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('work').'</td>
                                  <td colspan="3" align="left">: '.$ftamil['job'].' </td>
                               </tr>

                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('incometotal').'</td>
                                  <td colspan="3" align="left" class="lf">: '.$ftamil['annual_income'].'</td>
                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('Fathersn').' </td>
                                  <td colspan="3" align="left" class="lf">:  '.$ftamil['father_name'].'  </td>
                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('Mothers').' </td>
                                  <td colspan="3" align="left" class="lf">: '.$ftamil['mother_name'].'</td>
                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('bromarried').' </td>
                                  <td  class="lf" colspan="3" align="left">'.$CI->lang->line('marriedalt').' : '.$fvalue['bromarried'].'
                                  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; '.$CI->lang->line('unmarriedalt').' : '.$fvalue['brounmarried'].' </td>
                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('sismarried').' </td>
                                  <td class="lf" colspan="3" align="left">'.$CI->lang->line('marriedalt').' : '.$fvalue['sismarried'].'  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  '.$CI->lang->line('unmarriedalt').' : '.$fvalue['sisunmarried'].'</td>
                               </tr>
                               <!--<tr>
                                  <td class="lf" align="left">'.$CI->lang->line('informations').' </td>
                                  <td colspan="3" align="left">: '.$fvalue['information'].'</td>
                               </tr>-->

                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('birthinfo').'</td>
                                  <td colspan="3" align="left" class="lf">:  '.$CI->lang->line('tamil').' '.$ftamil['tamil_birth'].'
                                  '.$CI->lang->line('year').' '.$ftamil['tamil_year'].'
                                  '.$CI->lang->line('month').' '.$ftamil['tamil_month'].'
                                  '.$CI->lang->line('date').' '.$ftamil['tamil_date'].'
                                  '.$CI->lang->line('seconds').'   '.$ftamil['tamil_nazhigai'].'
                                  '.$CI->lang->line('time').' '.$ftamil['tamil_time'].'</td>
                               </tr>

                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('expectation').' </td>
                                  <td colspan="3" align="left">: '.$fvalue['expectation'].'</td>
                               </tr>
                               <!--<tr>
                                  <td class="lf" align="left">'.$CI->lang->line('district').' </td>
                                  <td colspan="3" align="left">: '.$fvalue['district'].'</td>
                               </tr>-->
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('houseall').'</td>
                                  <td align="left">: '.$fvalue['house'].'</td>
                                  <td class="lf" align="right">'.$CI->lang->line('land').'</td>
                                  <td align="left">: '.$fvalue['land'].'</td>
                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('vehicle').' </td>
                                  <td align="left">: '.$fvalue['vehicle'].'</td>
                                  <td class="lf" align="right">'.$CI->lang->line('totalassetinfo').'  1</td>
                                  <td align="left">: '.$fvalue['provalue1'].'</td>
                               </tr>
                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('totalassetinfo').' 2</td>
                                  <td align="left">: '.$fvalue['provalue2'].'</td>
                                  <td class="lf" align="right">'.$CI->lang->line('totalassetinfo').' 3</td>
                                  <td align="left">: '.$fvalue['provalue3'].'</td>
                               </tr>

                               <tr>
                                  <td class="lf" align="left">'.$CI->lang->line('totalasset').' </td>
                                  <td colspan="3" align="left">: '.$fvalue['totalpro'].'</td>
                               </tr>
                            </table>
                         </td>
                         <td rowspan="10" colspan="2" valign="top">
                            <img src="'.$url.'" width="200px">
                         </td>
                      </tr>
                   </table>
                </td>
             </tr>
             <tr>
                <td colspan="3"  >
                   <table width="100%">
                      <tr>
                         <td>
                            <table border="1" cellspacing="0" cellpadding="2" width="360px">
                               <tr>
                                  <td height="60px" width="25%"> '.(isset($rasi_details[12]) ? $rasi_details[12] : '&nbsp;' ).'  </td>
                                  <td width="25%"> '.(isset($rasi_details[1]) ? $rasi_details[1] : '&nbsp;' ).' </td>
                                  <td width="25%"> '.(isset($rasi_details[2]) ? $rasi_details[2] : '&nbsp;' ).' </td>
                                  <td width="25%"> '.(isset($rasi_details[3]) ? $rasi_details[3] : '&nbsp;' ).' </td>
                               </tr>
                               <tr>
                                  <td height="60px" width="25%"> '.(isset($rasi_details[11]) ? $rasi_details[11] : '&nbsp;' ).' </td>
                                  <td rowspan="2" colspan="2" align="center" height="120px" width="50%">
                                     <div style="vertica-align: middle;padding-top: 10px;font-size:30px;font-color:#cdcccc;text-shadow: 2px 2px #cdcdcd;">
                                        '.$CI->lang->line('rasi').'
                                     </div>
                                  </td>
                                  <td width="25%"> '.(isset($rasi_details[4]) ? $rasi_details[4] : '&nbsp;' ).' </td>
                               </tr>
                               <tr>
                                  <td height="60px" width="25%"> '.(isset($rasi_details[10]) ? $rasi_details[10] : '&nbsp;' ).' </td>
                                  <td width="25%"> '.(isset($rasi_details[5]) ? $rasi_details[5] : '&nbsp;' ).' </td>
                               </tr>
                               <tr>
                                  <td height="60px" width="25%"> '.(isset($rasi_details[9]) ? $rasi_details[9] : '&nbsp;' ).' </td>
                                  <td width="25%"> '.(isset($rasi_details[8]) ? $rasi_details[8] : '&nbsp;' ).' </td>
                                  <td width="25%">'.(isset($rasi_details[7]) ? $rasi_details[7] : '&nbsp;' ).' </td>
                                  <td width="25%"> '.(isset($rasi_details[6]) ? $rasi_details[6] : '&nbsp;' ).' </td>
                               </tr>
                            </table>
                         </td>

                         <td width="10%" class="lf">

                         </td>
                         <td>
                            <table border="1" cellspacing="0" cellpadding="2"  width="360px">
                               <tr>
                                  <td height="60px" width="25%"> '.(isset($amsam_details[12]) ? $amsam_details[12] : '' ).' &nbsp; </td>
                                  <td width="25%"> '.(isset($amsam_details[1]) ? $amsam_details[1] : '' ).' &nbsp; </td>
                                  <td width="25%"> '.(isset($amsam_details[2]) ? $amsam_details[2] : '' ).' &nbsp; </td>
                                  <td width="25%"> '.(isset($amsam_details[3]) ? $amsam_details[3] : '' ).' &nbsp; </td>
                               </tr>
                               <tr>
                                  <td height="60px" width="25%"> '.(isset($amsam_details[11]) ? $amsam_details[11] : '' ).' &nbsp; </td>
                                  <td rowspan="2" colspan="2" align="center" height="120px" width="50%">
                                     <div style="vertica-align: middle;padding-top: 10px;font-size:30px;font-color:#cdcccc;text-shadow: 2px 2px #cdcdcd;">
                                        '.$CI->lang->line('amsam').'
                                     </div>
                                  </td>
                                  <td width="25%">    '.(isset($amsam_details[4]) ? $amsam_details[4] : '' ).' &nbsp; </td>
                               </tr>
                               <tr>
                                  <td height="60px" width="25%"> '.(isset($amsam_details[10]) ? $amsam_details[10] : '' ).' &nbsp; </td>
                                  <td width="25%"> '.(isset($amsam_details[5]) ? $amsam_details[5] : '' ).' &nbsp; </td>
                               </tr>
                               <tr>
                                  <td height="60px" width="25%"> '.(isset($amsam_details[9]) ? $amsam_details[9] : '' ).' &nbsp; </td>
                                  <td width="25%"> '.(isset($amsam_details[8]) ? $amsam_details[8] : '' ).' &nbsp; </td>
                                  <td width="25%"> '.(isset($amsam_details[7]) ? $amsam_details[7] : '' ).' &nbsp; </td>
                                  <td width="25%"> '.(isset($amsam_details[6]) ? $amsam_details[6] : '' ).' &nbsp; </td>
                               </tr>
                            </table>
                         </td>
                      </tr>
                      <tr>
                      <td colspan="5" align="center">
                      <br>
                      <br>
                      <!--'.$jathagam[$fvalue['jathagam']].' -->
                      '.(!empty($fvalue['balance']) ? '  '.$CI->lang->line('iruppu').' : '.$fvalue['balance'] : '').'
                      '.(!empty($fvalue['byear']) ? ' - '.$CI->lang->line('year').' : '.$fvalue['byear'] : '').'
                      '.(!empty($fvalue['month']) ? ' - '.$CI->lang->line('month').' : '.$fvalue['month'] : '').'
                      </td>
                      </tr>
                   </table>
                </td>
             </tr>

          </table>';


          $CI->load->library("Mpdf");
          $mpdf = new Mpdf('ta_IN','A4',9,'lf',2,2,2,0,0,0);
          $mpdf->WriteHTML($stylesheet,1);
          $mpdf->WriteHTML($html);
          ob_clean();
          $mpdf->Output();
          exit;
      }
}
if (!function_exists('pdf_view'))
{
    function pdf_view()
    {
      $CI = get_instance();
      $CI->load->model('profile_model');
      $CI->load->helper('url');
      $CI->load->helper('cookie');
      $lang =  get_cookie('lang');
      $CI->lang->load('common', empty($lang) ? 'english' : $lang);
      $lang = $lang == 'tamil' ? 2 : 1;

      $id     = (int)$CI->input->get('id',true);
      if(empty($id)) {
        die('foo - No id specified');
      }
      $fvalue = $CI->profile_model->getProfile($id);
      $ftamil = $CI->profile_model->getLang($id, $lang);
      $fvalue['laknam'] = empty($fvalue['laknam']) ? 0 : $fvalue['laknam'];
      $fvalue['thisai'] = empty($fvalue['thisai']) ? 0 : $fvalue['thisai'];
      $fvalue['annual_income'] = empty($fvalue['annual_income']) ? 0 : $fvalue['annual_income'];
      $fvalue['nakshatram'] = empty($fvalue['nakshatram']) ? 0 : $fvalue['nakshatram'];
      $fvalue['rasi'] = empty($fvalue['rasi']) ? 0 : $fvalue['rasi'];
      $fvalue['jathagam'] = empty($fvalue['jathagam']) ? 0 : $fvalue['jathagam'];

      if($lang == 1) {

        $Lagnam = array(0=>"",1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
        $thisaiiruppu = array(0=>"",1=>"Guru thisai","Sani thisai","kethu thisai","Sukura thisai","Suriya thisai","Chandra thisai","Sewwai thisai","Ragu thisai","Budhan thisai");
        $rasi = array(0=>"",1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
        $nakshatram = array(0=>"",1=>"Aswini","Bharani","Karthikai",
  "Rohini","Mirugasiridam","Thiruvathirai","Punarpusam","Pusam","Ayilyam","Maham","Puram","Uthiram","Atham",
  "Chithirai","Swathiv","Visakam","Anusam","Ketai","Mulam", "Puradam","Uthiradam","Thiruvonam","Avitam",
  "Sathayam","Puratathi","Uthiratathi","Revathi");
      } else {

        $Lagnam = array(0=>"",1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $thisaiiruppu = array(0=>"",1=>"குருதிசை","சனிதிசை","கேதுதிசை","சுக்ரதிசை","சூரியதிசை","சந்திரதிசை","செவ்வாய்திசை","ராகுதிசை","புதன்திசை");
        $rasi = array(0=>"",1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $nakshatram = array(0=>"",1=>"அசுவினி","பரணி","கிருத்திகை",
        "ரோஹிணி","மிருகசீரிஷம்","திருவாதிரை","புனர்பூசம்","பூசம்","ஆயில்யம்","மகம்","பூரம்","உத்திரம்","ஹஸ்தம்",
        "சித்திரை","ஸ்வாதி","விசாகம்","அனுசம்","கேட்டை","மூலம்", "புராடம்","உத்ராடம்","திருவோணம்","அவிட்டம்",
        "சதயம்","புரட்டாதி","உத்திரட்டாதி","ரேவதி");
      }

      $aninco = $CI->lang->line('anincoar');
      $rasidata = $CI->lang->line('rasidataar');
      $laknamdata = $CI->lang->line('laknamdataar');
      $jathagam = $CI->lang->line('jathagamar');


      $jatsquare = array('rasi'=>array(),'amsam'=>array());
      if(!empty($fvalue['jatsquare'])) {
          $jatsquare = json_decode($fvalue['jatsquare'],true);
      }


      $rasi_details = array();
      $amsam_details = array();
      if(!empty($jatsquare['rasi'])) {
        foreach((array)$jatsquare['rasi'] as $val=>$key) {
           $rasi_details[$key] = isset($rasi_details[$key]) ? $rasi_details[$key].','.$rasidata[$val] : $rasidata[$val];
        }
      }
      if(!empty($jatsquare['amsam'])) {
        foreach((array)$jatsquare['amsam'] as $val=>$key) {
           $amsam_details[$key] = isset($amsam_details[$key]) ? $amsam_details[$key].' <br/>  '.$laknamdata[$val] : $laknamdata[$val];
        }
      }
      $url = '';
      if(!empty($fvalue['image'])) {
        $url = str_replace('apanel.php', '',base_url()).'uploads/'.$fvalue['image'];
      };


      $stylesheet = ' table {font-family:"arialuni" ;FONT-SIZE:9px}
      .lfh {font-family:arialuni;FONT-SIZE:16px}
      .lf {font-family:arialuni;FONT-SIZE:9px}
      .latha{font-family:"arialuni";FONT-SIZE:16px}';

      $html = '
      <table border="0" cellspacing="2" cellpadding="2" width="100%">
         <tr>
            <td align="left" class="lf"> '.$CI->lang->line('telephoneno').'  : 0422 2471928 - '.$CI->lang->line('phonenew').' : 94425 71928 </td>
            <td align="center" width="35%" class="lf"> '.$CI->lang->line('ramajeyam').' </td>
            <td align="right" width="30%" class="lf"> '.$CI->lang->line('workinghrs').'  : <span class="lf"> 9.30 AM to 5.00 PM </span> </td>
         </tr>

                    <tr>
                       <td colspan="4" align="center" class="lf"> <hr>   </td>
                    </tr>
         <tr>
            <td colspan="3" align="center" class="latha"> '.$CI->lang->line('pdftitle').'  </td>
         </tr>
         <tr>
            <td align="left" class="lf"> '.$CI->lang->line('sevvaiholiday').' </td>
            <td align="center" class="lf"> &nbsp; </td>
            <td align="right" class="lf"> '.$CI->lang->line('date').'  : '.date('d/m/Y').' </td>
         </tr>
         <tr>
            <td colspan="3" align="center" class="lf"> '.$CI->lang->line('addressfull').'   </td>
         </tr>

         <tr>
            <td colspan="4" align="center" class="lf"> <hr>   </td>
         </tr>
         <tr>
            <td align="left" class="lf"> '.$CI->lang->line('regnodate').'  : '.date('d/m/Y',strtotime($fvalue['reg_date'])).' </td>
            <td align="center" class="lf"> '.$CI->lang->line('jananjathagam').' </td>
            <td align="right" class="lf"> '.$CI->lang->line('regno').' : '.$fvalue['id'].' </td>
         </tr>
         <tr>
            <td colspan="4"><hr></td>
         </tr>
         <tr>
            <td colspan="3" align="center">
               <table border="0" width="100%">
                  <tr>
                     <td width="50%" valign="top">
                        <table border="0" width="100%" cellspacing="2" cellpadding="5">
                           <tr>
                              <td align="left" class="lf">'.$CI->lang->line('name').'</td>
                              <td colspan="3" align="left" class="lf">: '.$ftamil['name'].' </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('livingcity').'</td>
                              <td colspan="3" align="left" class="lf">: '.$ftamil['res_city'].' </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('kulam').'   </td>
                              <td  align="left" class="lf" colspan="3">: '.$ftamil['kulam'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('sevvai').' </td>
                              <td align="left" class="lf">: '.$ftamil['sevvai'].'  </td>
                              <td class="lf" align="right" >'.$CI->lang->line('ragukethu').'    </td>
                              <td  align="left" class="lf">:  '.$ftamil['ragukethu'].'  </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('laknam').'</td>
                              <td align="left" class="lf">: '.$Lagnam[$fvalue['laknam']].' </td>
                              <td class="lf" align="right">'.$CI->lang->line('thisaiiruppu').' </td>
                              <td align="left" class="lf">: '.$thisaiiruppu[$fvalue['thisai']].' </td>

                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('education').'</td>
                              <td colspan="3" align="left">: '.$ftamil['education'].' </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('incometotal').' </td>
                              <td colspan="3" align="left" class="lf">: '.$ftamil['annual_income'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('Mothers').' </td>
                              <td colspan="3" align="left" class="lf">: '.$ftamil['mother_name'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('sismarried').' </td>
                              <td class="lf" colspan="3" align="left">'.$CI->lang->line('marriedalt').' : '.$fvalue['sismarried'].' '.$CI->lang->line('unmarriedalt').' : '.$fvalue['sisunmarried'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('expectation').' </td>
                              <td colspan="3" align="left">: '.$fvalue['expectation'].'</td>
                           </tr>

                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('houseall').' </td>
                              <td align="left">: '.$fvalue['house'].'</td>
                              <td class="lf" align="left">'.$CI->lang->line('land').'</td>
                              <td align="left">: '.$fvalue['land'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('vehicle').'</td>
                              <td align="left">: '.$fvalue['vehicle'].'</td>
                              <td class="lf" align="left">'.$CI->lang->line('totalassetinfo').' 1</td>
                              <td align="left">: '.$fvalue['provalue1'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('totalassetinfo').' 2</td>
                              <td align="left">: '.$fvalue['provalue2'].'</td>
                              <td class="lf" align="left">'.$CI->lang->line('totalassetinfo').' 3</td>
                              <td align="left">: '.$fvalue['provalue3'].'</td>
                           </tr>

                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('totalasset').' </td>
                              <td colspan="3" align="left">: '.$fvalue['totalpro'].'</td>
                           </tr>



                        </table>
                     </td>
                     <td rowspan="10" colspan="2" valign="top">
                        <table border="0" width="100%" cellspacing="2" cellpadding="5">
                          <tr>
                             <td align="left" class="lf">'.$CI->lang->line('birthdate').' </td>
                             <td align="left" colspan="3">: '.date('d/m/Y',strtotime($fvalue['dob'])).' </td>
                          </tr>
                          <tr>
                             <td class="lf" align="left">'.$CI->lang->line('height').' </td>
                             <td align="left">: '.$ftamil['height'].' </td>
                             <td class="lf" align="right"> '.$CI->lang->line('color').' </td>
                             <td  align="left">:  '.$ftamil['color'].' </td>
                          </tr>

                          <tr>
                          <td class="lf" align="left"> '.$CI->lang->line('kulathievam').'  </td>
                          <td align="left" class="lf" colspan="3">: '.$ftamil['kulam_temple'].' </td>
                          </tr>

                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('jenmanakshatra').' </td>
                              <td align="left" class="lf">: '.$nakshatram[$fvalue['nakshatram']].'  </td>
                              <td class="lf" align="right" >'.$CI->lang->line('rasi').'  </td>
                              <td  align="left" class="lf">:  '.$rasi[$fvalue['rasi']].'  </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('birthinfo').'</td>
                              <td colspan="3" align="left" class="lf">:  '.$CI->lang->line('tamilenglish').' '.$ftamil['tamil_birth'].'
                              '.$CI->lang->line('year').' '.$ftamil['tamil_year'].'
                              '.$CI->lang->line('month').' '.$ftamil['tamil_month'].'
                              '.$CI->lang->line('date').' '.$ftamil['tamil_month'].'
                              '.$CI->lang->line('seconds').'   '.$ftamil['tamil_nazhigai'].'
                              '.$CI->lang->line('time').' '.$ftamil['tamil_time'].'</td>
                           </tr>

                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('work').'</td>
                              <td colspan="3" align="left">: '.$ftamil['job'].' </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('fathername').' </td>
                              <td colspan="3" align="left" class="lf">:  '.$ftamil['father_name'].'  </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left"> '.$CI->lang->line('bromarried').' </td>
                              <td  class="lf" colspan="3" align="left">'.$CI->lang->line('marriedalt').' : '.$fvalue['bromarried'].'
                              '.$CI->lang->line('unmarriedalt').' : '.$fvalue['brounmarried'].' </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('informations').' </td>
                              <td colspan="3" align="left">: '.$fvalue['information'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">'.$CI->lang->line('district').' </td>
                              <td colspan="3" align="left">: '.$fvalue['district'].'</td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td colspan="3"  >
               <table width="100%">
                  <tr>
                     <td>
                     <table border="1" cellspacing="0" cellpadding="2" width="360px">
                        <tr>
                           <td height="60px" width="25%"> '.(isset($rasi_details[12]) ? $rasi_details[12] : '&nbsp;' ).'  </td>
                           <td width="25%"> '.(isset($rasi_details[1]) ? $rasi_details[1] : '&nbsp;' ).' </td>
                           <td width="25%"> '.(isset($rasi_details[2]) ? $rasi_details[2] : '&nbsp;' ).' </td>
                           <td width="25%"> '.(isset($rasi_details[3]) ? $rasi_details[3] : '&nbsp;' ).' </td>
                        </tr>
                        <tr>
                           <td height="60px" width="25%"> '.(isset($rasi_details[11]) ? $rasi_details[11] : '&nbsp;' ).' </td>
                           <td rowspan="2" colspan="2" align="center" height="120px" width="50%">
                              <div style="vertica-align: middle;padding-top: 10px;font-size:30px;font-color:#cdcccc;text-shadow: 2px 2px #cdcdcd;">
                               '.$CI->lang->line('rasi').'
                              </div>
                           </td>
                           <td width="25%"> '.(isset($rasi_details[4]) ? $rasi_details[4] : '&nbsp;' ).' </td>
                        </tr>
                        <tr>
                           <td height="60px" width="25%"> '.(isset($rasi_details[10]) ? $rasi_details[10] : '&nbsp;' ).' </td>
                           <td width="25%"> '.(isset($rasi_details[5]) ? $rasi_details[5] : '&nbsp;' ).' </td>
                        </tr>
                        <tr>
                           <td height="60px" width="25%"> '.(isset($rasi_details[9]) ? $rasi_details[9] : '&nbsp;' ).' </td>
                           <td width="25%"> '.(isset($rasi_details[8]) ? $rasi_details[8] : '&nbsp;' ).' </td>
                           <td width="25%">'.(isset($rasi_details[7]) ? $rasi_details[7] : '&nbsp;' ).' </td>
                           <td width="25%"> '.(isset($rasi_details[6]) ? $rasi_details[6] : '&nbsp;' ).' </td>
                        </tr>
                     </table>
                     </td>
                     <td>
                        <table border="1" cellspacing="0" cellpadding="2"  width="360px">
                           <tr>
                              <td height="60px" width="25%"> '.(isset($amsam_details[12]) ? $amsam_details[12] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[1]) ? $amsam_details[1] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[2]) ? $amsam_details[2] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[3]) ? $amsam_details[3] : '' ).' &nbsp; </td>
                           </tr>
                           <tr>
                              <td height="60px" width="25%"> '.(isset($amsam_details[11]) ? $amsam_details[11] : '' ).' &nbsp; </td>
                              <td rowspan="2" colspan="2" align="center" height="120px" width="50%">
                                 <div style="vertica-align: middle;padding-top: 10px;font-size:30px;font-color:#cdcccc;text-shadow: 2px 2px #cdcdcd;">
                                    '.$CI->lang->line('amsam').'
                                 </div>
                              </td>
                              <td width="25%">    '.(isset($amsam_details[4]) ? $amsam_details[4] : '' ).' &nbsp; </td>
                           </tr>
                           <tr>
                              <td height="60px" width="25%"> '.(isset($amsam_details[10]) ? $amsam_details[10] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[5]) ? $amsam_details[5] : '' ).' &nbsp; </td>
                           </tr>
                           <tr>
                              <td height="60px" width="25%"> '.(isset($amsam_details[9]) ? $amsam_details[9] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[8]) ? $amsam_details[8] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[7]) ? $amsam_details[7] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[6]) ? $amsam_details[6] : '' ).' &nbsp; </td>
                           </tr>
                        </table>
                     </td>
                     <td width="10%" class="lf">
                        '.$jathagam[$fvalue['jathagam']].'
                        '.(!empty($fvalue['balance']) ? '<br> - '.$CI->lang->line('iruppu').' : '.$fvalue['balance'] : '').'
                        '.(!empty($fvalue['byear']) ? '<br> - '.$CI->lang->line('year').' : '.$fvalue['byear'] : '').'
                        '.(!empty($fvalue['month']) ? '<br> - '.$CI->lang->line('month').' : '.$fvalue['month'] : '').'
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>';

     $CI->load->library("Mpdf");
     $mpdf = new Mpdf('ta_IN','A4',9,'lf',2,2,2,0,0,0);
     $mpdf->WriteHTML($stylesheet,1);
     $mpdf->WriteHTML($html);
     ob_clean();
     $mpdf->Output();
     exit;
    }
}
