<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="container-fluid banner_bg text-right no-gutters pr-0 pl-0">
                <img src="<?php echo $base_url; ?>images/register.png" alt=""/>
                <h1>Forgot Password</h1>
            </div>
            <div class="container">
                <div class="row paddindb100">
                    <div class="col-12 col-md-7">
                        <form name="regform" id="regform" data-toggle="validator"  action="<?php echo $url; ?>index.php/login/forgot" method="post">
                            <div class="form_div">

                                <?php if(!empty($message)): ?>
                                <div class="alert alert-info">
                                  <?php echo $message; ?>
                                </div>
                                <?php endif; ?>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('email'); ?><span class="mandatory">*</span></label>

                                    <div class="col-12 col-sm-7">
                                        <input class="form-control form-control-lg" name="email" type="text" id="email" size="30" placeholder="E-Mail" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-12 col-sm-9">
                                      <div class="row">
                                          <div class="col-12 col-md-6">
                                              <button name="submit" type="submit" class="btn_all wave_button" id="submit">Change Password</button>
                                          </div>

                                          <div class="col-12 col-md-6">
                                              <a href="<?php echo $url; ?>index.php/login" class="btn_all" >Back To Login</a>
                                          </div>
                                      </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="form-rht-desc">
                            <h1><?php echo $this->lang->line('fsuitable'); ?><br>
                                <span><?php echo $this->lang->line('gavaratitle'); ?></span><br>
                                <?php echo $this->lang->line('Companion'); ?></h1>
                            <p><?php echo $this->lang->line('asaviewer'); ?>.</p>
                            <h2><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $this->lang->line('Benefits'); ?>:</h2>
                            <ul>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('Findmatchedhoros'); ?>.</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('Suitablesoul'); ?>.</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('receivehoroscope'); ?>.</li>
                            </ul>
                            <h2><i class="fa fa-search" aria-hidden="true"></i><?php echo $this->lang->line('informationwill'); ?>:</h2>
                            <ul>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('startyoursearch'); ?>.</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('informationexamined'); ?>.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
