            <div class="container-fluid pr-0 pl-0 groombg">
                <h1>Assigned Profiles</h1>
                <img src="<?php echo $base_url; ?>images/groom.jpg" alt="" style="max-height:200px"/>

            </div>
            <div class="container">
                <div class="row paddindb100">

                  <ul class="nav nav-pills">
                   <li><a href="<?php echo $url; ?>index.php/profile" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('myprofiles'); ?></a></li>
                   <li><a href="<?php echo $url; ?>index.php/profile/assigned" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('profilesheader'); ?></a></li>
                   <li><a href="<?php echo $url; ?>index.php/profile/feedback" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('feedbackheader'); ?></a></li>
                   <li><a href="<?php echo $url; ?>index.php/profile/change" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('changepassword'); ?></a></li>
                   <li><a href="<?php echo $url; ?>index.php/profile/logout" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('logout'); ?></a></li>
                  </ul>
                    <?php if(empty($profiles)): ?>
                      <div class="col-12">
                        <div class="col-12 txt-space pull-right"> No Profiles Found, Please contact us to get profile data </div>
                      </div>
                    <?php endif; ?>

                    <div class="col-12">
                    <?php foreach($profiles as $data): ?>
                    <div class="row profile_details">
                    <div class="col-12 col-md-3">
                        <div class="pic_show">
                            <div class="zoom_click text-center" data-pswp-uid="1">
                                <?php if(empty($data['image'])) : ?>
                                <a href="<?php echo $base_url; ?>images/<?php if($data['gender'] == 1):?>groomdefault.jpg<?php else: ?>groomdefault.jpg<?php endif; ?>" class="preview" title="">
                                    <img src="<?php echo $base_url; ?>images/<?php if($data['gender'] == 1):?>groomdefault.jpg<?php else: ?>groomdefault.jpg<?php endif; ?>" alt="" class="img-fluid">
                                </a>
                              <?php else:  ?>
                                <a href="<?php echo $url; ?>uploads/<?php echo $data['image']; ?>" class="preview" title="">
                                    <img src="<?php echo $url; ?>uploads/<?php echo $data['image']; ?>" alt="" class="img-fluid"  style="max-height:300px">
                                </a>
                              <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="row no-gutters">
                            <div class="col-6 txt-space"><?php echo $this->lang->line('regno'); ?></div>
                            <div class="col-6 txt-space"><?php echo $data['profile_id']; ?></div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-6 txt-space"><?php echo $this->lang->line('name'); ?></div>
                            <div class="col-6 txt-space"><?php echo $data['name']; ?></div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-6 txt-space"><?php echo $this->lang->line('education'); ?></div>
                            <div class="col-6 txt-space"><?php echo $data['education']; ?></div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-6 txt-space"><?php echo $this->lang->line('fathername'); ?></div>
                            <div class="col-6 txt-space"><?php echo $data['father_name']; ?></div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-6 txt-space"><?php echo $this->lang->line('district'); ?></div>
                            <div class="col-6 txt-space"><?php echo $data['district']; ?></div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-6 txt-space"><?php echo $this->lang->line('birthdate'); ?></div>
                            <div class="col-6 txt-space"><?php echo !empty($data['dob']) ? date('d/m/Y',strtotime($data['dob'])): ''; ?></div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-6 txt-space"><?php echo $this->lang->line('sevvai'); ?></div>
                            <div class="col-6 txt-space"><?php echo $data['sevvai']; ?></div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-6 txt-space"><?php echo $this->lang->line('totalasset'); ?></div>
                            <div class="col-6 txt-space"><?php echo $data['totalpro']; ?></div>
                        </div>

                        <div class="row no-gutters">
                            <div class="col-6 txt-space"><?php echo $this->lang->line('phone'); ?></div>
                            <div class="col-6 txt-space"><?php echo $data['phone']; ?></div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-6 txt-space"><?php echo $this->lang->line('email'); ?></div>
                            <div class="col-6 txt-space"><?php echo $data['email']; ?></div>
                        </div>

                    </div>
                    <div class="col-12 col-md-3">
                        <div class="horoscope row">
                            <div class="col-12 text-center">
                                <p><?php echo $this->lang->line('jathainfo'); ?></p>
                            </div>
                            <div class="col-12 text-center">
                              <a href="<?php echo $url; ?>index.php/groom/pdf?id=<?php echo $data['profile_id']; ?>" title="">
                                <img src="<?php echo $base_url; ?>images/horoscope.jpg" alt="" class="img-fluid">
                              </a>
                            </div>
                            <div class="col-12 text-center">
                                <a href="javascript:void(0)" class="more_horoscope"><?php echo $this->lang->line('extrainfo'); ?></a>
                            </div>
                        </div>
                    </div>
                    </div>
                    <?php endforeach; ?>
                    </div>

                    <style>

                  .pagination {
                    height: 36px;
                    margin: 18px 0;
                  }
                  .pagination ul {
                    display: inline-block;
                    *display: inline;
                    /* IE7 inline-block hack */

                    *zoom: 1;
                    margin-left: 0;
                    margin-bottom: 0;
                    -webkit-border-radius: 3px;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                    -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
                    -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
                    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
                  }
                  .pagination li {
                    display: inline;
                  }
                  .pagination a {
                    float: left;
                    padding: 0 14px;
                    line-height: 34px;
                    text-decoration: none;
                    border: 1px solid #ddd;
                    border-left-width: 0;
                  }
                  .pagination a:hover,
                  .pagination .active a {
                    background-color: #f5f5f5;
                  }
                  .pagination .active a {
                    color: #999999;
                    cursor: default;
                  }
                  .pagination .disabled span,
                  .pagination .disabled a,
                  .pagination .disabled a:hover {
                    color: #999999;
                    background-color: transparent;
                    cursor: default;
                  }
                  .pagination li:first-child a {
                    border-left-width: 1px;
                    -webkit-border-radius: 3px 0 0 3px;
                    -moz-border-radius: 3px 0 0 3px;
                    border-radius: 3px 0 0 3px;
                  }
                  .pagination li:last-child a {
                    -webkit-border-radius: 0 3px 3px 0;
                    -moz-border-radius: 0 3px 3px 0;
                    border-radius: 0 3px 3px 0;
                  }
                  .pagination-centered {
                    text-align: center;
                  }
                  .pagination-right {
                    text-align: right;
                  }
                  .pager {
                    margin-left: 0;
                    margin-bottom: 18px;
                    list-style: none;
                    text-align: center;
                    *zoom: 1;
                  }
                  .pager:before,
                  .pager:after {
                    display: table;
                    content: "";
                  }
                  .pager:after {
                    clear: both;
                  }
                  .pager li {
                    display: inline;
                  }
                  .pager a {
                    display: inline-block;
                    padding: 5px 14px;
                    background-color: #fff;
                    border: 1px solid #ddd;
                    -webkit-border-radius: 15px;
                    -moz-border-radius: 15px;
                    border-radius: 15px;
                  }
                  .pager a:hover {
                    text-decoration: none;
                    background-color: #f5f5f5;
                  }
                  .pager .next a {
                    float: right;
                  }
                  .pager .previous a {
                    float: left;
                  }
                  .pager .disabled a,
                  .pager .disabled a:hover {
                    color: #999999;
                    background-color: #fff;
                    cursor: default;
                  }
                    </style>
                    <div class="pull-right">
                      <nav aria-label="Page navigation example"><?php echo $pagination; ?></nav>
                    </div>
                </div>
            </div>
