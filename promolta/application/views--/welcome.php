<section class="cd-hero">
    <!--autoplay-->
    <ul class="cd-hero-slider autoplay">
        <li class="selected banner1_li">
            <h1 class="container logo_txt"><?php echo $this->lang->line('title'); ?></h1>
            <div class="cd-half-width cd-img-container">
                <img src="<?php echo $base_url; ?>images/slider/marriage1.png" alt="banner 1">
            </div>
            <div class="cd-half-width reg_txt reg_txt1 hidden-sm-down">
                <h3><?php echo $this->lang->line('seekpartner'); ?></h3>
                <h1><?php echo $this->lang->line('withtitle'); ?></h1>
                <a href="<?php echo $url; ?>index.php/contact" class="reg-btn wave_button"><?php echo $this->lang->line('regnow'); ?></a>
                <p><?php echo $this->lang->line('infoprotected'); ?></p>
            </div>
        </li>

        <li class="banner2_li">
            <h1 class="container logo_txt"><?php echo $this->lang->line('title'); ?></h1>
            <div class="cd-half-width reg_txt reg_txt2 hidden-sm-down">
                <h3><?php echo $this->lang->line('seekpartner'); ?></h3>
                <h1><?php echo $this->lang->line('withtitle'); ?></h1>
                <a href="<?php echo $url; ?>index.php/contact" class="reg-btn wave_button"><?php echo $this->lang->line('regnow'); ?></a>
                <p><?php echo $this->lang->line('infoprotected'); ?></p>
            </div>
            <div class="cd-half-width cd-img-container">
                <img src="<?php echo $base_url; ?>images/slider/marriage2.png" alt="banner 2">
            </div>
        </li>
    </ul>
</section>
<div class="container">
    <div class="row paddindb100">
        <div class="col-12 hidden-md-up">
            <div class="reg_txt reg_txt2">
                <h3><?php echo $this->lang->line('seekpartner'); ?></h3>
                <h1><?php echo $this->lang->line('withtitle'); ?></h1>
                <a href="<?php echo $url; ?>index.php/contact" class="reg-btn wave_button"><?php echo $this->lang->line('regnow'); ?></a>
                <p><?php echo $this->lang->line('infoprotected'); ?></p>
            </div>
        </div>
        <div class="col-12 col-md-7 home_txt">
            <h1><?php echo $this->lang->line('welcometitle'); ?></h1>
            <p><?php echo $this->lang->line('welcomecontent'); ?></p>
            <div class="home_serach">
                <a href="<?php echo $url; ?>index.php/bride" class="reg-btn wave_button"><?php echo $this->lang->line('searchbride'); ?></a>
                <a href="<?php echo $url; ?>index.php/groom" class="reg-btn reg-btn-groom wave_button"><?php echo $this->lang->line('searchgroom'); ?></a>
            </div>
        </div>
        <div class="col-12 col-md-5">
            <div class="reg_right">
                <h1><i class="fa fa-pencil-square-o fa-2x valignm" aria-hidden="true"></i><?php echo $this->lang->line('regeasily'); ?> </h1>
                <h2><?php echo $this->lang->line('choosereg'); ?></h2>
                <h1><i class="fa fa-inr fa-2x valignm" aria-hidden="true"></i> <?php echo $this->lang->line('paidreg'); ?></h1>
                <ul class="reg_ul">
                    <li><i class="fa fa-hand-o-right valignm" aria-hidden="true"></i> <?php echo $this->lang->line('regamount'); ?> /-</li>
                    <li><i class="fa fa-hand-o-right valignm" aria-hidden="true"></i> <?php echo $this->lang->line('reggetinfo'); ?></li>
                    <li><i class="fa fa-hand-o-right valignm" aria-hidden="true"></i> <?php echo $this->lang->line('regvalidity'); ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
