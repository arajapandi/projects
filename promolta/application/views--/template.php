<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title><?php echo $this->lang->line('title'); ?></title>
        <meta charset="UTF-8">
        <meta name="theme-color" content="#f55100" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link type="image/png" rel="icon" href="<?php echo $base_url; ?>images/Gicon.png" sizes="32x32 16x16">
        <link href="<?php echo $base_url; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base_url; ?>css/menu.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base_url; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base_url; ?>css/hero.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $base_url; ?>stylesheets/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="master_container">
            <div class="header_full">
                <header class="container">
                    <div id="fix-header">
                        <h1 class="hidden-md-up top_logo_txt"><?php echo $this->lang->line('title'); ?></h1>
                        <div class="material-menu-button"><span></span><span></span><span></span></div>
                        <div class="m_disp" id="menu-rheader">
                            <div class="material-menu">
                                <nav class="bmd-navbar">
                                    <ul>
                                        <li><a href="<?php echo $url; ?>" class="<?php echo ($page_name == 'welcome') ? 'active ': '';?>menulink wave_button"><?php echo $this->lang->line('Home'); ?></a></li>
                                        <li><a href="<?php echo $url; ?>index.php/bride" class="<?php echo ($page_name == 'bride') ? 'active ': '';?>menulink wave_button"><?php echo $this->lang->line('Bride'); ?></a></li>
                                        <li><a href="<?php echo $url; ?>index.php/groom" class="<?php echo ($page_name == 'groom') ? 'active ': '';?>menulink wave_button"><?php echo $this->lang->line('Groom'); ?></a></li>
                                        <!--<li><a href="<?php echo $url; ?>index.php/registration" class="<?php echo ($page_name == 'registration') ? 'active ': '';?>menulink wave_button"><?php echo $this->lang->line('Registration'); ?></a></li>-->
                                        <li><a href="<?php echo $url; ?>index.php/payment" class="<?php echo ($page_name == 'payment') ? 'active ': '';?>menulink wave_button"><?php echo $this->lang->line('Payment options'); ?></a></li>
                                        <li><a href="<?php echo $url; ?>index.php/contact" class="<?php echo ($page_name == 'contact') ? 'active ': '';?>menulink wave_button"><?php echo $this->lang->line('contactus'); ?></a></li>
                                        <li class="hidden-md-up"><a href="javascript:void(0);" class="menulink"><?php echo $this->lang->line('Select Your Language'); ?></a></li>
                                        <li class="hidden-md-up"><a href="<?php echo $url; ?>index.php/welcome/change?lang=english" class="menulink wave_button"><?php echo $this->lang->line('English'); ?></a></li>
                                        <li class="hidden-md-up"><a href="<?php echo $url; ?>index.php/welcome/change?lang=tamil" class="menulink wave_button tamil_lang"><?php echo $this->lang->line('tamil'); ?>d dd</a></li>
                                    </ul>
                                </nav>


                                <div class="lang_wrap row">

                                  <nav class="bmd-navbar pull-left">
                                    <ul>
                                        <li>
                                          <?php if(!empty($profile_id)): ?>
                                            <a href="<?php echo $url; ?>index.php/profile" class="<?php echo ($page_name == 'profile') ? 'active ': '';?>menulink wave_button">
                                              <?php echo $this->lang->line('profilesheader'); ?></a></li>
                                          <?php else:?>
                                            <a href="<?php echo $url; ?>index.php/login" class="<?php echo ($page_name == 'login') ? 'active ': '';?>menulink wave_button">
                                                <?php echo $this->lang->line('loginlo'); ?></a></li>
                                          <?php endif; ?>
                                      </ul>
                                    </nav>

                                    <ul class="pull-right">
                                    <li class="lang_li"><?php echo $this->lang->line('Select Your Language'); ?> :
                                      <span>
                                        <a href="<?php echo $url; ?>index.php/welcome/change?lang=english" class="wave_button"><?php echo $this->lang->line('English'); ?></a>
                                      </span> /
                                      <span>
                                        <a href="<?php echo $url; ?>index.php/welcome/change?lang=tamil" class="wave_button tamil_lang"><?php echo $this->lang->line('tamil'); ?></a>
                                      </span>
                                    </li></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>

            <?php echo $body_content; ?>


            <div class="container-fluid footer_full">
                <div class="row">
                    <div class="container">
                        <footer class="footer_cell">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <h1><?php echo $this->lang->line('Contact Us'); ?></h1>
                                    <ul class="list-unstyled footer_ul">
                                        <li class="sanfont"><b class="proxifont"><?php echo $this->lang->line('Call us'); ?>:</b> <a href="tel:9442571928" class="color-white">+91 94425 71928</a></li>
                                        <li class="sanfont"><b class="proxifont"><?php echo $this->lang->line('Email us'); ?>:</b> <a href="mailto:balijanaidutrust@gmail.com" class="color-white">balijanaidutrust@gmail.com</a></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-md-4">
                                    <h1><?php echo $this->lang->line('Follow Us'); ?></h1>
                                    <ul class="list-unstyled footer_ul">
                                        <li class="social_li">
                                            <span><a href="javascript:void(0);" class="fb"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></span>
                                            <span><a href="javascript:void(0);" class="tw"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a></span>
                                            <span><a href="javascript:void(0);" class="in"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a></span>
                                            <span><a href="javascript:void(0);" class="gp"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a></span>
                                        </li>
                                        <li class="sanfont"><b class="proxifont"><?php echo $this->lang->line('Timing'); ?>:</b> 9.30 AM to 5 PM</li>
                                        <li class="sanfont"><b class="proxifont"><?php echo $this->lang->line('Sunday Holiday'); ?> </b></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-md-4">
                                    <h1><?php echo $this->lang->line('Reach Us'); ?></h1>
                                    <ul class="list-unstyled footer_ul">
                                        <li class="sanfont">
                                          <?php echo $this->lang->line('addressf'); ?>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-12 copy_right">
                                    <p><?php echo $this->lang->line('reserved'); ?>.</p>
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
            </div>
            <script src="<?php echo $base_url; ?>js/jquery-3.1.1.min.js" type="text/javascript"></script>
            <script src="<?php echo $base_url; ?>js/modernizr.js" type="text/javascript"></script>
            <script src="<?php echo $base_url; ?>js/jquery.easing.1.3.min.js" type="text/javascript"></script>
            <script src="<?php echo $base_url; ?>js/materialmenu.js" type="text/javascript"></script>
            <script src="<?php echo $base_url; ?>js/waves.min.js" type="text/javascript"></script>
            <script src="<?php echo $base_url; ?>js/hero.js" type="text/javascript"></script>
            <script src="<?php echo $base_url; ?>js/web.js" type="text/javascript"></script>
            </body>
            </html>
