  <div class="container-fluid pr-0 pl-0 groombg">
      <h1>Profile</h1>
      <img src="<?php echo $base_url; ?>images/groom.jpg" alt="" style="max-height:200px"/>
  </div>



  <div class="container">
      <div class="row paddindb100">
          <div class="col-12">





<?php if(!empty($message)): ?>

  <div class="alert alert-info">
    <?php echo $message; ?>
  </div>

<?php endif; ?>

<ul class="nav nav-pills">
  <li><a href="<?php echo $url; ?>index.php/profile" class="btn_all btn_reset wave_button active"><?php echo $this->lang->line('myprofiles'); ?></a></li>
  <li><a href="<?php echo $url; ?>index.php/profile/assigned" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('profilesheader'); ?></a></li>
  <li><a href="<?php echo $url; ?>index.php/profile/feedback" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('feedbackheader'); ?></a></li>
  <li><a href="<?php echo $url; ?>index.php/profile/change" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('changepassword'); ?></a></li>
  <li><a href="<?php echo $url; ?>index.php/profile/logout" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('logout'); ?></a></li>
</ul>
<div class="row">

              <div class="col-12 col-md-6 profile_details">

                  <?php if(empty($profile_package)): ?>

                    <div class="row">
                      <div class="col-12 txt-space pull-right"> No Package Found, Please contact us to add package </div>
                    </div>

                  <?php else: ?>
                  <div class="row">
                    <div class="col-6 txt-space"><?php echo $this->lang->line('expireondat'); ?> : </div>
                    <div class="col-6 txt-space"><?php echo date('d/m/Y',strtotime($profile_package['end_time'])); ?></div>

                    <div class="col-6 txt-space"><?php echo $this->lang->line('packageremainingcount'); ?> : </div>
                      <div class="col-6 txt-space"><?php echo $profile_package['rem_count']; ?></div>


                      <div class="col-6 txt-space"><?php echo $this->lang->line('costheader'); ?> : </div>
                      <div class="col-6 txt-space"><?php echo $profile_package['cost']; ?></div>
                  </div>
                <?php endif; ?>

              </div>
            </div>

          </div>
      </div>
  </div>
