<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends REST_Controller {

    public function __construct() {
            parent::__construct();
            $this->load->model('message_model');
            $this->return  = array('success'=>false);
    }

    public function index_get()
    {
        die('get disabled.. ');
    }

    public function index_post() {
        
        $user 		= $this->input->post('user');
        $auth 		= $this->input->post('auth');
        if(!empty($user) && !empty($auth)) {
            $this->load->model('auth_model');
            $userData   = $this->auth_model->getUser($user);
            if(!empty($userData) && strlen($auth) == 32 && $userData['auth'] == $auth) {
                $call 		= $this->input->post('call');
                if(!empty($call)) { 
                    $call = $this->input->post('call');
                    //if(property_exists($this,"_".$call)) {
                    //    echo $call;exit;
                        $this->{'_'.$call}($user);
                    //}
                }
            }
        }
    }
    
    private function _getAccounts() {
        $accounts = $this->message_model->getAccounts();
        if(!empty($accounts)) {
            $this->return['data'] = $accounts;
            $this->return['success'] = true;
        }
        $this->response($this->return);
    }
    private function _getDraft($userId) {
        
        $message = $this->message_model->getDrafts($userId, 0, 100);
        if(!empty($message)) {
            $this->return['data'] = $message;
            $this->return['success'] = true;
        }

        $this->response($this->return);
    }
    private function _markMessage($userId) {
        
        $messageId      = $this->input->post('messageId');        
        $this->message_model->markMessages($messageId);
        $this->return['success'] = true;
        $this->response($this->return);
    }

    private function _getSent($userId) {
        $message = $this->message_model->sentMessages($userId, 0, 100);
        if(!empty($message)) {
            $this->return['data'] = $message;
            $this->return['success'] = true;
        }
        $this->response($this->return);
    }
    private function _trashMessage($userId) {
        $messageId      = $this->input->post('messageId');        
        $delete      = $this->input->post('delete');
        if($delete == 'yes') {
            $this->message_model->deleteMessages($messageId);
        } else {
            $this->message_model->trashMessages($messageId);
        }
        $this->return['success'] = true;
        $this->response($this->return);
    }
    private function _getTrash($userId) {
                
        $messages   = $this->message_model->getMessages($userId, 0, 100, 'trash');
        if(!empty($messages)) {

            $this->return['data'] = $messages;
            $this->return['success'] = true;
        }
        $this->response($this->return);
    }
    private function _getInbox($userId) {
                
        $messages   = $this->message_model->getMessages($userId);
        if(!empty($messages)) {

            $this->return['data'] = $messages;
            $this->return['success'] = true;
        }
        $this->response($this->return);
    }
}
