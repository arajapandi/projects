<?php

/**
 * Authentication model Class
 *
 * validate user data, Login,  Create authenticator, validate authenticator
 *
 * @category    	Model
 * @author        	Rajapandian a
 * @license         NA
 * @link            NA
 */
class Auth_model extends CI_Model {
    
    /**
     * @function __construct
     * @return NA
     */
    public function __construct() {

    }
    /**
     * @function login
     * @return string
     */

    public function login($user, $pass) {

        if(!empty($user) && !empty($pass)) {
            $dbObj = $this->db->get_where('accounts', array('username' => $user));
            $row = $dbObj->row_array();
            if(!empty($row)) {
                $md5 = md5($pass.$row['salt']);
                if($row['passwd'] == $md5) {
                    $auth = md5($user.time());
                    $this->updateAuth($row['id'], $auth);
                    return array('userId'=>$row['id'],'auth'=>$auth);
                }
            }
        }
        return array();
    }
    public function getUser($userId) {
        $dbObj = $this->db->get_where('accounts', array('id' => $userId));
        return $row = $dbObj->row_array();
    }
    
    private function updateAuth($userId, $auth) {
        try {

            if(!empty($auth) && !empty($userId)) {
                $data = array(
                       'auth' => $auth,
                       'updated_at'=>date('Y-m-d H:i:s')
                    );

                $this->db->where('id', $userId);
                $this->db->update('accounts', $data);

                return true;
            }
        } catch (Exception $e) {
            //log it..
        }
        return false;

    }
    /**
     * @function login
     * @return string
     */

    public function validateAuth($userId, $auth) {
        
        $query = $this->db->get_where('accounts', array('id' => $userId));
        if(!empty($query)) {
            $account    = $query->row_array();
            $hrs24      = 24*3600;
            $updated_df = time() - strtotime($account['updated_at']);

            if($updated_df < $hrs24 && $account['auth'] == $auth) {
                return true;
            }
        }

        return false;
    }



    /**
     * curl function - rest api call
     * 
     * @function get_url_content
     * @param string full url
     * @return string or bool
     */
    public function get_url_content($url) {

        $this->load->library('curl');
        $this->curl->create($url);
        $this->curl->http_method('get');
        $this->curl->option('connecttimeout', 600);
        $data = $this->curl->execute();
        $info = $this->curl->info;
        if ($info['http_code'] == 200) {
            return $data;
        }
        return false;
    }

}
