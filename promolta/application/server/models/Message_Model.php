<?php

/**
 * Message model Class
 *
 * @category    	Messages Model
 * @author        	Rajapandian a
 * @license         NA
 * @link            NA
 */
class Message_model extends CI_Model {
    
    /**
     * @function getMessages
     * @return object
     */    
    function getMessages($userId, $offset=0, $per_page=10, $status='inbox')
    {
        
        $this->db->select('um.id as messageId, um.user_id,um.status,um.mark,
                            um.created_at,ms.from,ms.to_accounts,ms.attach,ms.subject,ms.message,')
                     ->from('user_messages as um')
                     ->join('messages as ms', 'um.message_id = ms.id')
                     ->where('um.user_id', $userId);
        
        if($status == 'inbox') {
            $this->db->where('um.status !=', 2);
        } else if($status == 'trash') {
            $this->db->where('um.status =', 2);
        }
        $this->db->order_by('um.id', 'desc');
        $this->db->limit($per_page, $offset);
        
        return $this->db->get()->result_array();
    }
    function getAccounts() {
        
        $this->db->select('ms.name as key, ms.username as value')
                    ->from('accounts as ms');
        return $this->db->get()->result_array();
    }
    function sentMessages($userId, $offset=0, $per_page=10) {
        
        if((int)($userId) == $userId) {
            $this->load->model('auth_model');
            $object = $this->auth_model->getUser($userId);
            if(empty($object)) {
                return false;
            }
            $userId = $object['username'];
        }
        
        $this->db->select('ms.from,ms.to_accounts,ms.attach,ms.subject,ms.message,ms.created_at')
                    ->from('messages as ms')
                    ->where('ms.from', $userId);
        $this->db->order_by('ms.id', 'desc');
        $this->db->limit($per_page, $offset);
        
        return $this->db->get()->result_array();
    }
    
    function getDrafts($userId, $offset=0, $per_page=10) {
        
        $this->db->select('ms.*')
                    ->from('drafts as ms')
                    ->where('ms.user_id', $userId);
        $this->db->order_by('ms.id', 'desc');
        $this->db->limit($per_page, $offset);
        
        return $this->db->get()->result_array();        
    }
    
    function trashMessages($messageId) {
        $data = array(
               'status' => 2,
            );
        $this->db->where('id', $messageId);
        $this->db->update('user_messages', $data);
        return true;
    }
    function deleteMessages($messageId) {
        
        //$this->db->where('id', $messageId);
        //$this->db->where('id', $messageId);
        //$object = $this->db->get('user_messages');
        //$row = $object->row_array();
            
        $this->db->where('id', $messageId);
        $this->db->delete('user_messages');
        return true;
    }
    function markMessages($messageId) {
        
        $this->db->set('mark', 'if(mark=1,0,1)', FALSE);
        $this->db->where('id', $messageId);
        $this->db->update('user_messages');
        return true;
    }

}
