<?php

/**
 * Profile model Class
 *
 * @category        Profiles Model
 * @author          Rajapandian a
 * @license         NA
 * @link            NA
 */
class Profile_model extends CI_Model {



      function saveFeedback($fvalue) {

          $fvalue['updated_at'] = date('Y-m-d H:i:s');
          return $this->db->insert('profile_feedback', $fvalue);
      }


    /**
     * @function getAccounts
     * @return object
     */
    function getAccounts($offset=0, $per_page=10, $status=1, $gender = 0, $lang=1, $search_array = array())
    {


        $this->db->select('p.profile_id, p.name, p.education, um.image, p.father_name, um.district, um.dob, um.extra, p.sevvai, um.totalpro, um.created_at, um.updated_at')
                     ->from('profiles as um')
                     ->join('profile_details as p', 'um.id = p.profile_id and p.lang_id='. $lang);


        $this->db->where('gender =', $gender);

        if(!empty($status)) {
            $this->db->where('status =', $status);
        }
        $this->getProfileSearch($search_array);
        $this->db->order_by('p.id', 'desc');
        $this->db->limit($per_page, $offset);

        return $this->db->get()->result_array();
    }


    private function getProfileSearch($search_array) {
      if(!empty($search_array)) {

          if(!empty($search_array['profile_id'])) {
            $this->db->where('um.id = ', $search_array['profile_id']);
          }

          if(!empty($search_array['search'])) {
            $this->db->where ("um.name LIKE '%{$search_array['search']}%'");
          }

          $age_from  = 0;
          if(!empty($search_array['age_from'])) {
            $age_from =  $search_array['age_from'];
          }

          $str_where    = " TIMESTAMPDIFF(YEAR, um.dob, CURDATE()) between ";
          $where_string = "";
          if(!empty($search_array['age_to']) && !empty($age_from)) {
            $where_string = "   {$age_from} AND  ".$search_array['age_to'];
          } else if(!empty($age_from)) {
            $where_string = "   {$age_from} AND 1000 ";
          } else if(!empty($search_array['age_to'])) {
            $where_string = "   1 AND ".$search_array['age_to'];
          }
          if(!empty($where_string)) {
            $this->db->where($str_where.$where_string);
          }
        }
    }

    function getTotalProfiles($status = 1, $gender = 0, $lang=1, $search_array = array()) {
         $this->db->select(' count(1) as cnt')
                     ->from('profiles as um')
                     ->join('profile_details as p', 'um.id = p.profile_id and p.lang_id='. $lang);


        $this->db->where('um.gender =', $gender);

        if(!empty($status)) {
            $this->db->where('um.status =', $status);
        }
        $this->getProfileSearch($search_array);
        $row = $this->db->get()->row_array();

        if(!empty($row)) {
            return $row['cnt'];
        }

        return 0;
    }


    function getLang($profile_id, $lang=1) {

        $this->db->select(' * ')
                     ->from('profile_details as um');

        $this->db->where(array('profile_id'=>$profile_id,'lang_id'=>$lang));
        return $this->db->get()->row_array();

    }

        function getProfile($profile_id) {

            $this->db->select(' * ')
                         ->from('profiles as um');

            $this->db->where('id =', $profile_id);

            return $this->db->get()->row_array();
        }

}
