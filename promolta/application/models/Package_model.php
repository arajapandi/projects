<?php

/**
 * package model Class
 *
 * @category        package Model
 * @author          Rajapandian a
 * @license         NA
 * @link            NA
 */
class Package_model extends CI_Model {


    public function getProfilePackageCount($profile_id, $lang = 1, $status=1) {
        $this->db->select(' count(1) as cnt ')
                     ->from('profile_assigned as pp')
                     ->join('profiles as p', 'pp.assignd_id = p.id')
                     ->join('profile_details as p1', 'p1.profile_id = p.id');
         $this->db->where('pp.profile_id =', $profile_id);
         $this->db->where('p1.lang_id =', $lang);
         $this->db->where('p.status =', $status);
         $row = $this->db->get()->row_array();
         if(!empty($row)) {
             return $row['cnt'];
         }
         return 0;
    }

    public function getProfileAssign($profile_id, $lang = 1, $offset=0, $per_page=10, $status=1) {
      $this->db->select(' * ')
                   ->from('profile_assigned as pp')
                   ->join('profiles as p', 'pp.assignd_id = p.id')
                   ->join('profile_details as p1', 'p1.profile_id = p.id');

      $this->db->where('pp.profile_id =', $profile_id);
      $this->db->where('p1.lang_id =', $lang);
      $this->db->where('p.status =', $status);

      $this->db->order_by('p.id', 'desc');
      $this->db->limit($per_page, $offset);

      return $this->db->get()->result_array();
    }


    function getProfilePackage($profile_id) {
        $this->db->select(' * ')
                     ->from('profile_package as pp')
                     ->join('packages as p', 'pp.package_id = p.id');

        $this->db->where('pp.profile_id =', $profile_id);

        $result = $this->db->get()->row_array();
        if(!empty($result)) {
            $result['endtime'] = date('Y-m-d', strtotime($result['end_time']));
        }
        return $result;
    }

}
