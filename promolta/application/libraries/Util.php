<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Util class
 *
 * @author    Rajapandian A
 * @license   NA
 */
class Util {

    const SALT = 'saltsecret123&&8ds';
    /**
     * encrypt given string using default salt 
     * 
     * @function encrypt
     * @return string
     */
    public function encrypt($data) {
        $mcrypt = mcrypt_encrypt(
                            MCRYPT_RIJNDAEL_128,
                            self::SALT,
                            $data,
                            MCRYPT_MODE_CBC,
                            "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
                        );
        return base64_encode($mcrypt);
    }

    /**
     * decrypt given string using default salt 
     * 
     * @function decrypt
     * @return string
     */
    public function decrypt($data) {

        $decoded = base64_decode($data);
        return mcrypt_decrypt(
                    MCRYPT_RIJNDAEL_128,
                    self::SALT,
                    $decoded,
                    MCRYPT_MODE_CBC,
                    "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
            );

    }
}
