<?php

$lang['title'] = 'Gavara Maha Jana Sangam';
$lang['Home'] = 'Home';
$lang['Bride'] = 'Bride';
$lang['Groom'] = 'Groom';
$lang['Registration'] = 'Registration';
$lang['Payment options'] = 'Payment options';
$lang['Contact Us'] = 'Contact Us';
$lang['Select Your Language'] = 'Select Your Language';
$lang['English'] = 'English';
$lang['tamil'] = 'தமிழ்';
$lang['Contact Us'] = 'Contact Us';
$lang['Call us'] = 'Call us';
$lang['Email us'] = 'Email us';
$lang['Follow Us'] = 'Follow Us';
$lang['Sunday Holiday'] = 'Sunday Holiday';
$lang['Timing'] = 'Timing';
$lang['Reach Us'] = 'Reach Us';

$lang['addressf'] = '<p>No 170, Subramanium Road,<br>
                                                R.S.Puram, <br> Coimbatore- 641 002<br>
                                                Office No: 0422 2471928.</p>';
$lang['reserved'] = '© 2017 Gavara Maha jana Sangam. All Rights Reserved.';




//welcome.php
$lang['seekpartner'] = 'Seek out your life partner';
$lang['withtitle'] = '<span>with</span> Gavara Maha Jana Sangam';
$lang['regnow'] = 'Register Now';
$lang['infoprotected'] = 'Informations Protected';
$lang['welcometitle'] = '<span>Welcome to</span> Gavara Mahajana Sangam';
$lang['welcomecontent'] = 'Gavara Mahajana Sangam is a matrimonial portal under the aegis of Gavara Mahajana Sangam Trust, a charitable trust based in Coimbatore. The Sangam also has a wedding hall located in a strategic location in RS Puram, Coimbatore named Gavara Mahajana Sangam Kalyana Mandapam. The prestigious centre is headed by President Mr. K. Surendran an eminent philanthropist. The matrimonial portal is dedicated for&nbsp;Gavara &amp; Balija &nbsp;boys and girls in&nbsp; Gavara &amp; Balija Society in Tamil Nadu, India. The center was established in the year 1953&nbsp;by our past President&nbsp;Mr.E.Purushotham &nbsp;Gavara Mahajana Sangam has registered 14,000 horoscopes with nearly 2500 registrations being active....';
$lang['searchbride'] = 'Search Bride';
$lang['searchgroom'] = 'Search Groom';
$lang['regeasily'] = 'Register Easily';
$lang['choosereg'] = 'Choose your Registration';
$lang['paidreg'] = 'Paid Registration';
$lang['regamount'] = 'Registration amount Rs.500';
$lang['reggetinfo'] = 'Registration can get 25 horoscope information';
$lang['regvalidity'] = 'Registration valitity - 6 months';


//groom.php

$lang['Groom Profile'] = 'Groom Profile';
$lang['regno'] = 'Registration No';
$lang['name'] = 'Name';
$lang['education'] = 'Education';
$lang['fathername'] = 'Father Name';
$lang['district'] = 'District';
$lang['birthdate'] = 'Birth Date';
$lang['sevvai'] = 'Sevvai';
$lang['totalasset'] = 'Total Asset Value';
$lang['jathainfo'] = 'Click Below for Horoscope';
$lang['extrainfo'] = 'Please call us for more Information';

//bride.php

$lang['Bride Profile'] = 'Bride Profile';

//registration
$lang['Registration'] = 'Registration';
$lang['bscprde'] = 'Basic Profile Details';
$lang['name'] = 'Name';
$lang['sex'] = 'Sex';
$lang['male'] = 'Male';
$lang['femal'] = 'Female';
$lang['dob'] = 'Date Of Birth';
$lang['birthtime'] = 'Birth Time';
$lang['monring'] = 'Morning';
$lang['evening'] = 'Evening';
$lang['birthplace'] = 'Birth Place';
$lang['Height'] = 'Height';
$lang['Weight'] = 'Weight';
$lang['Subcaste'] = 'Subcaste';
$lang['Family'] = 'Family Deity';
$lang['Gotra'] = 'Gotra';
$lang['Blood'] = 'Blood Group';
$lang['Color'] = 'Color';
$lang['jdetails'] = 'Job details';
$lang['Education'] = 'Education';
$lang['Occupation'] = 'Occupation';
$lang['Annuali'] = 'Annual Income';
$lang['Informationoo'] = 'Information of Occupation &amp; Asset';
$lang['addressde'] = 'Address details';
$lang['State'] = 'State';
$lang['Country'] = 'Country';
$lang['District'] = 'District';
$lang['Address'] = 'Address';
$lang['Pincode'] = 'Pincode';
$lang['Mail'] = 'E-Mail';
$lang['Phonen'] = 'Phone No';
$lang['Familyd'] = 'Family Details';
$lang['Fathersn'] = 'Father’s name';
$lang['Mothers'] = 'Mother’s Name';

$lang['Brothers'] = 'Brothers';
$lang['Married'] = 'Married';
$lang['Unmarried'] = 'Unmarried';
$lang['Sisters'] = 'Sisters';
$lang['Informations'] = 'Informations';
$lang['Expectations'] = 'Expectations';
$lang['Registeredp'] = 'Registered person';
$lang['agreet'] = 'I agree the Terms of Service';
$lang['Submit'] = 'Submit';
$lang['fsuitable'] = 'Get a way to find a suitable';
$lang['gavaratitle'] = 'Gavara Maha jana Sangam';
$lang['Companion'] = 'Companion';
$lang['asaviewer'] = 'As a viewer, you can see our profile through our website';
$lang['Benefits'] = 'Benefits of Registering';
$lang['Findmatchedhoros'] = 'Find matched horoscopes quickly';
 $lang['Suitablesoul'] = 'Suitable soul mate from kongu vellala gounder community';
 $lang['receivehoroscope'] = 'Can receive horoscope and further details on mails';
 $lang['informationwill'] = 'Information’s will be protected.';
 $lang['startyoursearch'] = 'Start your search';
 $lang['informationexamined'] = 'All the given information’s will be examined';


//payment.php page..
$lang['payementoption'] ='Payment Options';
$lang['padireg'] = 'PAID REGISTRATION';
$lang['regamt'] ='Registration amount Rs';
$lang['reg25horo1'] = 'Registration can get 25 horoscope information';
$lang['reg25horo2'] = 'Registration valitity - 6 months';
$lang['regexpl'] = 'Residents from Coimbatore and local surroundings can pay the Registration fee directly at our office address . For more information’s call Mob';
$lang['contactus'] = 'Contact Us';

$lang['gavaratitle'] = 'Gavara Maha Jana Sangam';
$lang['add1'] = 'No 170, Subramanium Road, ';
$lang['add2'] = 'R.S.Puram, Coimbatore: 641 002';
$lang['add3'] = 'Office No';
$lang['callus'] = 'Call Us - Mobile No';
$lang['emailtxt'] = 'E - Mail';


//contact us
$lang['contactus'] = 'Contact Us';
$lang['email'] = 'Email';
$lang['phone'] = 'Mobile';
$lang['fax'] = 'Fax';
$lang['address'] = 'Address';
$lang['information'] = 'Information';


//common -- eerror messages
$lang['check_for_ver_mail'] = 'Check your mail for details';
$lang['unable_to_recover'] = 'Unable to Proceed Further';
$lang['change_password_using_below'] = 'Please change password using below link';

$lang['invalid_input_change'] = 'Check your mail for details';
$lang['password_changed'] = 'Check your mail for details';


//profile-section
$lang['myprofiles'] = 'My Profile';
$lang['profilesheader'] = 'Profiles';
$lang['feedbackheader'] = 'Feedback';
$lang['changepassword'] = 'Change Password';
$lang['logout'] = 'Logout';
$lang['loginlo'] = 'Login';
$lang['enterpassword'] = 'Enter Password்';
$lang['enteragain'] = 'Enter Again';
$lang['titleheader'] = 'Subject';
$lang['feedbackheader'] = 'Feedback';
$lang['expireondat'] = 'Expired On';
$lang['packageremainingcount'] = 'Package Remaining Count';
$lang['costheader'] = 'Cost';



//pdf creations...



$lang['anincoar'] = array(0=>"",1=>"Below 1 Lakh ","1 Lakh To  2 Lakh","2 Lakh to 4 Lakh","4 Lakh to 6 Lakh","6 Lakh to  10 Lakh ","10 Lakh to 20 Lakh","Above 20 Lakh  ","Others");
$lang['rasidataar']  = array('Laknam','Sun','Puthan','Sukran','Sevvai','Ragu','Kethu','Guru','Sandran','Sani','Manthi');
$lang['laknamdataar']    = array('Laknam','Sun','Puthan','Sukran','Sevvai','Ragu','Kethu','Guru','Sandran','Sani','Manthi');
$lang['jathagamar']  = array(0=>"",1=>'Pure Jathagam','Sevvai', 'Ragu Kethu Sevvai','Ragu Kethu');





//pdf
$lang['iruppu'] = 'Thisai Iruppu';
$lang['year'] = 'Year';
$lang['month'] = 'Month';
$lang['totalassetinfo'] = 'Asset Value';
$lang['vehicle'] = 'Vehicle';
$lang['land'] = 'Land';
$lang['houseall'] = 'House (Apt / Villa )';
$lang['expectation'] = 'Expectations';
$lang['informations'] = 'Informations';

$lang['marriedalt'] = 'Married';
$lang['unmarriedalt'] = 'Un Married';
$lang['sismarried'] = 'Sisters';
$lang['bromarried'] = 'Brothers';

$lang['incometotal'] = 'Income';
$lang['work'] = 'Job';
$lang['date'] = 'Date';
$lang['seconds'] = 'Minutes';
$lang['time'] = 'Time';
$lang['birthinfo'] = 'Birth Details';
$lang['thisaiiruppu'] = 'ThisaiIruppu';
$lang['laknam'] = 'Laknam';
$lang['rasi'] = 'Rasi';
$lang['jenmanakshatra'] = 'Jenma Nakshatram ';

$lang['ragukethu'] = 'Ragu Kethu ';
$lang['kulathievam'] = 'Kuladeivam Temple';
$lang['kulam'] = 'Kulam';
$lang['livingcity'] = 'Current City' ;
$lang['telephoneno'] = 'Telephone' ;

$lang['regnodate'] = 'Registration Date' ;
$lang['jananjathagam'] = 'Janana Jathagam' ;
$lang['regno'] = 'Registration No' ;
$lang['addressfull'] = '170 Subramanium Road, <span class="lf">R.S</span> Puram, Coimbatore - 641 002' ;
$lang['sevvaiholiday'] = 'Sunday Holiday' ;
$lang['pdftitle'] = 'Gavara Maha Jana Sangam' ;
$lang['phonenew'] = 'Mobile' ;
$lang['ramajeyam'] = 'Sri RamaJeyam' ;
$lang['workinghrs'] = 'Working Hours';
$lang['amsam'] = 'Amsam' ;
$lang['tamilenglish'] = 'Tamil';



$lang['filterprofile'] = 'Filter Profiles';
$lang['agefrom'] = 'Age From';
$lang['ageto'] = 'To';
$lang['old'] = 'Old';
