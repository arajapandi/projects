<?php

$lang['regno'] = 'பதிவு அடையாளம்';
$lang['name'] = 'பெயர்';
$lang['education'] = 'கல்வி';
$lang['fathername'] = 'தகப்பனார் பெயர்';
$lang['district'] = 'மாவட்டம்';
$lang['birthdate'] = 'பிறந்த தேதி';
$lang['sevvai'] = 'செவ்வாய்';
$lang['totalasset'] = 'மொத்த சொத்து மதிப்பு';
$lang['jathainfo'] = 'ஜாதகம் விவரம் கீழே சொடுக்கவும்';
$lang['extrainfo'] = 'மேலும் தகவல்களுக்கு எங்களை அழையுங்கள';
$lang['contactus'] = 'தொடர்பு கொள்க';
$lang['email'] = 'மின் அஞ்சல்';
$lang['phone'] = 'கைபேசி';
$lang['fax'] = 'பேக்ஸ்';
$lang['address'] = 'முகவரி';
$lang['information'] = 'தகவல்';
$lang['extrainfor'] = 'படத்தில் கொடுக்கப்பட்ட பாதுகாப்பு குறியீட்டை உள்ளிடவும்';
$lang['sendit'] = 'அனுப்புக்';
$lang['changeit'] = 'மாற்றி அமை';



$lang['title'] = 'கவர மகாஜன சங்கம்';
$lang['Home'] = 'முகப்பு';
$lang['Bride'] = 'மணமகள்';
$lang['Groom'] = 'மணமகன்';
$lang['Registration'] = 'பதிவு செய்ய';
$lang['Payment options'] = 'கட்டண விருப்பங்கள்';
$lang['Contact Us'] = 'தொடர்பு கொள்க';
$lang['Select Your Language'] = 'Select Your Language';
$lang['English'] = 'English';
$lang['tamil'] = 'தமிழ்';

$lang['Call us'] = 'அழையுங்கள்';
$lang['Email us'] = 'மின் அஞ்சல்';
$lang['Follow Us'] = 'பின் தொடருங்கள்';
$lang['Sunday Holiday'] = 'ஞாயிறு விடுமுறை';
$lang['Timing'] = 'நேரம்';
$lang['Reach Us'] = 'வாருங்கள்';


$lang['addressf'] = '<p>No 170,  சுப்பிரமணியம் ரோடு ,<br>
                                                R.S.புரம், <br> கோயம்புத்தூர்- 641 002<br>
                                                அலுவலக எண்: 0422 2471928.</p>';
$lang['reserved'] = '© 2017 Gavara Maha jana Sangam. All Rights Reserved.';




//welcome.php
$lang['seekpartner'] = 'வாழ்கை துணையை கண்டறியுங்கள்';
$lang['withtitle'] = 'கவர மகாஜன சங்கம் <span> உதவியுடன் </span>';
$lang['regnow'] = 'உடனடி பதிவு ';
$lang['infoprotected'] = 'தகவல்கள் பாதுகாக்கப்படும்';
$lang['welcometitle'] = '<span>கவர  மகாஜன சங்கத்திற்கு </span> வரவேற்கின்றோம்';
$lang['welcomecontent'] = 'கவர மகாஜன சங்கம் 1953-ஆம் ஆண்டு முதல், எங்களது இன மக்கள் நலனுக்காக ஆரம்பிக்கப்பட்டது. இச்சங்கத்தின் கீழ் 1995-ஆம் ஆண்டு முதல் கவர மகாஜன திருமண தகவல் மையம் இயங்கி வருகிறது. கடந்த பல ஆண்டுகளாக நமது இன மணமகனையும், மணமகளையும் இணைக்கும் பணியில் தொடர்ந்து ஈடுபட்டு வருகிறது. இத்திருமண தகவல் மையம் தற்போது கணினிமயம் ஆக்கப்பட்டு உள்ளதால், நடைமுறைகள் எளிதாக்கப்பட்டுள்ளன. அதனை பயன்படுத்தி உங்கள் வாழ்க்கை துணையை தேர்ந்தெடுங்கள்.';
$lang['searchbride'] = 'Search Bride';
$lang['searchgroom'] = 'Search Groom';
$lang['regeasily'] = 'சுலப பதிவு';
$lang['choosereg'] = 'வரன் தேடுவதில் ஒரு எளிய வழி';
$lang['paidreg'] = 'கட்டண பதிவு';
$lang['regamount'] = 'கட்டண பதிவு Rs.500';
$lang['reggetinfo'] = '25 உறுப்பினர்களின் விபரங்கள்';
$lang['regvalidity'] = '6 மாதம் செல்லுபடியாகும்';


//groom.php

$lang['Groom Profile'] = 'மணமகன்';
$lang['regno'] = 'பதிவு என்';
$lang['name'] = 'பெயர்';
$lang['education'] = 'படிப்பு';
$lang['fathername'] = 'தந்தை பெயர்';
$lang['district'] = 'மாவட்டம்';
$lang['birthdate'] = 'பிறந்த தேதி';
$lang['sevvai'] = 'செவ்வாய்';
$lang['totalasset'] = 'மொத்த சொத்து மதிப்பு';
$lang['jathainfo'] = 'ஜாதகக விபரங்கள் கீழே சொடுக்கவும்';
$lang['extrainfo'] = 'மேலும் தகவல்களுக்கு எங்களை அழையுங்கள்';

//bride.php

$lang['Bride Profile'] = 'மணமகள்';
//$lang['Bride Profile'] = 'மணமகன்';

//registration
//$lang['Registration'] = 'Registration';
$lang['bscprde'] = 'அடிப்படை விபரங்கள்';
$lang['sex'] = 'பாலினம்';
$lang['male'] = 'ஆண்';
$lang['femal'] = 'பெண்';
$lang['dob'] = 'பிறந்த தேதி';
$lang['birthtime'] = 'பிறந்த நேரம்';
$lang['monring'] = 'காலை';
$lang['evening'] = 'மாலை';
$lang['birthplace'] = 'பிறந்த இடம் ';
$lang['Height'] = 'உயரம்';
$lang['Weight'] = 'எடை';
$lang['Subcaste'] = 'உட்பிரிவு';
$lang['Family'] = 'குல தெய்வம்';
$lang['Gotra'] = 'கோத்ரம்';
$lang['Blood'] = 'இரத்தப் பிரிவு ';
$lang['Color'] = 'நிறம்';
$lang['jdetails'] = 'வேலை விவரங்கள்';
$lang['Education'] = 'கல்வி';
$lang['Occupation'] = 'தொழில்';
$lang['Annuali'] = 'Annual Income';
$lang['Informationoo'] = 'தொழில் மற்றும் சொத்து விபரம் பற்றி';
$lang['addressde'] = 'முகவரி விவரங்கள்';
$lang['State'] = 'மாநிலம்';
$lang['Country'] = 'நாடு';
$lang['District'] = 'மாவட்டம்';
$lang['Address'] = 'முகவரி';
$lang['Pincode'] = 'பின் கோடு';
$lang['Mail'] = 'மின் அஞ்சல்';
$lang['Phonen'] = 'தொலைபேசி எண்';
$lang['Familyd'] = 'குடும்ப விவரம்';
$lang['Fathersn'] = 'தந்தை பெயர் ';
$lang['Mothers'] = 'தாயார் பெயர் ';

$lang['Brothers'] = 'சகோதரர்கள்';
$lang['Married'] = 'திருமணம் ஆனவர்கள் ';
$lang['Unmarried'] = ' 	திருமணம் ஆகாதவர்கள் ';
$lang['Sisters'] = 'சகோதரிகள்';
$lang['Informations'] = 'ஜாதகர் பற்றி';
$lang['Expectations'] = 'எதிர்பார்க்கும் வரன் பற்றி';
$lang['Registeredp'] = 'பதிவு செய்தவர் ';
$lang['agreet'] = 'கவர பலிஜா திருமண தகவல் மையதின் விதிமுறைகளை ஏற்றுக்கொள்கிறேன் ';
$lang['Submit'] = 'பதிவு செய்க';

$lang['fsuitable'] = '<span> கவர மகாஜன சங்கம் </span><br>  துணையை கண்டறிய ஓர் <br> சிறப்பான  வழியை அளிக்கிறோம் ';
$lang['asaviewer'] = 'ஒரு பார்வையாளராக, நீங்கள் எங்கள் வலைதளத்தில் எங்கள் உறுப்பினர்களின் தகவல்களைப் பெறலாம்.';
$lang['Benefits'] = 'பதிவு செய்வதின் பயன்கள்';
$lang['Findmatchedhoros'] = 'வரன்களை விரைவாக தேடலாம்';
 $lang['Suitablesoul'] = 'கவர பலிஜா சமூகத்தில் உங்கள் எதிர்பார்ப்புகளை பூர்த்தி செய்யும் வரன்களை பெற்றிடலாம்';

 $lang['informationwill'] = 'உங்கள் துணைக்கான தேடல் இங்கே தொடங்குகிறது.';
 //$lang['receivehoroscope'] = 'உங்களுக்கு பொருத்தமான வரன்களை பற்றிய விவரங்கள், மின்னஞ்சல் மூலம் பெறலாம்.';
 //$lang['informationwill'] = 'உங்கள் தகவல்களை உரிய பாதுகாப்புடன் கையாள்வோம்.';
 $lang['startyoursearch'] = 'உங்கள் தகவல்களை உரிய பாதுகாப்புடன் கையாள்வோம்.';
 $lang['informationexamined'] = 'உங்கள் தகவல்கள் அனைத்தும் ஆய்வுக்கு உட்படுத்தப்படும்.';


//payment.php page..
$lang['payementoption'] ='கட்டண விருப்பங்கள்';
$lang['padireg'] = 'கட்டணப் பதிவு';
$lang['regamt'] ='கட்டணம் பதிவு - ரூ';
$lang['reg25horo1'] = 'உறுப்பினர்களின் 25 தொடர்பு விபரங்களைப் பார்க்கலாம்.';
$lang['reg25horo2'] = '6 மாதம் செல்லுபடியாகும';
$lang['regexpl'] = 'கோவை, திருப்பூர் மாவட்டங்களில் வசிப்பவர்களாக இருப்பின் நேரடியாக வந்தும் கட்டணம் பெற்றுக்கொள்ளப்படும். பிற மாவட்டங்களில் வசிப்பின் விதிமுறைகளுக்கு உட்பட்டது. விபரங்களுக்கு அழைக்கவும்';
//$lang['contactus'] = 'Contact Us';

$lang['gavaratitle'] = 'கவர மகாஜன சங்கம்';
$lang['add1'] = 'No 170, சுப்பிரமணியம் ரோடு , ';
$lang['add2'] = 'R.S.புரம் , கோயம்புத்தூர்: 641 002';
$lang['add3'] = 'அலுவலக எண் ';
$lang['callus'] = 'அழையுங்கள் - அலைபேசி எண்  ';
$lang['emailtxt'] = 'மின் அஞ்சல்';


//contact us
//$lang['contactus'] = 'Contact Us';
$lang['email'] = 'மின் அஞ்சல்';
$lang['phone'] = 'அலைபேசி எண';
$lang['fax'] = 'Fax';
$lang['address'] = 'முகவரி';
$lang['information'] = 'விபரங்கள்';

$lang['sendit'] = 'அனுப்புக்';
$lang['changeit'] = 'மாற்றி அமை';


//common
$lang['check_for_ver_mail'] = 'Check your mail for details';
$lang['unable_to_recover'] = 'Unable to Proceed Further';
$lang['change_password_using_below'] = 'Please change password using below link';

$lang['invalid_input_change'] = 'Check your mail for details';
$lang['password_changed'] = 'Check your mail for details';



//profile area
$lang['myprofiles'] = 'என் சுயவிவரம்';
$lang['profilesheader'] = 'சுயவிவரங்கள்';
$lang['feedbackheader'] = 'கருத்து';
$lang['changepassword'] = 'கடவுச்சொல்லை மாற்று';
$lang['logout'] = 'வெளியேறு';
$lang['loginlo'] = 'உள் நுழை';
$lang['enterpassword'] = 'கடவுச்சொல்லை உள்ளிடவும்';
$lang['enteragain'] = 'மீண்டும் உள்ளிடவும்';
$lang['titleheader'] = 'தலைப்பு';
$lang['feedbackheader'] = 'கருத்து';
$lang['expireondat'] = 'காலாவதியாகும் தேதி';
$lang['packageremainingcount'] = 'தொகுப்பு மீதமுள்ள எண்ணிக்கை';
$lang['costheader'] = 'கட்டணம்';


//pdf creations...

$lang['anincoar'] = array(0=>"",1=>"ஒரு லட்சத்திற்கு கீழ் ","1 லட்சம் முதல் 2 லட்சம் வரை","2 லட்சம் முதல் 4 லட்சம் வரை","4 லட்சம் முதல் 6 லட்சம் வரை","6 லட்சம் முதல் 10 லட்சம் வரை","10 லட்சம் முதல் 20 லட்சம் வரை","20 லட்சத்திற்கு மேல் ","மற்றவை");
$lang['rasidataar']  = array('லக்னம்','சூரியன்','புதன்','சுக்கிரன்','செவ்வாய்','ராகு','கேது','குரு','சந்திரன்','சனி','மாந்தி');
$lang['laknamdataar']    = array('லக்னம்','சூரியன்','புதன்','சுக்கிரன்','செவ்வாய்','ராகு','கேது','குரு','சந்திரன்','சனி','மாந்தி');
$lang['jathagamar']  = array(0=>"",1=>'சுத்தமான ஜாதகம்','செவ்வாய்', 'ராகு கேது செவ்வாய்','ரகு கேது');


//pdf
$lang['iruppu'] = 'திசை இருப்பு';
$lang['year'] = 'ஆண்டு';
$lang['month'] = 'மாதம்';
$lang['totalassetinfo'] = 'சொத்து மதிப்பு';
$lang['vehicle'] = 'வாகனம்';
$lang['land'] = 'நிலம்';
$lang['houseall'] = 'விடு (அடுக்கு / தனி )';
$lang['expectation'] = 'எதிர்பார்ப்பு';
$lang['informations'] = 'தகவல்கள்';

$lang['marriedalt'] = 'திருமணமானவர்கள்';
$lang['unmarriedalt'] = 'திருமணமாகாதவர்கள்';
$lang['sismarried'] = 'சகோதரிகள்';
$lang['bromarried'] = 'சகோதரர்கள்';

$lang['incometotal'] = 'வருமானம்';
$lang['work'] = 'உத்யோகம்';
$lang['date'] = 'தேதி';
$lang['seconds'] = 'நாழிகைக்கு';
$lang['time'] = 'நேரம்';
$lang['birthinfo'] = 'பிறப்பு விபரம்';
$lang['thisaiiruppu'] = 'திசையிருப்பு';
$lang['laknam'] = 'லக்னம்';
$lang['rasi'] = 'ராசி';
$lang['jenmanakshatra'] = 'ஜென்ம நட்சத்திர';

$lang['ragukethu'] = 'ராகு கேது ';
$lang['kulathievam'] = 'குலதெய்வம்கோயில்';
$lang['kulam'] = 'குலம்';
$lang['livingcity'] = 'வசிக்கும் நகரம்' ;
$lang['telephoneno'] = 'தொலைபேசி' ;

$lang['regnodate'] = 'பதிவு தேதி' ;
$lang['jananjathagam'] = 'ஜனன ஜாதகம்' ;
$lang['regno'] = 'பதிவுஎண்' ;
$lang['addressfull'] = '170 சுப்ரமணியம் ரோடு, <span class="lf">R.S</span> புரம், கோயமுத்துர் - 641 002' ;
$lang['sevvaiholiday'] = 'பிரதி ஞாயிறு விடுமுறை' ;
$lang['pdftitle'] = 'கவர மகாஜனசங்கம் - திருமணத்தகவல் மையம்' ;
$lang['phonenew'] = 'அலைபேசி' ;
$lang['ramajeyam'] = 'ஸ்ரீ  ராமஜெயம்' ;
$lang['workinghrs'] = 'வேலை நேரம்' ;
$lang['amsam'] = 'அம்சம்' ;
$lang['tamilenglish'] = 'தமிழ்';

$lang['filterprofile'] = 'விவரங்களை வடிகட்டவும்';
$lang['agefrom'] = 'வயது முதல்';
$lang['ageto'] = 'வரை';
$lang['old'] = 'பழைய';
