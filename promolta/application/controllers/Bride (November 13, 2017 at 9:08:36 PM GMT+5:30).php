<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bride extends MY_Controller {


    function index()
    {

        $this->load->helper('cookie');
        $lang =  get_cookie('lang');
        $this->lang->load('common', empty($lang) ? 'english' : $lang);
        $lang = $lang == 'tamil' ? 2 : 1;

        $this->load->model('profile_model');
        $this->load->helper('url');

        $page = intval(trim($this->input->get('page'), '/'));
        $per_page = 5;
        $this->load->library('pagination');
        $config['total_rows'] = $this->profile_model->getTotalProfiles(1, 1, $lang);
        $config['per_page'] = $per_page;
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['cur_page'] = $page;
        $config['base_url'] = base_url() . 'index.php/bride?page=';

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();


        $this->data['url'] = base_url();

        $this->data['base_url'] = base_url() . 'application/views/';
        $this->data['profiles'] = $this->profile_model->getAccounts($page, $per_page, 1, 1, $lang);
        //$this->data['message'] = $this->session->flashdata('message');
        $this->data['body_content'] = $this->load->view('bride', $this->data, true);
        $this->load->view('template', $this->data);

    }

    function pdf() {
      $this->load->model('profile_model');
      $this->load->helper('url');
      $this->load->helper('cookie');
      set_cookie('lang', 'english', 3600);
       $lang =  get_cookie('lang');

      $id     = (int)$this->input->get('id',true);
      if(empty($id)) {
        die('foo - No id specified');
      }
      $fvalue = $this->profile_model->getProfile($id);
      $ftamil = $this->profile_model->getLang($id, $lang == 'english' ? 1 : 2);
      $fvalue['laknam'] = empty($fvalue['laknam']) ? 0 : $fvalue['laknam'];
$fvalue['thisai'] = empty($fvalue['thisai']) ? 0 : $fvalue['thisai'];
$fvalue['annual_income'] = empty($fvalue['annual_income']) ? 0 : $fvalue['annual_income'];
$fvalue['nakshatram'] = empty($fvalue['nakshatram']) ? 0 : $fvalue['nakshatram'];
$fvalue['rasi'] = empty($fvalue['rasi']) ? 0 : $fvalue['rasi'];
$fvalue['jathagam'] = empty($fvalue['jathagam']) ? 0 : $fvalue['jathagam'];
      if($lang == 'english') {

        $Lagnam = array(0=>"",1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
        $thisaiiruppu = array(0=>"",1=>"Guru thisai","Sani thisai","kethu thisai","Sukura thisai","Suriya thisai","Chandra thisai","Sewwai thisai","Ragu thisai","Budhan thisai");
        $rasi = array(0=>"",1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
        $nakshatram = array(0=>"",1=>"Aswini","Bharani","Karthikai",
  "Rohini","Mirugasiridam","Thiruvathirai","Punarpusam","Pusam","Ayilyam","Maham","Puram","Uthiram","Atham",
  "Chithirai","Swathiv","Visakam","Anusam","Ketai","Mulam", "Puradam","Uthiradam","Thiruvonam","Avitam",
  "Sathayam","Puratathi","Uthiratathi","Revathi");
      } else {

        $Lagnam = array(0=>"",1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $thisaiiruppu = array(0=>"",1=>"குருதிசை","சனிதிசை","கேதுதிசை","சுக்ரதிசை","சூரியதிசை","சந்திரதிசை","செவ்வாய்திசை","ராகுதிசை","புதன்திசை");
        $rasi = array(0=>"",1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
        $nakshatram = array(0=>"",1=>"அசுவினி","பரணி","கிருத்திகை",
        "ரோஹிணி","மிருகசீரிஷம்","திருவாதிரை","புனர்பூசம்","பூசம்","ஆயில்யம்","மகம்","பூரம்","உத்திரம்","ஹஸ்தம்",
        "சித்திரை","ஸ்வாதி","விசாகம்","அனுசம்","கேட்டை","மூலம்", "புராடம்","உத்ராடம்","திருவோணம்","அவிட்டம்",
        "சதயம்","புரட்டாதி","உத்திரட்டாதி","ரேவதி");
      }

      $aninco = array(0=>"",1=>"ஒரு லட்சத்திற்கு கீழ் ","1 லட்சம் முதல் 2 லட்சம் வரை","2 லட்சம் முதல் 4 லட்சம் வரை","4 லட்சம் முதல் 6 லட்சம் வரை","6 லட்சம் முதல் 10 லட்சம் வரை","10 லட்சம் முதல் 20 லட்சம் வரை","20 லட்சத்திற்கு மேல் ","மற்றவை");
      $rasidata = array('லக்னம்','சன்','புதன்','சுக்கிரன்','செவ்வாய்','ராகு','கேது','குரு','சந்திரன்','சனி','மந்தி');
      $laknamdata   = array('லக்னம்','சன்','புதன்','சுக்கிரன்','செவ்வாய்','ராகு','கேது','குரு','சந்திரன்','சனி','மந்தி');
      $jathagam = array(0=>"",1=>'சுத்தமான ஜாதகம்','செவ்வாய்', 'ராகு கேது செவ்வாய்','ரகு கேது');

      $jatsquare = array('rasi'=>array(),'amsam'=>array());
      if(!empty($fvalue['jatsquare'])) {
          $jatsquare = json_decode($fvalue['jatsquare'],true);
      }

      /*$Lagnam = array(1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
      $thisaiiruppu = array(1=>"குருதிசை","சனிதிசை","கேதுதிசை","சுக்ரதிசை","சூரியதிசை","சந்திரதிசை","செவ்வாய்திசை","ராகுதிசை","புதன்திசை");
      $rasi = array(1=>"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருஷிகம்","தனுசு","மகரம்","கும்பம்","மீனம்");
      $nakshatram = array(1=>"அசுவினி","பரணி","கிருத்திகை",
      "ரோஹிணி","மிருகசீரிஷம்","திருவாதிரை","புனர்பூசம்","பூசம்","ஆயில்யம்","மகம்","பூரம்","உத்திரம்","ஹஸ்தம்",
      "சித்திரை","ஸ்வாதி","விசாகம்","அனுசம்","கேட்டை","மூலம்", "புராடம்","உத்ராடம்","திருவோணம்","அவிட்டம்",
      "சதயம்","புரட்டாதி","உத்திரட்டாதி","ரேவதி");*/


      $rasi_details = array();
      $amsam_details = array();
      if(!empty($jatsquare['rasi'])) {
        foreach((array)$jatsquare['rasi'] as $val=>$key) {
           $rasi_details[$key] = isset($rasi_details[$key]) ? $rasi_details[$key].','.$rasidata[$val] : $rasidata[$val];
        }
      }
      if(!empty($jatsquare['amsam'])) {
        foreach((array)$jatsquare['amsam'] as $val=>$key) {
           $amsam_details[$key] = isset($amsam_details[$key]) ? $amsam_details[$key].' <br/>  '.$laknamdata[$val] : $laknamdata[$val];
        }
      }
      $url = '';
      if(!empty($fvalue['image'])) {
        $url = str_replace('apanel.php', '',base_url()).'uploads/'.$fvalue['image'];
      };


      $stylesheet = ' table {font-family:"arialuni" ;FONT-SIZE:9px}
      .lfh {font-family:arialuni;FONT-SIZE:16px}
      .lf {font-family:arialuni;FONT-SIZE:9px}
      .latha{font-family:"latha";FONT-SIZE:16px}';

      $html = '
      <table border="0" cellspacing="2" cellpadding="2" width="100%">
         <tr>
            <td align="left" class="lf"> தொலைபேசி  : 0422 2471928 - அலைபேசி : 94425 71928 </td>
            <td align="center" width="35%" class="lf"> ஸ்ரீ  ராமஜெயம் </td>
            <td align="right" width="30%" class="lf"> வேலை நேரம்  : <span class="lf"> 10 AM to 6 PM </span> </td>
         </tr>

                    <tr>
                       <td colspan="4" align="center" class="lf"> <hr>   </td>
                    </tr>
         <tr>
            <td colspan="3" align="center" class="latha"> கவர மகாஜனசங்கம் திருமணத் தகவல் மையம்  </td>
         </tr>
         <tr>
            <td align="left" class="lf"> பிரதி செவ்வாய் விடுமுறை </td>
            <td align="center" class="lf"> &nbsp; </td>
            <td align="right" class="lf"> நாள்  : '.date('d/m/Y').' </td>
         </tr>
         <tr>
            <td colspan="3" align="center" class="lf"> 170 சுப்ரமணியம் ரோடு, <span class="lf">R.S</span> புரம், கோயமுத்துர் - 641 002   </td>
         </tr>

         <tr>
            <td colspan="4" align="center" class="lf"> <hr>   </td>
         </tr>
         <tr>
            <td align="left" class="lf"> பதிவு தேதி  : '.date('d/m/Y',strtotime($fvalue['reg_date'])).' </td>
            <td align="center" class="lf"> ஜனன ஜாதகம் </td>
            <td align="right" class="lf"> பதிவுஎண் : '.$fvalue['id'].' </td>
         </tr>
         <tr>
            <td colspan="4"><hr></td>
         </tr>
         <tr>
            <td colspan="3" align="center">
               <table border="0" width="100%">
                  <tr>
                     <td width="50%" valign="top">
                        <table border="0" width="100%" cellspacing="2" cellpadding="5">
                           <tr>
                              <td align="left" class="lf">பெயர்</td>
                              <td colspan="3" align="left" class="lf">: '.$ftamil['name'].' </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">வசிக்கும் நகரம்</td>
                              <td colspan="3" align="left" class="lf">: '.$ftamil['res_city'].' </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left"> குலம்   </td>
                              <td  align="left" class="lf" colspan="3">: '.$ftamil['kulam'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">செவ்வாய் </td>
                              <td align="left" class="lf">: '.$ftamil['sevvai'].'  </td>
                              <td class="lf" align="right" > ராகு கேது  </td>
                              <td  align="left" class="lf">:  '.$ftamil['ragukethu'].'  </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">லக்னம்</td>
                              <td align="left" class="lf">: '.$Lagnam[$fvalue['laknam']].' </td>
                              <td class="lf" align="right">திசையிருப்பு </td>
                              <td align="left" class="lf">: '.$thisaiiruppu[$fvalue['thisai']].' </td>

                           </tr>
                           <tr>
                              <td class="lf" align="left">கல்வி</td>
                              <td colspan="3" align="left">: '.$ftamil['education'].' </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left"> வருமானம்</td>
                              <td colspan="3" align="left" class="lf">: '.$aninco[$fvalue['annual_income']].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">தயார் பெயர் </td>
                              <td colspan="3" align="left" class="lf">: '.$ftamil['mother_name'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">சகோதரிகள் </td>
                              <td class="lf" colspan="3" align="left">திருமணமானவர்கள் : '.$fvalue['sismarried'].' திருமணமாகாதவர்கள் : '.$fvalue['sisunmarried'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">எதிர்பார்ப்பு </td>
                              <td colspan="3" align="left">: '.$fvalue['expectation'].'</td>
                           </tr>

                           <tr>
                              <td class="lf" align="left">விடு (அடுக்கு / தனி )</td>
                              <td align="left">: '.$fvalue['house'].'</td>
                              <td class="lf" align="left">நிலம்</td>
                              <td align="left">: '.$fvalue['land'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">வாகனம்</td>
                              <td align="left">: '.$fvalue['vehicle'].'</td>
                              <td class="lf" align="left">சொத்து மதிப்பு 1</td>
                              <td align="left">: '.$fvalue['provalue1'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">சொத்து மதிப்பு 2</td>
                              <td align="left">: '.$fvalue['provalue2'].'</td>
                              <td class="lf" align="left">சொத்து மதிப்பு 3</td>
                              <td align="left">: '.$fvalue['provalue3'].'</td>
                           </tr>

                           <tr>
                              <td class="lf" align="left">மொத்த சொத்து விபரம் </td>
                              <td colspan="3" align="left">: '.$fvalue['totalpro'].'</td>
                           </tr>



                        </table>
                     </td>
                     <td rowspan="10" colspan="2" valign="top">
                        <table border="0" width="100%" cellspacing="2" cellpadding="5">
                          <tr>
                             <td align="left" class="lf">பிறந்த தேதி </td>
                             <td align="left" colspan="3">: '.date('d/m/Y',strtotime($fvalue['dob'])).' </td>
                          </tr>
                          <tr>
                             <td class="lf" align="left">உயரம் </td>
                             <td align="left">: '.$ftamil['height'].' </td>
                             <td class="lf" align="right"> நிறம் </td>
                             <td  align="left">:  '.$ftamil['color'].' </td>
                          </tr>

                          <tr>
                          <td class="lf" align="left"> குலதெய்வம்கோயில்  </td>
                          <td align="left" class="lf" colspan="3">: '.$ftamil['kulam_temple'].' </td>
                          </tr>

                           <tr>
                              <td class="lf" align="left">ஜென்ம நட்சத்திரம் </td>
                              <td align="left" class="lf">: '.$nakshatram[$fvalue['nakshatram']].'  </td>
                              <td class="lf" align="right" > ராசி </td>
                              <td  align="left" class="lf">:  '.$rasi[$fvalue['rasi']].'  </td>
                              <!--<td class="lf" align="right"> பாதம் </td>
                              <td align="left">  2 </td>-->
                           </tr>
                           <tr>
                              <td class="lf" align="left">பிறப்பு விபரம்</td>
                              <td colspan="3" align="left" class="lf">:  தமிழ் '.$ftamil['tamil_birth'].'
                              ஆண்டு '.$ftamil['tamil_year'].'
                              மாதம் '.$ftamil['tamil_month'].'
                              தேதி '.$ftamil['tamil_month'].'
                              நாழிைகக்கு   '.$ftamil['tamil_nazhigai'].'
                              நேரம் '.$ftamil['tamil_time'].'</td>
                           </tr>

                           <tr>
                              <td class="lf" align="left">உத்யோகம்</td>
                              <td colspan="3" align="left">: '.$ftamil['job'].' </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">தகப்பனார் பெயர் </td>
                              <td colspan="3" align="left" class="lf">:  '.$ftamil['father_name'].'  </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left"> சகோதரர்கள் </td>
                              <td  class="lf" colspan="3" align="left">திருமணமானவர்கள் : '.$fvalue['bromarried'].'
                              திருமணமாகாதவர்கள் : '.$fvalue['brounmarried'].' </td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">தகவல்கள் </td>
                              <td colspan="3" align="left">: '.$fvalue['information'].'</td>
                           </tr>
                           <tr>
                              <td class="lf" align="left">மாவட்டம் </td>
                              <td colspan="3" align="left">: '.$fvalue['district'].'</td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td colspan="3"  >
               <table width="100%">
                  <tr>
                     <td>
                        <table border="1" cellspacing="0" cellpadding="2" width="360px">
                           <tr>
                              <td height="60px" width="25%"> '.(isset($rasi_details[1]) ? $rasi_details[1] : '&nbsp;' ).'  </td>
                              <td width="25%"> '.(isset($rasi_details[2]) ? $rasi_details[2] : '&nbsp;' ).' </td>
                              <td width="25%"> '.(isset($rasi_details[2]) ? $rasi_details[2] : '&nbsp;' ).' </td>
                              <td width="25%"> '.(isset($rasi_details[2]) ? $rasi_details[2] : '&nbsp;' ).' </td>
                           </tr>
                           <tr>
                              <td height="60px" width="25%"> '.(isset($rasi_details[12]) ? $rasi_details[12] : '&nbsp;' ).' </td>
                              <td rowspan="2" colspan="2" align="center" height="120px" width="50%">
                                 <div style="vertica-align: middle;padding-top: 10px;">
                                    ராசி
                                 </div>
                              </td>
                              <td width="25%"> '.(isset($rasi_details[5]) ? $rasi_details[5] : '&nbsp;' ).' </td>
                           </tr>
                           <tr>
                              <td height="60px" width="25%"> '.(isset($rasi_details[11]) ? $rasi_details[11] : '&nbsp;' ).' </td>
                              <td width="25%"> '.(isset($rasi_details[6]) ? $rasi_details[6] : '&nbsp;' ).' </td>
                           </tr>
                           <tr>
                              <td height="60px" width="25%"> '.(isset($rasi_details[10]) ? $rasi_details[10] : '&nbsp;' ).' </td>
                              <td width="25%"> '.(isset($rasi_details[9]) ? $rasi_details[9] : '&nbsp;' ).' </td>
                              <td width="25%">'.(isset($rasi_details[8]) ? $rasi_details[8] : '&nbsp;' ).' </td>
                              <td width="25%"> '.(isset($rasi_details[7]) ? $rasi_details[7] : '&nbsp;' ).' </td>
                           </tr>
                        </table>
                     </td>
                     <td>
                        <table border="1" cellspacing="0" cellpadding="2"  width="360px">
                           <tr>
                              <td height="60px" width="25%"> '.(isset($amsam_details[1]) ? $amsam_details[1] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[2]) ? $amsam_details[2] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[3]) ? $amsam_details[3] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[4]) ? $amsam_details[4] : '' ).' &nbsp; </td>
                           </tr>
                           <tr>
                              <td height="60px" width="25%"> '.(isset($amsam_details[12]) ? $amsam_details[12] : '' ).' &nbsp; </td>
                              <td rowspan="2" colspan="2" align="center" height="120px" width="50%">
                                 <div style="vertica-align: middle;padding-top: 10px;">
                                    அம்சம்
                                 </div>
                              </td>
                              <td width="25%">    '.(isset($amsam_details[5]) ? $amsam_details[5] : '' ).' &nbsp; </td>
                           </tr>
                           <tr>
                              <td height="60px" width="25%"> '.(isset($amsam_details[11]) ? $amsam_details[11] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[6]) ? $amsam_details[6] : '' ).' &nbsp; </td>
                           </tr>
                           <tr>
                              <td height="60px" width="25%"> '.(isset($amsam_details[10]) ? $amsam_details[10] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[9]) ? $amsam_details[9] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[8]) ? $amsam_details[8] : '' ).' &nbsp; </td>
                              <td width="25%"> '.(isset($amsam_details[7]) ? $amsam_details[7] : '' ).' &nbsp; </td>
                           </tr>
                        </table>
                     </td>
                     <td width="10%" class="lf">
                        '.$jathagam[$fvalue['jathagam']].'
                        '.(!empty($fvalue['balance']) ? '<br> - இருப்பு : '.$fvalue['balance'] : '').'
                        '.(!empty($fvalue['byear']) ? '<br> - ஆண்டு : '.$fvalue['byear'] : '').'
                        '.(!empty($fvalue['month']) ? '<br> - மாதம் : '.$fvalue['month'] : '').'
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>';

     $this->load->library("Mpdf");
     $mpdf = new Mpdf('ta_IN','A4',9,'lf',2,2,2,0,0,0);
     $mpdf->WriteHTML($stylesheet,1);
     $mpdf->WriteHTML($html);
     ob_clean();
     $mpdf->Output();
     exit;
    }
}
