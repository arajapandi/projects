<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {


    function index()
    {
        $this->load->helper('cookie');
        $lang =  get_cookie('lang');
        $this->lang->load('common', empty($lang) ? 'english' : $lang);

        $lang = $lang == 'tamil' ? 2 : 1;

        $this->data['message'] = $this->session->flashdata('message');
        $this->load->helper('url');
        $this->data['url'] = base_url();
        $this->data['base_url'] = base_url() . 'application/views/';
        $this->data['body_content'] = $this->load->view('contact', $this->data, true);
        $this->load->view('template', $this->data);
    }
    function send() {

      $txtname     = $this->input->post('txtname',true);
      $txtemail     = $this->input->post('txtemail',true);
      $txtphone     = $this->input->post('txtphone',true);
      $fax      = $this->input->post('fax',true);
      $address     = $this->input->post('address',true);
      $message     = $this->input->post('message',true);
      $capcha_response     = $this->input->post('g-recaptcha-response',true);



      $secret = '6LdLlzEUAAAAAIFCxdVPx0_EKIGlSvlT-N2hW3jd';
      //get verify response data
      $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
      $responseData = json_decode($verifyResponse);

      if(!empty($txtemail) && filter_var($txtemail, FILTER_VALIDATE_EMAIL) && $responseData->success) {

        //$to_email = "admin@gavaramahajanamatrimony.com";
        $to_email = "balijanaidutrust@gmail.com";
        //$txtemail = 'info@sempiyan.com';

        //Load email library
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.sempiyan.com',
            'smtp_port' => 25,
            'smtp_user' => 'test@sempiyan.com',
            'smtp_pass' => 'test@123',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->from($txtemail, $txtname);
        $this->email->to($to_email);
        $this->email->subject('New Contact Request');
        $body = "From: $txtname\n E-Mail: $txtemail\n Phone: $txtphone\n Address:\n $address \n Message:\n $message";
        $this->email->message($body);

        //Send mail
        if($this->email->send()) {
          $this->data['message'] = $this->session->set_flashdata('message','Sending Success!');
        } else {
          $this->data['message'] = $this->session->set_flashdata('message','Sending Failed!');
        }
          //$this->session->set_flashdata("email_sent","Email sent successfully.");
        //else
        //  $this->session->set_flashdata("email_sent","Error in sending Email.");
      } else {
        $this->data['message'] = $this->session->set_flashdata('message','Sending Failed!');
      }
      $this->load->helper('url');
      redirect(base_url().'index.php/contact', 'refresh');

    }
}
