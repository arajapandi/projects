<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends MY_Controller {


    function index()
    {

        redirect(base_url().'index.php/contact', 'refresh');

        $this->load->helper('cookie');
        $lang =  get_cookie('lang');
        $this->lang->load('common', empty($lang) ? 'english' : $lang);
        $lang = $lang == 'tamil' ? 2 : 1;


        $this->data['message'] = $this->session->flashdata('message');
        $this->load->helper('url');
        $this->data['url'] = base_url();
        $this->data['base_url'] = base_url() . 'application/views/';
        $this->data['body_content'] = $this->load->view('registration', $this->data, true);
        $this->load->view('template', $this->data);
    }
    public function send() {

              redirect(base_url().'index.php/contact', 'refresh');

              $secret = '6LdLlzEUAAAAAIFCxdVPx0_EKIGlSvlT-N2hW3jd';
              //get verify response data
              $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
              $responseData = json_decode($verifyResponse);
              $txtemail = $this->input->post('txtemail',true);
              if(!empty($txtemail) && filter_var($txtemail, FILTER_VALIDATE_EMAIL) && $responseData->success) {

                $text = "Title : ". $this->input->post('titlee',true)." ".$this->input->post('txtname',true)." \n <br>";
                $text .= "Gender : ". $this->input->post('gender',true)." \n <br>";
                $text .= "Date Of Birth : ". $this->input->post('dob',true)." \n <br>";
                $text .= "Birth Time : ". $this->input->post('time',true)."". $this->input->post('sec',true)."". $this->input->post('dobtime',true)." \n <br>";
                $text .= "Birth Place : ". $this->input->post('birthpla',true)." \n <br>";
                $text .= "Height : ". $this->input->post('height',true)." \n <br>";
                $text .= "weight : ". $this->input->post('weight',true)." \n <br>";
                $text .= "Kulam : ". $this->input->post('kulam',true)." \n <br>";
                $text .= "family_address : ". $this->input->post('family_address',true)." \n <br>";
                $text .= "gotra : ". $this->input->post('gotra',true)." \n <br>";
                $text .= "bgroup : ". $this->input->post('bgroup',true)." \n <br>";

                $text .= "colour : ". $this->input->post('colour',true)." \n <br>";
                $text .= "education : ". $this->input->post('education',true)." \n <br>";
                $text .= "occupation : ". $this->input->post('occupation',true)." \n <br>";
                $text .= "income : ". $this->input->post('income',true)." \n <br>";

                $text .= "property : ". $this->input->post('property',true)." \n <br>";
                $text .= "country : ". $this->input->post('country',true)." \n <br>";
                $text .= "state : ". $this->input->post('state',true)." \n <br>";
                $text .= "district : ". $this->input->post('district',true)." \n <br>";

                $text .= "address : ". $this->input->post('address',true)." \n <br>";
                $text .= "pincode : ". $this->input->post('pincode',true)." \n <br>";
                $text .= "txtemail : ". $this->input->post('txtemail',true)." \n <br>";
                $text .= "txtphone : ". $this->input->post('txtphone',true)." \n <br>";

                $text .= "father : ". $this->input->post('father',true)." \n <br>";
                $text .= "mother : ". $this->input->post('mother',true)." \n <br>";
                $text .= "Brother : Married". $this->input->post('bromarried',true)." Un Married ".$this->input->post('brounmarried',true)." \n <br>";
                $text .= "Sister : Married". $this->input->post('sismarried',true)." Un Married ".$this->input->post('sisunmarried',true)." \n <br>";
                $text .= "information : ". $this->input->post('information',true)." \n <br>";
                $text .= "expectations : ". $this->input->post('expectations',true)." \n <br>";
                $text .= "regperson : ". $this->input->post('regperson',true)." \n <br>";

                //$to_email = "admin@gavaramahajanamatrimony.com";
                $to_email = "balijanaidutrust@gmail.com";
                //Load email library
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'mail.sempiyan.com',
                    'smtp_port' => 25,
                    'smtp_user' => 'test@sempiyan.com',
                    'smtp_pass' => 'test@123',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email', $config);
                $this->email->from('info@sempiyan.com', $this->input->post('txtname',true));
                $this->email->to($to_email);
                $this->email->subject('New Registration Request');
                $body = $text;
                $this->email->message($body);

                //Send mail
                if($this->email->send()) {
                  $this->data['message'] = $this->session->set_flashdata('message','Registration Success, You will get confirmation after verification!');
                } else {
                  $this->data['message'] = $this->session->set_flashdata('message','Registration Failed!');
                }
              } else {
                $this->data['message'] = $this->session->set_flashdata('message','Registration Failed!');
              }


              $this->load->helper('url');
              redirect(base_url().'index.php/registration', 'refresh');


              //Array ( [titlee] => late Mrs. [txtname] => Rajapandian [gender] => Male [dob] => 14/03/1985 [time] => 01 [sec] => 03 [dobtime] => evening [place] => birthpla [height] => 140 to 145 [weight] => 40 [kulam] => subcas [family_address] => family [gotra] => gotra [bgroup] => O+ [colour] => Fair [education] => 10th [occupation] => Government Employee [income] => 4lakhs - 6lakhs [property] => adsf [country] => India [state] => Assam [district] => Coimbatore [address] => asdf [pincode] => asdf [txtemail] => asdf [txtphone] => asdf [father] => asdf [mother] => asdf [bromarried] => 2 [brounmarried] => 3 [sismarried] => 3 [sisunmarried] => 4 [information] => asdf [expectations] => asdf [regperson] => Sibling


    }

}
