<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

    function __construct() {

      parent::__construct();

      $profile_id = $this->session->userdata('profile_id');
      $email = $this->session->userdata('email');
      if(empty($profile_id) || empty($email)) {
        redirect(base_url().'index.php/login', 'refresh');
      }

    }
    function index()
    {

        $this->load->helper('cookie');
        $lang =  get_cookie('lang');
        $this->lang->load('common', empty($lang) ? 'english' : $lang);
        $lang = $lang == 'tamil' ? 2 : 1;

        $this->load->model('profile_model');
        $this->load->model('package_model');



        $profile_id = $this->session->userdata('profile_id');
        $profile_package = $this->package_model->getProfilePackage($profile_id);
        $this->data['profile_package'] = $profile_package;

        $this->data['message'] = $message = $this->session->flashdata('message');
        $this->data['url'] = base_url();
        $this->data['base_url'] = base_url() . 'application/views/';
        //$this->data['message'] = $this->session->flashdata('message');
        $this->data['body_content'] = $this->load->view('profile', $this->data, true);
        $this->load->view('template', $this->data);

    }
    function feedback() {

      $this->load->helper('cookie');
      $lang =  get_cookie('lang');
      $this->lang->load('common', empty($lang) ? 'english' : $lang);
      $lang = $lang == 'tamil' ? 2 : 1;


      $this->load->model('profile_model');
      $this->load->model('package_model');

      $profile_id = $this->session->userdata('profile_id');
      $subject = $this->input->post('subject',true);
      $feedback = $this->input->post('feedback',true);
      if(!empty($subject) && !empty($feedback)) {
        $fvalue = array('profile_id'=>$profile_id,'subject'=>$subject,'feedback'=>$feedback,'status'=>0);
        $result = $this->profile_model->saveFeedback($fvalue);
        if(!empty($result)) {
          $this->session->set_flashdata('message','Feedback Sent!');
          redirect(base_url().'index.php/profile', 'refresh');
        }
      }

      $this->data['url'] = base_url();
      $this->data['base_url'] = base_url() . 'application/views/';
      //$this->data['message'] = $this->session->flashdata('message');
      $this->data['body_content'] = $this->load->view('feedback', $this->data, true);
      $this->load->view('template', $this->data);
    }
    function logout() {
      $this->session->sess_destroy();
      redirect(base_url().'index.php/', 'refresh');
    }
    function assigned() {


      $this->load->helper('cookie');
      $lang =  get_cookie('lang');
      $this->lang->load('common', empty($lang) ? 'english' : $lang);
      $lang = $lang == 'tamil' ? 2 : 1;
      $this->load->model('profile_model');
      $this->load->model('package_model');
      $profile_id = $this->session->userdata('profile_id');

      $page = intval(trim($this->input->get('page'), '/'));
      $per_page = 5;
      $this->load->library('pagination');
      $config['total_rows'] = $this->package_model->getProfilePackageCount($profile_id, $lang);
      $config['per_page'] = $per_page;
      $config['next_link'] = 'Next';
      $config['prev_link'] = 'Prev';
      $config['cur_page'] = $page;
      $config['base_url'] = base_url() . 'index.php/profile/assigned?page=';

      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';
      $config['prev_tag_open'] = '<li class="prev">';
      $config['prev_tag_close'] = '</li>';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';
      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $this->pagination->initialize($config);
      $this->data['pagination'] = $this->pagination->create_links();

      $this->data['message'] = $message = $this->session->flashdata('message');
      $this->data['url'] = base_url();
      $this->data['base_url'] = base_url() . 'application/views/';
      $this->data['profiles'] = $this->package_model->getProfileAssign($profile_id, $lang , $page, $per_page);


      $this->data['body_content'] = $this->load->view('assigned', $this->data, true);
      $this->load->view('template', $this->data);
    }
    function change() {
        $this->load->model('client_model');
        $password = $this->input->post('password');
        $re_password = $this->input->post('re_password');

        $profile_id = $this->session->userdata('profile_id');
        $profile_data = $this->client_model->getMember($profile_id, 1, 'id');

        if(!empty($profile_data) && !empty($password) && strlen(trim($password))>5 && $password == $re_password) {
          if($this->client_model->change_password($password,$profile_data->email)) {
              /* remove reset hash from the databse */
              $this->client_model->remove_reset_hash($profile_data->email);
              //$this->redirect($this->url->http('login&from=reset'));
              $this->session->set_flashdata('message','password_changed');
              redirect(base_url().'index.php/profile', 'refresh');
          }
        }

        $this->data['url'] = base_url();
        $this->data['handle'] = 'profile';
        $this->data['base_url'] = base_url() . 'application/views/';
        $this->data['body_content'] = $this->load->view('change', $this->data, true);
        //$this->data['user_id'] = $this->client_model->get_id();
        $this->load->view('template', $this->data);
    }

}
