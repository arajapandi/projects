<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('client_model');
    }

    function index()
    {

        $profile_id = $this->session->userdata('profile_id');

        $this->load->helper('cookie');
        $lang =  get_cookie('lang');
        $this->lang->load('common', empty($lang) ? 'english' : $lang);
        $lang = $lang == 'tamil' ? 2 : 1;

        $this->data['message'] = $message = $this->session->flashdata('message');
        $this->data['url'] = base_url();
        $this->data['base_url'] = base_url() . 'application/views/';
        $this->data['body_content'] = $this->load->view('login', $this->data, true);

        //$this->data['user_id'] = $this->client_model->get_id();
        $this->load->view('template', $this->data);
    }

    function check()
    {

      $phone    = $this->input->post('phone');
      $password = $this->input->post('password');
      $grecaptcharesponse = $this->input->post('g-recaptcha-response');
      if(!empty($phone) && !empty($password) &&
      filter_var($phone, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^[0-9]{10}$/")))) {
      //&& filter_var($email, FILTER_VALIDATE_EMAIL)) {

        $secret = '6LdLlzEUAAAAAIFCxdVPx0_EKIGlSvlT-N2hW3jd';
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$grecaptcharesponse);
        $responseData = json_decode($verifyResponse);

        if($responseData->success || 1) {
          if($this->client_model->login($phone, $password)) {
              redirect(base_url().'index.php/profile', 'refresh');
          }
        }
      }

      $this->session->set_flashdata('message', 'Login Failed!');
      redirect(base_url().'index.php/login', 'refresh');
    }

    function forgot()
    {
        $email = $this->input->post('email');

        if(!empty($email)) {
            if($this->client_model->reset_password($email)) {
                //$this->session->set_flashdata('message',$this->lang->line('check_for_ver_mail'));
                $this->data['message'] = $this->lang->line('check_for_ver_mail');
            } else {
                //$this->session->set_flashdata('message',$this->lang->line('unable_to_recover'));
                $this->data['message'] = $this->lang->line('unable_to_recover');
            }
        }
        $this->data['url'] = base_url();
        $this->data['base_url'] = base_url() . 'application/views/';
        $this->data['body_content'] = $this->load->view('forgot', $this->data, true);
        $this->load->view('template', $this->data);
    }


    function change()
    {


        $key_post = $this->input->post('key');
        if(!empty($key_post)) {
            if($profile_id = $this->client_model->validate_reset_password($key_post)){
                   $profile_data = $this->client_model->getMember($profile_id, 1, 'id');
                   if(!empty($profile_data) && $this->input->post('password') && strlen(trim($this->input->post('password')))>5 && $this->input->post('password') == $this->input->post('re_password')) {
                    if($this->client_model->change_password($this->input->post('password'),$profile_data->email)) {
                        /* remove reset hash from the databse */
                        $this->client_model->remove_reset_hash($profile_data->email);
                        //$this->redirect($this->url->http('login&from=reset'));
                        $this->session->set_flashdata('message','password_changed');
                        redirect(base_url().'index.php/login', 'refresh');
                    }
                }
            }
            $this->session->set_flashdata('message','invalid_input_change');
            redirect(base_url().'index.php/login', 'refresh');
        } else {

            $key = ($key_post) ? $key_post : $this->input->get('key');
            $this->data['key'] = $key;
            if($key) {
                $this->client_model->validate_reset_password($key);
                if($this->client_model->validate_reset_password($key)) {
                    $this->data['message'] = $this->lang->line('change_password_using_below');
                } else {
                    redirect(base_url().'index.php/login', 'refresh');
                }
            } else {
                redirect(base_url().'index.php/login', 'refresh');
            }
        }

        $this->data['url'] = base_url();
        $this->data['base_url'] = base_url() . 'application/views/';
        $this->data['body_content'] = $this->load->view('change', $this->data, true);
        //$this->data['user_id'] = $this->client_model->get_id();
        $this->load->view('template', $this->data);
    }

    function logout()
    {
        $this->client_model->logout();
        redirect(base_url().'logme');
    }

    function otp()
    {
        //if(!$this->_check_session($this->input->post('session_for_safe'))) {
          //  die($this->lang->line('error_user_pass'));
        //}
        if($this->input->post('otp') && $this->member_model->otp_check($this->input->post('otp'))) {
            echo 1;
            exit;
        }
        die($this->lang->line('error_user_pass'));
    }

}
