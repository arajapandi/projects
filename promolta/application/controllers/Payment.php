<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {


    function index()
    {

        $this->load->helper('cookie');
        $lang =  get_cookie('lang');
        $this->lang->load('common', empty($lang) ? 'english' : $lang);

        $this->load->helper('url');

        $this->data['url'] = base_url();
        $this->data['base_url'] = base_url() . 'application/views/';
        $this->data['body_content'] = $this->load->view('payment', $this->data, true);
        $this->load->view('template', $this->data);
    }

}
