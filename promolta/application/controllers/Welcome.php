<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {


    function index()
    {
        $this->load->helper('cookie');

        $this->data['url'] = base_url();
        $this->data['base_url'] = base_url().'application/views/';
        $this->data['body_content'] = $this->load->view('welcome', $this->data, true);
        $this->load->view('template', $this->data);
    }


    function change() {

      try {
        $lang     = $this->input->get('lang',true);
        if(!in_array($lang,array('tamil','english'))) {
          die('foo');
        }
        set_cookie('lang', $lang, 3600);
        redirect(base_url());
      } catch(Exception $e) {

      }

      //redirect(base_url().'index.php/profile', 'refresh');
    }

}
