<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groom extends MY_Controller {


    function index()
    {
        $this->load->helper('cookie');
        $lang =  get_cookie('lang');
        $this->lang->load('common', empty($lang) ? 'english' : $lang);
        $lang = $lang == 'tamil' ? 2 : 1;

        $this->load->model('profile_model');
        $this->load->helper('url');

        $this->load->helper('security');
        $search_array = array();
        $search     = $this->input->get('search',true);
        if(!empty($search)) {
          if($search == ctype_digit($search)) {
            $search_array['profile_id'] = intval($search);
          } else {
            $search_array['search'] = trim($search);
          }
        }

        $age_from   = intval($this->input->get('age_from'));
        if(!empty($age_from)) {
          $search_array['age_from'] = $age_from;
        }

        $age_to     = intval($this->input->get('age_to'));
        if(!empty($age_to)) {
          $search_array['age_to'] = $age_to;
        }

        $profile_id   = intval($this->input->get('profile_id'));
        if(!empty($profile_id)) {
          $search_array['profile_id'] = $profile_id;
        }
        $search_array = $this->security->xss_clean($search_array);


        $page = intval(trim($this->input->get('page'), '/'));
        $per_page = 5;
        $this->load->library('pagination');
        $config['total_rows'] = $this->profile_model->getTotalProfiles(1, 0, $lang, $search_array);
        $config['per_page'] = $per_page;
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['cur_page'] = $page;
        $config['base_url'] = base_url() . 'index.php/groom?page=';

        $config['full_tag_open'] = '<ul class="pagination pagination-lg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#" >';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        if(!empty($_GET)) {
          if(isset($_GET['page'])) { unset($_GET['page']); }
          $config['suffix'] = '&'.http_build_query($_GET,"","&");
        }
        $this->data['search_array'] = $search_array;

        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();



        $this->data['url'] = base_url();

        $this->data['base_url'] = base_url() . 'application/views/';
        $this->data['profiles'] = $this->profile_model->getAccounts($page, $per_page, 1, 0, $lang,$search_array);
        //$this->data['message'] = $this->session->flashdata('message');
        $this->data['body_content'] = $this->load->view('groom', $this->data, true);
        $this->load->view('template', $this->data);

    }

    function pdf() {
      $this->load->helper('pdf');
      pdf_view_new();
    }
}
