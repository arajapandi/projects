
<div class="container-fluid pr-0 pl-0 groombg">
    <h1>Feedback</h1>
    <img src="<?php echo $base_url; ?>images/groom.jpg" alt="" style="max-height:200px"/>

</div>
<div class="container">
    <div class="row paddindb100">

      <ul class="nav nav-pills">
        <li><a href="<?php echo $url; ?>index.php/profile" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('myprofiles'); ?></a></li>
        <li><a href="<?php echo $url; ?>index.php/profile/assigned" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('profilesheader'); ?></a></li>
        <li><a href="<?php echo $url; ?>index.php/profile/feedback" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('feedbackheader'); ?></a></li>
        <li><a href="<?php echo $url; ?>index.php/profile/change" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('changepassword'); ?></a></li>
        <li><a href="<?php echo $url; ?>index.php/profile/logout" class="btn_all btn_reset wave_button"><?php echo $this->lang->line('logout'); ?></a></li>
      </ul>


        <div class="col-12">

          <form name="regform" id="regform" data-toggle="validator"  action="<?php echo $url; ?>index.php/profile/feedback" method="post">
              <div class="form_div">
<br>
<br>
                  <?php if(!empty($message)): ?>
                  <div class="alert alert-info">
                    <?php echo $message; ?>
                  </div>
                  <?php endif; ?>




                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('titleheader'); ?><span class="mandatory">*</span></label>
                        <div class="col-12 col-sm-7">
                            <input class="form-control form-control-lg" name="subject" placeholder="Subject" required>

                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('feedbackheader'); ?><span class="mandatory">*</span></label>
                        <div class="col-12 col-sm-7">
                            <textarea name="feedback" class="form-control form-control-lg"  placeholder="Feedback"  required></textarea>
                        </div>
                    </div>



                  <div class="form-group row">
                      <label class="col-12 col-sm-3 col-form-label">&nbsp;</label>
                      <div class="col-12 col-sm-9">
                        <div class="row">
                            <div class="col-12">


                                <button name="submit" type="submit" class="btn_all wave_button" id="submit">Send</button>

                            </div>

                        </div>
                      </div>
                  </div>
              </div>

          </form>

        </div>
    </div>
</div>
