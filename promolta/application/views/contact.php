<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="container-fluid banner_bg text-right no-gutters pr-0 pl-0">
                <img src="<?php echo $base_url; ?>images/contactusbg.png" alt=""/>
                <h1>Contact Us</h1>
            </div>
            <div class="container">
                <div class="row paddindb100">
                    <div class="col-12 col-md-7">
                        <form name="regform" id="regform"  data-toggle="validator"  action="<?php echo $url; ?>index.php/contact/send" method="post">
                            <div class="form_div">
                                <?php if($message): ?>
                                <div class="alert alert-info">
                                  <?php echo $message; ?>
                                </div>
                                <?php endif; ?>

                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('name'); ?><span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-9">
                                        <input class="form-control form-control-lg" name="txtname" type="text" id="txtname" size="30" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('email'); ?><span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-9">
                                        <input autocomplete="off" name="txtemail" type="email" id="txtemail" size="30" class="form-control form-control-lg" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('phone'); ?><span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-9">
                                        <input autocomplete="off" name="txtphone" type="tel" id="txtphone" size="30" class="form-control form-control-lg" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label">Fax</label>
                                    <div class="col-12 col-sm-9">
                                        <input type="text" name="fax" id="fax" size="30" class="form-control form-control-lg">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('address'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <textarea class="form-control form-control-lg" rows="3" name="address" cols="30" id="address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('information'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <textarea class="form-control form-control-lg" rows="3" name="message" cols="30" id="message"></textarea>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-12 col-sm-9">
                                        <div class="g-recaptcha" data-sitekey="6LdLlzEUAAAAAOjm8sgBgFTs-k9wPvd-e_9sDh34"></div>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-12 text-right">
                                        <button name="reset" type="reset" class="btn_all btn_reset wave_button" id="reset">Reset</button>
                                        <button name="submit" type="submit" class="btn_all wave_button" id="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="form-rht-desc">
                            <div class="paid-payment">
                                <h2 class="contact_h1"><?php echo $this->lang->line('gavaratitle'); ?></h2>
                                <p><?php echo $this->lang->line('add1'); ?>,</p>
                                <p><?php echo $this->lang->line('add2'); ?></p>
                                <p><?php echo $this->lang->line('add3'); ?>: <a href="tel:04222471928">0422 2471928</a>,</p>
                                <p><?php echo $this->lang->line('callus'); ?>: <a href="tel:09442571928">94425 71928</a></p>
                                <p><?php echo $this->lang->line('emailtxt'); ?>: <a href="mailto:balijanaidutrust@gmail.com">balijanaidutrust@gmail.com</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
