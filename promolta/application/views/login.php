
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>

            <div class="container-fluid pr-0 pl-0 groombg">
              <h1> Login </h1>
                <img src="<?php echo $base_url; ?>images/groom.jpg" alt="" style="max-height:200px" />
                <img src="<?php echo $base_url; ?>images/bride.jpg" alt="" style="max-height:200px" class="pull-left"/>

            </div>



            <div class="container">
                <div class="row paddindb100">
                    <div class="col-12 col-md-7">
                        <form name="regform" id="regform"  data-toggle="validator"  action="<?php echo $url; ?>index.php/login/check" method="post">
                            <div class="form_div">

                                <?php if(!empty($message)): ?>
                                <div class="alert alert-info">
                                  <?php echo $message; ?>
                                </div>
                                <?php endif; ?>
                                <input type="hidden" name="key" value="<?php echo $key; ?>">
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label">Enter Mobile <span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-7">
                                        <input class="form-control form-control-lg" placeholder="Mobile Number" name="phone" type="text" pattern=".{10}" autofocus required>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label">Password<span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-7">
                                        <input class="form-control form-control-lg" name="password" type="password" id="password" size="30" pattern=".{5,15}" title="5 to 15 characters" placeholder="Enter Password" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('password'); ?> &nbsp; <span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-7">
                                        <div class="g-recaptcha" data-sitekey="6LdLlzEUAAAAAOjm8sgBgFTs-k9wPvd-e_9sDh34"></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-12 col-sm-9">
                                      <div class="row">
                                          <div class="col-12 col-md-6">
                                              <button name="submit" type="submit" class="btn_all wave_button" id="submit"> Login </button>
                                          </div>

                                          <div class="col-12 col-md-6">
                                              <a href="<?php echo $url; ?>index.php/login/forgot" class="btn_all wave_button" >Forgot Password</a>
                                          </div>

                                      </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="form-rht-desc">
                            <h1><?php echo $this->lang->line('fsuitable'); ?><br>
                                <span><?php echo $this->lang->line('gavaratitle'); ?></span><br>
                                <?php echo $this->lang->line('Companion'); ?></h1>
                            <p><?php echo $this->lang->line('asaviewer'); ?>.</p>
                            <h2><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $this->lang->line('Benefits'); ?>:</h2>
                            <ul>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('Findmatchedhoros'); ?>.</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('Suitablesoul'); ?>.</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('receivehoroscope'); ?>.</li>
                            </ul>
                            <h2><i class="fa fa-search" aria-hidden="true"></i><?php echo $this->lang->line('informationwill'); ?>:</h2>
                            <ul>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('startyoursearch'); ?>.</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('informationexamined'); ?>.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

<!--


            <div class="container">
                <div class="row paddindb100">
                    <div class="col-12">


                      <div class="reg_right col-md-6">
                          <h1><i class="fa fa-pencil-square-o fa-2x valignm" aria-hidden="true"></i> Sign In </h1>
                          <ul class="reg_ul">
                              <li><input class="form-control" placeholder="E-Mail" name="email" type="email" autofocus required> </li>
                              <li> <input class="form-control" placeholder="Password" name="password" type="password" required> </li>
                              <li> <div class="col-12 col-sm-9">

                                  </div>
                              </li>
                              <li>
                                <div  class="pull-right rows">
                                <button name="submit" type="submit" class="btn_all btn_reset wave_button waves-effect waves-button waves-float" id="reset">Login</button>
                                </div>
                                <br>
                              </li>
                          </ul>
                      </div>



                            <div class="reg_right col-6">
                              <form name="regform" id="regform"  data-toggle="validator"  action="<?php echo $url; ?>index.php/login/check" method="post">
                                <h1><i class="fa fa-pencil-square-o fa-2x valignm" aria-hidden="true"></i> Sign In </h1>
                                <?php if($message): ?>
                                <div class="alert alert-info">
                                  <?php echo $message; ?>
                                </div>
                                <?php endif; ?>
                                <ul class="reg_ul">

                                    <li><input class="form-control" placeholder="E-Mail" name="email" type="email" autofocus required> </li>
                                    <li> <input class="form-control" placeholder="Password" name="password" type="password" required> </li>
                                    <li> <div class="col-12 col-sm-9">
                                        <div class="g-recaptcha" data-sitekey="6LdLlzEUAAAAAOjm8sgBgFTs-k9wPvd-e_9sDh34"></div>
                                        </div>
                                    </li>
                                    <li>
                                      <div  class="pull-right rows">
                                      <button name="submit" type="submit" class="btn_all btn_reset wave_button waves-effect waves-button waves-float" id="reset">Login</button>
                                      </div>
                                      <br>
                                    </li>
                                </ul>
                                </form>
                            </div>


                    </div>
                </div>
            </div>-->
