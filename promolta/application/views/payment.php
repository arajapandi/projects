<div class="container-fluid banner_bg text-right no-gutters pr-0 pl-0">
                <img src="<?php echo $base_url; ?>images/payment.png" alt=""/>
                <h1><?php echo $this->lang->line('payementoption'); ?> </h1>
            </div>
            <div class="container">
                <div class="row paddindb100">
                    <div class="col-12 col-md-8">
                        <div class="bank-payment">
                            <div class="payment-top">
                                <h1><?php echo $this->lang->line('padireg'); ?> </h1>
                                <img src="<?php echo $base_url; ?>images/iob.png" alt="" class="iob"/>
                                <img src="<?php echo $base_url; ?>images/bank.png" alt="" class="bank"/>
                            </div>
                            <div class="paid-payment padtop20">
                                <p>Deposit the registration fee through cash / cheque / dd / in the following bank A/c.</p>
                                <p> A/C number - 007901000019114</p>
                                <p> A/C name - Gavara Maha Jana Sangam</p>
                                <p> IFSC code - IOBA0000079</p>
                                <p> Bank name - Indian Overseas Bank , RS Puram Branch</p>
                                <p> and confirm the payment to the office by calling Mob: <a href="tel:09442571928">94425 71928</a>, </p>
                                <p> E-mail: <a href="mailto:balijanaidutrust@gmail.com">balijanaidutrust@gmail.com</a></p>
                            </div>
                        </div>
                        <div class="home-payment padtop20">
                            <div class="payment-top text-right">
                                <h1 class="text-left">DOOR STEP</h1>
                                <img src="<?php echo $base_url; ?>images/homestep.png" alt="" class="homes"/>
                            </div>
                            <div class="paid-payment padtop20">
                                <p><?php echo $this->lang->line('regexpl'); ?>: <a href="tel:09442571928">94425 71928</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="paid-payment">
                            <h1><i class="fa fa-inr fa-1x valignm" aria-hidden="true"></i> <?php echo $this->lang->line('padireg'); ?> </h1>
                            <ul class="reg_ul">
                                <li><i class="fa fa-hand-o-right valignm" aria-hidden="true"></i> <?php echo $this->lang->line('regamt'); ?>.500 /-</li>
                                <li><i class="fa fa-hand-o-right valignm" aria-hidden="true"></i> <?php echo $this->lang->line('reg25horo1'); ?></li>
                                <li><i class="fa fa-hand-o-right valignm" aria-hidden="true"></i> <?php echo $this->lang->line('reg25horo2'); ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-8 padtop20">
                        <div class="payment-top text-right contactbg">
                            <h1 class="text-left"><?php echo $this->lang->line('contactus'); ?></h1>
                            <img src="<?php echo $base_url; ?>images/contactus.png" alt="" class="homes"/>
                        </div>
                        <div class="paid-payment padtop20">
                            <h2><?php echo $this->lang->line('gavaratitle'); ?></h2>
                            <p><?php echo $this->lang->line('add1'); ?></p>
                            <p><?php echo $this->lang->line('add2'); ?></p>
                            <p><?php echo $this->lang->line('add3'); ?>: <a href="tel:04222471928">0422 2471928</a>,</p>
                            <p><?php echo $this->lang->line('callus'); ?>: <a href="tel:09442571928">94425 71928</a></p>
                            <p><?php echo $this->lang->line('emailtxt'); ?>: <a href="mailto:balijanaidutrust@gmail.com">balijanaidutrust@gmail.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
