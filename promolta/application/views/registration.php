<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="container-fluid banner_bg text-right no-gutters pr-0 pl-0">
                <img src="<?php echo $base_url; ?>images/register.png" alt=""/>
                <h1><?php echo $this->lang->line('Registration'); ?></h1>
            </div>
            <div class="container">
                <div class="row paddindb100">
                    <div class="col-12 col-md-7">
                        <form name="regform" id="regform" data-toggle="validator"  action="<?php echo $url; ?>index.php/registration/send" method="post">
                            <div class="form_div">
                                <h1><i class="fa fa-user" aria-hidden="true"></i><?php echo $this->lang->line('bscprde'); ?> </h1>
                                <?php if($message): ?>
                                <div class="alert alert-info">
                                  <?php echo $message; ?>
                                </div>
                                <?php endif; ?>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('name'); ?><span class="mandatory">*</span></label>
                                    <div class="col-11 col-sm-2 pr-0">
                                        <select name="titlee" id="titlee" class="form-control form-control-lg">
                                            <option value="selected">Select</option>
                                            <option  value="Mr.">Mr.</option>
                                            <option  value="Ms.">Ms.</option>
                                            <option  value="Miss.">Miss.</option>
                                            <option  value="Mrs.">Mrs.</option>
                                            <option  value="Dr.">Dr.</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-sm-7">
                                        <input class="form-control form-control-lg" name="txtname" type="text" id="txtname" size="30" placeholder="Name" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('sex'); ?> <span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-9">
                                        <div class="form-check">
                                            <label class="form-check-label sm-label">
                                                <input class="form-check-input" name="gender" type="radio" id="gender" value="Male" required>
                                                <?php echo $this->lang->line('male'); ?>
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label sm-label">
                                                <input class="form-check-input" name="gender" type="radio" id="gender" value="Female" required>
                                                <?php echo $this->lang->line('femal'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('dob'); ?> <span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-9">
                                        <input type="text" name="dob" id="dob" size="30" class="form-control form-control-lg" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('birthtime'); ?> <span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-3">
                                        <select name="time" class="form-control form-control-lg" id="time" required>
                                            <option selected="selected" value="">Select</option>
                                            <option value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-sm-3">
                                        <select name="sec" id="sec" class="form-control form-control-lg" required>
                                            <option selected="selected" value="00">00</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                            <option value="32">32</option>
                                            <option value="33">33</option>
                                            <option value="34">34</option>
                                            <option value="35">35</option>
                                            <option value="36">36</option>
                                            <option value="37">37</option>
                                            <option value="38">38</option>
                                            <option value="39">39</option>
                                            <option value="40">40</option>
                                            <option value="41">41</option>
                                            <option value="42">42</option>
                                            <option value="43">43</option>
                                            <option value="44">44</option>
                                            <option value="45">45</option>
                                            <option value="46">46</option>
                                            <option value="47">47</option>
                                            <option value="48">48</option>
                                            <option value="49">49</option>
                                            <option value="50">50</option>
                                            <option value="51">51</option>
                                            <option value="52">52</option>
                                            <option value="53">53</option>
                                            <option value="54">54</option>
                                            <option value="55">55</option>
                                            <option value="56">56</option>
                                            <option value="57">57</option>
                                            <option value="58">58</option>
                                            <option value="59">59</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-sm-3">
                                        <select name="dobtime" id="dobtime" class="form-control form-control-lg" required>
                                            <option value="morning"><?php echo $this->lang->line('monring'); ?></option>
                                            <option  value="evening"><?php echo $this->lang->line('evening'); ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('birthplace'); ?> </label>
                                    <div class="col-12 col-sm-9">
                                        <input name="place" type="text" id="place" size="30" class="form-control form-control-lg">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Height'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <select name="height" id="height" class="form-control form-control-lg">
                                            <option selected="selected" value="">Select</option>
                                            <option value="140 to 145">140 cm to 145 cm</option>
                                            <option value="145 to 150">145 cm to 150 cm</option>
                                            <option value="150 to 155">150 cm to 155 cm</option>
                                            <option value="155 to 160">155 cm to 160 cm</option>
                                            <option value="160 to 165">160 cm to 165 cm</option>
                                            <option value="165 to 170">165 cm to 170 cm</option>
                                            <option value="170 to 175">170 cm to 175 cm</option>
                                            <option value="175 to 180">175 cm to 180 cm</option>
                                            <option value="180 to 185">180 cm to 185 cm</option>
                                            <option value="185 to 190">185 cm to 190 cm</option>
                                            <option value="190 to 195">190 cm to 195 cm</option>
                                            <option value="195 to 200">195 cm to 200 cm</option>
                                            <option value="200 to 205">200 cm to 205 cm</option>
                                            <option value="205 to 210">205 cm to 210 cm</option>
                                            <option value="210 to 215">210 cm to 215 cm</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Weight'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <select name="weight" id="weight" class="form-control form-control-lg">
                                            <option selected="selected" value="">Select</option>
                                            <option value="40">40 kg</option>
                                            <option value="41">41 kg</option>
                                            <option value="42">42 kg</option>
                                            <option value="43">43 kg</option>
                                            <option value="44">44 kg</option>
                                            <option value="45">45 kg</option>
                                            <option value="46">46 kg</option>
                                            <option value="47">47 kg</option>
                                            <option value="48">48 kg</option>
                                            <option value="49">49 kg</option>
                                            <option value="50">50 kg</option>
                                            <option value="51">51 kg</option>
                                            <option value="52">52 kg</option>
                                            <option value="53">53 kg</option>
                                            <option value="54">54 kg</option>
                                            <option value="55">55 kg</option>
                                            <option value="56">56 kg</option>
                                            <option value="57">57 kg</option>
                                            <option value="58">58 kg</option>
                                            <option value="59">59 kg</option>
                                            <option value="60">60 kg</option>
                                            <option value="61">61 kg</option>
                                            <option value="62">62 kg</option>
                                            <option value="63">63 kg</option>
                                            <option value="64">64 kg</option>
                                            <option value="65">65 kg</option>
                                            <option value="66">66 kg</option>
                                            <option value="67">67 kg</option>
                                            <option value="68">68 kg</option>
                                            <option value="69">69 kg</option>
                                            <option value="70">70 kg</option>
                                            <option value="71">71 kg</option>
                                            <option value="72">72 kg</option>
                                            <option value="73">73 kg</option>
                                            <option value="74">74 kg</option>
                                            <option value="75">75 kg</option>
                                            <option value="76">76 kg</option>
                                            <option value="77">77 kg</option>
                                            <option value="78">78 kg</option>
                                            <option value="79">79 kg</option>
                                            <option value="80">80 kg</option>
                                            <option value="81">81 kg</option>
                                            <option value="82">82 kg</option>
                                            <option value="83">83 kg</option>
                                            <option value="84">84 kg</option>
                                            <option value="85">85 kg</option>
                                            <option value="86">86 kg</option>
                                            <option value="87">87 kg</option>
                                            <option value="88">88 kg</option>
                                            <option value="89">89 kg</option>
                                            <option value="90">90 kg</option>
                                            <option value="91">91 kg</option>
                                            <option value="92">92 kg</option>
                                            <option value="93">93 kg</option>
                                            <option value="94">94 kg</option>
                                            <option value="95">95 kg</option>
                                            <option value="96">96 kg</option>
                                            <option value="97">97 kg</option>
                                            <option value="98">98 kg</option>
                                            <option value="99">99 kg</option>
                                            <option value="100">100 kg</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Subcaste'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <input name="kulam" type="text" id="kulam" size="30" class="form-control form-control-lg">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Family'); ?> </label>
                                    <div class="col-12 col-sm-9">
                                        <textarea class="form-control form-control-lg" name="family_address" cols="30" id="family_address" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Gotra'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <textarea class="form-control form-control-lg" name="gotra" cols="10" id="gotra" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Blood'); ?> </label>
                                    <div class="col-12 col-sm-9">
                                        <select name="bgroup" id="bgroup" class="form-control form-control-lg">
                                            <option selected="selected" value="">Select</option>
                                            <option value="O+">O+</option>
                                            <option value="O-">O-</option>
                                            <option value="A+">A+</option>
                                            <option value="A1+">A1+</option>
                                            <option value="A-">A-</option>
                                            <option value="A1-">A1-</option>
                                            <option value="B+">B+</option>
                                            <option value="B1+">B1+</option>
                                            <option value="B-">B-</option>
                                            <option value="B1-">B1-</option>
                                            <option value="AB+">AB+</option>
                                            <option value="AB-">AB-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Color'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <select name="colour" id="colour" class="form-control form-control-lg">
                                            <option selected="selected" value="">Select</option>
                                            <option value="Fair">Fair</option>
                                            <option value="Black">Black</option>
                                            <option value="Modest">Modest</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form_div">
                                <h1><i class="fa fa-briefcase" aria-hidden="true"></i> <?php echo $this->lang->line('jdetails'); ?></h1>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Education'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <select name="education" id="education" class="form-control form-control-lg">
                                            <option selected="selected" value="">Select</option>
                                            <option value="10th">10th</option>
                                            <option value="12th">12th</option>
                                            <option value="Below high school">Below high school</option>
                                            <option value="Uneducated">Uneducated</option>
                                            <option value="Aeronatical Engg">Aeronatical Engg</option>
                                            <option value="BA">BA</option>
                                            <option value="BA.BL">BA.BL</option>
                                            <option value="B Arch">B Arch</option>
                                            <option value="BCA">BCA</option>
                                            <option value="BBA">BBA</option>
                                            <option value="B.COM">B.COM</option>
                                            <option value="BCS">BCS</option>
                                            <option value="B.Com.CA">B.Com.CA</option>
                                            <option value="B.Com. FCA">B.Com. FCA</option>
                                            <option value="B.Com. MBA">B.Com. MBA</option>
                                            <option value="BE">BE</option>
                                            <option value="BE. MBA">BE. MBA</option>
                                            <option value="B.Ed">B.Ed</option>
                                            <option value="B.BHM">B.BHM</option>
                                            <option value="B.H.M">B.H.M</option>
                                            <option value="B.Pharm">B.Pharm</option>
                                            <option value="B.Phil">B.Phil</option>
                                            <option value="B.Plan">B.Plan</option>
                                            <option value="B.S">B.S</option>
                                            <option value="B.S (FT)">B.S (FT)</option>
                                            <option value="B.Sc">B.Sc</option>
                                            <option value="B.Sc, BL">B.Sc, BL</option>
                                            <option value="BSc, MBA">BSc, MBA</option>
                                            <option value="B.Tech">B.Tech</option>
                                            <option value="BAMS">BAMS</option>
                                            <option value="BBM">BBM</option>
                                            <option value="BDS">BDS</option>
                                            <option value="BL">BL</option>
                                            <option value="BGL">BGL</option>
                                            <option value="BPT">BPT</option>
                                            <option value="CA">CA</option>
                                            <option value="CS">CS</option>
                                            <option value="Diploma">Diploma</option>
                                            <option value="I.T.I">I.T.I</option>
                                            <option value="IAS">IAS</option>
                                            <option value="ICWA">ICWA</option>
                                            <option value="IES">IES</option>
                                            <option value="IPS">IPS</option>
                                            <option value="IFS">IFS</option>
                                            <option value="IRS">IRS</option>
                                            <option value="MBA">MBA</option>
                                            <option value="MBBS">MBBS</option>
                                            <option value="MCA">MCA</option>
                                            <option value="PGDCA">PGDCA</option>
                                            <option value="PHD">PHD</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Occupation'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <select name="occupation" id="occupation" class="form-control form-control-lg">
                                            <option selected="selected" value="">Select</option>
                                            <option value="Agriculture">Agriculture</option>
                                            <option value="Government Employee">Government Employee</option>
                                            <option value="Private sector Employee">Private sector Employee</option>
                                            <option value="IT">IT</option>
                                            <option value="Self Employed">Self Employed</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Annuali'); ?> </label>
                                    <div class="col-12 col-sm-9">
                                        <select name="income" id="income" class="form-control form-control-lg">
                                            <option selected="selected" value="">Select</option>
                                            <option value="Below 1lakh">Below 1lakh</option>
                                            <option value="1Lakh - 2lakhs">1Lakh - 2lakhs</option>
                                            <option value="2lakhs - 4lakhs">2lakhs - 4lakhs</option>
                                            <option value="4lakhs - 6lakhs">4lakhs - 6lakhs</option>
                                            <option value="6lakhs - 10lakhs">6lakhs - 10lakhs</option>
                                            <option value="10lakhs - 20lakhs">10lakhs - 20lakhs</option>
                                            <option value="Above 20lakhs">Above 20lakhs</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Informationoo'); ?>   . <span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-9">
                                        <textarea class="form-control form-control-lg" name="property" cols="30" id="property" rows="3" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form_div">
                                <h1><i class="fa fa-address-book" aria-hidden="true"></i> <?php echo $this->lang->line('addressde'); ?> </h1>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('regno'); ?>Country</label>
                                    <div class="col-12 col-sm-9">
                                        <select name="country" class="form-control form-control-lg" id="country">
                                            <option selected="selected" value="">Select</option>
                                            <option value="India">India</option>
                                            <option value="others">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('State'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <select name="state" class="form-control form-control-lg" id="state">
                                            <option selected="selected" value="">Select</option>
                                            <option value="Andaman">Andaman</option>
                                            <option value="Andhra Pradesh">Andhra Pradesh</option>
                                            <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                            <option value="Assam">Assam</option>
                                            <option value="Bihar">Bihar</option>
                                            <option value="Chandigarh">Chandigarh</option>
                                            <option value="Chhattisgarh">Chhattisgarh</option>
                                            <option value="Dadra and Nagar">Dadra and Nagar</option>
                                            <option value="Daman and Diu">Daman and Diu</option>
                                            <option value="Goa">Goa</option>
                                            <option value="Gujarat">Gujarat</option>
                                            <option value="Haryan">Haryana</option>
                                            <option value="Himachal Pradesh">Himachal Pradesh</option>
                                            <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                            <option value="Jharkhand">Jharkhand</option>
                                            <option value="Karnataka">Karnataka</option>
                                            <option value="Kerala">Kerala</option>
                                            <option value="Lakshadweep">Lakshadweep</option>
                                            <option value="Madhya Pradesh">Madhya Pradesh</option>
                                            <option value="Maharashtra">Maharashtra</option>
                                            <option value="Manipur">Manipur</option>
                                            <option value="Meghalaya">Meghalaya</option>
                                            <option value="Mizoram">Mizoram</option>
                                            <option value="Nagaland">Nagaland</option>
                                            <option value="NewDelhi">NewDelhi</option>
                                            <option value="Orissa">Orissa</option>
                                            <option value="Puducherry">Puducherry</option>
                                            <option value="Punjab">Punjab</option>
                                            <option value="Rajasthan">Rajasthan</option>
                                            <option value="Sikkim">Sikkim</option>
                                            <option value="Tamil Nadu">Tamil Nadu</option>
                                            <option value="Tripura">Tripura</option>
                                            <option value="Uttar Pradesh">Uttar Pradesh</option>
                                            <option value="Uttarakhand">Uttarakhand</option>
                                            <option value="West Bengal">West Bengal</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Country'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <select name="district" class="form-control form-control-lg" id="district">
                                            <option selected="selected" value="">Select</option>
                                            <option value="Ariyalur">Ariyalur</option>
                                            <option value="Chennai">Chennai </option>
                                            <option value="Coimbatore">Coimbatore</option>
                                            <option value="Cuddalore">Cuddalore</option>
                                            <option value="Dharmapuri ">Dharmapuri</option>
                                            <option value="Dindigul">Dindigul</option>
                                            <option value="Erode">Erode</option>
                                            <option value="Kanchipuram">Kanchipuram</option>
                                            <option value="Kanyakumari">Kanyakumari</option>
                                            <option value="Karur">Karur</option>
                                            <option value="Krishnagiri">Krishnagiri</option>
                                            <option value="Madurai">Madurai</option>
                                            <option value="Nagapattinam">Nagapattinam</option>
                                            <option value="Namakkal">Namakkal</option>
                                            <option value="Nilgiris">Nilgiris</option>
                                            <option value="Perambalur ">Perambalur</option>
                                            <option value="Pudukkottai">Pudukkottai</option>
                                            <option value="Ramanathapuram">Ramanathapuram </option>
                                            <option value="Salem">Salem</option>
                                            <option value="Sivagangai ">Sivagangai</option>
                                            <option value="Theni">Theni</option>
                                            <option value="Thoothukudi">Thoothukudi</option>
                                            <option value="Tiruchirapalli">Tiruchirapalli</option>
                                            <option value="Tirunelveli">Tirunelveli </option>
                                            <option value="Tirupur">Tirupur</option>
                                            <option value="Tiruvallur">Tiruvallur</option>
                                            <option value="Tiruvannamalai ">Tiruvannamalai</option>
                                            <option value="Tiruvarur ">Tiruvarur</option>
                                            <option value="Vellore">Vellore </option>
                                            <option value="Viluppuram">Viluppuram</option>
                                            <option value="Virudhunagar">Virudhunagar</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Address'); ?> <span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-9">
                                        <textarea class="form-control form-control-lg" rows="3" name="address" cols="30" id="address" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Pincode'); ?> <span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-9">
                                        <input autocomplete="off" name="pincode" class="form-control form-control-lg" id="pincode" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Mail'); ?><span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-9">
                                        <input autocomplete="off" name="txtemail" type="text" id="txtemail" size="30" class="form-control form-control-lg" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Phonen'); ?>  <span class="mandatory">*</span></label>
                                    <div class="col-12 col-sm-9">
                                        <input autocomplete="off" name="txtphone" type="text" id="txtphone" size="30" class="form-control form-control-lg" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form_div">
                                <h1><i class="fa fa-users" aria-hidden="true"></i><?php echo $this->lang->line('Familyd'); ?>  </h1>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Fathersn'); ?></label>
                                    <div class="col-11 col-sm-2 pr-0">
                                        <select name="titlee" id="titlee" class="form-control form-control-lg">
                                            <option value="selected">Select</option>
                                            <option value="Mr.">Mr.</option>
                                            <option value="late Mr.">late Mr.</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-sm-7">
                                        <input autocomplete="off" name="father" class="form-control form-control-lg" id="father" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Mothers'); ?></label>
                                    <div class="col-11 col-sm-2 pr-0">
                                        <select name="titlee" id="titlee" class="form-control form-control-lg">
                                            <option value="selected">Select</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="late Mrs.">late Mrs.</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-sm-7">
                                        <input autocomplete="off" name="mother" class="form-control form-control-lg" id="mother" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Brothers'); ?></label>
                                    <div class="col-12 col-sm-2">
                                        <label class="col-form-label sm-label"><?php echo $this->lang->line('Married'); ?></label>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <input name="bromarried" id="bromarried" type="text" class="form-control form-control-lg">
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <label class="col-form-label sm-label"><?php echo $this->lang->line('Unmarried'); ?></label>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <input name="brounmarried" id="brounmarried" type="text" class="form-control form-control-lg">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Sisters'); ?></label>
                                    <div class="col-12 col-sm-2">
                                        <label class="col-form-label sm-label"><?php echo $this->lang->line('Married'); ?></label>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <input name="sismarried" id="sismarried" type="text" class="form-control form-control-lg">
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <label class="col-form-label sm-label"><?php echo $this->lang->line('Unmarried'); ?></label>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <input name="sisunmarried" id="sisunmarried" type="text" class="form-control form-control-lg">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Informations'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <textarea name="information" cols="30" id="information" class="form-control form-control-lg" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Expectations'); ?></label>
                                    <div class="col-12 col-sm-9">
                                        <textarea name="expectations" cols="30" id="expectations" class="form-control form-control-lg" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label"><?php echo $this->lang->line('Registeredp'); ?> </label>
                                    <div class="col-12 col-sm-9">
                                        <select name="regperson" class="form-control form-control-lg" id="regperson">
                                            <option selected="selected" value="">Select</option>
                                            <option value="self">Self</option>
                                            <option value="parents">Parents</option>
                                            <option value="Sibling">Sibling</option>
                                            <option value="Relative">Relative</option>
                                            <option value="Friends">Friends</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-12 col-sm-9">
                                        <div class="g-recaptcha" data-sitekey="6LdLlzEUAAAAAOjm8sgBgFTs-k9wPvd-e_9sDh34"></div>
                                    </div>
                                </div>



                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-12 col-sm-9">
                                      <div class="form-check">
                                          <label class="form-check-label sm-label">
                                              <input type="checkbox" class="form-check-input" id="checkbox">
                                              <?php echo $this->lang->line('agreet'); ?>
                                          </label>
                                      </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-12 col-sm-3 col-form-label">&nbsp;</label>
                                    <div class="col-12 col-sm-9">
                                      <div class="row">
                                          <div class="col-12">
                                              <button name="submit" type="submit" class="btn_all wave_button" id="submit"><?php echo $this->lang->line('Submit'); ?></button>
                                          </div>
                                      </div>
                                    </div>
                                </div>




                            </div>

                        </form>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="form-rht-desc">
                            <h1>
                              <?php echo $this->lang->line('fsuitable'); ?></h1>
                            <p><?php echo $this->lang->line('asaviewer'); ?>.</p>
                            <h2><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $this->lang->line('Benefits'); ?>:</h2>
                            <ul>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('Findmatchedhoros'); ?>.</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('Suitablesoul'); ?>.</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('receivehoroscope'); ?>.</li>
                            </ul>
                            <h2><i class="fa fa-search" aria-hidden="true"></i><?php echo $this->lang->line('informationwill'); ?>:</h2>
                            <ul>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('startyoursearch'); ?>.</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> <?php echo $this->lang->line('informationexamined'); ?>.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
