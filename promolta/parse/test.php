<?php

$var = <<<END


<html xmlns="http://www.w3.org/1999/xhtml"><head>



<script src="../js/jquery-1.3.1.min.js" type="text/javascript"></script>

<script src="../js/main.js" type="text/javascript"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Gavara Maha Jana Sangam:: ADMIN AREA</title>

<link href="../css/admin.css" rel="stylesheet" type="text/css">



<link rel="stylesheet" type="text/css" href="../css/jquery.datepick.css">

<script type="text/javascript" src="../js/jquery.datepick.js"></script>

<script type="text/javascript" language="javascript">

 $(function(){

  $("#dob").datepick();
   $("#curr_date").datepick();



});

</script>

<script>
function genstar(value,name)
{
data=name.split('_');
type=data[0]+"_";
typetxt=data[0]+"_txt_";
       for(i=1;i<13;i++)
       {
               str=$('#'+type+i).html().replace(value," ");
               $('#'+type+i).html(str);
			   strtxt=$('#'+typetxt+i).val().replace(value," ");
               $('#'+typetxt+i).val(strtxt);
       }
   var current=$('#'+type+$('#'+name).val()).html();
   $('#'+type+$('#'+name).val()).html(value+' '+current);
    var currenttxt=$('#'+typetxt+$('#'+name).val()).val();
   $('#'+typetxt+$('#'+name).val()).val(value+' '+currenttxt);
}
</script>

<script>
function getval(val){

if(val==1){

document.getElementById('groombox').style.display="block";

document.getElementById('bridebox').style.display="none";
}

else if(val==2){

document.getElementById('groombox').style.display="none";

document.getElementById('bridebox').style.display="block";
}
}
</script>

</head>



<body>



<div id="main">



<div id="main-header">

<div id="admin-area1">Admin Area for : <span class="white20px">Gavara Maha Jana sangam</span></div>

<div id="top-btns">

<span class="dashboard"></span>

<a href="welcome_admin.php" class="admin-btn">&nbsp;</a>

<a href="../tamil/index.html" class="live-site-btn" target="_blank">&nbsp;</a>

<div class="welcome-admin">Welcome <strong>Admin</strong>  |


 <a href="index.php?aflg=0">Logout</a>


</div>


</div>

</div>





<div id="main-nav-area">

  <table width="830" cellspacing="0" cellpadding="0" border="0">

    <tbody><tr>

      <td width="184" valign="top" align="left">


	<script src="../js/menu.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/admin.css">
	<ul id="menu1" class="menu" style="list-style:none">


	<li>
			<a href="#" class="side-nm">&nbsp;Profile Management</a>
			<ul>
			<!--<li><a href="job_category.php">View Job Category</a></li>-->
				<li><a href="ta_groom_mgmt.php">View Profile</a></li>

			</ul>
		</li>

		<li>
			<a href="#" class="side-sm">Settings</a>
			<ul>
				<li><a href="password_change.php">Change Password</a></li>
				<!--<li><a href="paginate_counts.php">Pagination Count</a></li>-->
				<li><a href="index.php">Logout</a></li>
			</ul>
		</li>
	</ul>



	  </td>

      <td width="15">&nbsp;</td>

      <td valign="top" align="left"><table width="100%" cellspacing="0" cellpadding="0" border="0">

        <tbody><tr>

          <td class="details-top" height="37"><img src="../admin_images/page-top-icon.jpg" alt="" width="44" align="left" height="37"><span class="detailswhite22px"> Profile Management </span></td>

        </tr>

       <tr>

          <td valign="middle" align="left" height="30"><a href="ta_groom_mgmt.php">

	  <img src="../admin_images/btn_back_02.gif" border="0">

	  </a></td>

        </tr>

		 <tr>

          <td valign="middle" align="left" height="25">&nbsp;Fields marked with an asterisk <span class="txtRedBold">*</span> are required.		 </td>

        </tr>

        <tr>

          <td><style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>



    <form action="/administrator/ta_edit_groom.php" method="post" enctype="multipart/form-data" name="form1" id="form1">

  <table style="border:1px solid #0AA0E9; margin:0px;" width="600" border="0">

    <tbody><tr>

  <td colspan="2" class="details-blue-gradient" height="32">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;புதிய பதிவு செய்தது விவரம் </td>
  </tr>

    <tr>
      <td align="left">&nbsp;&nbsp;பதிவு எண் <span class="txtRedBold">*</span></td>
      <td align="left"><input name="regno" class="fieldss" readonly="readonly" id="regno" size="60" value="633" type="text"></td>
    </tr>
    <tr>

      <td width="144" align="left">&nbsp;&nbsp;பெயர் <span class="txtRedBold">*</span></td>

      <td width="360" align="left"><input name="name" class="fieldss" id="name" size="60" value="சக்திவிக்னேஷ்" type="text"></td>
    </tr>

    <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;தேதி</td>

      <td class="txtNormal" align="left" height="31"><input name="curr_date" class="fieldss" id="curr_date" size="55" value="07/12/2017" type="text"></td>
    </tr>

      <tr><td class="txtNormal" align="left" height="31">&nbsp;&nbsp;Sevvai / செவ்வாய்</td>

      <td class="txtNormal" align="left" height="31"><input name="sew" class="fieldss" id="sew" value="ராகு கேது" size="55" type="text"></td>
    </tr>
    <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;Raagu / கேது</td>

      <td class="txtNormal" align="left" height="31"><input name="raagu" class="fieldss" id="raagu" value="ராகு கேது " size="55" type="text"></td>
    </tr>

    <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;கல்வி<span class="txtRedBold">*</span></td>

      <td class="txtNormal" align="left" height="31"><input name="edu" class="fieldss" id="edu" value="B.E Software Eng" size="60" type="text"></td>
    </tr>

    <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;வருட வருமானம்</td>

      <td class="txtNormal" align="left" height="31"><select name="income" id="income" class="fieldsselect">
                  <option value="Select">Select</option>
                  <option value="Below 1lakh">Below 1lakh</option>
                  <option value="1Lakh - 2lakhs">1Lakh - 2lakhs</option>
                  <option value="2lakhs - 4lakhs">2lakhs - 4lakhs</option>
                  <option value="4lakhs - 6lakhs">4lakhs - 6lakhs</option>
                  <option value="6lakhs - 10lakhs">6lakhs - 10lakhs</option>
                  <option value="10lakhs - 20lakhs">10lakhs - 20lakhs</option>
                  <option value="Above 20lakhs">Above 20lakhs</option>
                  <option value="Others">Others</option>
              </select></td>
    </tr>
<tr>
      <td class="txtNormal" align="left" height="31">&nbsp;வேலை</td>

      <td class="txtNormal" align="left" height="31"><input name="job" class="fieldss" id="job" size="60" value="HCL Bangalore Rs.70,000/-" type="text"></td>
    </tr>
    <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;தந்தையின் பெயர்</td>

      <td class="txtNormal" align="left" height="31"><input name="fname" class="fieldss" id="fname" value="V.சுந்தா்ராஜ்" size="60" type="text"></td>
    </tr>

	<tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;அம்மாவின் பெயர்</td>

      <td class="txtNormal" align="left" height="31"><input name="mname" class="fieldss" id="mname" size="60" value="தேன்மொழி" type="text"></td>
    </tr>

     <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;வசிக்கும் நகரம்</td>

      <td class="txtNormal" align="left" height="31"><input name="city" class="fieldss" id="city" size="60" value="கோவை" type="text"></td>
    </tr>

     <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;உயரம்<span class="txtRedBold">*</span></td>

      <td class="txtNormal" align="left" height="31"><input name="height" class="fieldss" id="height" size="60" value="5'11" "="" required="" type="text"></td>
    </tr>

     <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;நிறம்</td>

      <td class="txtNormal" align="left" height="31"><input name="color" class="fieldss" id="color" size="60" value="மாநிறம்" type="text"></td>
    </tr>

     <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;குலம்</td>

      <td class="txtNormal" align="left" height="31"><input name="kulam" class="fieldss" id="kulam" size="60" value="செட்டேவாலு" type="text"></td>
    </tr>

     <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;குலா தெய்வம் கோயில்</td>

      <td class="txtNormal" align="left" height="31"><input name="kula_theivam" class="fieldss" id="kula_theivam" size="60" value="அங்காளஅம்மன்" type="text"></td>
    </tr>

     <tr>
         <td>
     <p><strong>பிறப்பு விவரங்கள்</strong></p>
         </td>
  </tr>
  <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;தமிழ்</td>

      <td class="txtNormal" align="left" height="31"><input name="tamil" class="fieldss" id="tamil" value="விபவ" size="55" type="text"></td>
    </tr>
    <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;ஆண்டு</td>

      <td class="txtNormal" align="left" height="31"><input name="year" class="fieldss" id="year" value="1988" size="55" type="text"></td>
    </tr>
    <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;மாதம்</td>

      <td class="txtNormal" align="left" height="31"><input name="month" class="fieldss" id="month" value="ஜப்பசி" size="55" type="text"></td>
    </tr>
    <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;தேதி</td>

      <td class="txtNormal" align="left" height="31"><input name="date" class="fieldss" id="date" value="27" size="60" type="text"></td>
    </tr>
     <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;நாழிைகக்கு</td>

      <td class="txtNormal" align="left" height="31"><input name="nazhiga" class="fieldss" id="nazhiga" value="" size="60" type="text"></td>
    </tr>
    <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;நேரம்</td>

      <td class="txtNormal" align="left" height="31"><input name="time" class="fieldss" id="time" value="11.30 pm" size="60" type="text"></td>
    </tr>

     <tr>
            <td>&nbsp;&nbsp;நட்சத்திரத்தில்</td>
            <td><select name="profile_star" id="profile_star" class="fieldsselect">
              <option value="Select">Select</option>
              <option value="Aswini">Aswini</option>
              <option value="Bharani">Bharani</option>
              <option value="Karthikai">Karthikai</option>
              <option value="Rohini">Rohini</option>
              <option value="Mirugasiridam">Mirugasiridam</option>
              <option value="Thiruvathirai">Thiruvathirai</option>
              <option value="Punarpusam">Punarpusam</option>
              <option value="Pusam">Pusam</option>
              <option value="Ayilyam">Ayilyam</option>
              <option value="Maham">Maham</option>
              <option value="Puram">Puram</option>
              <option value="Uthiram">Uthiram</option>
              <option value="Atham">Atham</option>
              <option value="Chithirai">Chithirai</option>
              <option value="Swathi">Swathi</option>
              <option value="Visakam">Visakam</option>
              <option value="Anusam">Anusam</option>
              <option value="Ketai">Ketai</option>
              <option value="Mulam" selected="selected">Mulam</option>
              <option value="Puradam">Puradam</option>

              <option value="Uthiradam">Uthiradam</option>
              <option value="Thiruvonam">Thiruvonam</option>
              <option value="Avitam">Avitam</option>
              <option value="Sathayam">Sathayam</option>
              <option value="Puratathi">Puratathi</option>
              <option value="Uthiratathi">Uthiratathi</option>
              <option value="Revathi">Revathi</option>
            </select></td>
          </tr>

          <tr>
            <td>&nbsp;&nbsp;ராசி</td>
            <td><select name="profile_zodiac1" id="profile_zodiac1" class="fieldsselect">
              <option value="Select">Select</option>
              <option value="Mesham">Mesham</option>
              <option value="Rishabam">Rishabam</option>
              <option value="Mithunam"> Mithunam</option>
              <option value="Kadagam">Kadagam</option>
              <option value="Simmam">Simmam</option>
              <option value="Kanni">Kanni</option>
              <option value="Thulam">Thulam</option>
              <option value="Virushigam">Virushigam</option>
              <option value="Thanusu" selected="selected">Thanusu</option>
              <option value="Magaram">Magaram</option>
              <option value="Kumbam">Kumbam</option>
              <option value="Meenam">Meenam</option>
            </select></td>
          </tr>

          <tr>
            <td>&nbsp;&nbsp;Lagnam</td>
            <td><select name="profile_zodiac2" id="profile_zodiac2" class="fieldsselect">
              <option value="Select">Select</option>
              <option value="Mesham">Mesham</option>
              <option value="Rishabam">Rishabam</option>
              <option value="Mithunam">Mithunam</option>
              <option value="Kadagam" selected="selected">Kadagam</option>
              <option value="Simmam">Simmam</option>
              <option value="Kanni">Kanni</option>
              <option value="Thulam">Thulam</option>
              <option value="Virushigam">Virushigam</option>
              <option value="Thanusu">Thanusu</option>
              <option value="Magaram">Magaram</option>
              <option value="Kumbam">Kumbam</option>
              <option value="Meenam">Meenam</option>
            </select></td>
          </tr>

          <tr>
            <td>&nbsp;&nbsp;Thisai Eruppu</td>
            <td><select name="profile_zodiac3" id="profile_zodiac3" class="fieldsselect">
                <option value="Select">Select</option>
                <option value="Guru thisai">Guru thisai</option>
                <option value="Sani thisai">Sani thisai</option>
                <option value="kethu thisai" selected="selected">kethu thisai</option>
                <option value="Sukura thisai">Sukura thisai</option>
                <option value="Suriya thisai">Suriya thisai</option>
                <option value="Chandra thisai">Chandra thisai</option>
                <option value="Sewwai thisai">Sewwai thisai</option>
                <option value="Ragu thisai">Ragu thisai</option>
                <option value="Budhan thisai">Budhan thisai</option>
            </select></td>
          </tr>


            <tr>
            <td>

            <table width="140" cellspacing="0" cellpadding="0" border="0" height="261">
              <tbody>
                <tr>
                  <td style="padding-top:5px;" width="87">&nbsp;&nbsp;Laknam</td>

                  <td style="padding-top:5px;" width="53">
                  <select name="rasi_select1" id="rasi_select1" onchange="genstar('Lak','rasi_select1')">
                      <option> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>

                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;சூரியன்</td>
                  <td style="padding-top:5px;">
                  <select name="rasi_select2" id="rasi_select2" onchange="genstar('Sur','rasi_select2')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>

                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;Budhan</td>
                  <td style="padding-top:5px;">
                  <select name="rasi_select3" id="rasi_select3" onchange="genstar('Bud','rasi_select3')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>

                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;Sukuran</td>
                  <td style="padding-top:5px;">
                  <select name="rasi_select4" id="rasi_select4" onchange="genstar('Suk','rasi_select4')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>

                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;Sewwai</td>
                  <td style="padding-top:5px;">
                  <select name="rasi_select5" id="rasi_select5" onchange="genstar('Sew','rasi_select5')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>

                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;ரகு</td>
                  <td style="padding-top:5px;">
                  <select name="rasi_select6" id="rasi_select6" onchange="genstar('Rag','rasi_select6')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>

                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>

                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;கேது</td>
                  <td style="padding-top:5px;"><select name="rasi_select7" id="rasi_select7" onchange="genstar('Ket','rasi_select7')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>

                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;குரு</td>
                  <td style="padding-top:5px;"><select name="rasi_select8" id="rasi_select8" onchange="genstar('Gur','rasi_select8')">
                      <option value=""></option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>

                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;சந்திரன்</td>
                  <td style="padding-top:5px;"><select name="rasi_select9" id="rasi_select9" onchange="genstar('cha','rasi_select9')">
                      <option value=""></option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>

                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;சனி</td>
                  <td style="padding-top:5px;"><select name="rasi_select10" id="rasi_select10" onchange="genstar('San','rasi_select10')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>

                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;Manthi</td>
                  <td style="padding-top:5px;"><select name="rasi_select11" id="rasi_select11" onchange="genstar('Man','rasi_select11')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
              </tbody>
            </table>            </td>

            <td>

            <table width="288" cellspacing="0" cellpadding="0" border="0" align="left" height="260">
              <tbody>
                <tr class="brd11">


                  <td class="brd11" id="rasi_box12" style="background:#fff url(../images/numbers/12.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_12">Sew </span><input name="rasi_12" id="rasi_txt_12" value="Sew " type="hidden"></td>
                  <td class="brd11" id="rasi_box1" style="background:#fff url(../images/numbers/1.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_1"></span><input name="rasi_1" id="rasi_txt_1" value="" type="hidden"></td>
                  <td class="brd11" id="rasi_box2" style="background:#fff url(../images/numbers/2.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_2">Gur </span><input name="rasi_2" id="rasi_txt_2" value="Gur " type="hidden"></td>
                  <td class="brd11" id="rasi_box3" style="background:#fff url(../images/numbers/3.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_3"></span><input name="rasi_3" id="rasi_txt_3" value="" type="hidden"></td>
                </tr>
                <tr>
                  <td class="brd11" id="rasi_box11" style="background:#fff url(../images/numbers/11.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_11">Rag </span><input name="rasi_11" id="rasi_txt_11" value="Rag " type="hidden"></td>
                  <td colspan="2" rowspan="2" style="vertical-align:middle; text-align:center; background-color:#FFF; border:#CCC solid 1px;" valign="middle" align="center"><span class="txtBold">Rasi</span></td>
                  <td class="brd11" id="rasi_box4" style="background:#fff url(../images/numbers/4.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_4">Lak Man </span><input name="rasi_4" id="rasi_txt_4" value="Lak Man " type="hidden"></td>
                </tr>
                <tr>
                  <td class="brd11" id="rasi_box10" style="background:#fff url(../images/numbers/10.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_10"></span><input name="rasi_10" id="rasi_txt_10" value="" type="hidden"></td>
                  <td class="brd11" id="rasi_box5" style="background:#fff url(../images/numbers/5.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_5">Ket </span><input name="rasi_5" id="rasi_txt_5" value="Ket " type="hidden"></td>
                </tr>
                <tr>
                  <td class="brd11" id="rasi_box9" style="background:#fff url(../images/numbers/9.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_9">San cha </span><input name="rasi_9" id="rasi_txt_9" value="San cha " type="hidden"></td>
                  <td class="brd11" id="rasi_box8" style="background:#fff url(../images/numbers/8.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_8">  </span><input name="rasi_8" id="rasi_txt_8" value="  " type="hidden"></td>
                  <td class="brd11" id="rasi_box7" style="background:#fff url(../images/numbers/7.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_7">Bud Sur </span><input name="rasi_7" id="rasi_txt_7" value="Bud Sur " type="hidden"></td>
                  <td class="brd11" id="rasi_box6" style="background:#fff url(../images/numbers/6.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="rasi_6">Suk </span><input name="rasi_6" id="rasi_txt_6" value="Suk " type="hidden"></td>
                </tr>
              </tbody>
            </table>            </td>
          </tr>


          <tr>
            <td>
<table width="140" cellspacing="0" cellpadding="0" border="0" height="261">
              <tbody>
                <tr>
                  <td style="padding-top:5px;" width="87">&nbsp;&nbsp;Laknam</td>
                  <td style="padding-top:5px;" width="53"><select name="amsam_select1" id="amsam_select1" onchange="genstar('Lak','amsam_select1')">

                      <option> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;சூரியன்</td>
                  <td style="padding-top:5px;"><select name="amsam_select2" id="amsam_select2" onchange="genstar('Sur','amsam_select2')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;Budhan</td>
                  <td style="padding-top:5px;"><select name="amsam_select3" id="amsam_select3" onchange="genstar('Bud','amsam_select3')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;Sukuran</td>
                  <td style="padding-top:5px;"><select name="amsam_select4" id="amsam_select4" onchange="genstar('Suk','amsam_select4')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;Sewwai</td>
                  <td style="padding-top:5px;"><select name="amsam_select5" id="amsam_select5" onchange="genstar('Sew','amsam_select5')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;ரகு</td>
                  <td style="padding-top:5px;"><select name="amsam_select6" id="amsam_select6" onchange="genstar('Rag','amsam_select6')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;கேது</td>
                  <td style="padding-top:5px;"><select name="amsam_select7" id="amsam_select7" onchange="genstar('Ket','amsam_select7')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;குரு</td>
                  <td style="padding-top:5px;"><select name="amsam_select8" id="amsam_select8" onchange="genstar('Gur','amsam_select8')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;சந்திரன்</td>
                  <td style="padding-top:5px;"><select name="amsam_select9" id="amsam_select9" onchange="genstar('cha','amsam_select9')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;சனி</td>
                  <td style="padding-top:5px;"><select name="amsam_select10" id="amsam_select10" onchange="genstar('San','amsam_select10')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
                <tr>
                  <td style="padding-top:5px;">&nbsp;&nbsp;Manthi</td>
                  <td style="padding-top:5px;"><select name="amsam_select11" id="amsam_select11" onchange="genstar('Man','amsam_select11')">
                      <option value=""> </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select></td>
                </tr>
              </tbody>
            </table>            </td>

            <td>
             <table width="288" cellspacing="0" cellpadding="0" border="0" align="left" height="260">
<tbody>
<tr class="brd11">

<td class="brd11" id="rasi_box13" style="background:#fff url(../images/numbers/12.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_12">Rag Gur </span><input name="amsam_12" id="amsam_txt_12" value="Rag Gur " type="hidden"></td>

                  <td class="brd11" id="rasi_box14" style="background:#fff url(../images/numbers/1.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_1"></span><input name="amsam_1" id="amsam_txt_1" value="" type="hidden"></td>
                  <td class="brd11" id="rasi_box15" style="background:#fff url(../images/numbers/2.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_2">San cha </span><input name="amsam_2" id="amsam_txt_2" value="San cha " type="hidden"></td>
                  <td class="brd11" id="rasi_box16" style="background:#fff url(../images/numbers/3.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_3">Sur </span><input name="amsam_3" id="amsam_txt_3" value="Sur " type="hidden"></td>
                </tr>
                <tr>
                  <td class="brd11" id="rasi_box17" style="background:#fff url(../images/numbers/11.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_11">Bud </span><input name="amsam_11" id="amsam_txt_11" value="Bud " type="hidden"></td>
                  <td colspan="2" rowspan="2" style="vertical-align:middle; text-align:center; background-color:#FFF; border:#CCC solid 1px;" valign="middle" align="center"><span class="txtBold">Amsam</span></td>
                  <td class="brd11" id="rasi_box18" style="background:#fff url(../images/numbers/4.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_4">Suk </span><input name="amsam_4" id="amsam_txt_4" value="Suk " type="hidden"></td>
                </tr>
                <tr>
                  <td class="brd11" id="rasi_box19" style="background:#fff url(../images/numbers/10.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_10"></span><input name="amsam_10" id="amsam_txt_10" value="" type="hidden"></td>
                  <td class="brd11" id="rasi_box20" style="background:#fff url(../images/numbers/5.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_5"></span><input name="amsam_5" id="amsam_txt_5" value="" type="hidden"></td>
                </tr>
                <tr>
                  <td class="brd11" id="rasi_box21" style="background:#fff url(../images/numbers/9.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_9">Man </span><input name="amsam_9" id="amsam_txt_9" value="Man " type="hidden"></td>
                  <td class="brd11" id="rasi_box22" style="background:#fff url(../images/numbers/8.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_8">Lak </span><input name="amsam_8" id="amsam_txt_8" value="Lak " type="hidden"></td>
                  <td class="brd11" id="rasi_box23" style="background:#fff url(../images/numbers/7.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_7"></span><input name="amsam_7" id="amsam_txt_7" value="" type="hidden"></td>
                  <td class="brd11" id="rasi_box24" style="background:#fff url(../images/numbers/6.jpg) no-repeat center;" width="70" height="70"><span class="rasi" id="amsam_6">Ket Sew </span><input name="amsam_6" id="amsam_txt_6" value="Ket Sew " type="hidden"></td>
                </tr>
              </tbody>
            </table>           </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><!--<form id="form4" name="form4" method="post" action="">-->
              <label></label>
              <table width="233" border="0">
                <tbody><tr>
                  <td width="45"><input name="horo" class="input3" id="horo" value="Clean Horoscope" type="radio"></td>
                  <td width="178">சுத்தமான ஜாதகம்</td>
                </tr>
                <tr>
                  <td><input name="horo" class="input3" id="horo" value="Sewwai" type="radio"></td>
                  <td>Sewwai</td>
                </tr>
                <tr>
                  <td><input name="horo" class="input3" id="horo" value="Ragu Kethusewwai" type="radio"></td>
                  <td>ரகு Kethusewwai</td>
                </tr>
				<tr>
                  <td><input name="horo" class="input3" id="horo" value="Ragu Kethu" checked="" type="radio"></td>
                  <td>ரகு கேது</td>
                </tr>
              </tbody></table>
            <!--</form>--></td>
          </tr>



    <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;இருப்பு</td>

      <td class="txtNormal" align="left" height="31"><input name="balance" class="fieldss" id="balance" size="60" value="கேது" type="text"></td>
    </tr>
    <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;ஆண்டு</td>

      <td class="txtNormal" align="left" height="31"><input name="year1" class="fieldss" id="year1" size="60" value="4" type="text"></td>
    </tr>
    <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;மாதம்</td>

      <td class="txtNormal" align="left" height="31"><input name="month1" class="fieldss" id="month1" size="60" value="7-14" type="text"></td>
    </tr>


                 <tr>
              <td>&nbsp;&nbsp;தகவல்கள்</td>
              <td><textarea name="information" class="fieldss" id="asset" rows="10" cols="45"></textarea></td>
            </tr>
            <tr>
              <td>&nbsp;&nbsp;எதிர்பார்ப்புகள்</td>
              <td><textarea name="expectations" class="fieldss" id="asset" rows="10" cols="45">நல்ல குடும்பம் வேலையில் உள்ள பெண்</textarea></td>
            </tr>

	 <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;மாவட்ட<span class="txtRedBold">*</span></td>

      <td class="txtNormal" align="left" height="31"><input name="dist" class="fieldss" id="dist" value="Coimbatore" size="55" type="text"></td>
    </tr>

	 <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;பிறந்த நாள் <span class="txtRedBold">*</span></td>

      <td class="txtNormal" align="left" height="31"><input name="dob" class="fieldss" id="dob" value="12/11/1988" size="55" type="text"></td>
    </tr>


     <tr>

       <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;பாலினம்<span class="txtRedBold">*</span></td>

       <td class="txtNormal" align="left" height="31"><input name="gender" value="Male" checked="" type="radio">

         ஆண்

           <input name="gender" value="Female" type="radio">

       பெண்</td>
     </tr>
          <tr>
         <td>
     <p><strong>குடும்ப விவரங்கள்</strong></p>
         </td>
  </tr>
          <tr>
              <td>&nbsp;&nbsp;பிரதர்ஸ்</td>
              <td valign="middle"><!--<form id="form2" name="form2" method="post" action="">-->
                  <table width="400" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                      <td>திருமணம்</td>
                      <td width="50"><input name="bromarried" id="bromarried" style="width:30px;" value="" type="text"></td>
                      <td>திருமணமாகாதவர்</td>
                      <td><input name="brounmarried" id="brounmarried" style="width:30px;" value="" type="text"></td>
                    </tr>
                </tbody></table></td>
            </tr>

             <tr>
              <td>&nbsp;&nbsp;சகோதரிகள்</td>
              <td><table width="400" cellspacing="0" cellpadding="0" border="0">
                  <tbody><tr>
                    <td>திருமணம்</td>
                    <td width="50"><input name="sismarried" id="sismarried" style="width:30px;" value="" type="text"></td>
                    <td>திருமணமாகாதவர்</td>
                    <td><input name="sisunmarried" id="sisunmarried" style="width:30px;" value="" type="text"></td>
                  </tr>
              </tbody></table></td>
            </tr>

     <tr>
         <td colspan="2">
     <p><strong>குடும்ப சொத்து விபரங்கள்</strong></p>
         </td>
  </tr>
      <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;ஹவுஸ் (கான்கிரீட் ) / (அடுக்கு )</td>

      <td class="txtNormal" align="left" height="31"><input name="house" class="fieldss" id="house" size="60" value="yes" type="text"></td>
    </tr>

    <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;நில</td>

      <td class="txtNormal" align="left" height="31"><input name="land" class="fieldss" id="land" size="60" value="5 acre தென்னந்தோப்பு" type="text"></td>
    </tr>

    <tr>
      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;வாகன</td>

      <td class="txtNormal" align="left" height="31"><input name="vehicle" class="fieldss" id="vehicle" size="60" value="yes" type="text"></td>
    </tr>
 <tr>
         <td>
     <p><strong>மதிப்பு</strong></p>
         </td>
  </tr>
  <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;Value1</td>

      <td class="txtNormal" align="left" height="31"><input name="value1" class="fieldss" id="value1" value="3 rented houses" size="55" type="text"></td>
    </tr>
    <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;Value2</td>

      <td class="txtNormal" align="left" height="31"><input name="value2" class="fieldss" id="value2" value="" size="55" type="text"></td>
    </tr>
    <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;Value3</td>

      <td class="txtNormal" align="left" height="31"><input name="value3" class="fieldss" id="value3" value="" size="55" type="text"></td>
    </tr>

    <tr>

      <td class="txtNormal" align="left" height="31">&nbsp;&nbsp;மொத்த சொத்து மதிப்பு</td>

	   <td class="txtNormal" align="left" height="31"><textarea name="asset" class="fieldss" id="asset" rows="10" cols="45">2 Cr</textarea></td>
    </tr>


    <tr>

      <td colspan="" align="" height="31"> &nbsp;&nbsp;பதிவேற்றுக  </td>

	   <td colspan="" align="" height="31"><input name="userfile1" type="file">

	   <img src="../uploads/ta_groom/photo/6330001.jpg" alt="" width="50" height="50">
	  </td>
    </tr>
	 	<tr><td colspan="2">
		</td>
	</tr>


        <tr>
          <td colspan="2" align="center" height="5">&nbsp;</td>
        </tr>
    <tr>

      <td align="center" height="31">                  </td>
      <td align="left" height="31"><input name="insert_btn" class="button" id="insert_btn" value="சேமிக்க" onclick="return validation();" type="submit">

		  <input name="edit_id" value="3530" type="hidden"></td>
    </tr>
  </tbody></table>

</form>

<script language="javascript">

function validation()

{




	var pgName1=document.getElementById("name").value;

var name1=stripBlanks(pgName1);

if(name1=="")

	{

	 window.alert("Please Enter the Name");

	 document.getElementById("name").focus();

	 return false;

	}

//Check Page name...

var pgName=document.getElementById("edu").value;

var name=stripBlanks(pgName);

if(name=="")

	{

	 window.alert("Please Enter the Education");

	 document.getElementById("edu").focus();

	 return false;

	}



	//Check Page name...

var pgName=document.getElementById("dist").value;

var name=stripBlanks(pgName);

if(name=="")

	{

	 window.alert("Please Enter the District");

	 document.getElementById("dist").focus();

	 return false;

	}





	//Check Page name...

var pgName=document.getElementById("dob").value;

var name=stripBlanks(pgName);

if(name=="")

	{

	 window.alert("Please Enter the Date of Birth");

	 document.getElementById("dob").focus();

	 return false;

	}







return true;













}//End of validation()



function stripBlanks(fld) {

var result = "";

var c = 0;

for (i=0; i<fld.length; i++) {

if (fld.charAt(i) != " " || c > 0) {

result += fld.charAt(i);

if (fld.charAt(i) != " ") c = result.length;

}

}

return result.substr(0,c);

}


</script>

</td>

        </tr>



      </tbody></table></td>

    </tr>

  </tbody></table>

</div>

<div id="footer">

All rights are reserved. Powered by <a href="http://www.meridian.net.in">Meridian Solutions Inc</a>.</div>



</div>







<script language="javascript">

function validation()

{



//Check Page name...

var pgName=document.getElementById("txt_pgname").value;

var name=stripBlanks(pgName);

if(name=="")

	{

	 window.alert("Please Enter Page Name");

	 document.getElementById("txt_pgname").focus();

	 return false;

	}

		//Check Page slug...

var pgSlug=document.getElementById("txt_pgslug").value;

var slug=stripBlanks(pgSlug);



if(slug=="")

	{

	 window.alert("Please Enter Page Slug");

	 document.getElementById("txt_pgslug").focus();

	 return false;

	}

	var iChars = "*|,\":<>[]{}`\';()@&$#%";

for (var i = 0; i < pgSlug.length; i++)

{

if (iChars.indexOf(pgSlug.charAt(i)) != -1)

{

alert ("Please remove the illegal characters from Page Slug!");

document.getElementById("txt_pgslug").value;

return false;

}



}

//Check Page title...

var pgTitle=document.getElementById("txt_pgtitle").value;

var title=stripBlanks(pgTitle);

if(title=="")

	{

	 window.alert("Please Enter Page Title");

	 document.getElementById("txt_pgtitle").focus();

	 return false;

	}

//Check Page heading...

var pgHeading=document.getElementById("txt_pgheading").value;

var pgheading=stripBlanks(pgHeading);

if(pgheading=="")

	{

	 window.alert("Please Enter Page Heading");

	 document.getElementById("txt_pgheading").focus();

	 return false;

	}

	return true;



}//End of validation()



function stripBlanks(fld) {

var result = "";

var c = 0;

for (i=0; i<fld.length; i++) {

if (fld.charAt(i) != " " || c > 0) {

result += fld.charAt(i);

if (fld.charAt(i) != " ") c = result.length;

}

}

return result.substr(0,c);

}





</script>

<script language="javascript">

function validation()

{



//Check Page name...

var pgName=document.getElementById("txt_pgname").value;

var name=stripBlanks(pgName);

if(name=="")

	{

	 window.alert("Please Enter Page Name");

	 document.getElementById("txt_pgname").focus();

	 return false;

	}

	var pgSlug=document.getElementById("txt_pgslug").value;

var slug=stripBlanks(pgSlug);



if(slug=="")

	{

	 window.alert("Please Enter Page Slug");

	 document.getElementById("txt_pgslug").focus();

	 return false;

	}

	var iChars = "*|,\":<>[]{}`\';()@&$#%";

for (var i = 0; i < pgSlug.length; i++)

{

if (iChars.indexOf(pgSlug.charAt(i)) != -1)

{

alert ("Please remove the illegal characters from Page Slug!");

document.getElementById("txt_pgslug").value;

return false;

}



}

//Check Page title...

var pgTitle=document.getElementById("txt_pgtitle").value;

var title=stripBlanks(pgTitle);

if(title=="")

	{

	 window.alert("Please Enter Page Title");

	 document.getElementById("txt_pgtitle").focus();

	 return false;

	}

//Check Page heading...

var pgHeading=document.getElementById("txt_pgheading").value;

var pgheading=stripBlanks(pgHeading);

if(pgheading=="")

	{

	 window.alert("Please Enter Page Heading");

	 document.getElementById("txt_pgheading").focus();

	 return false;

	}

	return true;



}//End of validation()



function stripBlanks(fld) {

var result = "";

var c = 0;

for (i=0; i<fld.length; i++) {

if (fld.charAt(i) != " " || c > 0) {

result += fld.charAt(i);

if (fld.charAt(i) != " ") c = result.length;

}

}

return result.substr(0,c);

}





</script>

</body></html>

END;

function gen_one_to_three($no = 15) {
    for ($i = 1; $i < $no; $i++) {
        // Note that $i is preserved between yields.
        yield $i;
    }
}


include 'simple_html_dom.php';
$html = new simple_html_dom();
$html->load($var);


echo $html->find('input[name="regno"]',0)->value;
echo $html->find('input[name="name"]',0)->value;
echo $html->find('input[name="sew"]',0)->value;
echo $html->find('input[name="raagu"]',0)->value;
echo $html->find('input[name="edu"]',0)->value;
echo $html->find('input[name="job"]',0)->value;
$income = $html->find('#income',0)->find('option[selected]',0);//income
if(!empty($income)) {
  echo $income->value;
}
echo $html->find('input[name="fname"]',0)->value;
echo $html->find('input[name="mname"]',0)->value;
echo $html->find('input[name="city"]',0)->value;

echo $html->find('input[name="height"]',0)->value;
echo $html->find('input[name="color"]',0)->value;


echo $html->find('input[name="kulam"]',0)->value;
echo $html->find('input[name="kula_theivam"]',0)->value;
echo $html->find('input[name="tamil"]',0)->value;
echo $html->find('input[name="year"]',0)->value;
echo $html->find('input[name="month"]',0)->value;
echo $html->find('input[name="date"]',0)->value;
echo $html->find('input[name="nazhiga"]',0)->value;
echo $html->find('input[name="time"]',0)->value;

$nakshatram = array(1=>"Aswini","Bharani","Karthikai",
"Rohini","Mirugasiridam","Thiruvathirai","Punarpusam","Pusam","Ayilyam","Maham","Puram","Uthiram","Atham",
"Chithirai","Swathiv","Visakam","Anusam","Ketai","Mulam", "Puradam","Uthiradam","Thiruvonam","Avitam",
"Sathayam","Puratathi","Uthiratathi","Revathi");
$rasilagnam = array(1=>"Mesham","Rishabam","Mithunam","Kadagam","Simmam","Kanni","Thulam","Virushigam","Thanusu","Magaram","Kumbam","Meenam");
$thisaiiruppuo = array(1=>"Guru thisai","Sani thisai","kethu thisai","Sukura thisai","Suriya thisai","Chandra thisai","Sewwai thisai","Ragu thisai","Budhan thisai");

$naks = $html->find('#profile_star',0)->find('option[selected]',0);//nakshthram
$naks = !empty($naks) ? $naks->value : '';
if(!empty($naks)) {
  $naks_int = array_search($naks, $nakshatram);
  if(empty($naks_int)) {
    echo "Failed -- ".$naks;exit;
  }
}

$rasi = $html->find('#profile_zodiac1',0)->find('option[selected]',0);//rasi
$rasi = !empty($rasi) ? $rasi->value : '';
if(!empty($rasi)) {
  $ras_int = array_search($rasi, $rasilagnam);
  if(empty($ras_int)) {
    echo "Failed -- ".$rasi;exit;
  }
}


$lagname = $html->find('#profile_zodiac2',0)->find('option[selected]',0);//lagnam
$lagname = !empty($lagname) ? $lagname->value : '';
if(!empty($lagname)) {
  $lag_int = array_search($lagname, $rasilagnam);
  if(empty($lag_int)) {
    echo "Failed -- ".$lagname;exit;
  }
}

$thisaiiruppu = $html->find('#profile_zodiac3',0)->find('option[selected]',0);//nakshthram
$thisaiiruppu = !empty($thisaiiruppu) ? $thisaiiruppu->value : '';
if(!empty($thisaiiruppu)) {
  $thi_int = array_search($thisaiiruppu, $thisaiiruppuo);
  if(empty($thi_int)) {
    echo "Failed -- ".$thisaiiruppu;exit;
  }
}
function getra($value, $index) {

  $array = array(
  'Lak',
  'Sur',
  'Bud',
  'Suk',
  'Sew',
  'Rag',
  'Ket',
  'Gur',
  'cha',
  'San',
  'Man'
  );

  $return = array();
  $value = trim($value);
  if(!empty($value)) {
    $rasi_explode = explode(" ",$value);
    foreach($rasi_explode as $v) {
      $cur_index = array_search($v,$array);
      $return[$cur_index] = $index;
    }
  }
  return $return;
}

$rasi_j = array();
$amsam_j = array();
foreach(gen_one_to_three(13) as $value) {

  $rasi = $html->find('#rasi_'.$value,0)->plaintext;
  if(!empty($rasi))
  $rasi_j = $rasi_j + getra($rasi, $value);

  $amsam = $html->find('#amsam_'.$value,0)->plaintext;
  if(!empty($amsam))
  $amsam_j = $amsam_j  + getra($amsam, $value);
}
//{"rasi":["0","7","7","6","12","11","5","2","9","9","4"],"amsam":["8","3","11","4","6","12","6","12","2","2","9"]}
//{"rasi":[4,7,7,6,12,11,5,2,9,9,4],"amsam":[8,3,11,4,6,12,6,12,2,2,9]}

$firstIndexToEnsure = 1;
$lastIndexToEnsure = 10;
$defaults = array_fill($firstIndexToEnsure,
                       $lastIndexToEnsure - $firstIndexToEnsure + 1,
                       0);
$rasi_j = $rasi_j + $defaults;
ksort($rasi_j);

$amsam_j = $amsam_j + $defaults;
ksort($amsam_j);
echo json_encode(['rasi'=>$rasi_j,'amsam'=>$amsam_j]);


echo "<br>";
$horo = 0;
foreach($html->find('input[name="horo"]') as $key=>$checkbox) {
    if (isset($checkbox->checked))
        $horo = $key+1;
}
echo $horo;


echo $html->find('input[name="balance"]',0)->value;
echo $html->find('input[name="year1"]',0)->value;
echo $html->find('input[name="month1"]',0)->value;

echo $html->find('textarea[name="information"]',0)->innertext;
echo $html->find('textarea[name="expectations"]',0)->innertext;

echo $html->find('input[name="dist"]',0)->value;
echo $html->find('input[name="dob"]',0)->value;


echo "<br>";
$sex = 0;
foreach($html->find('input[name="gender"]') as $key=>$checkbox) {
    if (isset($checkbox->checked))
        $sex = $key;
}
echo $sex;


echo $html->find('input[name="bromarried"]',0)->value;
echo $html->find('input[name="brounmarried"]',0)->value;
echo $html->find('input[name="sismarried"]',0)->value;
echo $html->find('input[name="sisunmarried"]',0)->value;
echo $html->find('input[name="house"]',0)->value;
echo $html->find('input[name="land"]',0)->value;
echo $html->find('input[name="vehicle"]',0)->value;

echo $html->find('input[name="value1"]',0)->value;
echo $html->find('input[name="value2"]',0)->value;
echo $html->find('input[name="value3"]',0)->value;


echo $html->find('textarea[name="asset"]',0)->innertext;


$url = $html->find('input[name="userfile1"]',0)->next_sibling()->src;
if(!empty($url)) {
  trim($url);
  ltrim($url,'.');
  echo 'http://gavaramahajanamatrimony.com/'.$url;
}










exit;

exit;


exit;



print_r($result1);
exit;
